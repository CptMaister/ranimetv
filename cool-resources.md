# Cool resources
For getting a lot of Japanese background immersion.


# Condensed audio
As inspired from [Optimizing Passive Immersion: Condensed Audio](https://www.youtube.com/watch?v=QOLTeO-uCYU), many already extracted audios in Anki Decks at https://www.mediafire.com/folder/p17g5uk4phb41/User_Uploaded_Anki_Decks, just import a deck and copy all mp3 files of the deck from the Anki collection.media folder to somewhere else and use a program to merge the many audios together to single episode files.

Not yet finished, but a simple python script to merge mp3 file for now (only needs ffmpeg and python 3.6): [mp3merge](https://gitlab.com/CptMaister/mp3merge) (made by me). Run the script like this to see instructions in your terminal: `python3 mp3merge.py --help`


# YouTube
## Download playlists using youtube-dl (for Ubuntu/Linux)
- don't stop downloading when errors occur with `--ignore-errors`
- have more control over what videos to download by using the `--playlist-start` and `--playlist-end` options, specifying the first and last video to download
- avoid videos of extreme length by setting a filesize limit, like 800m for 800MB (actually caps at something just above 830MB) with `--max-filesize`
- use `--yes-playlist` if you paste a link of a video that is played in a playlist, not needed if it is the link of a whole playlist
- save storage space using `--format worst`
- get audios using `--extract-audio`
- set audio format to mp3 with `--audio-format mp3`

Here is an example with the playlist of all of Nintendo公式チャネル's videos:   
`youtube-dl --ignore-errors --playlist-start 1001 --playlist-end 1800 --max-filesize 800m --yes-playlist --format worst https://www.youtube.com/playlist?list=UUkH3CcMfqww9RsZvPRPkAJA`

## Recommendations
+ [Nintendo公式チャネル](https://www.youtube.com/user/NintendoJPofficial) - many small commercials (10-50 seconds), game trailers (30sec - 5min), extended game presentations and nintendo directs (15-30min), shows like Game Center, よゐこ and Neko Neko Mario (15-45min) and special game tournaments (1-6 hours)
+ [フェルミ研究所 FermiLab](https://www.youtube.com/channel/UC3-1iYGHfR43q_b974vUNYg) - original short stories (3-6min) visualized in manga panels that are read out at decent speed, each episode contains nice unusual words as well
+ [大愚和尚の一問一答/Osho Taigu’s Heart of Buddha](https://www.youtube.com/channel/UC4arQnli3ffEuCSrSgAD_Ug) - a monk talking about one of his viewer's sent-in problems per video (10-50min), including all sorts of personal problems like love, work, family
+ [Asian Boss](https://www.youtube.com/user/askasianboss) - videos of interviews with asian people on the street about cultural or current topics (~5-10min) and solo interviews with special people (~15-20min), [here is a playlist with all mostly Japanese videos](https://www.youtube.com/playlist?list=PLXCqeywEdx3afHYSYtpwBVD8lqy2iV1Np)
+ [iwatismforever](https://www.youtube.com/user/iwatismforever) - calmly commented playthroughs of a few classic JRPGs (~30min per video)
+ [電撃ランキング](https://www.youtube.com/channel/UCqltwdVS2hLvSjC9eFmfrWg) - trashy rankings of clickbaity useless facts, which has an extreme amount of exotic vocabulary (~5min)
+ [The BEST Japanese Video Game Commercials](https://www.youtube.com/playlist?list=PLATUCKmXKADJKmLarE8hngOSM3yPicOv3) - a playlist with 450+ short commercials (~15-30sec) by 83Chrisaaron 
+ [日本語の森](https://www.youtube.com/channel/UCVx6RFaEAg46xfAsD2zz16w) - Japanese lessons of all levels, go for a playlist that includes many example sentences or lots of talking

### Podcasts
+ [American Life (YouTube)](https://www.youtube.com/channel/UCtEzeI6wZA-v-L-9ff0WZNA)
+ [Nihongo con Teppei (YouTube)](https://www.youtube.com/channel/UCH88l3_ltyJm67gAFzDFNRw)
+ [Slow and Polite Japanese](https://taisetsu-japanese.seesaa.net/) (日本語を大切にしたい) - easier to download [HERE](https://www.listennotes.com/podcasts/%E6%97%A5%E6%9C%AC%E8%AA%9E%E3%82%92%E5%A4%A7%E5%88%87%E3%81%AB%E3%81%97%E3%81%9F%E3%81%84-slow-and-polite-japanese-okNT9WFMK3Z/)
+ [Let's learn Japanese from small talk! (Spotify)](https://open.spotify.com/show/1tHH40EmOge2Ale4nlYWTq) - the [Website](https://smalltalkinjapanese.hatenablog.com/) has words on all episodes. Please let me know if you find this Podcast downloadable anywhere