#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-

import copy
import math
import threading
import time
from math import floor
import sys
import os.path
import random
from enum import Enum, unique, auto as auto_enum

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import hashlib, colorsys

import vlc


from equalizer_bar import EqualizerBar
from ranimetv_gui_components import *
from ranimetv_script_json import *


if __name__ == "__main__":
    # app = QApplication(sys.argv)
    app = QApplication([])
    # app.setStyle(QStyleFactory.create("Fusion"))
    # print(QStyleFactory.keys())
    player = UiMainWindow()
    player.show()
    player.resize(720, 540) # TODO: better sizing
    # if sys.argv[1:]:
    #     player.open_file_dialog(sys.argv[1])
    sys.exit(app.exec_())
