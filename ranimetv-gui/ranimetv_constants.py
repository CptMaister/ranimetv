#! /usr/bin/python
import copy
import math
import threading
import time
from math import floor
import sys
import os.path
import random
from enum import Enum, unique, auto as auto_enum
from typing import List

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from equalizer_bar import EqualizerBar
import hashlib, colorsys

import vlc


TIMER_PLAYBACK_UPDATE_MS = 250
TIMER_EQUALIZER_UPDATE_MS = 100
BUTTON_PLAYBACK_SQUARE_SIZE = 35
BUTTON_PLAYBACK_ICON_SIZE = 25
BUTTON_PLAYBACK_NARROW_WIDTH = 20
BUTTON_PLAYBACK_SQUARE_FONT_SIZE = 20
BUTTON_QUEUE_SHOWS_WIDTH = 23
BUTTON_QUEUE_SHOWS_HEIGHT = 23
BUTTON_QUEUE_SHOWS_ICON_SIZE = 20
BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM = 16

ICON_LOGO = "icons/logo_1.svg"
ICON_PLAY = "icons/play.svg"
ICON_PLAY_START = "icons/play_start.svg"
ICON_PLAY_BLACK = "icons/play_black.svg"
ICON_PAUSE = "icons/pause.svg"
ICON_STOP = "icons/stop.svg"
ICON_SKIP = "icons/skip.svg"
ICON_BACKWARD = "icons/backward.svg"
ICON_FORWARD = "icons/forward.svg"
ICON_LOOP = "icons/loop.svg"
ICON_LOOP = "icons/loop.svg"
ICON_LOOPING = "icons/looping.svg"
ICON_TIMER = "icons/timer.svg"
ICON_TIMED = "icons/timed.svg"
ICON_FASTER = "icons/faster.svg"
ICON_SLOWER = "icons/slower.svg"
ICON_SPEAKER_UNMUTED = "icons/speaker_unmuted.svg"
ICON_SPEAKER_MUTED = "icons/speaker_muted.svg"
ICON_FOLDER = "icons/folder.svg"
ICON_FOLDER_BLACK = "icons/folder_black.svg"
ICON_FULLSCREEN = "icons/fullscreen.svg"
ICON_EDIT = "icons/edit.svg"
ICON_EDIT_THIN = "icons/edit_thin.svg"
ICON_SAVE = "icons/save.svg"
ICON_ADD = "icons/add_plus.svg"
ICON_ADD_THIN = "icons/add_plus_thin.svg"
ICON_REMOVE = "icons/remove_x.svg"
ICON_REMOVE_THIN = "icons/remove_x_thin.svg"
ICON_RELOAD = "icons/reload.svg"
ICON_SORT = "icons/sort.svg"
ICON_SORT_THIN = "icons/sort_thin.svg"
ICON_SORT_CHANGE = "icons/sort_change.svg"
ICON_SORT_CHANGE_THIN = "icons/sort_change_thin.svg"
ICON_MOVE_UP = "icons/move_up.svg"
ICON_MOVE_DOWN = "icons/move_down.svg"
ICON_MOVE_LEFT = "icons/move_left.svg"
ICON_DUPLICATE = "icons/duplicate.svg"

TEXT_TRUE_MARK = "✔"
TEXT_FALSE_MARK = "✘"

TEXT_RADIO_BINARY_TRUE = TEXT_TRUE_MARK
TEXT_RADIO_BINARY_FALSE = TEXT_FALSE_MARK

TEXT_RADIO_TERNARY_HARD = "✔✔"
TEXT_RADIO_TERNARY_TRUE = TEXT_TRUE_MARK
TEXT_RADIO_TERNARY_FALSE = TEXT_FALSE_MARK
TEXT_RADIO_TERNARY_NONE = "~"


PROGRAM_NAME = "RAnimeTV GUI (working title)"

### dummy lists, to be replaced by configs later ###
DUMMY_USER_LIST = ["Alex", "Extra", "User3"]
DUMMY_PROFILE_LIST = ["Normal", "Fokus", "Chill"]
DUMMY_BASEPATH_LIST = ["/media/alex/678001755456034B/Sprachen", "D:\\Sprachen"]

DUMMY_SORT_LIST = ["Creation Date", "Name", "Tags", "Current Episode"]

DUMMY_SHOW_LIST = ["Death Note", "Samurai Champloo", "Dr. Stone", "Psycho Pass S2", "Mob Psycho 100"]
DUMMY_SHOW_LIST_T = [
    ("Death Note", 2, ["tag1", "tag2"]),
    ("Samurai Champloo", 3, ["tag1", "tag2"]),
    ("Dr. Stone", 4, ["tag1", "tag2"]),
    ("Psycho Pass S2", 6, ["tag1", "tag2"]),
    ("Mob Psycho 100", 5, ["tag1", "tag2"])
]

DUMMY_SUSPENDED_QUEUE_LIST = ["Samurai Champloo", "Dr. Stone", "Mob Psycho 100", "Samurai Champloo", "Psycho Pass S2"]
DUMMY_STARTWITHSHOWS_LIST = ["Ajinomoto", "Dr. Stone"]

DUMMY_CMS_LIST = ["Ajinomoto", "Nintendo cms 1min", "Sony"]
DUMMY_CMS_LIST_T = [
    ("Ajinomoto", 7, None, 1),
    ("Nintendo cms 1min", 2, 1, None),
    ("Sony", 3, None, None),
    ("Test langer Name Werbung ho cms 15-30sec", 2, 1, None)
]

DUMMY_QUEUE_LIST = ["Mob Psycho 100", "Death Note", "Psycho Pass S2"]
DUMMY_QUEUE_LIST_L = ["Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2"]
DUMMY_QUEUE_LIST_T = [
    ("Mob Psycho 100", 3, TEXT_TRUE_MARK, TEXT_TRUE_MARK),
    ("Death Note", 17, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Psycho Pass S2", 5, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Death Note", 18, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
]
DUMMY_QUEUE_LIST_T_L = [
    ("Mob Psycho 100", 3, TEXT_TRUE_MARK, TEXT_TRUE_MARK),
    ("Death Note", 17, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Psycho Pass S2", 5, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Death Note", 18, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Mob Psycho 100", 3, TEXT_TRUE_MARK, TEXT_TRUE_MARK),
    ("Death Note", 17, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Psycho Pass S2", 5, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Death Note", 18, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Mob Psycho 100", 3, TEXT_TRUE_MARK, TEXT_TRUE_MARK),
    ("Death Note", 17, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Psycho Pass S2", 5, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Death Note", 18, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Mob Psycho 100", 3, TEXT_TRUE_MARK, TEXT_TRUE_MARK),
    ("Death Note", 17, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Psycho Pass S2", 5, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
    ("Death Note", 18, TEXT_TRUE_MARK, TEXT_FALSE_MARK),
]
DUMMY_HISTORY_LIST = ["path/to/Death Note 2", "long/path/very/long/to/Samurai Champloo 1", "home/Dr. Stone", "Psycho Pass S2 13", "Mob Psycho 100"]
DUMMY_HISTORY_LIST_T = [
    ("path/to/Death Note 2", "0:23"),
    ("long/path/very/long/to/Samurai Champloo 1", "7:23"),
    ("home/Dr. Stone", "7:56"),
    ("Psycho Pass S2 13", "9:23"),
    ("Mob Psycho 100", "2:43:01")
]

DUMMY_LIST_PROFILES = ["Profile1", "Fokus", "Late Night Funkin", "Background", "News stuff", "Commercials Only wtf", "P7", "P8", "P9"]

DUMMY_LIST_EXCLUDE_PROFILES = ["Profile1", "P9"]
DUMMY_LIST_INCLUDE_PROFILES = ["Fokus", "Late Night Funkin", "Background", "News stuff", "P8"]

DUMMY_LIST_TAGS = ["youtube", "anime", "series", "teach", "commercial", "let's play"]


DUMMY_VERSION_TEXT = "v0.0.0"




##### Set Limits, Standard values etc #####

VOLUME_LIMIT_LOWER = 0
VOLUME_LIMIT_UPPER = 200
SPEED_LIMIT_LOWER = 40
SPEED_LIMIT_UPPER = 300
MONITOR_LIMIT_LOWER = 1
PROBABILITY_LIMIT_LOWER = 5
PROBABILITY_LIMIT_UPPER = 100
QUEUE_LOAD_LIMIT_UPPER = 25


QUEUE_SIZE_STANDARD = 3
BACK_PLAYBACK_SIZE_STANDARD = 5
FORWARD_PLAYBACK_SIZE_STANDARD = 5
JUMP_PLAYBACK_BUMPER_SIZE_STANDARD = 20
SUSPEND_BACK_SIZE_STANDARD = 10
SUSPENDED_OPENING_TIME = 60
END_PLAYBACK_SIZE_STANDARD = 5
DEBUG_PLAYER_JUMPS_ANOMALY_SENSITIVITY_SIZE_STANDARD = 2
DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD = 1
MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD = 3

GUI_END_BLOCKED_TIME_ZONE_SEC = 3


@unique
class UiDockedState(Enum):
    PLAYER_WINDOW = auto_enum()
    EXTRA_WINDOW = auto_enum()
    NOWHERE = auto_enum()


@unique
class UiMediaAreaState(Enum):
    VIDEO_EQUALIZER = auto_enum()
    VIDEO_NOEQUALIZER = auto_enum()
    EQUALIZERONLY = auto_enum()
    MANAGEMENT_ONLY = auto_enum()
    NOTHING = auto_enum()

    @staticmethod
    def next_value(state):
        if state == UiMediaAreaState.VIDEO_EQUALIZER:
            return UiMediaAreaState.VIDEO_NOEQUALIZER
        elif state == UiMediaAreaState.VIDEO_NOEQUALIZER:
            return UiMediaAreaState.EQUALIZERONLY
        elif state == UiMediaAreaState.EQUALIZERONLY:
            return UiMediaAreaState.MANAGEMENT_ONLY
        elif state == UiMediaAreaState.MANAGEMENT_ONLY:
            return UiMediaAreaState.NOTHING
        elif state == UiMediaAreaState.NOTHING:
            return UiMediaAreaState.VIDEO_EQUALIZER


@unique
class SortingState(str, Enum):
    CREATION_DATE = auto_enum()
    NAME = auto_enum()
    TAGS_FREQUENCY_HIGHEST = auto_enum()
    TAGS_FREQUENCY_SUM = auto_enum()
    TAGS_AMOUNT = auto_enum()
    CURRENT_EPISODE = auto_enum()
    EPISODES_AMOUNT = auto_enum()
    COMMERCIAL_VALUE = auto_enum()

    @staticmethod
    def getValues(shows_only: bool = False) -> List:
        returnList = [
            SortingState.CREATION_DATE,
            SortingState.NAME,
            SortingState.TAGS_FREQUENCY_HIGHEST,
            SortingState.TAGS_FREQUENCY_SUM,
            SortingState.TAGS_AMOUNT,
            SortingState.CURRENT_EPISODE,
            SortingState.EPISODES_AMOUNT
        ]
        if not shows_only:
            returnList.append(SortingState.COMMERCIAL_VALUE)

        return returnList

    @staticmethod
    def toString(state):
        if state == SortingState.CREATION_DATE:
            return "Creation Date"
        elif state == SortingState.NAME:
            return "Name"
        elif state == SortingState.TAGS_FREQUENCY_HIGHEST:
            return "Tags Frequency Max"
        elif state == SortingState.TAGS_FREQUENCY_SUM:
            return "Tags Frequency Sum"
        elif state == SortingState.TAGS_AMOUNT:
            return "Tags Amount"
        elif state == SortingState.CURRENT_EPISODE:
            return "Current Episode"
        elif state == SortingState.EPISODES_AMOUNT:
            return "Episodes Amount"
        elif state == SortingState.COMMERCIAL_VALUE:
            return "Commercial Value"

    @staticmethod
    def fromString(string):
        if string == "Creation Date":
            return SortingState.CREATION_DATE
        elif string == "Name":
            return SortingState.NAME
        elif string == "Tags Amount":
            return SortingState.TAGS_AMOUNT
        elif string == "Tags Frequency Max":
            return SortingState.TAGS_FREQUENCY_HIGHEST
        elif string == "Tags Frequency Sum":
            return SortingState.TAGS_FREQUENCY_SUM
        elif string == "Current Episode":
            return SortingState.CURRENT_EPISODE
        elif string == "Episodes Amount":
            return SortingState.EPISODES_AMOUNT
        elif string == "Commercial Value":
            return SortingState.COMMERCIAL_VALUE

    @staticmethod
    def fromValue(value):
        if value == SortingState.CREATION_DATE:
            return SortingState.CREATION_DATE
        elif value == SortingState.NAME:
            return SortingState.NAME
        elif value == SortingState.TAGS_AMOUNT:
            return SortingState.TAGS_AMOUNT
        elif value == SortingState.TAGS_FREQUENCY_HIGHEST:
            return SortingState.TAGS_FREQUENCY_HIGHEST
        elif value == SortingState.TAGS_FREQUENCY_SUM:
            return SortingState.TAGS_FREQUENCY_SUM
        elif value == SortingState.CURRENT_EPISODE:
            return SortingState.CURRENT_EPISODE
        elif value == SortingState.EPISODES_AMOUNT:
            return SortingState.EPISODES_AMOUNT
        elif value == SortingState.COMMERCIAL_VALUE:
            return SortingState.COMMERCIAL_VALUE


class VlcPlayerTextPosition(Enum):
    # 0 center, 1 left center, 2 right center, 3 left center, 4 top center, 5 top left, 6 top right, 7 top left, 8 bottom center, 9 bottom left, 10 bottom right, 11 bottom left, 12 top center, 13 top left, ....
    CENTER = 0
    LEFT_CENTER = 1
    RIGHT_CENTER = 2
    TOP_CENTER = 4
    TOP_LEFT = 5
    TOP_RIGHT = 6
    BOTTOM_CENTER = 8
    BOTTOM_LEFT = 9
    BOTTOM_RIGHT = 10