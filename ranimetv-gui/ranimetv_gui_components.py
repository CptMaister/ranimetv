#! /usr/bin/python
import copy
import math
import threading
import time
from math import floor
import sys
import os.path
import random
from enum import Enum, unique, auto as auto_enum
from typing import Union, List

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from equalizer_bar import EqualizerBar
import hashlib, colorsys

from ranimetv_constants import *
from ranimetv_script_json import *

import vlc

# import gettext
# _ = gettext.gettext



# noinspection PyUnresolvedReferences
class UiMainWindow(QMainWindow):
    """A simple Media Player using VLC and Qt
    """
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.setWindowTitle(PROGRAM_NAME)

        # creating a basic vlc instance
        self.instance = vlc.Instance()
        # creating an empty vlc media player
        self.mediaplayer = self.instance.media_player_new()

        player_attach_events(self.mediaplayer)

        self.main_setup = None
        create_config_if_none_existing() # TODO CONTINUE: check without config and add welcome windows
        self.loadMainSetupUserCurrent()

        self.createUI()

    
    def loadMainSetupUserCurrent(self):
        self.main_setup = load_user_current()
        print("henlo new main_setup current user")
    

    # def loadMainSetupUserById(self, userUuid: str):
    #     self.main_setup.load_user(userUuid)
    #     print("henlo new main_setup user with id " + userUuid)
    

    def loadProfileById(self, profileId: int):
        self.main_setup.load_profile(profileId)


    def createUI(self):
        """Set up the user interface, signals & slots
        """
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)

        self.ui_media_area = UiMediaArea(
            self.mediaplayer,
            self.style(),
            self.main_setup,
            self.loadProfileById
        )

        # playback control stuff (time slider, volume, all playback controlling buttons)
        self.ui_playback_control = UiPlaybackControlVBox(
            self.style(),
            self.mediaplayer,
            self.main_setup,
            self.ui_media_area.equalizer,
            # self.open_file_dialog,
            self.play_start_gui,
            self.ui_media_area.timer_equalizer.start,
            self.ui_media_area.timer_equalizer.stop,
            self.ui_media_area.toggle_media_area_state,
            self.ui_media_area.set_media_area_state,
            self.ui_media_area.get_media_area_state,
            self.ui_media_area.handle_media_area_state,
            self.ui_media_area.showTextOnMedia,
            self.ui_media_area.management_ui.toggleManagementTopRowEnabled,
            self.loadMainSetupUserCurrent,
            self.check_upcoming_show_playable
        )

        # combine all layouts
        self.vboxlayout = QVBoxLayout()
        self.vboxlayout.addLayout(self.ui_media_area)
        self.vboxlayout.addLayout(self.ui_playback_control)
        self.vboxlayout.setContentsMargins(0, 0, 0, 0)

        self.vboxlayout.setAlignment(self.ui_media_area, Qt.AlignCenter)
        self.vboxlayout.setAlignment(self.ui_playback_control, Qt.AlignBottom)

        self.widget.setLayout(self.vboxlayout)


        # create menu bar
        self.ui_menubar_player = UiMenubarPlayer(
            self.main_setup,
            self.mediaplayer,
            self.ui_playback_control,
            self.ui_media_area.management_ui.control_top_row
        )

        self.setMenuBar(self.ui_menubar_player)
        # self.statusBar() # activate status tip bar
        self.status_bar = self.statusBar()
        self.status_bar.addPermanentWidget(QLabel(DUMMY_VERSION_TEXT))
        self.setStatusBar(self.status_bar)

        self.setWindowIcon(QIcon(ICON_LOGO))



    # some methods

    # def open_file_dialog(self, filename=None):
    # def open_file_dialog(self): # TODO delete
    #     """Open a media file in a MediaPlayer
    #     """
    #     # if filename is None:
    #     #     filename = QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
    #     filename = QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
    #     if not filename:
    #         return
    #
    #     # create the media
    #     if sys.version < '3':
    #         filename = unicode(filename)
    #     self.media = self.instance.media_new(filename)
    #     # put the media in the media player
    #     self.mediaplayer.set_media(self.media)
    #
    #     # parse the metadata of the file
    #     self.media.parse()
    #     # set the title of the track as window title
    #     self.setWindowTitle(self.media.get_meta(0))
    #
    #     self.ui_playback_control.handlePlayPausePlayback()
    #     self.ui_media_area.timer_equalizer.start()


    def play_start_gui(self):

        print("play start gui go")
        playThread = Thread(target=play_start, args=(self.main_setup, self.mediaplayer), daemon=True)
        playThread.start()
        print("play start gui started")


    def check_upcoming_show_playable(self) -> bool:
        upcoming_show = get_next_show(self.main_setup)
        if upcoming_show is None:
            return False
        show_episodes_paths = show_generate_all_episodes_paths(upcoming_show, self.main_setup.global_config)

        title = ""
        text = ""
        informativeText = ""

        if show_episodes_paths is None:
            title = "Directory Not Found"
            text = "Upcoming show directory not found."
            informativeText = "Directory for the show queued up next cannot be found. Maybe the wrong base path was selected?"

        elif len(show_episodes_paths) == 0:
            title = "No Files Found"
            text = "No files found for upcoming show."
            informativeText = "Directory found for the upcoming show, but no files in there..."

        elif len(show_episodes_paths) < upcoming_show.currentEpisode:
            title = "'Current Episode' Too High"
            text = "Upcoming show has not enough files."
            informativeText = "The 'Current Episode' is more than the amount of episodes itself... Could be caused by a manually set 'Last Episode' that is more than the amount of files in the directory."

        elif not os.path.isfile(show_generate_full_episode_path(upcoming_show, self.main_setup.global_config)):
            title = "Upcoming Show Episode Not Found"
            text = "Upcoming show file not found."
            informativeText = "What the hell."

        # error handling
        if title != "":
            if session_running_event.is_set():
                self.ui_playback_control.handleStopPlayback()
            showWarningDialog(title, text, informativeText)
            return False
        else:
            return True





class UiMediaArea(QVBoxLayout):

    def __init__(
            self,
            mediaplayer: vlc.MediaPlayer,
            window_style: QStyle,
            main_setup: MainSetup,
            loadProfileById
    ):
        super().__init__()

        self.mediaplayer = mediaplayer
        self.main_setup = main_setup
        self.loadProfileById = loadProfileById

        # In this widget, the video will be drawn
        if sys.platform == "darwin":  # for MacOS
            from PyQt5.QtWidgets import QMacCocoaViewContainer
            self.mediaplayer_frame = QMacCocoaViewContainer(0)
        else:
            self.mediaplayer_frame = QFrame()
        self.palette = self.mediaplayer_frame.palette()
        self.palette.setColor(QPalette.Window,
                              QColor(0, 0, 0))
        self.mediaplayer_frame.setPalette(self.palette)
        self.mediaplayer_frame.setAutoFillBackground(True)
        self.mediaplayer_frame.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding) #FIXME: i have no idea why this works

        self.h_box_mediaplayer = QHBoxLayout()
        self.h_box_mediaplayer.addWidget(self.mediaplayer_frame)
        self.h_box_mediaplayer.setContentsMargins(0, 0, 0, 0)

        self.h_box_mediaplayer_frame = QFrame()
        self.h_box_mediaplayer_frame.setLayout(self.h_box_mediaplayer)
        # self.h_box_mediaplayer_frame.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.h_box_mediaplayer_frame.setHidden(True) # hide all stuff first, show desired ones in self.handle_media_area_state()


        # the media player has to be 'connected' to the QFrame
        # (otherwise a video would be displayed in it's own window)
        # this is platform specific!
        # you have to give the id of the QFrame (or similar object) to
        # vlc, different platforms have different functions for this
        if sys.platform.startswith('linux'): # for Linux using the X Server
            self.mediaplayer.set_xwindow(self.mediaplayer_frame.winId())
        elif sys.platform == "win32": # for Windows
            self.mediaplayer.set_hwnd(self.mediaplayer_frame.winId())
        elif sys.platform == "darwin": # for MacOS
            self.mediaplayer.set_nsobject(int(self.mediaplayer_frame.winId()))


        # Equalizer that visualizes media without a video track
        self.equalizer = EqualizerBar(8, 3)  # number of columns, height (defined by colors amount, else red)
        self.equalizer.setColors(['#0C0786', '#40039C', '#6A00A7', '#8F0DA3', '#B02A8F', '#CA4678', '#E06461','#F1824C', '#FCA635', '#FCCC25', '#EFF821'])
        # self.equalizer.setVisible(True)  # make visible when no video track available
        self.equalizer.setDecayFrequencyMs(TIMER_EQUALIZER_UPDATE_MS)
        # self.equalizer.setColors(["#810f7c", "#8856a7", "#8c96c6", "#b3cde3", "#edf8fb"]) # adjust color of bars (and height automatically)
        # self.equalizer.setBarPadding(90) # set padding outside of equalizer, in pixel
        self.equalizer.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding) #FIXME: i have no idea why this works

        self.equalizer_layout = QHBoxLayout()
        self.equalizer_layout.addWidget(self.equalizer)
        self.equalizer_layout.setContentsMargins(0, 0, 0, 0)
        self.setupEqualizerText()

        self.equalizer_frame = QFrame()
        self.equalizer_frame.setLayout(self.equalizer_layout)
        # self.equalizer_frame.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.equalizer_frame.setHidden(True) # hide all stuff first, show desired ones in self.handle_media_area_state()


        # create timer updating the equalizer
        self.timer_equalizer = QTimer()
        self.timer_equalizer.setInterval(TIMER_EQUALIZER_UPDATE_MS)
        self.timer_equalizer.timeout.connect(self.update_equalizer)

        # put equalizer in a frame that can be easily hidden
        # self.equalizer_frame = QFrame()
        # self.equalizer_frame.setLayout(self.equalizer_layout)
        # self.equalizer_frame.setHidden(True)


        self.management_ui = UiManagementMain(
            window_style,
            self.main_setup,
            self.mediaplayer,
            self.loadProfileById
        )

        self.management_ui_frame = QFrame()
        self.management_ui_frame.setLayout(self.management_ui)
        self.management_ui_frame.setContentsMargins(0, 0, 0, 0)
        # this is some hard cheese to get this thing to work (so that .setHidden(False) won't pop it out into a new window) FIXME
        self.management_ui_h_box_tmp = QHBoxLayout()
        self.management_ui_h_box_tmp.addWidget(self.management_ui_frame)
        # self.management_ui_h_box_tmp.setContentsMargins(5, 5, 5, 0)

        self.management_ui_frame_tmp = QFrame()
        self.management_ui_frame_tmp.setLayout(self.management_ui_h_box_tmp)

        # self.management_ui_frame_tmp.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding) #FIXME: i have no idea why this works
        # self.management_ui_frame.setHidden(True) # hide all stuff first, show desired ones in self.handle_media_area_state()



        # combine all media area things (video player, equalizer, show management)
        self.addWidget(self.h_box_mediaplayer_frame)
        self.addWidget(self.equalizer_frame)
        self.addWidget(self.management_ui_frame_tmp)
        # self.addWidget(self.management_frame)


        self.visibility_state: UiMediaAreaState = UiMediaAreaState.VIDEO_EQUALIZER
        self.handle_media_area_state()


        updateEqualizerColorsLoopThread = Thread(target=self.updateEqualizerColorsLoop, daemon=True)
        updateEqualizerColorsLoopThread.start()

    def updateEqualizerColorsLoop(self):
        while (True):
            gui_event_media_changed_update_equalizer_visuals.wait()
            gui_event_media_changed_update_equalizer_visuals.clear()
            self.updateEqualizerColors()

    def updateEqualizerColors(self, initial_update: bool = False):
        show = deepcopy(self.main_setup.currently_running_show)
        if show is None:
            return

        title = show.showName

        show.generate_first_and_last_episode_tmp_ints(self.main_setup.global_config, False)
        episodes = max(1, show.lastEpisode_tmp - show.firstEpisode_tmp)

        equalizerAdjustByMedia(self.equalizer, title, episodes)



    def toggle_media_area_state(self):
        # rotate through the enum
        self.visibility_state = UiMediaAreaState.next_value(self.visibility_state)

        print(self.visibility_state)

        self.handle_media_area_state()


    def set_media_area_state(self, state: UiMediaAreaState):
        self.visibility_state = state
        self.handle_media_area_state()


    def get_media_area_state(self):
        return self.visibility_state


    def handle_media_area_state(self):
        self.handle_show_hide_equalizer()

        if (not self.mediaplayer.get_media() is None) and (
                self.visibility_state == UiMediaAreaState.VIDEO_EQUALIZER
                or self.visibility_state == UiMediaAreaState.VIDEO_NOEQUALIZER
        ) and self.mediaplayer.has_vout() == 1 and session_running_event.is_set():
            self.h_box_mediaplayer_frame.setHidden(False)
        else:
            self.h_box_mediaplayer_frame.setHidden(True)

        if (
            self.visibility_state == UiMediaAreaState.MANAGEMENT_ONLY
            or (
                (
                    not self.visibility_state == UiMediaAreaState.NOTHING
                )
                and not session_running_event.is_set()
            )
            or (
                self.visibility_state == UiMediaAreaState.VIDEO_NOEQUALIZER
                and self.mediaplayer.has_vout() == 0
            )
        ):
            self.management_ui_frame.setHidden(False)
            pass
        else:
            self.management_ui_frame.setHidden(True)
            pass


    def update_equalizer(self):
        self.equalizer.setValues([
            min(100, v + random.randint(0, 50) if random.randint(0, 5) > 2 else v)
            for v in self.equalizer.values()
        ])


    def handle_show_hide_equalizer(self):
        # handle if visualizer should be shown instead of media if no video track present # TODO: move into player handle options post play

        # print ("self.mediaplayer.has_vout(): " + str(self.mediaplayer.has_vout()))

        if not session_running_event.is_set() or (
                self.visibility_state is UiMediaAreaState.VIDEO_NOEQUALIZER
                or self.visibility_state is UiMediaAreaState.MANAGEMENT_ONLY
                or self.visibility_state is UiMediaAreaState.NOTHING
                or (
                    self.visibility_state is UiMediaAreaState.VIDEO_EQUALIZER
                    and self.mediaplayer.has_vout() == 1  # 1 if has video track, 0 if none
                )
        ):
            self.hide_equalizer()
        else:
            self.show_equalizer()


    def show_equalizer(self):
        if self.mediaplayer.is_playing():
            self.timer_equalizer.start()
        self.equalizer_frame.setHidden(False)


    def hide_equalizer(self):
        self.timer_equalizer.stop()
        self.equalizer_frame.setHidden(True)


    def showTextOnMedia(self, text: str, duration: int = 2000):
        vlcPlayerTextActivate(self.mediaplayer, text, VlcPlayerTextPosition.TOP_LEFT, duration)
        self.eqTextActivate(text, duration)


    def setupEqualizerText(self):
        self.eq_texts = []

        self.eq_text_shade1 = QLabel(self.equalizer)
        # self.eq_text_shade1.setText("Henlo")
        self.tmp_font = self.eq_text_shade1.font()
        self.tmp_font.setPointSize(int(self.equalizer.height() / 22))
        self.eq_text_shade1.setFont(self.tmp_font)
        self.eq_text_shade1.move(22, 22)
        self.eq_text_shade1.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade1)

        self.eq_text_shade2 = QLabel(self.equalizer)
        # self.eq_text_shade2.setText("Henlo")
        self.eq_text_shade2.setFont(self.tmp_font)
        self.eq_text_shade2.move(18, 18)
        self.eq_text_shade2.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade2)

        self.eq_text_shade3 = QLabel(self.equalizer)
        # self.eq_text_shade3.setText("Henlo")
        self.eq_text_shade3.setFont(self.tmp_font)
        self.eq_text_shade3.move(22, 18)
        self.eq_text_shade3.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade3)

        self.eq_text_shade4 = QLabel(self.equalizer)
        # self.eq_text_shade4.setText("Henlo")
        self.eq_text_shade4.setFont(self.tmp_font)
        self.eq_text_shade4.move(18, 22)
        self.eq_text_shade4.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade4)

        self.eq_text = QLabel(self.equalizer)
        # self.eq_text.setText("Henlo")
        self.eq_text.setFont(self.tmp_font)
        self.eq_text.move(20, 20)
        self.eq_text.setStyleSheet("color: white")
        self.eq_texts.append(self.eq_text)


    def eqTextActivate(self, text: str, duration: int = 2000):
        self.eq_text_thread = threading.Thread(target=self.eqTextActivateThread, args=(text, duration), daemon=True)
        self.eq_text_thread.start()


    def eqTextActivateThread(self, text: str, duration: int = 2000):
        self.tmp_font.setPointSize(int(self.equalizer.height() / 22))
        for t in self.eq_texts:
            t.setText(text)
            t.setFont(self.tmp_font)
            t.adjustSize()

        time.sleep(duration / 1000)

        if text != self.eq_text.text():
            return

        for t in self.eq_texts:
            t.setText("")





class UiManagementMain(QVBoxLayout):

    def __init__(
            self,
            window_style: QStyle,
            main_setup: MainSetup,
            mediaplayer: vlc.MediaPlayer,
            loadProfileById
    ):
        super().__init__()

        self.main_setup = main_setup
        self.mediaplayer = mediaplayer
        self.loadProfileById = loadProfileById

        # some important selection stuff
        self.control_top_row = UiManagementTopRow(self.main_setup)

        self.control_top_row.user_select_combo.currentIndexChanged.connect(self.handleUserChanged)
        self.control_top_row.profile_select_combo.currentIndexChanged.connect(self.handleProfileChanged)
        self.control_top_row.basepath_checkbox.clicked.connect(self.handleBasepathCheckChanged)
        self.control_top_row.basepath_select_combo.currentIndexChanged.connect(self.handleBasepathChanged)


        # queue and show management
        self.qh_tabs = UiManagementQueueHistoryTabs(
            self.main_setup,
            self.handle_queue_buttons_state
        )

        self.qh_tabs.queue_table.doubleClicked.connect(self.handleQueueShowEdit)

        self.qh_tabs.queue_buttons.add_one.clicked.connect(self.handleQueueAdd)
        self.qh_tabs.queue_buttons.remove.clicked.connect(self.handleQueueRemoveSelected)
        self.qh_tabs.queue_buttons.reload.clicked.connect(self.handleQueueReload)
        self.qh_tabs.queue_buttons.move_up.clicked.connect(self.handleQueueMoveUpSelected)
        self.qh_tabs.queue_buttons.move_down.clicked.connect(self.handleQueueMoveDownSelected)

        self.qh_tabs.history_table.doubleClicked.connect(self.handleHistoryOpenFolder)

        self.qh_tabs.history_buttons.open_folder.clicked.connect(self.handleHistoryOpenFolder)


        self.sc_tabs = UiManagementShowsCmsTabs(
            self.main_setup,
            self.handle_queue_buttons_state,
            self.handle_shows_cms_buttons_state
        )

        self.sc_tabs.shows_table.doubleClicked.connect(self.handleShowEdit)

        self.sc_tabs.shows_buttons.add_to_queue.clicked.connect(self.handleShowsAddToQueue)
        self.sc_tabs.shows_buttons.sort.clicked.connect(self.handleShowSort)
        self.sc_tabs.shows_buttons.sort_change.clicked.connect(self.handleShowSortChange)
        self.sc_tabs.shows_buttons.play_now.clicked.connect(self.handleShowPlayNow)

        self.sc_tabs.shows_buttons.add_new.clicked.connect(self.handleShowAddNew)
        self.sc_tabs.shows_buttons.edit.clicked.connect(self.handleShowEdit)

        self.sc_tabs.commercials_table.doubleClicked.connect(self.handleCmsEdit)

        self.sc_tabs.commercials_buttons.sort.clicked.connect(self.handleCommercialSort)
        self.sc_tabs.commercials_buttons.sort_change.clicked.connect(self.handleCommercialSortChange)
        self.sc_tabs.commercials_buttons.add_new.clicked.connect(self.handleShowAddNew)
        self.sc_tabs.commercials_buttons.edit.clicked.connect(self.handleCmsEdit)


        self.handle_queue_buttons_state()
        self.handle_shows_cms_buttons_state()


        # put into splitter, makes the width of the queues and shows tables adjustable
        self.splitter_queue_shows = QSplitter(Qt.Horizontal)
        self.splitter_queue_shows.addWidget(self.qh_tabs)
        self.splitter_queue_shows.addWidget(self.sc_tabs)
        self.splitter_queue_shows.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)


        # combine all
        self.addLayout(self.control_top_row)
        self.addWidget(self.splitter_queue_shows)
        self.setContentsMargins(0, 0, 0, 0)



    def handleUserChanged(self):
        """Changes the current user.
        """
        if session_running_event.is_set():
            return
        
        selected_index = self.control_top_row.user_select_combo.currentIndex()
        selected_user = self.control_top_row.user_objects[selected_index]

        if selected_user.userUuid != self.main_setup.global_config.currentUserUuid:
            print("User changed: " + selected_user.get_name())
            self.main_setup.global_config.currentUserUuid = selected_user.userUuid
            self.main_setup.load_current_user()
            updateAllGui()
            save_global_config_complete(self.main_setup.global_config)


    def handleProfileChanged(self):
        """Changes the current profile.
        """
        if session_running_event.is_set():
            return
        
        selected_index = self.control_top_row.profile_select_combo.currentIndex()
        selected_profile = self.control_top_row.profile_objects[selected_index]

        if selected_profile.profileId != self.main_setup.user_config.currentProfileId:
            print("Profile changed: " + selected_profile.profileName)
            self.loadProfileById(selected_profile.profileId)


    def handleBasepathCheckChanged(self):
        """Handle Basepath Check changed.
        """
        if session_running_event.is_set():
            return

        self.main_setup.global_config.useBasePaths = self.control_top_row.basepath_checkbox.isChecked()


    def handleBasepathChanged(self):
        """Changes the current basepath.
        """
        if session_running_event.is_set():
            return

        selected_index = self.control_top_row.basepath_select_combo.currentIndex()
        print("Basepath changed: " + self.control_top_row.basepath_objects[selected_index])
        print("Basepath checked: " + str(self.control_top_row.basepath_checkbox.isChecked()))

        self.main_setup.global_config.currentBasePathIndex = selected_index

        if not os.path.isdir(convert_path_slashes(self.main_setup.global_config.basePaths[selected_index])):
            showWarningDialog("Base Path Warning", "Directory of given base path not found.", "The currently selected base path points to a directory that currently is not found. Consider switching your base path before starting a session.")


    def toggleManagementTopRowEnabled(self, enabled: bool):
        self.control_top_row.user_profiles_paths_save_button.setEnabled(enabled)
        self.control_top_row.user_profiles_paths_edit_button.setEnabled(enabled)
        self.control_top_row.user_select_combo.setEnabled(enabled)
        self.control_top_row.profile_select_combo.setEnabled(enabled)
        self.control_top_row.basepath_select_combo.setEnabled(enabled)
        self.control_top_row.basepath_checkbox.setEnabled(enabled)


    def handle_queue_buttons_state(self):
        if not hasattr(self, "sc_tabs") or not hasattr(self, "qh_tabs"):
            return
        queue_empty = len(self.qh_tabs.queue_table.queued_shows_objects) == 0
        shows_empty = len(self.sc_tabs.shows_table.shows_objects) == 0
        self.qh_tabs.queue_buttons.add_one.setDisabled(shows_empty)
        self.qh_tabs.queue_buttons.remove.setDisabled(queue_empty)
        self.qh_tabs.queue_buttons.reload.setDisabled(shows_empty)
        self.qh_tabs.queue_buttons.move_up.setDisabled(queue_empty)
        self.qh_tabs.queue_buttons.move_down.setDisabled(queue_empty)

    def handleQueueAdd(self):
        self.qh_tabs.queue_table.selectionModel().clear()
        self.main_setup.populate_automatic(1, update_gui_event_set=False)
        self.qh_tabs.queue_table.updateTableContents()


    def handleQueueRemoveSelected(self):
        selected_rows = sorted(set(index.row() for index in self.qh_tabs.queue_table.selectedIndexes()))
        self.main_setup.pop_queue(selected_rows, False)
        sleep(0.07) # TODO remove all sleep()s
        self.qh_tabs.queue_table.updateTableContents()


    def handleQueueReload(self):
        self.main_setup.clear_queue()
        if (
            len(self.qh_tabs.queue_table.queued_shows_objects) > 0
            and len(self.main_setup.queue_shows.queue) > 0
            and (not session_running_event.is_set())
            and (
                len(self.main_setup.get_current_profile().suspendedQueue) == 0
                or (
                    self.main_setup.get_current_profile().suspendedQueue[0] !=
                    self.qh_tabs.queue_table.queued_shows_objects[0].showId
                ) or self.qh_tabs.queue_table.queued_shows_objects[0].suspendedTimeSeconds is None
                or self.qh_tabs.queue_table.queued_shows_objects[0].suspendedTimeSeconds <= 10
            )
        ):
            self.main_setup.queue_shows.queue.pop(0)
        self.main_setup.populate_automatic()


    def handleQueueMoveUpSelected(self):
        self.handleQueueMoveSelected(self.main_setup.queue_move_up)


    def handleQueueMoveDownSelected(self):
        self.handleQueueMoveSelected(self.main_setup.queue_move_down)

    def handleQueueMoveSelected(self, move_direction):
        selected_rows = sorted(set(index.row() for index in self.qh_tabs.queue_table.selectedIndexes()))
        if selected_rows is not None and len(selected_rows) > 0:
            new_indexes = move_direction(selected_rows, False)
            sleep(0.07)
            # gui_event_queue_update_table.set()
            self.qh_tabs.queue_table.updateTableContents()
            selectRowsInTableView(self.qh_tabs.queue_table, new_indexes)


    def handleHistoryOpenFolder(self):
        selected_rows = sorted(set(index.row() for index in self.qh_tabs.history_table.selectedIndexes()))
        selected_row = selected_rows[0] if len(selected_rows) > 0 else 0
        open_file(os.path.dirname(self.qh_tabs.history_table.history_objects[selected_row][0]))


    def handle_shows_cms_buttons_state(self):
        if not hasattr(self, "sc_tabs") or not hasattr(self, "qh_tabs"):
            return
        shows_empty = len(self.sc_tabs.shows_table.shows_objects) == 0
        cms_empty = len(self.sc_tabs.commercials_table.cms_objects) == 0
        no_shows = len(self.main_setup.user_config.shows) == 0
        self.sc_tabs.shows_buttons.add_to_queue.setDisabled(shows_empty)
        self.sc_tabs.shows_buttons.sort.setDisabled(shows_empty)
        self.sc_tabs.shows_buttons.sort_change.setDisabled(shows_empty)
        self.sc_tabs.shows_buttons.play_now.setDisabled(shows_empty)
        self.sc_tabs.shows_buttons.edit.setDisabled(no_shows)

        self.sc_tabs.commercials_buttons.sort.setDisabled(cms_empty)
        self.sc_tabs.commercials_buttons.edit.setDisabled(no_shows)

    def handleShowsAddToQueue(self):
        if len(self.sc_tabs.shows_table.shows_objects) == 0:
            showWarningDialog("There Are No Shows", "There don't seem to be any shows", "You don't have any shows saved. Add a new show before trying to add one to the queue.")
            return

        selected_rows = sorted(set(index.row() for index in self.sc_tabs.shows_table.selectedIndexes()))
        for index in selected_rows:
            input_handle_queue_show_x(self.main_setup, self.sc_tabs.shows_table.shows_objects[index].showId, False)
        self.qh_tabs.queue_table.updateTableContents()

    def handleShowSort(self):
        if len(self.sc_tabs.shows_table.shows_objects) == 0:
            showWarningDialog("There Are No Shows", "There don't seem to be any shows", "You don't have any shows saved. Add a new show before trying to sort existing shows.")
            return

        item, okPressed = QInputDialog.getItem(
            self.sc_tabs.shows_buttons.sort,
            "Sort Shows",
            "Sort by:",
            list(map(SortingState.toString, SortingState.getValues(True))),
            (0 if self.main_setup.global_config.windowSettings.sortShows is None else SortingState.getValues().index(self.main_setup.global_config.windowSettings.sortShows)),
            False
        )
        if okPressed and item:
            self.main_setup.global_config.windowSettings.sortShows = SortingState.fromString(item)
            self.sc_tabs.shows_table.updateTableContents()

    def handleShowSortChange(self):
        current_ascending = self.main_setup.global_config.windowSettings.sortShowsAscending
        current_ascending = True if current_ascending is None else current_ascending
        self.main_setup.global_config.windowSettings.sortShowsAscending = not current_ascending
        self.sc_tabs.shows_table.updateTableContents()

    def handleCommercialSort(self):
        if len(self.sc_tabs.commercials_table.cms_objects) == 0:
            showWarningDialog("There Are No Commercials", "There don't seem to be any commercials", "You don't have any commercials saved. Add a new show and set its commercial value before trying to sort existing commercials.")
            return

        item, okPressed = QInputDialog.getItem(
            self.sc_tabs.commercials_buttons.sort,
            "Sort Commercials",
            "Sort by:",
            list(map(SortingState.toString, SortingState.getValues())),
            (0 if self.main_setup.global_config.windowSettings.sortCommercials is None else SortingState.getValues().index(self.main_setup.global_config.windowSettings.sortCommercials)),
            False
        )
        if okPressed and item:
            self.main_setup.global_config.windowSettings.sortCommercials = SortingState.fromString(item)
            self.sc_tabs.commercials_table.updateTableContents()

    def handleCommercialSortChange(self):
        current_ascending = self.main_setup.global_config.windowSettings.sortCommercialsAscending
        current_ascending = True if current_ascending is None else current_ascending
        self.main_setup.global_config.windowSettings.sortCommercialsAscending = not current_ascending
        self.sc_tabs.commercials_table.updateTableContents()


    def handleShowPlayNow(self):
        if len(self.sc_tabs.shows_table.shows_objects) == 0:
            showWarningDialog("There Are No Shows", "There don't seem to be any shows", "You don't have any shows saved. Add a new show before trying to add one to the queue.")
            return

        selected_rows = sorted(set(index.row() for index in self.sc_tabs.shows_table.selectedIndexes()))
        for index in range(len(selected_rows)):
            if session_running_event.is_set() and index == 0:
                input_handle_play_show_x(self.mediaplayer, self.main_setup.get_show_by_id(self.main_setup.queue_shows.queue[0]), self.main_setup, self.sc_tabs.shows_table.shows_objects[selected_rows[index]].showId, False) # TODO? (<- what?)
            else:
                self.main_setup.queue_shows.queue.insert(index, self.sc_tabs.shows_table.shows_objects[selected_rows[index]].showId)

        self.qh_tabs.queue_table.updateTableContents()


    def handleShowAddNew(self):
        """Open widget in new window to create new show
        """
        self.dialog_new_show = UiShowsNewDialog(self.main_setup)
        self.dialog_new_show.show()


    def handleShowEdit(self):
        """Open widget in new window to edit selected show
        """
        if len(self.main_setup.user_config.shows) == 0:
            showWarningDialog("There Are No Shows", "There don't seem to be any shows", "You don't have any shows saved. Add a new show before trying to edit existing shows.")
            return

        selected_rows = sorted(set(index.row() for index in self.sc_tabs.shows_table.selectedIndexes()))
        selected_row = selected_rows[0] if len(selected_rows) > 0 else 0
        print(selected_row)
        if len(self.sc_tabs.shows_table.shows_objects) == 0:
            show_id_selected = -1
        else:
            show_id_selected = self.sc_tabs.shows_table.shows_objects[selected_row].showId

        self.dialog_edit_shows = UiShowsEditDialog(
            self.main_setup,
            show_id_selected
        )
        self.dialog_edit_shows.show()


    def handleCmsEdit(self):
        """Open widget in new window to edit selected commercial
        """
        if len(self.main_setup.user_config.shows) == 0:
            showWarningDialog("There Are No Shows", "There don't seem to be any shows", "You don't have any shows saved. Add a new show before trying to edit existing shows.")
            return

        selected_rows = sorted(set(index.row() for index in self.sc_tabs.commercials_table.selectedIndexes()))
        selected_row = selected_rows[0] if len(selected_rows) > 0 else 0
        print(selected_row)
        if len(self.sc_tabs.commercials_table.cms_objects) == 0:
            show_id_selected = -1
        else:
            show_id_selected = self.sc_tabs.commercials_table.cms_objects[selected_row].showId

        self.dialog_edit_shows = UiShowsEditDialog(
            self.main_setup,
            show_id_selected
        )
        self.dialog_edit_shows.show()


    def handleQueueShowEdit(self):
        """Open widget in new window to edit selected commercial
        """
        if len(self.main_setup.user_config.shows) == 0:
            showWarningDialog("There Are No Shows", "There don't seem to be any shows", "You don't have any shows saved. Add a new show before trying to edit existing shows.")
            return

        selected_rows = sorted(set(index.row() for index in self.qh_tabs.queue_table.selectedIndexes()))
        selected_row = selected_rows[0] if len(selected_rows) > 0 else 0
        print(selected_row)
        if len(self.qh_tabs.queue_table.queued_shows_objects) == 0:
            show_id_selected = -1
        else:
            show_id_selected = self.qh_tabs.queue_table.queued_shows_objects[selected_row].showId

        self.dialog_edit_shows = UiShowsEditDialog(
            self.main_setup,
            show_id_selected
        )
        self.dialog_edit_shows.show()






class UiManagementTopRow(QHBoxLayout):

    def __init__(
            self,
            main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.user_profiles_paths_save_button = UiManagementToolButton(ICON_SAVE, "Save the current state.", tooltip="Auto save is triggered only on some occasions to avoid saving too often:\nwhen a session starts, ends, changes playback media or when the window for editing users, profiles and base paths is closed.") # TODO CONTINUE: save on closing edit window??
        self.user_profiles_paths_save_button.clicked.connect(self.saveManualOutOfSession)

        self.user_profiles_paths_edit_button = UiManagementToolButton(ICON_EDIT, "Edit Users, Profiles and Base Paths.")
        self.user_profiles_paths_edit_button.clicked.connect(self.editUserProfilePath)

        self.user_select_label = QLabel("User:")
        self.user_select_label.setStatusTip("Current User.")

        self.user_select_combo = QComboBox()
        self.user_select_combo.setMaximumWidth(110)
        self.updateUsersCombo()
        self.user_select_combo.setStatusTip("Select the current User. Only if no running session.")

        self.sep_1 = QLabel("||")

        self.profile_select_label = QLabel("Profile:")
        self.profile_select_label.setStatusTip("Current Profile.")

        self.profile_select_combo = QComboBox()
        self.profile_select_combo.setMaximumWidth(110)
        self.updateProfilesCombo()
        self.profile_select_combo.setStatusTip("Select the current Profile. Only if no running session.")

        self.sep_2 = QLabel("||")

        self.basepath_checkbox = QCheckBox("Base Path:")
        self.basepath_checkbox.setStatusTip("Toggle if base path should be used. Only if no running session.")

        self.basepath_select_combo = QComboBox()
        self.basepath_select_combo.setMaximumWidth(110)
        self.updateBasepathCombo()
        self.basepath_select_combo.setStatusTip("Select the current base path. Only if no running session.")

        if len(self.main_setup.global_config.basePaths) == 0:
            self.basepath_checkbox.setDisabled(True)
            self.basepath_select_combo.setDisabled(True)


        self.addWidget(self.user_profiles_paths_save_button)
        self.addStretch()
        # self.addLayout(self.user_select_box)
        # self.addLayout(self.profile_select_box)
        # self.addLayout(self.basepath_select_box)
        self.addWidget(self.user_profiles_paths_edit_button)
        self.addWidget(self.user_select_label)
        self.addWidget(self.user_select_combo)
        self.addWidget(self.sep_1)
        self.addWidget(self.profile_select_label)
        self.addWidget(self.profile_select_combo)
        self.addWidget(self.sep_2)
        # self.addWidget(self.basepath_select_label)
        self.addWidget(self.basepath_checkbox)
        self.addWidget(self.basepath_select_combo)

        self.setContentsMargins(0, 5, 0, 5)

        updateUsersComboThread = Thread(target=self.updateUsersLoop, daemon=True)
        updateUsersComboThread.start()

        updateProfilesComboThread = Thread(target=self.updateProfilesLoop, daemon=True)
        updateProfilesComboThread.start()

        updateBasepathComboThread = Thread(target=self.updateBasepathLoop, daemon=True)
        updateBasepathComboThread.start()

    def updateUsersLoop(self):
        while (True):
            gui_event_users_update_combo.wait()
            gui_event_users_update_combo.clear()
            self.updateUsersCombo()
            print("updated users combo")


    def updateUsersCombo(self):
        self.user_objects: List[ConfigGlobalUser] = self.main_setup.global_config.users
        self.user_select_combo.clear()
        self.user_select_combo.addItems(
            list(
                map(
                    ConfigGlobalUser.get_name,
                    self.user_objects
                )
            )
        )

        loaded_index = self.user_objects.index(
                next((x for x in self.user_objects if x.userUuid == self.main_setup.global_config.currentUserUuid), 0)
        )
        self.user_select_combo.setCurrentIndex(loaded_index)
        printdev("set current user index: " + str(loaded_index))


    def updateProfilesLoop(self):
        while (True):
            gui_event_profiles_update_combo.wait()
            gui_event_profiles_update_combo.clear()
            self.updateProfilesCombo()
            print("updated profiles combo")


    def updateProfilesCombo(self):
        self.profile_objects: List[ConfigUserProfile] = self.main_setup.user_config.profiles
        self.profile_select_combo.clear()
        self.profile_select_combo.addItems(
            list(
                map(
                    lambda p: p.profileName,
                    self.main_setup.user_config.profiles
                )
            )
        )

        loaded_index = self.profile_objects.index(
                next((x for x in self.profile_objects if x.profileId == self.main_setup.user_config.currentProfileId), 0)
        )
        self.profile_select_combo.setCurrentIndex(loaded_index)
        printdev("set current profile index: " + str(loaded_index))


    def updateBasepathLoop(self):
        while (True):
            gui_event_basepaths_update_combo.wait()
            gui_event_basepaths_update_combo.clear()
            self.updateBasepathCombo()
            print("updated base path combo")


    def updateBasepathCombo(self):
        self.basepath_objects = self.main_setup.global_config.basePaths
        self.basepath_select_combo.clear()
        self.basepath_select_combo.addItems(self.basepath_objects)

        self.basepath_checkbox.setChecked(self.main_setup.global_config.useBasePaths)

        has_basepaths = len(self.main_setup.global_config.basePaths) > 0
        self.basepath_checkbox.setEnabled(has_basepaths)
        self.basepath_select_combo.setEnabled(has_basepaths)

        if self.main_setup.global_config.currentBasePathIndex is not None:
            self.basepath_select_combo.setCurrentIndex(self.main_setup.global_config.currentBasePathIndex)
        printdev("set current basepath index: " + str(self.main_setup.global_config.currentBasePathIndex))


    def editUserProfilePath(self):
        """Open widget in new window to edit users, profiles, base paths
        """
        if session_running_event.is_set():
            return

        self.dialog_edit = UiEditUserProfilePath(
            self.main_setup,
            self.user_select_combo.currentIndex(),
            self.profile_select_combo.currentIndex(),
            self.basepath_select_combo.currentIndex()
        )
        self.dialog_edit.show()

    def saveManualOutOfSession(self):
        if session_running_event.is_set():
            return

        current_profile = self.main_setup.get_current_profile()
        if current_profile.profileSettings.suspendQueue:
            profile_index = self.main_setup.user_config.index_of_profile_by_id(current_profile.profileId)
            self.main_setup.user_config.profiles[profile_index].suspendedQueue = self.main_setup.queue_shows.queue

        save_main_setup_complete(self.main_setup)




class UiEditUserProfilePath(QDialog):

    def __init__(
            self,
            main_setup: MainSetup,
            index_user: int = 0,
            index_profile: int = 0,
            index_path: int = 0
    ):
        super().__init__()

        self.main_setup = main_setup

        self.setWindowTitle("Edit users, profiles, base paths")
        self.setWindowModality(Qt.ApplicationModal)

        self.edit_generalSettings_widget = UiEditGeneralSettingsWidget(self.main_setup, index_path)
        self.edit_users_widget = UiEditUsersWidget(self.main_setup, index_user)
        self.edit_profiles_widget = UiEditProfilesWidget(self.main_setup, index_profile)

        # put into tabs
        self.tabs = QTabWidget()
        self.tabs.addTab(self.edit_generalSettings_widget, "General Settings")
        self.tabs.addTab(self.edit_users_widget, "Users")
        self.tabs.addTab(self.edit_profiles_widget, "Profiles")

        self.tabs.setCurrentIndex(2)

        self.layout_all = QVBoxLayout()
        self.layout_all.addWidget(self.tabs)

        self.setLayout(self.layout_all)



class UiEditUsersWidget(QFrame):

    def __init__(
            self,
            main_setup: MainSetup,
            index_current: int = 0
    ):
        super().__init__()

        self.main_setup = main_setup

        self.warned_duplicateName = False
        self.edited_unsaved = False

        self.currently_selected_show_id = None

        # form layouts for the settings
        self.main_settings = UiUserMainSettings(self.main_setup)

        # list with buttons
        self.users_list = QListWidget()
        self.updateListContents()
        self.users_list.setSelectionMode(QAbstractItemView.SingleSelection)
        if index_current == -1:
            self.selectRow(0)
        else:
            self.selectRow(index_current)

        self.users_list.setFixedWidth(150)
        self.users_list.setAlternatingRowColors(True)
        self.users_list.currentTextChanged.connect(self.handleUsersEditListSelected)

        self.users_buttons = UiEditUsersButtons()
        self.users_buttons.add_one.clicked.connect(self.addUserDialog)
        self.users_buttons.remove.clicked.connect(self.removeUserDialog)
        # self.users_buttons.duplicate.clicked.connect(self.duplicateUserDialog)

        self.v_box_users = QVBoxLayout()
        self.v_box_users.addWidget(self.users_list)
        self.v_box_users.addLayout(self.users_buttons)
        self.v_box_users.setContentsMargins(0, 0, 0, 0)

        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save,
            Qt.Horizontal
        )
        self.button_box.accepted.connect(self.handleSaveUser)

        # combine settings and buttons
        self.v_box_settings_all = QVBoxLayout()
        self.v_box_settings_all.addLayout(self.main_settings)
        self.v_box_settings_all.addWidget(self.button_box)

        # combine all in stack-like layout
        self.layout_all = QHBoxLayout()
        self.layout_all.addLayout(self.v_box_users)
        self.layout_all.addLayout(self.v_box_settings_all)
        self.layout_all.addStretch()

        self.setLayout(self.layout_all)

        # updateListLoopThread = Thread(target=self.updateListLoop, daemon=True)
        # updateListLoopThread.start()

        # make the cancel button appear
        self.main_settings.name_input.textChanged.connect(lambda: self.setUserEdited(True))



    def objectToListItemMapper(self, next_user: ConfigGlobalUser):
        return next_user.userName


    def updateListContents(self):
        self.all_users_objects: List[ConfigGlobalUser] = self.main_setup.global_config.users

        index_remembered = self.users_list.currentRow()
        self.users_list.clear()

        for i in range(len(self.all_users_objects)):
            next_show = self.all_users_objects[i]
            self.users_list.addItem(self.objectToListItemMapper(next_show))

        sleep(0.07)
        self.users_list.setCurrentRow(index_remembered)


    def handleReloadUser(self):
        self.main_settings.loadUserSettingsById(self.currently_selected_user_uuid)
        self.setUserEdited(False)
        # TODO warn if not saved


    def selectRow(self, index: int):
        self.users_list.setCurrentRow(index)
        self.currently_selected_user_uuid = self.all_users_objects[index].userUuid
        self.handleReloadUser()
        if hasattr(self, "button_box"):
            self.setUserEdited(False)
        print("selectRow " + str(index))
        # TODO warn if not saved


    def handleUsersEditListSelected(self):
        """Switch the settings to the selected user. Warn if not saved.
        """
        if len(self.users_list) == 0:
            return

        self.selectRow(self.users_list.currentIndex().row())


    def setUserEdited(self, edited: bool = True):
        if self.edited_unsaved == edited:
            return
        self.edited_unsaved = edited

        if self.edited_unsaved:
            self.button_box.hide()
            self.button_box = QDialogButtonBox(
                QDialogButtonBox.Save
                | QDialogButtonBox.Cancel,
                Qt.Horizontal
            )
            self.button_box.accepted.connect(self.handleSaveUser)
            self.button_box.rejected.connect(self.handleReloadUser)
            self.v_box_settings_all.addWidget(self.button_box)
        else:
            self.button_box.hide()
            self.button_box = QDialogButtonBox(
                QDialogButtonBox.Save,
                Qt.Horizontal
            )
            self.button_box.accepted.connect(self.handleSaveUser)
            self.v_box_settings_all.addWidget(self.button_box)
            self.warned_duplicateName = False


    def handleSaveUser(self):
        """Save user with the given attributes. Check validity before saving
        """
        name_state, name_text, name_pos = self.main_settings.name_validator.validate(self.main_settings.name_input.text(), 0)
        # print("Name input regex: " + str(name_state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable. name_text: " + name_text + ", name_pos: " + str(name_pos))
        if name_state != 2:
            showWarningDialog("Name Missing", "Enter a name for the user", "Every user needs a name. Enter something in the name input field and try again.")
            return

        newName = whitespace_reduce(name_text)
        all_but_current_user = list(filter(lambda u: u.userUuid != self.currently_selected_user_uuid, self.main_setup.global_config.users))

        if not self.warned_duplicateName and newName in list(map(lambda u: u.userName, all_but_current_user)):
            self.warned_duplicateName = True
            showWarningDialog("Duplicate User Name", "Name Already In Use", "The name you specified for this user is already in use.\nYou can still save this show, if you want. Just try again.")
            return

        save_user = self.main_setup.global_config.get_user_by_uuid(self.currently_selected_user_uuid)

        save_user.userName = newName

        save_global_config_complete(self.main_setup.global_config)
        print("saved user")

        self.setUserEdited(False)
        self.updateListContents()
        gui_event_users_update_combo.set()


    def addUserDialog(self):
        if self.main_setup.global_config.noAutosave:
            showWarningDialog("Can Not Create User", "Can't create a new user if 'No Autosave' setting is on", "Turn of the 'No Autosave' setting if you want to create a new user. Creating a new user creates a new file for the user.")
        else:
            userName, okPressed = QInputDialog.getText(self, "New User", "Name of new user:", QLineEdit.Normal, "")
            if okPressed:
                self.main_setup.global_config.create_new_user(userName) # TODO CONTINUE: add noAutosave compatibility, also for all other edit users / profiles / shows / basepath code (already done for user add and delete, but nowhere else)
                print("saved new user")

                self.updateListContents()
                gui_event_users_update_combo.set()


    def removeUserDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete user '" + self.users_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText("This action cannot be undone. You will lose all settings for all the profiles and shows of this user.")
        msg.setWindowTitle("Delete User")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeUserConfirmed)
        msg.exec_()

    def removeUserConfirmed(self):
        selected_index = self.users_list.currentIndex().row()
        user_to_delete = self.all_users_objects[selected_index]

        if len(self.main_setup.global_config.users) > 1:
            self.main_setup.delete_user_by_uuid(user_to_delete.userUuid)
            self.updateListContents()
            updateAllGui()
        else:
            showWarningDialog("User Deletion Failed", "Cannot Delete The Last User", "A user can only be deleted, if it is not the last remaining user. If you really want to delete this user, create a new one first.")

        print("Delete " + user_to_delete.userName)




class UiEditUsersButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new User.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected User.", True, True)
        # self.duplicate = UiManagementToolButton(ICON_DUPLICATE, "Duplicate the current User.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        # self.addWidget(self.duplicate)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)




class UiUserMainSettings(QFormLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.name_label = UiLabelTooltip("Name", "Give the user a name.")
        self.name_input = QLineEdit()
        self.name_input.setMinimumWidth(350)
        self.name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.name_input.setValidator(self.name_validator)
        self.name_input.setPlaceholderText("Enter user name")

        self.addRow(self.name_label, self.name_input)


    def loadUserSettingsById(self, user_uuid: str):
        user = self.main_setup.get_user_by_uuid(user_uuid)

        self.name_input.setText(user.userName)





class UiEditProfilesWidget(QFrame):

    def __init__(
            self,
            main_setup: MainSetup,
            index_current: int = 0
    ):
        super().__init__()

        self.main_setup = main_setup

        self.warned_duplicateName = False
        self.edited_unsaved = False

        # form layouts for the settings
        self.main_settings = UiProfileMainSettings(self.main_setup)
        self.extra_settings = UiProfileExtraSettings(self.main_setup)
        self.common_settings = UiShowProfileCommonSettings(False, self.main_setup)

        self.currently_selected_profile_id = None

        # create stack layout
        self.profiles_list = QListWidget()
        self.updateListContents()
        self.profiles_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.selectRow(index_current)
        self.profiles_list.setFixedWidth(150)
        self.profiles_list.setAlternatingRowColors(True)
        self.profiles_list.currentTextChanged.connect(self.handleProfilesEditListSelected)

        self.profiles_buttons = UiEditProfilesButtons()
        self.profiles_buttons.add_one.clicked.connect(self.addProfileDialog)
        self.profiles_buttons.remove.clicked.connect(self.removeProfileDialog)
        self.profiles_buttons.duplicate.clicked.connect(self.duplicateProfileDialog)

        self.v_box_profiles = QVBoxLayout()
        self.v_box_profiles.addWidget(self.profiles_list)
        self.v_box_profiles.addLayout(self.profiles_buttons)
        self.v_box_profiles.setContentsMargins(0, 0, 0, 0)

        # # form layouts for the settings
        # self.main_settings = UiProfileMainSettings(self.main_setup)
        # self.extra_settings = UiProfileExtraSettings(self.main_setup)
        # self.common_settings = UiShowProfileCommonSettings(False, self.main_setup)

        # add extra and common settings into an HBox
        self.settings_h_box = QHBoxLayout()
        self.settings_h_box.addLayout(self.extra_settings)
        self.settings_h_box.addLayout(self.common_settings)

        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save,
            Qt.Horizontal
        )
        self.button_box.accepted.connect(self.handleSaveProfile)

        # combine settings and buttons
        self.v_box_settings_all = QVBoxLayout()
        self.v_box_settings_all.addLayout(self.main_settings)
        self.v_box_settings_all.addLayout(self.settings_h_box)
        self.v_box_settings_all.addWidget(self.button_box)

        # combine all in stack-like layout
        self.layout_all = QHBoxLayout()
        self.layout_all.addLayout(self.v_box_profiles)
        self.layout_all.addLayout(self.v_box_settings_all)

        self.setLayout(self.layout_all)

        # make the cancel button appear
        self.main_settings.name_input.textChanged.connect(self.refreshWarningsName)

        self.extra_settings.playAllShows_check.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.commercialsPlay_check.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.probability_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.probability_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.commercialsBreakWeight_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.commercialsBreakWeight_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.commercialsStartWeight_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.commercialsStartWeight_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))

        # save_profile.suspendedQueue = -> can't edit suspended Queue here

        for rad in self.extra_settings.form.uniqueShows_radio.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.showsLimit_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.showsLimit_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.shutdown_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.shutdown_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))

        self.profilesInclude_model = self.extra_settings.form.profilesInclude_list.model()
        self.profilesInclude_model.rowsInserted.connect(lambda: self.setProfileEdited(True))
        # self.profilesInclude_model.rowsRemoved.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.profilesInclude_remove.clicked.connect(lambda: self.setProfileEdited(True))

        self.profilesExclude_model = self.extra_settings.form.profilesExclude_list.model()
        self.profilesExclude_model.rowsInserted.connect(lambda: self.setProfileEdited(True))
        # self.profilesExclude_model.rowsRemoved.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.profilesExclude_remove.clicked.connect(lambda: self.setProfileEdited(True))

        self.startWithShows_model = self.extra_settings.form.startWithShows_list.model()
        self.startWithShows_model.rowsInserted.connect(lambda: self.setProfileEdited(True))
        # self.startWithShows_model.rowsRemoved.connect(lambda: self.setProfileEdited(True))
        self.extra_settings.form.startWithShows_remove.clicked.connect(lambda: self.setProfileEdited(True))

        for rad in self.common_settings.opening_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        for rad in self.common_settings.ending_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        # self.common_settings.
        self.common_settings.volume_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.common_settings.volume_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        self.common_settings.speed_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.common_settings.speed_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        self.common_settings.jump_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.common_settings.jump_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        self.common_settings.end_box.c_enable.clicked.connect(lambda: self.setProfileEdited(True))
        self.common_settings.end_box.s_value.valueChanged.connect(lambda: self.setProfileEdited(True))
        for rad in self.common_settings.pause_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        for rad in self.common_settings.mute_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        for rad in self.common_settings.fullscreen_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        # self.common_settings.
        for rad in self.common_settings.suspendShow_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        for rad in self.common_settings.suspendQueue_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        for rad in self.common_settings.noRerun_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))
        # self.common_settings.,
        # self.common_settings.,
        for rad in self.common_settings.debugPlayerJumps_box.all_radios:
            rad.clicked.connect(lambda: self.setProfileEdited(True))


    def updateListContents(self):
        self.all_profiles_objects: List[ConfigUserProfile] = self.main_setup.user_config.profiles

        index_remembered = self.profiles_list.currentRow()
        self.profiles_list.clear()
        self.profiles_list.addItems(list(map(
            lambda p: p.profileName,
            self.all_profiles_objects
        )))

        sleep(0.07)
        self.profiles_list.setCurrentRow(index_remembered)


    def handleReloadProfile(self):
        self.main_settings.loadProfileSettingsById(self.currently_selected_profile_id)
        self.extra_settings.loadProfileSettingsById(self.currently_selected_profile_id)
        self.extra_settings.form.loadProfileSettingsById(self.currently_selected_profile_id)
        self.common_settings.loadProfileSettingsById(self.currently_selected_profile_id)
        self.setProfileEdited(False)
        # TODO warn if not saved


    def selectRow(self, index: int):
        self.profiles_list.setCurrentRow(index)
        self.currently_selected_profile_id = self.all_profiles_objects[index].profileId
        self.handleReloadProfile()
        if hasattr(self, "button_box"):
            self.setProfileEdited(False)
        print("selectRow " + str(index))
        # TODO warn if not saved


    def handleProfilesEditListSelected(self):
        """Switch the settings to the selected profile. Warn if not saved.
        """
        if len(self.profiles_list) == 0:
            return

        self.selectRow(self.profiles_list.currentIndex().row())


    def setProfileEdited(self, edited: bool = True):
        if self.edited_unsaved == edited:
            return
        self.edited_unsaved = edited

        if self.edited_unsaved:
            self.button_box.hide()
            self.button_box = QDialogButtonBox(
                QDialogButtonBox.Save
                | QDialogButtonBox.Cancel,
                Qt.Horizontal
            )
            self.button_box.accepted.connect(self.handleSaveProfile)
            self.button_box.rejected.connect(self.handleReloadProfile)
            self.v_box_settings_all.addWidget(self.button_box)
        else:
            self.button_box.hide()
            self.button_box = QDialogButtonBox(
                QDialogButtonBox.Save,
                Qt.Horizontal
            )
            self.button_box.accepted.connect(self.handleSaveProfile)
            self.v_box_settings_all.addWidget(self.button_box)


    def refreshWarningsName(self):
        self.warned_duplicateName = False
        self.setProfileEdited(True)


    def handleSaveProfile(self):
        """Save profile with the given attributes. Check validity before saving
        """
        name_state, name_text, name_pos = self.main_settings.name_validator.validate(self.main_settings.name_input.text(), 0)
        # print("Name input regex: " + str(name_state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable. name_text: " + name_text + ", name_pos: " + str(name_pos))
        if name_state != 2:
            showWarningDialog("Name Missing", "Enter A Name For The Profile", "Every profile needs a name. Enter something in the name input field and try again.")
            return

        newName = whitespace_reduce(name_text)
        all_but_current_profile = list(filter(lambda p: p.profileId != self.currently_selected_profile_id, self.main_setup.user_config.profiles))

        if not self.warned_duplicateName and newName in list(map(lambda p: p.profileName, all_but_current_profile)):
            self.warned_duplicateName = True
            showWarningDialog("Duplicate Profile Name", "Name Already In Use", "The name you specified for this profile is already in use.\nYou can still save this show, if you want. Just try again.")
            return

        save_profile = self.main_setup.get_profile_by_id(self.currently_selected_profile_id)

        # main settings
        save_profile.profileName = newName
        save_profile.startWithShows = list(map(lambda x: x.showId, self.extra_settings.form.startWithShows_list.objects))
        # save_profile.suspendedQueue = 0
        save_profile.playAllShows = self.extra_settings.playAllShows_check.isChecked()
        save_profile.profilesInclude = list(map(lambda x: x.profileId, self.extra_settings.form.profilesInclude_list.objects))
        save_profile.profilesExclude = list(map(lambda x: x.profileId, self.extra_settings.form.profilesExclude_list.objects))

        # extra and common settings
        save_profile.profileSettings = self.get_profile_ec_settings()

        save_current_user_config_complete(self.main_setup)
        print("saved profile")

        self.setProfileEdited(False)
        self.updateListContents()
        gui_event_profiles_update_combo.set()


    def get_profile_ec_settings(self) -> ConfigUserProfileSettings:
        return ConfigUserProfileSettings(
            self.extra_settings.commercialsPlay_check.isChecked(),
            self.extra_settings.form.commercialsStartWeight_box.getValueNoneable(),
            self.extra_settings.form.commercialsBreakWeight_box.getValueNoneable(),
            self.extra_settings.form.probability_box.getValueNoneable(),
            self.extra_settings.form.shutdown_box.getValueNoneable(),
            self.extra_settings.form.showsLimit_box.getValueNoneable(),
            self.extra_settings.form.uniqueShows_radio.getRadioBool(),
            (True if self.extra_settings.form.uniqueShows_radio.getRadioBool() is None else None),

            self.common_settings.opening_box.getRadioBool(),
            self.common_settings.ending_box.getRadioBool(),
            # self.common_settings.,
            self.common_settings.volume_box.getValueNoneable(),
            self.common_settings.speed_box.getValueNoneable(),
            self.common_settings.jump_box.getValueNoneable(),
            self.common_settings.end_box.getValueNoneable(),
            self.common_settings.pause_box.getRadioBool(),
            self.common_settings.mute_box.getRadioBool(),
            self.common_settings.fullscreen_box.getRadioBool(),
            # self.common_settings.,
            self.common_settings.suspendShow_box.getRadioBool(),
            self.common_settings.suspendQueue_box.getRadioBool(),
            self.common_settings.noRerun_box.getRadioBool(),
            # self.common_settings.,
            # self.common_settings.,
            self.common_settings.debugPlayerJumps_box.getRadioBool(),
        )


    def addProfileDialog(self):
        profileName, okPressed = QInputDialog.getText(self, "New Profile", "Name of new profile:", QLineEdit.Normal, "")
        if okPressed:
            self.main_setup.user_config.create_new_profile(profileName)
            print("saved new profile")

            self.updateListContents()
            gui_event_profiles_update_combo.set()


    def removeProfileDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete profile '" + self.profiles_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText("This action cannot be undone.")
        msg.setWindowTitle("Delete Profile")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeProfileConfirmed)
        msg.exec_()

    def removeProfileConfirmed(self):
        selected_index = self.profiles_list.currentIndex().row()
        profile_to_delete = self.all_profiles_objects[selected_index]

        if len(self.main_setup.user_config.profiles) > 1:
            self.main_setup.delete_profile(profile_to_delete)
            if not self.main_setup.global_config.noAutosave:
                save_current_user_config_complete(self.main_setup)

            self.updateListContents()
            gui_event_profiles_update_combo.set()
            print("Delete " + profile_to_delete.profileName)
        else:
            showWarningDialog("Profile Deletion Failed", "Cannot Delete The Last Profile", "A profile can only be deleted, if it is not the last remaining profile. If you really want to delete this profile, create a new one first.")

    def duplicateProfileDialog(self):
        selected_index = self.profiles_list.currentIndex().row()
        profile_to_duplicate = self.all_profiles_objects[selected_index]

        text, okPressed = QInputDialog.getText(self, "Duplicate Profile", "Name of copy of '" + profile_to_duplicate.profileName + "':", QLineEdit.Normal, "")
        if okPressed:
            self.main_setup.user_config.duplicate_profile(profile_to_duplicate, whitespace_reduce(text))
            save_current_user_config_complete(self.main_setup)
            self.updateListContents()

            print("duplicated " + profile_to_duplicate.profileName)




class UiEditGeneralSettingsWidget(QFrame):

    def __init__(
            self,
            main_setup: MainSetup,
            index_current_base_path: int = 0
    ):
        super().__init__()

        self.main_setup = main_setup

        self.general_settings = UiEditGeneralSettingsLayout()

        self.base_paths = UiEditBasePathsLayout(self.main_setup, index_current_base_path)

        self.layout_all = QVBoxLayout()
        self.layout_all.addLayout(self.general_settings)
        self.layout_all.addLayout(self.base_paths)

        self.setLayout(self.layout_all)



class UiEditGeneralSettingsLayout(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.form = UiEditGeneralSettingsForm()

        self.title_label = UiLabelTooltip("General Settings and GUI", "Some settings influencing all users and profiles. Also some graphical user interface settings.", True)

        self.addWidget(self.title_label)
        self.addLayout(self.form)




class UiEditGeneralSettingsForm(QFormLayout):

    def __init__(self):
        super().__init__()

        self.noAutosave_label = UiLabelTooltip("No Save", "When enabled, nothing will be saved until this is turned off again.\nThis setting will be disabled when you close the program, since this setting can't be saved.")
        self.noAutosave_box = UiFormRadioBinary(default=False)

        self.dynamicSettings_label = UiLabelTooltip("Dynamic Settings", "When enabled, changing one of the following values in the playback control, it will be automatically updated in the show's settings:\nvolume, speed, video track, audio track, subtitle track.\n\nA second volume slider will be added to the playback controls. The normal slider will be on top, the show volume slider below.")
        self.dynamicSettings_box = UiFormRadioBinary(default=False)

        self.excludeFilesEndingWith_label = UiLabelTooltip("Exclude Files", "Ignore all files that end with a specified string.\nSeparate multiple with comma.")
        self.excludeFilesEndingWith_input = QLineEdit()
        self.excludeFilesEndingWith_input.setMinimumWidth(250)
        self.excludeFilesEndingWith_input.setMaxLength(500)
        self.excludeFilesEndingWith_input.setPlaceholderText("wav, .mp4, full.mp3, .txt, ...")


        self.addRow(self.noAutosave_label, self.noAutosave_box)
        self.addRow(self.dynamicSettings_label, self.dynamicSettings_box)
        self.addRow(self.excludeFilesEndingWith_label, self.excludeFilesEndingWith_input)




class UiEditBasePathsLayout(QVBoxLayout):

    def __init__(
            self,
            main_setup: MainSetup,
            index_current: int = 0
    ):
        super().__init__()

        self.main_setup = main_setup

        self.title_label = UiLabelTooltip("Base Paths", "Add, edit and delete base paths.", True)

        self.basePaths_list = QListWidget()
        self.basePaths_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.basePaths_list.setCurrentRow(index_current)
        self.basePaths_list.setAlternatingRowColors(True)

        self.basePaths_buttons = UiEditBasePathsButtons()
        self.basePaths_buttons.add_one.clicked.connect(self.addBasePathDialog)
        self.basePaths_buttons.remove.clicked.connect(self.removeBasePathDialog)
        self.basePaths_buttons.edit.clicked.connect(self.editBasePathDialog)

        self.updateBasepathList()

        self.h_box = QHBoxLayout()
        self.h_box.addLayout(self.basePaths_buttons)
        self.h_box.addWidget(self.basePaths_list)

        self.addWidget(self.title_label)
        self.addLayout(self.h_box)


    def updateBasepathList(self):
        self.basepath_objects = self.main_setup.global_config.basePaths
        self.basePaths_list.clear()
        self.basePaths_list.addItems(self.basepath_objects)

        self.basePaths_buttons.remove.setEnabled(len(self.main_setup.global_config.basePaths) > 0)


    def addBasePathDialog(self): # TODO
        self.abp_dialog = QDialog()
        self.abp_dialog.setWindowTitle("Create new Base Path")
        self.abp_dialog.setWindowModality(Qt.ApplicationModal)
        self.abp_dialog.setMinimumWidth(500)

        path_box = UiPathBox()

        button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal
        )
        button_box.accepted.connect(lambda: self.addBasePathConfirmed(path_box.path_input.text().strip(), self.abp_dialog))
        button_box.rejected.connect(self.abp_dialog.hide)

        # combine path box and buttons
        layout_all = QVBoxLayout()
        layout_all.addLayout(path_box)
        layout_all.addWidget(button_box)

        self.abp_dialog.setLayout(layout_all)
        self.abp_dialog.show()

    def addBasePathConfirmed(self, new_basePath: str, dialog):
        if new_basePath == "":
            showWarningDialog("Empty New Base Path", "Base Path is Empty", "If you don't want to use a base path, uncheck the checkbox for base paths instead of using an empty base path.")
        elif new_basePath not in self.main_setup.global_config.basePaths:
            self.main_setup.global_config.basePaths.append(new_basePath)
            save_global_config_complete(self.main_setup.global_config)
            self.updateBasepathList()
            gui_event_basepaths_update_combo.set()
            dialog.hide()
        else:
            showWarningDialog("Duplicate Base Path", "Base Path Already Exists", "Can't save a base path that already exists.")


    def removeBasePathDialog(self):
        selected_index = self.basePaths_list.currentIndex().row()
        selected_index = len(self.basepath_objects) - 1 if selected_index == -1 else selected_index

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete base path '" + self.basepath_objects[selected_index] + "'?")
        msg.setInformativeText(
            "This action cannot be undone.")
        msg.setWindowTitle("Delete Base Path")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(lambda: self.removeBasePathConfirmed(selected_index))
        msg.exec_()

    def removeBasePathConfirmed(self, selected_index: int):
        basePath_to_delete = self.basepath_objects[selected_index]

        # if len(self.main_setup.user_config.basePaths) > 1:
        self.main_setup.global_config.basePaths.remove(basePath_to_delete)
        save_global_config_complete(self.main_setup.global_config)

        self.updateBasepathList()
        gui_event_basepaths_update_combo.set()
        print("Delete " + basePath_to_delete)
        # else:
        #     showWarningDialog("Base Path Deletion Failed", "Cannot Delete The Last Base Path", "A base path can only be deleted, if it is not the last remaining base path. If you really want to delete this base path, create a new one first.")


    def editBasePathDialog(self):
        self.ebp_dialog = QDialog()
        self.ebp_dialog.setWindowTitle("Edit Base Path")
        self.ebp_dialog.setWindowModality(Qt.ApplicationModal)
        self.ebp_dialog.setMinimumWidth(500)

        path_box = UiPathBox()
        selected_index = self.basePaths_list.currentRow()
        path_box.path_input.setText(self.basepath_objects[selected_index])

        button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal
        )
        button_box.accepted.connect(lambda: self.editBasePathConfirmed(path_box.path_input.text().strip(), selected_index, self.ebp_dialog))
        button_box.rejected.connect(self.ebp_dialog.hide)

        # combine path box and buttons
        layout_all = QVBoxLayout()
        layout_all.addLayout(path_box)
        layout_all.addWidget(button_box)

        self.ebp_dialog.setLayout(layout_all)
        self.ebp_dialog.show()

    def editBasePathConfirmed(self, new_basePath: str, selected_index: int, dialog):
        pre_change_basePath = self.basepath_objects[selected_index]
        if new_basePath == "":
            showWarningDialog("Empty New Base Path", "Base Path is Empty", "If you don't want to use a base path, uncheck the checkbox for base paths instead of using an empty base path.")
        elif new_basePath not in list(bp for bp in self.main_setup.global_config.basePaths if pre_change_basePath != bp):
            self.main_setup.global_config.basePaths[selected_index] = new_basePath
            save_global_config_complete(self.main_setup.global_config)
            self.updateBasepathList()
            gui_event_basepaths_update_combo.set()
            dialog.hide()
        else:
            showWarningDialog("Duplicate Base Path", "Base Path Already Exists", "Can't save a base path that already exists.")






class UiEditBasePathsButtons(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new Base Path.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected Base Path.", True, True)
        self.edit = UiManagementToolButton(ICON_EDIT_THIN, "Edit the selected Base Path.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.edit)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)




class UiManagementQueueHistoryTabs(QTabWidget):

    def __init__(
            self,
            main_setup: MainSetup,
            handle_queue_buttons_state
    ):
        super().__init__()

        self.main_setup = main_setup
        self.handle_queue_buttons_state = handle_queue_buttons_state


        self.queue_table = UiQueueTable(
            self.main_setup,
            self.handle_queue_buttons_state
        )
        self.queue_table.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # buttons for queue table interaction
        self.queue_buttons = UiManagementQueueButtons()

        self.v_box_queue = QVBoxLayout()
        self.v_box_queue.addWidget(self.queue_table)
        self.v_box_queue.addLayout(self.queue_buttons)
        self.v_box_queue.setContentsMargins(0, 0, 0, 0)

        self.queue_widget = QWidget()
        self.queue_widget.setLayout(self.v_box_queue)


        # history, showing all played files so far
        self.history_table = UiHistoryTable(self.main_setup)
        self.history_table.setSelectionMode(QAbstractItemView.SingleSelection)

        # buttons for history table interaction
        self.history_buttons = UiManagementHistoryButtons()

        self.v_box_history = QVBoxLayout()
        self.v_box_history.addWidget(self.history_table)
        self.v_box_history.addLayout(self.history_buttons)
        self.v_box_history.setContentsMargins(0, 0, 0, 0)

        self.history_widget = QWidget()
        self.history_widget.setLayout(self.v_box_history)


        # put into tabs
        self.addTab(self.queue_widget, "Queue")
        self.addTab(self.history_widget, "History")





class UiQueueTable(QTableView):

    def __init__(
        self,
        main_setup: MainSetup,
        handle_queue_buttons_state
    ):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.main_setup = main_setup
        self.handle_queue_buttons_state = handle_queue_buttons_state

        self.model = QStandardItemModel(0, 4, self)
        self.model.setHorizontalHeaderLabels(["Name", "Ep", "op", "ed"])

        tmp_it = QStandardItem("tmp")
        self.tmp_font_bold = tmp_it.font()
        self.tmp_font_bold.setBold(True)

        self.updateTableContents(True)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(2, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(3, QHeaderView.ResizeToContents)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()

        updateTableLoopThread = Thread(target=self.updateTableLoop, daemon=True)
        updateTableLoopThread.start()

    def updateTableLoop(self):
        while (True):
            gui_event_queue_update_table.wait()
            gui_event_queue_update_table.clear()
            self.updateTableContents()


    def objectToTableItemMapper(self, show: ConfigUserShow):
        return (
            show.showName,
            show.currentEpisode,
            TEXT_TRUE_MARK if show.showSettings.opening else TEXT_FALSE_MARK,
            TEXT_TRUE_MARK if show.showSettings.ending else TEXT_FALSE_MARK
        )


    def updateTableContents(self, initial_update: bool = False):
        self.queued_shows_objects: List[ConfigUserShow] = list(map(
            self.main_setup.get_show_by_id,
            self.main_setup.queue_shows.queue
        ))
        while None in self.queued_shows_objects:
            self.queued_shows_objects.remove(None)

        self.queued_shows_items = list(
            map(
                self.objectToTableItemMapper,
                self.queued_shows_objects
            )
        )

        self.model.removeRows(0, self.model.rowCount())

        # for name, episode_number, opening_mark, ending_mark in DUMMY_QUEUE_LIST_T_L:
        for name, episode_number, opening_mark, ending_mark in self.queued_shows_items:
            it_name = QStandardItem(name)
            it_ep = QStandardItem(str(episode_number))
            it_op = QStandardItem(opening_mark)
            it_ed = QStandardItem(ending_mark)

            it_ep.setTextAlignment(Qt.AlignCenter)
            it_op.setTextAlignment(Qt.AlignCenter)
            it_ed.setTextAlignment(Qt.AlignCenter)

            it_name.setFont(self.tmp_font_bold)
            it_name.setEditable(False)
            it_ep.setEditable(False)
            it_op.setEditable(False)
            it_ed.setEditable(False)
            self.model.appendRow([it_name, it_ep, it_op, it_ed])

        self.setModel(self.model)

        if not initial_update:
            self.handle_queue_buttons_state()

        print("updated queue table")




class UiHistoryTable(QTableView):

    def __init__(
            self,
            main_setup: MainSetup
    ):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.main_setup = main_setup

        self.model = QStandardItemModel(0, 2, self)
        self.model.setHorizontalHeaderLabels(["File Path", "Length"])
        self.setToolTip("Base Path: TODO")

        self.updateTableContents()

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()

        updateTableLoopThread = Thread(target=self.updateTableLoop, daemon=True)
        updateTableLoopThread.start()


    def updateTableLoop(self):
        while (True):
            gui_event_history_update_table.wait()
            gui_event_history_update_table.clear()
            self.updateTableContents()
            print("updated history table")


    def objectToTableItemMapper(self, fullpath_shortpath_length_tuple):
        return (
            fullpath_shortpath_length_tuple[0],
            fullpath_shortpath_length_tuple[1],
            get_h_m_s_from_milliseconds(fullpath_shortpath_length_tuple[2])
        )


    def updateTableContents(self):
        self.history_objects: List[(str, str, int)] = self.main_setup.queue_shows.startedPlaybacks
        self.history_items = list(
            map(
                self.objectToTableItemMapper,
                self.history_objects
            )
        )

        self.model.removeRows(0, self.model.rowCount())

        for fullpath, path, length in self.history_items:
            it_path = QStandardItem(path)
            it_length = QStandardItem(length)

            it_length.setTextAlignment(Qt.AlignCenter)

            it_path.setEditable(False)
            it_length.setEditable(False)
            self.model.appendRow([it_path, it_length])

        self.setModel(self.model)



    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())





class UiManagementShowsCmsTabs(QTabWidget):

    def __init__(
            self,
            main_setup: MainSetup,
            handle_queue_buttons_state,
            handle_shows_cms_buttons_state,
    ):
        super().__init__()

        self.main_setup = main_setup
        self.handle_queue_buttons_state = handle_queue_buttons_state
        self.handle_shows_cms_buttons_state = handle_shows_cms_buttons_state

        self.cms_tab_added = False

        self.shows_table = UiShowsTable(self.main_setup, self.handle_queue_buttons_state, self.handle_shows_cms_buttons_state)
        self.shows_table.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # buttons for show list interaction
        self.shows_buttons = UiManagementShowsCmsButtons()

        self.v_box_shows = QVBoxLayout()
        self.v_box_shows.addWidget(self.shows_table)
        self.v_box_shows.addLayout(self.shows_buttons)
        self.v_box_shows.setContentsMargins(0, 0, 0, 0)

        self.shows_widget = QWidget()
        self.shows_widget.setLayout(self.v_box_shows)

        self.addTab(self.shows_widget, "Shows")


        # add widget for commercials
        self.commercials_table = UiCommercialsTable(
            self.main_setup,
            self.handleAddRemoveCmsTabs
        )
        self.commercials_table.setSelectionMode(QAbstractItemView.SingleSelection)

        # buttons for show list interaction
        self.commercials_buttons = UiManagementShowsCmsButtons(isShows=False)


        self.v_box_commercials = QVBoxLayout()
        self.v_box_commercials.addWidget(self.commercials_table)
        self.v_box_commercials.addLayout(self.commercials_buttons)
        self.v_box_commercials.setContentsMargins(0, 0, 0, 0)

        self.commercials_widget = QWidget()
        self.commercials_widget.setLayout(self.v_box_commercials)

        if len(self.commercials_table.cms_objects) > 0:
            self.addTab(self.commercials_widget, "Commercials")
            self.cms_tab_added = True

    def handleAddRemoveCmsTabs(self):
        if hasattr(self, "commercials_table"):
            printdev("hasattr")
            if self.cms_tab_added:
                printdev("is added already")
                if len(self.commercials_table.cms_objects) == 0:
                    printdev("remove!!")
                    self.removeTab(1)
                    self.cms_tab_added = False
            else:
                if len(self.commercials_table.cms_objects) > 0:
                    self.addTab(self.commercials_widget, "Commercials")
                    self.cms_tab_added = True




class UiShowsTable(QTableView):

    def __init__(
        self,
        main_setup: MainSetup,
        handle_queue_buttons_state,
        handle_shows_cms_buttons_state
    ):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.main_setup = main_setup
        self.handle_queue_buttons_state = handle_queue_buttons_state
        self.handle_shows_cms_buttons_state = handle_shows_cms_buttons_state

        self.model = QStandardItemModel(0, 3, self)
        self.model.setHorizontalHeaderLabels(["Name", "Ep", "Tags"])

        self.updateTableContents(True)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(2, QHeaderView.Stretch)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()

        updateTableLoopThread = Thread(target=self.updateTableLoop, daemon=True)
        updateTableLoopThread.start()


    def updateTableLoop(self):
        while (True):
            gui_event_shows_update_table.wait()
            gui_event_shows_update_table.clear()
            self.updateTableContents()
            print("updated shows table")


    def objectToTableItemMapper(self, show: ConfigUserShow):
        return (
            show.showName,
            show.currentEpisode,
            # ", ".join(show.tags)
            show.tags
        )


    def updateTableContents(self, initial_update: bool = False):
        self.shows_objects = self.main_setup.get_all_active_shows_for_current_profile()

        sorting = SortingState.fromValue(self.main_setup.global_config.windowSettings.sortShows)
        ascending = self.main_setup.global_config.windowSettings.sortShowsAscending
        ascending = True if ascending is None else ascending

        self.main_setup.user_config.load_all_tags()
        all_tags_counted = self.main_setup.user_config.tmp_tags_list_counted

        self.shows_objects = sortShowObjects(self.shows_objects, sorting, ascending, all_tags_counted, self.main_setup.global_config)

        self.shows_items = list(
            map(
                self.objectToTableItemMapper,
                self.shows_objects
            )
        )

        self.model.removeRows(0, self.model.rowCount())

        for name, ep, tags in self.shows_items:
            it_name = QStandardItem(name)
            it_ep = QStandardItem(str(ep))
            it_tags = QStandardItem(", ".join(tags))

            it_ep.setTextAlignment(Qt.AlignCenter)

            it_name.setEditable(False)
            it_ep.setEditable(False)
            it_tags.setEditable(False)
            self.model.appendRow([it_name, it_ep, it_tags])

        self.setModel(self.model)

        if not initial_update:
            self.handle_queue_buttons_state()
            self.handle_shows_cms_buttons_state()


    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())




def sortShowObjects(
        show_objects: [ConfigUserShow],
        sorting: SortingState,
        ascending: bool,
        all_tags_counted,
        global_config: ConfigGlobal
):
    if sorting is None or sorting is SortingState.CREATION_DATE:
        return sorted(show_objects, key=lambda show_o: show_o.showId, reverse=not ascending)
    elif sorting is SortingState.NAME:
        return sorted(show_objects, key=lambda show_o: show_o.showName, reverse=not ascending)
    elif sorting is SortingState.TAGS_FREQUENCY_HIGHEST:
        return sorted(show_objects, key=lambda show_o: show_o.get_tags_frequency_highest(all_tags_counted), reverse=not ascending)
    elif sorting is SortingState.TAGS_FREQUENCY_SUM:
        return sorted(show_objects, key=lambda show_o: show_o.get_tags_frequency_sum(all_tags_counted), reverse=not ascending)
    elif sorting is SortingState.TAGS_AMOUNT:
        return sorted(show_objects, key=lambda show_o: len(show_o.tags), reverse=not ascending)
    elif sorting is SortingState.CURRENT_EPISODE:
        return sorted(show_objects, key=lambda show_o: show_o.currentEpisode, reverse=not ascending)
    elif sorting is SortingState.EPISODES_AMOUNT:
        return sorted(show_objects, key=lambda show_o: show_o.get_episodes_amount_tmp(global_config, False), reverse=not ascending)
    elif sorting is SortingState.COMMERCIAL_VALUE:
        return sorted(show_objects, key=lambda show_o: show_o.showSettings.commercialWeight, reverse=not ascending)







class UiCommercialsTable(QTableView):

    def __init__(
            self,
            main_setup: MainSetup,
            handleAddRemoveCmsTabs
    ):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.main_setup = main_setup
        self.handleAddRemoveCmsTabs = handleAddRemoveCmsTabs

        self.model = QStandardItemModel(0, 4, self)
        self.model.setHorizontalHeaderLabels(["Name", "Val", "Min", "Max"])

        self.updateTableContents()

        self.setModel(self.model)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.resizeColumnToContents(1)
        self.resizeColumnToContents(2)
        self.resizeColumnToContents(3)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()

        updateTableLoopThread = Thread(target=self.updateTableLoop, daemon=True)
        updateTableLoopThread.start()


    def updateTableLoop(self):
        while (True):
            gui_event_cms_update_table.wait()
            gui_event_cms_update_table.clear()
            self.updateTableContents()
            print("updated cms table")


    def objectToTableItemMapper(self, show: ConfigUserShow):
        return (
            show.showName,
            show.showSettings.commercialWeight,
            show.showSettings.commercialMin,
            show.showSettings.commercialMax
        )


    def updateTableContents(self):
        self.cms_objects: [ConfigUserShow] = self.main_setup.get_all_active_cms_for_current_profile()

        self.handleAddRemoveCmsTabs()

        sorting = SortingState.fromValue(self.main_setup.global_config.windowSettings.sortCommercials)
        ascending = self.main_setup.global_config.windowSettings.sortCommercialsAscending
        ascending = True if ascending is None else ascending

        self.main_setup.user_config.load_all_tags()
        all_tags_counted = self.main_setup.user_config.tmp_tags_list_counted

        self.cms_objects = sortShowObjects(self.cms_objects, sorting, ascending, all_tags_counted, self.main_setup.global_config)

        self.cms_items = list(
            map(
                self.objectToTableItemMapper,
               self.cms_objects
            )
        )

        self.model.removeRows(0, self.model.rowCount())

        for name, cm_value, cm_min, cm_max in self.cms_items:
            it_name = QStandardItem(name)
            it_cm_value = QStandardItem(str(cm_value) if cm_value is not None else "")
            it_cm_min = QStandardItem(str(cm_min) if cm_min is not None else "")
            it_cm_max = QStandardItem(str(cm_max) if cm_max is not None else "")

            it_cm_value.setTextAlignment(Qt.AlignCenter)
            it_cm_min.setTextAlignment(Qt.AlignCenter)
            it_cm_max.setTextAlignment(Qt.AlignCenter)

            it_name.setEditable(False)
            it_cm_value.setEditable(False)
            it_cm_min.setEditable(False)
            it_cm_max.setEditable(False)
            self.model.appendRow([it_name, it_cm_value, it_cm_min, it_cm_max])

        self.setModel(self.model)


    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())





class UiManagementShowsCmsButtons(QHBoxLayout):

    def __init__(self, isShows: bool = True):
        super().__init__()

        entity_str = "show" if isShows else "commercial"

        self.add_to_queue = UiManagementToolButton(ICON_MOVE_LEFT, ("Add selected shows to end of queue." if isShows else ""))
        self.sort = UiManagementToolButton(ICON_SORT, f"Select {entity_str} sort mode.")
        self.sort_change = UiManagementToolButton(ICON_SORT_CHANGE, "Toggle between ascending and descending sorting.")
        self.play_now = UiManagementToolButton(ICON_PLAY_BLACK, f"Put selected show at beginning of queue." if isShows else "")

        self.add_new = UiManagementToolButton(ICON_ADD, f"Add a new show{' or commercial' if not isShows else ''}.")
        self.edit = UiManagementToolButton(ICON_EDIT, f"Edit {entity_str}.")

        if not isShows:
            self.add_to_queue.setDisabled(True)
            self.play_now.setDisabled(True)

        self.addWidget(self.add_to_queue)
        self.addWidget(self.sort)
        self.addWidget(self.sort_change)
        self.addWidget(self.play_now)
        self.addStretch()
        self.addWidget(self.add_new)
        self.addWidget(self.edit)
        self.setContentsMargins(5, 0, 5, 5)





class UiManagementQueueButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD, "Add one show to queue.")
        self.remove = UiManagementToolButton(ICON_REMOVE, "Remove selection from queue.")
        self.reload = UiManagementToolButton(ICON_RELOAD, "Reload the queue.")
        self.move_up = UiManagementToolButton(ICON_MOVE_UP, "Move selection up in queue.")
        self.move_down = UiManagementToolButton(ICON_MOVE_DOWN, "Move selection down in queue.")

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.reload)
        self.addStretch()
        self.addWidget(self.move_up)
        self.addWidget(self.move_down)
        self.setContentsMargins(5, 0, 5, 5)





class UiManagementHistoryButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.open_folder = UiManagementToolButton(ICON_FOLDER_BLACK, "Open file folder.")

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.open_folder)
        self.addStretch()
        self.setContentsMargins(5, 0, 5, 5)




class UiManagementToolButton(QToolButton):

    def __init__(
            self,
            icon_path: str,
            status_tip: str = "",
            tooltip_instead_of_statustip: bool = False,
            bigger: bool = False,
            tooltip: str = None
    ):
        super().__init__()
        self.setIcon(QIcon(icon_path))
        self.setIconSize(
            QSize(int(BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM * (1.4 if bigger else 1)), int(BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM * (1.4 if bigger else 1))))
        self.setFixedSize(int(BUTTON_QUEUE_SHOWS_WIDTH * (1.4 if bigger else 1)), int(BUTTON_QUEUE_SHOWS_HEIGHT * (1.4 if bigger else 1)))
        if tooltip_instead_of_statustip:
            self.setToolTip(status_tip)
        else:
            self.setStatusTip(status_tip)
            if tooltip is not None:
                self.setToolTip(tooltip)





class UiShowsNewDialog(QDialog):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.warned_hasFiles = False
        self.warned_isDir = False
        self.warned_duplicateName = False
        self.warned_duplicateDir = False

        self.setWindowTitle("Add a new show")
        self.setWindowModality(Qt.ApplicationModal)


        # form layout for the main settings
        self.main_settings = UiShowMainSettings(self.main_setup, True)

        self.main_settings.form.path_box.path_input.textChanged.connect(self.refreshWarningsBasepath)
        self.main_settings.form.name_input.textChanged.connect(self.refreshWarningsName)

        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal,
        )

        self.button_box.accepted.connect(self.handleSaveNewShow)
        self.button_box.rejected.connect(self.hide)


        # combine all (form and buttons)
        self.layout_all = QVBoxLayout()
        self.layout_all.addLayout(self.main_settings)
        self.layout_all.addStretch()
        self.layout_all.addWidget(self.button_box)

        self.setLayout(self.layout_all)


    def handleSaveNewShow(self):
        """Create new show with the given attributes. Check validity before saving
        """
        name_state, name_text, name_pos = self.main_settings.form.name_validator.validate(self.main_settings.form.name_input.text(), 0)
        # print("Name input regex: " + str(name_state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable. name_text: " + name_text + ", name_pos: " + str(name_pos))
        if name_state != 2:
            showWarningDialog("Name Missing", "Enter A Name For New Show", "Every show needs a name. Enter something in the name input field and try again.")
            return

        path_state, path_text, path_pos = self.main_settings.form.path_box.path_validator.validate(self.main_settings.form.path_box.path_input.text(), 0)
        # print("Name input regex: " + str(path_state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable. path_text: " + path_text + ", path_pos: " + str(pos))
        if path_state != 2:
            showWarningDialog("Path Missing", "Choose A Directory For This Show's Path", "Use the 'Search' Button to find the directory with this show's files.")
            return

        use_base_path = self.main_setup.global_config.useBasePaths and not self.main_settings.form.no_basepath.isChecked()

        if use_base_path:
            complete_path = convert_path_slashes(os.path.join(self.main_settings.form.current_base_path, path_text))
        else:
            complete_path = convert_path_slashes(path_text)

        showName = whitespace_reduce(name_text)

        # check if directory exists (and contains files)
        if os.path.isdir(complete_path):
            if not (self.warned_hasFiles or len(get_files_from_directory(complete_path)) > 0):
                self.warned_hasFiles = True
                showWarningDialog("No Files Found", "New Show Without Files?", "There seem to be no files in the directory for this new show.\nYou can still save this show, if you want. Just try again.")
                return
        elif not self.warned_isDir:
            self.warned_isDir = True
            showWarningDialog("Directory Not Found", "New Show Without Directory?", "The path specified for this show's files does not seem to be valid, directory cannot be found.\nYou can still save this show, if you want. Just try again.")
            return

        all_but_current_show = self.main_setup.user_config.shows

        if not self.warned_duplicateName and showName in list(map(lambda s: s.showName, all_but_current_show)):
            self.warned_duplicateName = True
            showWarningDialog("Duplicate Show Name", "Name Already In Use", "The name you specified for this show is already in use.\nYou can still save this show, if you want. Just try again.")
            return

        if not self.warned_duplicateDir and path_text in list(map(lambda s: s.directoryPath, all_but_current_show)):
            self.warned_duplicateDir = True
            index_dir_dup = list(map(lambda s: s.directoryPath, all_but_current_show)).index(path_text)
            show_name_dir_dup = all_but_current_show[index_dir_dup].showName
            showWarningDialog("Duplicate Show Path", "Directory Path Already In Use", f"The directory path you specified for this show is already in use for the show '{show_name_dir_dup}'.\nYou can still save this show, if you want. Just try again.")
            return

        new_show = self.main_setup.user_config.create_new_show(
            showName,
            self.main_settings.form.active_check.isChecked(),
            self.main_settings.form.no_basepath.isChecked(),
            path_text,
            self.main_settings.form.get_profiles_ids(),
            self.main_settings.form.episode_first_box.getValueNoneable(),
            self.main_settings.form.episode_last_box.getValueNoneable(),
            self.main_settings.form.episode_current_input.value(),
            self.main_settings.form.get_tags()
        )

        save_current_user_config_complete(self.main_setup)
        print("saved new show")

        updatedShowUpdateGui()

        self.hide()


    def refreshWarningsBasepath(self):
        self.warned_isDir = False
        self.warned_hasFiles = False
        self.warned_duplicateDir = False


    def refreshWarningsName(self):
        self.warned_duplicateName = False





class UiShowMainSettings(QVBoxLayout):

    def __init__(
        self,
        main_setup: MainSetup,
        new_show: bool,
        setEditedTrue = None
    ):
        super().__init__()
        self.main_setup = main_setup
        self.setEditedTrue = setEditedTrue

        self.form = UiShowFormMainSettings(self.main_setup, new_show, self.setEditedTrue)

        self.title_label = UiLabelTooltip("Main Show Settings", "Essential properties of the show.", True)

        self.addWidget(self.title_label)
        self.addLayout(self.form)
        self.addStretch()




class UiShowFormMainSettings(QFormLayout):

    def __init__(
        self,
        main_setup: MainSetup,
        new_show: bool,
        setEditedTrue = None
    ):
        super().__init__()
        self.main_setup = main_setup
        self.setEditedTrue = setEditedTrue

        self.can_use_base_path = len(self.main_setup.global_config.basePaths) > 0
        self.current_base_path = self.main_setup.global_config.basePaths[self.main_setup.global_config.currentBasePathIndex] if self.can_use_base_path else ""

        self.name_label = UiLabelTooltip("Name", "Give the show a name. Can be different from the actual file and directory names.")
        self.name_input = QLineEdit()
        self.name_input.setMinimumWidth(350)
        self.name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.name_input.setValidator(self.name_validator)
        self.name_input.setPlaceholderText("Enter show name")


        self.active_check = QCheckBox("Active")
        self.active_check.setChecked(True)
        self.active_check.setToolTip("Whether this show should be queued in any profile.")

        self.no_basepath = QCheckBox("No Base Path")
        self.no_basepath.setToolTip("If checked, the base path will not be used for this show, even if base paths are activated in the main window.")
        if not self.can_use_base_path:
            # self.no_basepath.setChecked(False)
            self.no_basepath.setDisabled(True)
        self.no_basepath.clicked.connect(self.toggleNoBasepath)

        self.h_box_active_basepath = QHBoxLayout()
        self.h_box_active_basepath.addWidget(self.active_check)
        self.h_box_active_basepath.addWidget(self.no_basepath)
        self.h_box_active_basepath.addStretch()


        self.path_label = QLabel("Path")
        self.path_box = UiPathBox(
            basePath=self.current_base_path,
            useBasepath=self.useBasepath,
            enableNoBasepath=(lambda: self.no_basepath.setChecked(True))
        )


        self.episodes_label = QLabel("Episode")

        self.episode_current_label = QLabel("Current")
        self.episode_current_input = QSpinBox()
        self.episode_current_input.setMinimum(1)
        self.episode_current_input.setMaximum(99999)

        self.episode_first_label = UiLabelTooltip("First")
        self.episode_first_box = UiFormEnableSpin(spin_minimum=1, spin_default=1)

        self.episode_last_label = UiLabelTooltip("Last")
        self.episode_last_box = UiFormEnableSpin(spin_minimum=1, spin_default=1)


        self.episodes_h_box = QHBoxLayout()
        self.episodes_h_box.addWidget(self.episode_current_label)
        self.episodes_h_box.addWidget(self.episode_current_input)
        self.episodes_h_box.addStretch()
        self.episodes_h_box.addWidget(self.episode_first_label)
        self.episodes_h_box.addLayout(self.episode_first_box)
        self.episodes_h_box.addStretch()
        self.episodes_h_box.addWidget(self.episode_last_label)
        self.episodes_h_box.addLayout(self.episode_last_box)
        self.episodes_h_box.addStretch()


        self.profiles_label = QLabel("Active Profiles")
        # self.profiles_label.setWordWrap(True)
        self.profiles_box = UiShowProfilesBox(self.main_setup)


        self.tags_label = UiLabelTooltip("Tags", "Tags are only used for sorting and filtering in the user interface. They do not influence the playback in any way.")

        self.tags_box = UiShowTagsBox(self.main_setup, self.setEditedTrue)


        if not new_show:
            self.suspended_label = UiLabelTooltip("Suspended at", "This show will continue playing at the specified time. This usually happens when a show is interrupted during playback by skipping the show or stopping the session.")
            self.suspended_time = QLineEdit()
            self.suspended_time.setText("not suspended")
            self.suspended_time.setDisabled(True)


        self.addRow(self.name_label, self.name_input)
        self.addRow(QLabel(""), self.h_box_active_basepath)
        self.addRow(self.path_label, self.path_box)
        self.addRow(self.episodes_label, self.episodes_h_box)
        self.addRow(self.profiles_label, self.profiles_box)
        self.addRow(self.tags_label, self.tags_box)
        if not new_show:
            self.addRow(self.suspended_label, self.suspended_time)


    def loadShowSettingsById(self, show_id: int):
        show = self.main_setup.get_show_by_id(show_id)

        self.name_input.setText(show.showName)
        self.active_check.setChecked(show.isActive if show.isActive is not None else True)
        self.no_basepath.setChecked(show.doNotUseBasePath if show.doNotUseBasePath is not None else False)
        self.path_box.path_input.setText(show.directoryPath)
        self.path_box.path_input.setDisabled(False)
        self.episode_current_input.setValue(show.currentEpisode)
        self.episode_first_box.setValue(show.firstEpisode)
        if not hasattr(show, "lastEpisode_tmp") or show.lastEpisode is None:
            show.generate_first_and_last_episode_tmp_ints(self.main_setup.global_config, False)
            self.episode_last_box.s_value.setValue(show.lastEpisode_tmp)
        else:
            self.episode_last_box.setValue(show.lastEpisode)

        self.profiles_box.profiles_check_all.setChecked(0 in show.activeProfiles)
        for i in range(len(self.profiles_box.profile_checks)):
            self.profiles_box.profile_checks[i].setChecked(self.main_setup.user_config.profiles[i].profileId in show.activeProfiles)

        for i in range(len(self.tags_box.tag_checks)):
            self.tags_box.tag_checks[i].setChecked(self.main_setup.user_config.tmp_tags_list_counted[i][0] in show.tags)

        # for check in self.tags_box.tag_checks:
        #     check.setChecked(check.text() in show.tags)

        self.suspended_time.setText(get_h_m_s_from_milliseconds(show.suspendedTimeSeconds * 1000) if show.suspendedTimeSeconds else "not suspended")


    def get_profiles_ids(self) -> List[int]:
        saved_profile_ids = []

        if self.profiles_box.profiles_check_all.isChecked():
            saved_profile_ids.append(0)

        for i in range(len(self.profiles_box.profile_checks)):
            if self.profiles_box.profile_checks[i].isChecked():
                saved_profile_ids.append(self.main_setup.user_config.profiles[i].profileId)

        return saved_profile_ids


    def get_tags(self) -> List[str]:
        saved_tags = []

        # for check in self.tags_box.tag_checks:
        #     if check.isChecked():
        #         saved_tags.append(check.text()) # TODO CONTINUE

        for i in range(len(self.tags_box.tag_checks)):
            if self.tags_box.tag_checks[i].isChecked():
                saved_tags.append(self.main_setup.user_config.tmp_tags_list_counted[i][0])

        return saved_tags


    def toggleNoBasepath(self):
        if self.useBasepath():
            self.path_box.path_input.setPlaceholderText(self.current_base_path)
        else:
            self.path_box.path_input.setPlaceholderText("") # TODO make empty

        if self.current_base_path is None or self.current_base_path == "" or self.main_setup.global_config.useBasePaths is False:
            return

        base_path_sl = convert_path_slashes(self.current_base_path)
        path_text_sl = convert_path_slashes(self.path_box.path_input.text())
        if self.no_basepath.isChecked():
            if self.path_box.path_input.text() != "" and not path_text_sl.startswith(base_path_sl):
                self.path_box.path_input.setText(os.path.join(base_path_sl, path_text_sl))
        else:
            if path_text_sl.startswith(base_path_sl):
                self.path_box.path_input.setText(path_text_sl[len(base_path_sl)+1:])

    def useBasepath(self):
        return self.current_base_path is not None and len(self.current_base_path) > 0 and (self.main_setup.global_config.useBasePaths and not self.no_basepath.isChecked())




class UiPathBox(QHBoxLayout):

    def __init__(
            self,
            basePath: str = None,
            useBasepath = None,
            enableNoBasepath = None,
            search_file: bool = False
    ):
        super().__init__()

        self.basePath = convert_path_slashes(basePath) if basePath is not None else None
        self.useBasepath = useBasepath
        self.enableNoBasepath = enableNoBasepath
        self.search_file = search_file

        self.path_input = QLineEdit()
        self.path_input.setPlaceholderText("Select a file" if search_file else "Directory of the files.")
        if self.useBasepath and self.useBasepath():
            self.path_input.setPlaceholderText(self.basePath)

        self.path_regex = QRegExp('^.+$')
        self.path_validator = QRegExpValidator(self.path_regex)
        self.path_input.setValidator(self.path_validator)

        self.path_file_button = QToolButton()
        self.path_file_button.setText("Search")
        self.path_file_button.clicked.connect(self.openDirectoryDialog)

        self.path_open_button = UiManagementToolButton(ICON_FOLDER_BLACK, "Open the folder.")
        self.path_open_button.clicked.connect(self.openPathDirectory)
        self.path_open_button.setToolTip("Open selected directory.")

        self.addWidget(self.path_input)
        self.addWidget(self.path_file_button)
        self.addWidget(self.path_open_button)


    def openDirectoryDialog(self):
        select_dialog = QFileDialog()

        if self.path_input.text() == "":
            if self.basePath and self.useBasepath and self.useBasepath():
                select_dialog.setDirectory(self.basePath)
        else:
            path = convert_path_slashes(
                self.path_input.text() if (not self.useBasepath or not self.useBasepath()) else os.path.join(self.basePath, self.path_input.text())
            )
            select_dialog.setDirectory(os.path.dirname(path))

        # select_dialog.setFileMode(QFileDialog.DirectoryOnly)
        # select_dialog.setAcceptMode(QFileDialog.AcceptOpen)
        if self.search_file:
            dir_name = select_dialog.getOpenFileName(self.path_file_button, "Select a file") # TODO only used for Mp3Merge, make only .apkg files selectable
        else:
            dir_name = select_dialog.getExistingDirectory(self.path_file_button, "Select directory of the files")
        # QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
        if not dir_name:
            return

        if self.useBasepath and self.useBasepath():
            if dir_name.startswith(self.basePath):
                self.path_input.setText(dir_name[len(self.basePath)+1:])
            else:
                self.path_input.setText(dir_name)
                if self.enableNoBasepath:
                    self.enableNoBasepath()
        else:
            self.path_input.setText(dir_name)



    def openPathDirectory(self):
        path = convert_path_slashes(
            self.path_input.text() if (not self.useBasepath or not self.useBasepath()) else os.path.join(self.basePath, self.path_input.text())
        )
        print("open directory: " + path)
        open_file(path)




class UiShowProfilesBox(QVBoxLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.profiles_check_all = QCheckBox("All Profiles")
        self.profiles_check_all.setChecked(True)
        self.profiles_check_all.setToolTip(
            "If True, then played in all profiles. Ignores Checkboxes below and settings like 'Exclude Profile' (in Profile Settings)")

        self.addWidget(self.profiles_check_all)

        all_profiles = self.main_setup.user_config.profiles
        self.profile_checks = []

        self.profiles_checks_grid_box = QGridLayout()
        grid_cols = 3
        for i in range(len(all_profiles)):
            new_checkbox = QCheckBox(all_profiles[i].profileName)
            self.profile_checks.append(new_checkbox)
            self.profiles_checks_grid_box.addWidget(self.profile_checks[i], int(i / grid_cols) + 1, i % grid_cols)

        self.profiles_checks_grid_h_box = QHBoxLayout()
        self.profiles_checks_grid_h_box.addLayout(self.profiles_checks_grid_box)
        self.profiles_checks_grid_h_box.addStretch()

        self.addLayout(self.profiles_checks_grid_h_box)




class UiShowTagsBox(QVBoxLayout):

    def __init__(
        self,
        main_setup: MainSetup,
        setEditedTrue = None
    ):
        super().__init__()

        self.main_setup = main_setup
        self.setEditedTrue = setEditedTrue

        self.tags_add_input = QLineEdit()
        self.tags_add_input.setPlaceholderText("Add a tag. Unused tags are deleted on restart.")
        self.tags_add_input.setMaxLength(50)

        self.tags_add_button = UiManagementToolButton(ICON_ADD, "Add tag from input.")
        self.tags_add_button.setToolTip("Add tag from input.")
        self.tags_add_button.clicked.connect(self.addNewTag)

        self.tags_add_h_box = QHBoxLayout()
        self.tags_add_h_box.addWidget(self.tags_add_input)
        self.tags_add_h_box.addWidget(self.tags_add_button)

        self.main_setup.user_config.load_all_tags()
        self.all_tags_counted = self.main_setup.user_config.tmp_tags_list_counted
        self.tag_checks = []

        self.tags_checks_grid_box = QGridLayout()
        self.grid_cols = 3
        amount_all_tags_counted = len(self.all_tags_counted)
        for i in range(amount_all_tags_counted):
            new_checkbox = QCheckBox(f"{self.all_tags_counted[i][0]} ({self.all_tags_counted[i][1]})")
            self.tag_checks.append(new_checkbox)
            self.tags_checks_grid_box.addWidget(self.tag_checks[i], int(i / self.grid_cols) + 1, i % self.grid_cols)

        self.tags_checks_grid_h_box = QHBoxLayout()
        self.tags_checks_grid_h_box.addLayout(self.tags_checks_grid_box)
        self.tags_checks_grid_h_box.addStretch()

        self.addLayout(self.tags_add_h_box)
        self.addLayout(self.tags_checks_grid_h_box)


    def addNewTag(self):
        new_tag = whitespace_reduce(self.tags_add_input.text())
        if new_tag not in self.main_setup.user_config.tmp_tags_list_counted and new_tag not in self.main_setup.user_config.tmp_all_tags_even_unused_of_session and new_tag != "":
            self.tags_add_input.setText("")
            old_size = len(self.main_setup.user_config.tmp_tags_list_counted)
            self.main_setup.user_config.add_new_tag(new_tag)

            new_checkbox = QCheckBox(new_tag)
            self.tag_checks.append(new_checkbox)
            self.tags_checks_grid_box.addWidget(self.tag_checks[old_size], int(old_size / self.grid_cols) + 1, old_size % self.grid_cols)

        if self.setEditedTrue is not None:
            new_checkbox.clicked.connect(self.setEditedTrue)




class UiShowExtraSettings(QVBoxLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.form = UiShowFormExtraSettings(self.main_setup)

        self.title_label = UiLabelTooltip("Extra Show Settings", "Some settings that are mainly separate from the 'Common Settings' by not being available in Profile Settings.", True)

        self.addWidget(self.title_label)
        self.addLayout(self.form)
        self.addStretch()


class UiShowFormExtraSettings(QFormLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.random_label = UiLabelTooltip("Random", "Randomize episode playback")
        self.random_box = UiFormRadioBinary(default=False)

        self.multipleEpisodes_label = UiLabelTooltip("Multiple Episodes", "Play this many episodes at once when this show is on. Default is 1.")
        self.multipleEpisodes_box = UiFormEnableSpin(1)

        self.probability_label = UiLabelTooltip("Probability", "Probability in % that this show will be played when it is randomly picked. Default is the value in the profile settings.")
        self.probability_box = UiFormEnableSpin(PROBABILITY_LIMIT_LOWER, PROBABILITY_LIMIT_UPPER, 100, fixed_width=60, add_string="%")

        self.noCommercialsAfter_label = UiLabelTooltip("No Cms After", "Disables commercials after this show.")
        self.noCommercialsAfter_box = UiFormRadioBinary(default=False)

        self.noCommercialsBefore_label = UiLabelTooltip("No Cms Before", "Disables commercials after this show.")
        self.noCommercialsBefore_box = UiFormRadioBinary(default=False)

        self.commercialWeight_label = UiLabelTooltip("Cm Weight", "Makes this show a commercial if it is enabled. Set the commercial weight of this show.")
        self.commercialWeight_box = UiFormEnableSpin(0, spin_default=2)

        self.commercialMax_label = UiLabelTooltip("Cm Max", "Set the upper limit of this commercial. Default is no Limit.")
        self.commercialMax_box = UiFormEnableSpin(1)

        self.commercialMin_label = UiLabelTooltip("Cm Min", "Set the lower limit of this commercial. Default is 0.")
        self.commercialMin_box = UiFormEnableSpin()

        self.videoTrack_label = UiLabelTooltip("Video Track", "Set the video track for this show. Default is usually 0.")
        self.videoTrack_box = UiFormEnableSpin()

        self.audioTrack_label = UiLabelTooltip("Audio Track", "Set the audio track for this show. Default is usually 0.")
        self.audioTrack_box = UiFormEnableSpin()

        self.subtitleTrack_label = UiLabelTooltip("Subtitle Track", "Set the subtitle track for this show. Default is usually 0.")
        self.subtitleTrack_box = UiFormEnableSpin()

        self.recursionLevel_label = UiLabelTooltip("Recursion Level", "Set how many levels of folders to look into in the folder that contains the media for this show. Default is 0.")
        self.recursionLevel_box = UiFormEnableSpin()


        self.addRow(self.random_label, self.random_box)
        self.addRow(self.multipleEpisodes_label, self.multipleEpisodes_box)
        self.addRow(self.probability_label, self.probability_box)
        self.addRow(self.noCommercialsAfter_label, self.noCommercialsAfter_box)
        self.addRow(self.noCommercialsBefore_label, self.noCommercialsBefore_box)
        self.addRow(self.commercialWeight_label, self.commercialWeight_box)
        self.addRow(self.commercialMax_label, self.commercialMax_box)
        self.addRow(self.commercialMin_label, self.commercialMin_box)
        self.addRow(self.videoTrack_label, self.videoTrack_box)
        self.addRow(self.audioTrack_label, self.audioTrack_box)
        self.addRow(self.subtitleTrack_label, self.subtitleTrack_box)
        self.addRow(self.recursionLevel_label, self.recursionLevel_box)


    def loadShowSettingsById(self, show_id: int):
        show = self.main_setup.get_show_by_id(show_id)
        settings = show.showSettings

        self.random_box.setRadio(settings.random)
        self.multipleEpisodes_box.setValue(settings.multipleEpisodes)
        self.probability_box.setValue(settings.probability)
        self.noCommercialsAfter_box.setRadio(settings.noCommercialsAfter)
        self.noCommercialsBefore_box.setRadio(settings.noCommercialsBefore)
        self.commercialWeight_box.setValue(settings.commercialWeight)
        self.commercialMax_box.setValue(settings.commercialMax)
        self.commercialMin_box.setValue(settings.commercialMin)
        self.videoTrack_box.setValue(settings.videoTrack)
        self.audioTrack_box.setValue(settings.audioTrack)
        self.subtitleTrack_box.setValue(settings.subtitleTrack)
        self.recursionLevel_box.setValue(settings.recursionLevel)





class UiShowProfileCommonSettings(QVBoxLayout):

    def __init__(
        self,
        is_show_settings: bool,
        main_setup: MainSetup,
        split_layout: bool = False
    ):
        super().__init__()

        self.main_setup = main_setup

        self.title_label = UiLabelTooltip("Common Settings", "Settings that are available for profile settings and show settings. Usually show settings override profile settings, when both are set. Depends on the property. Check more information by hovering the properties with your mouse.", True)
        self.addWidget(self.title_label)


        # create the form here
        self.opening_label = UiLabelTooltip("Opening", "Play an opening before every episode of tashow.\n\nThe opening file should be placed into the same directory as a show's episodes, named 'op' and then the file ending, like 'op.mp3'.\nYou can add a number, like 'op17.mp3', to set this as the opening for every episode from 17 and up. Use this to add different openings for different seasons or whatever.")
        self.opening_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.ending_label = UiLabelTooltip("Ending", "Similar to the 'Opening' setting. Play an ending after every episode of a show.\n\nThe ending file should be placed into the same directory as a show's episodes, named 'ed' and then the file ending, like 'ed.mkv'.\nYou can add a number, like 'ed17.mkv', to set this as the ending for every episode from 17 and up. Use this to add different endings for different seasons or whatever.")
        self.ending_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(default=None)

        self.volume_label = UiLabelTooltip("Volume", ("Set the volume for all episodes of this show." if is_show_settings else "Set the volume for all shows in this profile.") + "\nDoes not affect the openings and endings of a show.\nThe shows' volume (in percent) gets multiplied by the volume in the profile settings and the volume of the volume slider in the playback control.")
        self.volume_box = UiFormEnableSpin(VOLUME_LIMIT_LOWER, VOLUME_LIMIT_UPPER, 100, 60, "%")

        self.speed_label = UiLabelTooltip("Speed", ("Set the speed for all episodes of this show." if is_show_settings else "Set the speed for all shows in this profile.") + "\nDoes not affect the openings and endings of a show.\nThe shows' speed (in percent) gets multiplied by the speed in the profile settings.")
        self.speed_box = UiFormEnableSpin(SPEED_LIMIT_LOWER, SPEED_LIMIT_UPPER, 100, 60, "%")

        self.jump_label = UiLabelTooltip("Jump", "Jump to this many seconds of any episode at the start of the episode.\nNot if continuing a suspended episode.")
        self.jump_box = UiFormEnableSpin(fixed_width=60, add_string="sec")

        self.end_label = UiLabelTooltip("End", "End all episodes this many seconds before they are actually at their end.")
        self.end_box = UiFormEnableSpin(fixed_width=60, add_string="sec")

        self.pause_label = UiLabelTooltip("Pause", "Pause every episode immediately when playback starts.")
        self.pause_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(default=None)

        self.mute_label = UiLabelTooltip("Mute", "Mute every episode immediately when playback starts.")
        self.mute_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(default=None)

        self.fullscreen_label = UiLabelTooltip("Fullscreen", "Activate Fullscreen for every episode immediately when playback starts, if there is a video, not only audio.")
        self.fullscreen_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.suspendShow_label = UiLabelTooltip("Save Time", "Save where playback of an episode was interrupted and continue there the next time the show is played.")
        self.suspendShow_box = UiFormRadioBinary(default=True) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.suspendQueue_label = UiLabelTooltip("Save Queue", "Save the current queue when the session is stopped.")
        self.suspendQueue_box = UiFormRadioBinary(default=True) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.noRerun_label = UiLabelTooltip("No Reruns", "Deactivate a show when its last episode was played. Only works if show's playback is not random.")
        self.noRerun_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.debugPlayerJumps_label = UiLabelTooltip("Debug Jumps", "Experimental debugger that recognizes if the playback is corrupted.\nIf the player length changes, it will restart the playback and jump to a bit second after the vlc mediaplayer's player_length was changed")
        self.debugPlayerJumps_box = UiFormRadioBinary(default=True) if not is_show_settings else UiFormRadioTernary(
            default=None)

        # add form elements together
        self.form_all = QHBoxLayout()

        if split_layout:
            self.split_h_box = QHBoxLayout()

            self.form_left = QFormLayout()
            self.form_left.addRow(self.opening_label, self.opening_box)
            self.form_left.addRow(self.ending_label, self.ending_box)
            self.form_left.addRow(self.volume_label, self.volume_box)
            self.form_left.addRow(self.speed_label, self.speed_box)
            self.form_left.addRow(self.jump_label, self.jump_box)
            self.form_left.addRow(self.end_label, self.end_box)

            self.form_right = QFormLayout()
            self.form_right.addRow(self.pause_label, self.pause_box)
            self.form_right.addRow(self.mute_label, self.mute_box)
            self.form_right.addRow(self.fullscreen_label, self.fullscreen_box)
            self.form_right.addRow(self.suspendShow_label, self.suspendShow_box)
            self.form_right.addRow(self.suspendQueue_label, self.suspendQueue_box)
            self.form_right.addRow(self.noRerun_label, self.noRerun_box)
            self.form_right.addRow(self.debugPlayerJumps_label, self.debugPlayerJumps_box)

            self.split_h_box.addLayout(self.form_left)
            self.split_h_box.addLayout(self.form_right)

            self.form_all.addLayout(self.split_h_box)

        else:
            self.form_solo = QFormLayout()

            self.form_solo.addRow(self.opening_label, self.opening_box)
            self.form_solo.addRow(self.ending_label, self.ending_box)
            self.form_solo.addRow(self.volume_label, self.volume_box)
            self.form_solo.addRow(self.speed_label, self.speed_box)
            self.form_solo.addRow(self.jump_label, self.jump_box)
            self.form_solo.addRow(self.end_label, self.end_box)
            self.form_solo.addRow(self.pause_label, self.pause_box)
            self.form_solo.addRow(self.mute_label, self.mute_box)
            self.form_solo.addRow(self.fullscreen_label, self.fullscreen_box)
            self.form_solo.addRow(self.suspendShow_label, self.suspendShow_box)
            self.form_solo.addRow(self.suspendQueue_label, self.suspendQueue_box)
            self.form_solo.addRow(self.noRerun_label, self.noRerun_box)
            self.form_solo.addRow(self.debugPlayerJumps_label, self.debugPlayerJumps_box)

            self.form_all.addLayout(self.form_solo)


        # add all together
        self.addLayout(self.form_all)
        self.addStretch()


    def loadShowSettingsById(self, show_id: int):
        show = self.main_setup.get_show_by_id(show_id)
        settings = show.showSettings
        self.loadSettings(settings)

    def loadProfileSettingsById(self, profile_id: int):
        profile = self.main_setup.get_profile_by_id(profile_id)
        settings = profile.profileSettings
        self.loadSettings(settings)

    def loadSettings(self, settings: Union[ConfigUserShowSettings, ConfigUserProfileSettings]):
        self.opening_box.setRadio(settings.opening)
        self.ending_box.setRadio(settings.ending)
        self.volume_box.setValue(settings.volume)
        self.speed_box.setValue(settings.speed)
        self.jump_box.setValue(settings.jump)
        self.end_box.setValue(settings.end)
        self.pause_box.setRadio(settings.pause)
        self.mute_box.setRadio(settings.mute)
        self.fullscreen_box.setRadio(settings.fullscreen)
        self.suspendShow_box.setRadio(settings.suspend)
        self.suspendQueue_box.setRadio(settings.suspendQueue)
        self.noRerun_box.setRadio(settings.noRerun)
        self.debugPlayerJumps_box.setRadio(settings.debugPlayerJumps)



class UiEditProfilesButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new Profile.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected Profile.", True, True)
        self.duplicate = UiManagementToolButton(ICON_DUPLICATE, "Duplicate the current Profile.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.duplicate)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)




class UiProfileMainSettings(QFormLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.name_label = UiLabelTooltip("Name", "Give the profile a name.")
        self.name_input = QLineEdit()
        self.name_input.setMinimumWidth(350)
        self.name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.name_input.setValidator(self.name_validator)
        self.name_input.setPlaceholderText("Enter profile name")

        self.addRow(self.name_label, self.name_input)

    def loadProfileSettingsById(self, profile_id: int):
        profile = self.main_setup.get_profile_by_id(profile_id)
        self.name_input.setText(profile.profileName)




class UiProfileExtraSettings(QVBoxLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.form = UiProfileFormExtraSettings(self.main_setup)

        self.title_label = UiLabelTooltip("Profile Settings", "Some optional profile settings.", True)

        # add to checks here
        self.playAllShows_check = QCheckBox("Play All Shows")
        self.playAllShows_check.setToolTip("Play all available shows in this profile.\n\nIgnores settings like 'Exclude Profiles' and ignores which profiles are toggled in a show.")

        self.commercialsPlay_check = QCheckBox("Play Commercials")
        self.commercialsPlay_check.setToolTip("Play commercials if any are available.\n\nDeactivating this is the same as disabling the settings 'Cms Break Weight' and 'Cms Start Weight'.")
        self.commercialsPlay_check.setChecked(True)

        self.checks_h_box = QHBoxLayout()
        self.checks_h_box.addWidget(self.playAllShows_check)
        self.checks_h_box.addWidget(self.commercialsPlay_check)

        # combine all
        self.addWidget(self.title_label)
        self.addLayout(self.checks_h_box)
        self.addLayout(self.form)
        self.addStretch()


    def loadProfileSettingsById(self, profile_id: int):
        profile = self.main_setup.get_profile_by_id(profile_id)
        self.playAllShows_check.setChecked(profile.playAllShows if profile.playAllShows is not None else False)
        self.commercialsPlay_check.setChecked(profile.profileSettings.commercialsPlay if profile.profileSettings.commercialsPlay is not None else True)


class UiProfileFormExtraSettings(QFormLayout):

    def __init__(
        self,
        main_setup: MainSetup
    ):
        super().__init__()

        self.main_setup = main_setup

        self.profile_id = 0

        self.probability_label = UiLabelTooltip("Shows Probability", "Probability in % that any show will be played when it is randomly picked.\nUse this to set the baseline probability for every show to something else than 100 and override the value in single shows' settings.")
        self.probability_box = UiFormEnableSpin(PROBABILITY_LIMIT_LOWER, PROBABILITY_LIMIT_UPPER, 100, fixed_width=60, add_string="%")

        self.commercialsBreakWeight_label = UiLabelTooltip("Cms Break Weight", "Set the value for commercials that should be played between shows.\nIf enabled, then between shows commercials will be picked until the sum of their values is as close as possible to the weight set here.\n\nNo effect if there are no shows that have been set as commercials (do this by enabling a shows setting 'Cm Weight' and setting the commercials' weight).")
        self.commercialsBreakWeight_box = UiFormEnableSpin()

        self.commercialsStartWeight_label = UiLabelTooltip("Cms Start Weight", "Same as 'Cms Break Weight', but for commercials before the first show of a session is played.")
        self.commercialsStartWeight_box = UiFormEnableSpin()

        self.suspendedQueue_label = UiLabelTooltip("Suspended Queue", "Shows that were in the queue when the last session was stopped.\nThese will be loaded into the queue next time this profile is played.") # TODO only show if suspended queue exists
        self.suspendedQueue_list = UiObjectListSimple(False, (lambda x: x.showName))

        self.uniqueShows_label = UiLabelTooltip("Unique Shows", "Avoid playing the same show twice in a session.\nThis does not apply to commercials, only to queued shows.\nSession will end automatically if the strong version is picked once all shows are played once.")
        self.uniqueShows_radio = UiFormRadioNoYesHard()

        self.shutdown_label = UiLabelTooltip("Shutdown Timer", "Automatically stop a session after this many minutes.")
        self.shutdown_box = UiFormEnableSpin(fixed_width=60, add_string="min")

        self.showsLimit_label = UiLabelTooltip("Shows Limit", "Automatically stop a session after this many shows are played.\n\nCommercials are ignored for this counter. Shows that are played as part of the 'Start With Shows' setting are counted.")
        self.showsLimit_box = UiFormEnableSpin()


        self.profilesInclude_label = UiLabelTooltip("Include Profiles", "Include shows that are played in these profiles into the current profile.\nThis is applied before 'Exclude Profiles'.\nShows that are toggled to be active in all profiles are ignored by this.\n\nNo effect if 'Play All Shows' is toggled.")
        self.profilesInclude_list = UiObjectListSimple(True, (lambda x: x.profileName))

        self.profilesInclude_add_one = UiManagementToolButton(ICON_ADD_THIN, "Add one profile.", True)
        self.profilesInclude_add_one.clicked.connect(self.addProfilesInclude)
        self.profilesInclude_remove = UiManagementToolButton(ICON_REMOVE_THIN, "Remove selected profile.", True)
        self.profilesInclude_remove.clicked.connect(self.removeProfilesInclude)
        self.profilesInclude_buttons = QVBoxLayout()
        self.profilesInclude_buttons.setContentsMargins(0, 0, 0, 0)
        self.profilesInclude_buttons.addWidget(self.profilesInclude_add_one)
        self.profilesInclude_buttons.addWidget(self.profilesInclude_remove)

        self.profilesInclude_box = QHBoxLayout()
        self.profilesInclude_box.addLayout(self.profilesInclude_buttons)
        self.profilesInclude_box.addWidget(self.profilesInclude_list)


        self.profilesExclude_label = UiLabelTooltip("Exclude Profiles", "Exclude shows that are played in these profiles into the current profile.\nThis is applied after 'Include Profiles'.\nShows that are toggled to be active in all profiles are ignored by this.\n\nNo effect if 'Play All Shows' is toggled.")
        self.profilesExclude_list = UiObjectListSimple(True, (lambda x: x.profileName))

        self.profilesExclude_add_one = UiManagementToolButton(ICON_ADD_THIN, "Add one profile.", True)
        self.profilesExclude_add_one.clicked.connect(self.addProfilesExclude)
        self.profilesExclude_remove = UiManagementToolButton(ICON_REMOVE_THIN, "Remove selected profile.", True)
        self.profilesExclude_remove.clicked.connect(self.removeProfilesExclude)
        self.profilesExclude_buttons = QVBoxLayout()
        self.profilesExclude_buttons.setContentsMargins(0, 0, 0, 0)
        self.profilesExclude_buttons.addWidget(self.profilesExclude_add_one)
        self.profilesExclude_buttons.addWidget(self.profilesExclude_remove)

        self.profilesExclude_box = QHBoxLayout()
        self.profilesExclude_box.addLayout(self.profilesExclude_buttons)
        self.profilesExclude_box.addWidget(self.profilesExclude_list)


        self.startWithShows_label = UiLabelTooltip("Start With Shows", "Set Shows that will always play at the beginning of a session.\nIf there is a suspended queue, it will play after these shows here.\nWhen the currently playing queue is suspended and there are shows left from this option, they will be saved into the suspended queue and recognized on the next session start.")
        self.startWithShows_list = UiObjectListSimple(True, (lambda x: x.showName))

        self.startWithShows_add_one = UiManagementToolButton(ICON_ADD_THIN, "Add one show.", True)
        self.startWithShows_add_one.clicked.connect(self.addStartWithShows)
        self.startWithShows_remove = UiManagementToolButton(ICON_REMOVE_THIN, "Remove selected shows.", True)
        self.startWithShows_remove.clicked.connect(self.removeStartWithShows)

        self.startWithShows_buttons = QVBoxLayout()
        self.startWithShows_buttons.setContentsMargins(0, 0, 0, 0)
        self.startWithShows_buttons.addWidget(self.startWithShows_add_one)
        self.startWithShows_buttons.addWidget(self.startWithShows_remove)

        self.startWithShows_box = QHBoxLayout()
        self.startWithShows_box.addLayout(self.startWithShows_buttons)
        self.startWithShows_box.addWidget(self.startWithShows_list)

        # combine all into form layout
        self.addRow(self.probability_label, self.probability_box)
        self.addRow(self.commercialsBreakWeight_label, self.commercialsBreakWeight_box)
        self.addRow(self.commercialsStartWeight_label, self.commercialsStartWeight_box)
        self.addRow(self.suspendedQueue_label, self.suspendedQueue_list)
        self.addRow(self.uniqueShows_label, self.uniqueShows_radio)
        self.addRow(self.shutdown_label, self.shutdown_box)
        self.addRow(self.showsLimit_label, self.showsLimit_box)
        self.addRow(self.profilesInclude_label, self.profilesInclude_box)
        self.addRow(self.profilesExclude_label, self.profilesExclude_box)
        self.addRow(self.startWithShows_label, self.startWithShows_box)


    def loadProfileSettingsById(self, profile_id: int):
        self.profile_id = profile_id
        profile = self.main_setup.get_profile_by_id(profile_id)
        settings = profile.profileSettings

        suspendedQueue_objects = self.getSuspendedShows(profile_id)
        profilesInclude_objects = self.getProfilesInclude(profile_id)
        profilesExclude_objects = self.getProfilesExclude(profile_id)
        startWithShows_objects = self.getStartWithShows(profile_id)

        self.probability_box.setValue(settings.probability)
        self.commercialsBreakWeight_box.setValue(settings.commercialsBreakWeight)
        self.commercialsStartWeight_box.setValue(settings.commercialsStartWeight)
        self.suspendedQueue_list.updateListContents(suspendedQueue_objects)
        self.uniqueShows_radio.setRadio(settings.uniqueShows, settings.hardUniqueShows)
        self.shutdown_box.setValue(settings.shutdown)
        self.showsLimit_box.setValue(settings.showsLimit)
        self.profilesInclude_list.updateListContents(profilesInclude_objects)
        self.profilesExclude_list.updateListContents(profilesExclude_objects)
        self.startWithShows_list.updateListContents(startWithShows_objects)

        self.handleEnableProfilesIncludeButtons(profilesInclude_objects)
        self.handleEnableProfilesExcludeButtons(profilesExclude_objects)

        if len(self.main_setup.user_config.shows) == 0:
            self.startWithShows_add_one.setEnabled(False)
            self.startWithShows_remove.setEnabled(False)


    def getSuspendedShows(self, profile_id: int):
        return self.main_setup.user_config.get_suspended_shows_by_profile_id(profile_id)


    def handleEnableProfilesIncludeButtons(self, profilesInclude_objects: List[ConfigUserProfile] = None):
        all_other_profiles = list(p for p in self.main_setup.user_config.profiles if p.profileId != self.profile_id)
        if len(all_other_profiles) == 0:
            self.profilesInclude_add_one.setEnabled(False)
            self.profilesInclude_remove.setEnabled(False)
        else:
            profilesInclude_objects = self.profilesInclude_list.objects if profilesInclude_objects is None else profilesInclude_objects
            profiles_not_yet_picked = list(p for p in all_other_profiles if p not in profilesInclude_objects)
            self.profilesInclude_add_one.setEnabled(len(profiles_not_yet_picked) > 0)
            self.profilesInclude_remove.setEnabled(len(profilesInclude_objects) > 0)

    def handleEnableProfilesExcludeButtons(self, profilesExclude_objects: List[ConfigUserProfile] = None):
        all_other_profiles = list(p for p in self.main_setup.user_config.profiles if p.profileId != self.profile_id)
        if len(all_other_profiles) == 0:
            self.profilesExclude_add_one.setEnabled(False)
            self.profilesExclude_remove.setEnabled(False)
        else:
            profilesExclude_objects = self.profilesExclude_list.objects if profilesExclude_objects is None else profilesExclude_objects
            profiles_not_yet_picked = list(p for p in all_other_profiles if p not in profilesExclude_objects)
            self.profilesExclude_add_one.setEnabled(len(profiles_not_yet_picked) > 0)
            self.profilesExclude_remove.setEnabled(len(profilesExclude_objects) > 0)


    def getProfilesInclude(self, profile_id: int):
        return self.main_setup.user_config.get_profiles_by_ids(self.main_setup.user_config.get_profile_by_id(profile_id).profilesInclude)

    def addProfilesInclude(self):
        item, okPressed = QInputDialog.getItem(
            self.startWithShows_add_one,
            "Add Include Profile",
            "Add profile to include:",
            list(map(
                lambda x: x.profileName,
                (profile for profile in self.main_setup.user_config.profiles if (profile.profileId != self.profile_id and profile not in self.profilesInclude_list.objects))
            )),
            0,
            False
        )
        if okPressed and item:
            currently_loaded_profiles = self.profilesInclude_list.objects
            added_profile = self.main_setup.user_config.get_profile_by_name(item)
            if added_profile is not None:
                currently_loaded_profiles.append(added_profile)
                self.profilesInclude_list.updateListContents(currently_loaded_profiles)
            self.handleEnableProfilesIncludeButtons()

    def removeProfilesInclude(self):
        index = self.profilesInclude_list.currentRow()
        currently_loaded_profiles = self.profilesInclude_list.objects
        amount_profiles = len(currently_loaded_profiles)
        if amount_profiles > 0:
            index = amount_profiles - 1 if index == -1 else index
            currently_loaded_profiles.pop(index)
            self.profilesInclude_list.updateListContents(currently_loaded_profiles)
        self.handleEnableProfilesIncludeButtons()


    def getProfilesExclude(self, profile_id: int):
        return self.main_setup.user_config.get_profiles_by_ids(self.main_setup.user_config.get_profile_by_id(profile_id).profilesExclude)

    def addProfilesExclude(self):
        item, okPressed = QInputDialog.getItem(
            self.startWithShows_add_one,
            "Add Exclude Profile",
            "Add profile to exclude:",
            list(map(
                lambda x: x.profileName,
                (profile for profile in self.main_setup.user_config.profiles if (profile.profileId != self.profile_id and profile not in self.profilesExclude_list.objects))
            )),
            0,
            False
        )
        if okPressed and item:
            currently_loaded_profiles = self.profilesExclude_list.objects
            added_profile = self.main_setup.user_config.get_profile_by_name(item)
            if added_profile is not None:
                currently_loaded_profiles.append(added_profile)
                self.profilesExclude_list.updateListContents(currently_loaded_profiles)
            self.handleEnableProfilesExcludeButtons()

    def removeProfilesExclude(self):
        index = self.profilesExclude_list.currentRow()
        currently_loaded_profiles = self.profilesExclude_list.objects
        amount_profiles = len(currently_loaded_profiles)
        if amount_profiles > 0:
            index = amount_profiles - 1 if index == -1 else index
            currently_loaded_profiles.pop(index)
            self.profilesExclude_list.updateListContents(currently_loaded_profiles)
        self.handleEnableProfilesExcludeButtons()


    def getStartWithShows(self, profile_id: int):
        return self.main_setup.user_config.get_start_with_shows_by_profile_id(profile_id)

    def addStartWithShows(self):
        item, okPressed = QInputDialog.getItem(
            self.startWithShows_add_one,
            "Add Show",
            "Add to the end of the shows to start with:",
            list(map(
                lambda x: x.showName,
                (show for show in self.main_setup.user_config.shows if (show.isActive and show.get_is_show()))
            )),
            0,
            False
        )
        if okPressed and item:
            currently_loaded_shows = self.startWithShows_list.objects
            added_show = self.main_setup.user_config.get_show_by_name(item)
            if added_show is not None:
                currently_loaded_shows.append(added_show)
                self.startWithShows_list.updateListContents(currently_loaded_shows)

    def removeStartWithShows(self):
        index = self.startWithShows_list.currentRow()
        currently_loaded_shows = self.startWithShows_list.objects
        amount_shows = len(currently_loaded_shows)
        if amount_shows == 0:
            return
        index = amount_shows - 1 if index == -1 else index
        currently_loaded_shows.pop(index)
        self.startWithShows_list.updateListContents(currently_loaded_shows)






class UiObjectListSimple(QListWidget):

    def __init__(
        self,
        selectable,
        getObjectName
    ):
        super().__init__()

        self.getObjectName = getObjectName

        if selectable:
            self.setSelectionMode(QAbstractItemView.SingleSelection)
        else:
            self.setSelectionMode(QAbstractItemView.NoSelection)
        self.setAlternatingRowColors(True)
        self.setMaximumHeight(60)
        self.setMinimumWidth(200)

    def objectToListItemMapper(self, next_object: ConfigUserShow):
        next_entry = QListWidgetItem()
        next_entry.setText(self.getObjectName(next_object))
        return next_entry

    def updateListContents(self, objects: List):
        self.objects = objects

        index_remembered = self.currentRow()
        self.clear()

        for i in range(len(self.objects)):
            next_object = self.objects[i]
            self.addItem(self.objectToListItemMapper(next_object))

        sleep(0.07)
        self.setCurrentRow(index_remembered)





class UiLabelTooltip(QLabel):

    def __init__(
        self,
        name: str,
        tooltip: str = "",
        bold: bool = False
    ):
        super().__init__(name)
        self.setToolTip(tooltip)

        if bold:
            tmp_font = self.font()
            tmp_font.setBold(True)
            self.setFont(tmp_font)



class UiFormRadioBinary(QHBoxLayout):

    def __init__(
            self,
            tooltip_true: str = "",
            tooltip_false: str = "",
            default: bool = False,
    ):
        super().__init__()

        self.r_true = QRadioButton(TEXT_RADIO_BINARY_TRUE)
        self.r_true.setToolTip(tooltip_true)
        self.r_false = QRadioButton(TEXT_RADIO_BINARY_FALSE)
        self.r_false.setToolTip(tooltip_false)

        self.group = QButtonGroup()
        self.group.addButton(self.r_true)
        self.group.addButton(self.r_false)

        self.default = default
        if default is True:
            self.r_true.setChecked(True)
        else:
            self.r_false.setChecked(True)

        self.addWidget(self.r_true)
        self.addWidget(self.r_false)

        self.setAlignment(Qt.AlignLeft)

        self.all_radios = []
        self.all_radios.append(self.r_true)
        self.all_radios.append(self.r_false)

    def setRadio(self, boolean: bool):
        if boolean is None:
            if self.default is True:
                self.r_true.setChecked(True)
            else:
                self.r_false.setChecked(True)
        if boolean is True:
            self.r_true.setChecked(True)
        elif boolean is False:
            self.r_false.setChecked(True)

    def getRadioBool(self) -> bool:
        return self.r_true.isChecked()



class UiFormRadioTernary(QHBoxLayout):

    def __init__(
            self,
            tooltip_true: str = "",
            tooltip_false: str = "",
            tooltip_none: str = "Use this setting from profile settings.",
            default: bool = None
    ):
        super().__init__()

        self.r_true = QRadioButton(TEXT_RADIO_TERNARY_TRUE)
        self.r_true.setToolTip(tooltip_true)
        self.r_false = QRadioButton(TEXT_RADIO_TERNARY_FALSE)
        self.r_false.setToolTip(tooltip_false)
        self.r_none = QRadioButton(TEXT_RADIO_TERNARY_NONE)
        self.r_none.setToolTip(tooltip_none)

        tmp_font = self.r_none.font()
        tmp_font.setBold(True)
        self.r_none.setFont(tmp_font)

        self.group = QButtonGroup()
        self.group.addButton(self.r_true)
        self.group.addButton(self.r_false)
        self.group.addButton(self.r_none)

        self.default = default
        if default is True:
            self.r_true.setChecked(True)
        elif default is False:
            self.r_false.setChecked(True)
        else:
            self.r_none.setChecked(True)

        self.addWidget(self.r_true)
        self.addWidget(self.r_false)
        self.addWidget(self.r_none)

        self.setAlignment(Qt.AlignLeft)

        self.all_radios = []
        self.all_radios.append(self.r_true)
        self.all_radios.append(self.r_false)
        self.all_radios.append(self.r_none)

    def setRadio(self, boolean: bool):
        if boolean is None:
            if self.default is True:
                self.r_true.setChecked(True)
            elif self.default is False:
                self.r_false.setChecked(True)
            else:
                self.r_none.setChecked(True)
        if boolean is True:
            self.r_true.setChecked(True)
        elif boolean is False:
            self.r_false.setChecked(True)

    def getRadioBool(self) -> bool:
        if self.r_true.isChecked():
            return True
        elif self.r_none.isChecked():
            return None
        else:
            return False



class UiFormRadioNoYesHard(QHBoxLayout):

    def __init__(
            self,
            tooltip_hard: str = "",
            tooltip_true: str = "",
            tooltip_false: str = "",
            default: bool = False
    ):
        super().__init__()

        self.r_hard = QRadioButton(TEXT_RADIO_TERNARY_HARD)
        self.r_hard.setToolTip(tooltip_hard)
        self.r_true = QRadioButton(TEXT_RADIO_TERNARY_TRUE)
        self.r_true.setToolTip(tooltip_true)
        self.r_false = QRadioButton(TEXT_RADIO_TERNARY_FALSE)
        self.r_false.setToolTip(tooltip_false)

        self.group = QButtonGroup()
        self.group.addButton(self.r_hard)
        self.group.addButton(self.r_true)
        self.group.addButton(self.r_false)

        if default is True:
            self.r_true.setChecked(True)
        elif default is False:
            self.r_false.setChecked(True)
        else:
            self.r_hard.setChecked(True)

        self.addWidget(self.r_hard)
        self.addWidget(self.r_true)
        self.addWidget(self.r_false)

        self.setAlignment(Qt.AlignLeft)

        self.all_radios = []
        self.all_radios.append(self.r_true)
        self.all_radios.append(self.r_false)
        self.all_radios.append(self.r_hard)

    def setRadio(self, unique: bool, hard_unique: bool):
        if hard_unique is True:
            self.r_true.setChecked(False)
            self.r_false.setChecked(False)
            self.r_hard.setChecked(True)
        elif unique is True:
            self.r_true.setChecked(True)
            self.r_false.setChecked(False)
            self.r_hard.setChecked(False)
        else:
            self.r_true.setChecked(False)
            self.r_false.setChecked(True)
            self.r_hard.setChecked(False)

    def getRadioBool(self) -> bool:
        if self.r_true.isChecked():
            return True
        elif self.r_false.isChecked():
            return False
        else:
            return None



class UiFormEnableSpin(QHBoxLayout):

    def __init__(
            self,
            spin_minimum: int = 0,
            spin_maximum: int = 9999,
            spin_default: int = 0,
            fixed_width: int = 70,
            add_string: str = None,
    ):
        super().__init__()

        self.c_enable = QCheckBox()
        self.c_enable.clicked.connect(self.toggleEnabled)

        self.s_value = QSpinBox()
        self.s_value.setFixedWidth(fixed_width)
        self.s_value.setMinimum(spin_minimum)
        self.s_value.setMaximum(spin_maximum)
        self.s_value.setValue(spin_default)
        self.spin_default = spin_default
        self.s_value.setDisabled(True)

        self.addWidget(self.c_enable)
        self.addWidget(self.s_value)
        if add_string is not None:
            self.addWidget(QLabel(add_string))

        self.setAlignment(Qt.AlignLeft)


    def toggleEnabled(self):
        self.s_value.setDisabled(not self.c_enable.isChecked())

    def setValue(self, value: int):
        if value is None:
            self.c_enable.setChecked(False)
            self.s_value.setDisabled(True)
            self.s_value.setValue(self.spin_default)
        else:
            self.c_enable.setChecked(True)
            self.s_value.setDisabled(False)
            self.s_value.setValue(value)

    def getValueNoneable(self):
        if not self.c_enable.isChecked():
            return None
        else:
            return self.s_value.value()






class UiShowsEditDialog(QDialog):

    def __init__(
            self,
            main_setup: MainSetup,
            show_id_selected: int = 0
    ):
        super().__init__()

        self.main_setup = main_setup

        self.warned_hasFiles = False
        self.warned_isDir = False
        self.warned_duplicateName = False
        self.warned_duplicateDir = False
        self.edited_unsaved = False

        self.setWindowTitle("Edit Shows")
        self.setWindowModality(Qt.ApplicationModal)

        # form layout for the main settings
        self.main_settings = UiShowMainSettings(
            self.main_setup,
            False,
            (lambda: self.setShowEdited(True))
        )
        self.extra_settings = UiShowExtraSettings(self.main_setup)
        self.common_settings = UiShowProfileCommonSettings(True, self.main_setup)

        self.currently_selected_show_id = None

        # create stack layout
        self.shows_list = QListWidget()
        self.createListFonts()
        self.updateListContents()
        self.shows_list.setSelectionMode(QAbstractItemView.SingleSelection)
        if show_id_selected == -1:
            self.selectRow(0)
        else:
            self.selectRow(
                self.all_shows_objects.index(
                    list(filter(
                        lambda x: x.showId == show_id_selected,
                        self.all_shows_objects
                    ))[0]
                )
            )

        self.shows_list.setFixedWidth(250)
        self.shows_list.setAlternatingRowColors(True)
        self.shows_list.currentTextChanged.connect(self.handleShowsEditListSelected)

        self.shows_buttons = UiShowsEditButtons()
        self.shows_buttons.add_one.clicked.connect(self.addShowDialog)
        self.shows_buttons.remove.clicked.connect(self.removeShowDialog)
        self.shows_buttons.duplicate.clicked.connect(self.duplicateShowDialog)
        self.shows_buttons.sort.clicked.connect(self.handleShowSort)
        self.shows_buttons.sort_change.clicked.connect(self.handleShowSortChange)

        self.v_box_shows = QVBoxLayout()
        self.v_box_shows.addWidget(self.shows_list)
        self.v_box_shows.addLayout(self.shows_buttons)
        self.v_box_shows.setContentsMargins(0, 0, 0, 0)


        # add extra and common settings into an HBox
        self.settings_h_box = QHBoxLayout()
        self.settings_h_box.addLayout(self.main_settings)
        self.settings_h_box.addStretch()
        self.settings_h_box.addLayout(self.extra_settings)
        self.settings_h_box.addStretch()
        self.settings_h_box.addLayout(self.common_settings)
        self.settings_h_box.addStretch()


        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save,
            Qt.Horizontal
        )
        self.button_box.accepted.connect(self.handleSaveShow)


        # combine settings and buttons
        self.v_box_settings_all = QVBoxLayout()
        # self.v_box_settings_all.addLayout(self.main_settings)
        self.v_box_settings_all.addLayout(self.settings_h_box)
        self.v_box_settings_all.addWidget(self.button_box)


        # combine all in stack-like layout
        self.layout_all = QHBoxLayout()
        self.layout_all.addLayout(self.v_box_shows)
        self.layout_all.addStretch()
        self.layout_all.addLayout(self.v_box_settings_all)
        self.layout_all.addStretch()

        self.setLayout(self.layout_all)

        updateListLoopThread = Thread(target=self.updateListLoop, daemon=True)
        updateListLoopThread.start()


        # make the cancel button appear
        self.main_settings.form.name_input.textChanged.connect(self.refreshWarningsName)
        self.main_settings.form.active_check.clicked.connect(lambda: self.setShowEdited(True))
        self.main_settings.form.no_basepath.clicked.connect(self.refreshWarningsBasepath)
        self.main_settings.form.path_box.path_input.textChanged.connect(self.refreshWarningsBasepath)
        self.main_settings.form.profiles_box.profiles_check_all.clicked.connect(lambda: self.setShowEdited(True))
        for pcheck in self.main_settings.form.profiles_box.profile_checks:
            pcheck.clicked.connect(lambda: self.setShowEdited(True))
        self.main_settings.form.episode_current_input.valueChanged.connect(lambda: self.setShowEdited(True))
        self.main_settings.form.episode_first_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.main_settings.form.episode_first_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.main_settings.form.episode_last_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.main_settings.form.episode_last_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        for tcheck in self.main_settings.form.tags_box.tag_checks:
            tcheck.clicked.connect(lambda: self.setShowEdited(True))

        for rad in self.extra_settings.form.random_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.multipleEpisodes_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.multipleEpisodes_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.probability_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.probability_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        for rad in self.extra_settings.form.noCommercialsAfter_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        for rad in self.extra_settings.form.noCommercialsBefore_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.commercialWeight_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.commercialWeight_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.commercialMax_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.commercialMax_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.commercialMin_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.commercialMin_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.videoTrack_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.videoTrack_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.audioTrack_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.audioTrack_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.subtitleTrack_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.subtitleTrack_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.recursionLevel_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.extra_settings.form.recursionLevel_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))

        for rad in self.common_settings.opening_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        for rad in self.common_settings.ending_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        # self.common_settings.
        self.common_settings.volume_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.common_settings.volume_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.common_settings.speed_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.common_settings.speed_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.common_settings.jump_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.common_settings.jump_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        self.common_settings.end_box.c_enable.clicked.connect(lambda: self.setShowEdited(True))
        self.common_settings.end_box.s_value.valueChanged.connect(lambda: self.setShowEdited(True))
        for rad in self.common_settings.pause_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        for rad in self.common_settings.mute_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        for rad in self.common_settings.fullscreen_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        # self.common_settings.
        for rad in self.common_settings.suspendShow_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        for rad in self.common_settings.suspendQueue_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        for rad in self.common_settings.noRerun_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))
        # self.common_settings.,
        # self.common_settings.,
        for rad in self.common_settings.debugPlayerJumps_box.all_radios:
            rad.clicked.connect(lambda: self.setShowEdited(True))


    def updateListLoop(self):
        while (True):
            gui_event_shows_cms_edit_list_update_list.wait()
            gui_event_shows_cms_edit_list_update_list.clear()
            self.updateListContents()


    def objectToListItemMapper(self, next_show: ConfigUserShow, active_ids):
        next_entry = QListWidgetItem()
        next_entry.setText(next_show.showName)
        decide_active_cm = (
            next_show.showId in active_ids,
            next_show.get_is_commercial()
        )
        if decide_active_cm == (True, True):
            next_entry.setFont(self.font_bold_italic)
        elif decide_active_cm == (False, True):
            next_entry.setFont(self.font_italic)
        elif decide_active_cm == (True, False):
            next_entry.setFont(self.font_bold)

        return next_entry


    def updateListContents(self):
        self.active_shows_cms_ids = self.main_setup.get_all_active_show_and_cm_ids_for_current_profile()
        self.all_shows_objects: List[ConfigUserShow] = self.main_setup.user_config.shows

        sorting = SortingState.fromValue(self.main_setup.global_config.windowSettings.sortShowsCmsEdit)
        ascending = self.main_setup.global_config.windowSettings.sortShowsCmsEditAscending
        ascending = True if ascending is None else ascending

        self.main_setup.user_config.load_all_tags()
        all_tags_counted = self.main_setup.user_config.tmp_tags_list_counted

        self.all_shows_objects = sortShowObjects(self.all_shows_objects, sorting, ascending, all_tags_counted, self.main_setup.global_config)

        index_remembered = self.shows_list.currentRow()
        self.shows_list.clear()

        for i in range(len(self.all_shows_objects)):
            next_show = self.all_shows_objects[i]
            self.shows_list.addItem(self.objectToListItemMapper(next_show, self.active_shows_cms_ids))

        new_list_size = len(self.shows_list)
        if new_list_size == 0:
            self.hide()
        else:
            sleep(0.07)
            self.shows_list.setCurrentRow(min(index_remembered, new_list_size - 1))



    def createListFonts(self):
        bold_entry = QListWidgetItem()
        self.font_bold = bold_entry.font()
        self.font_bold.setBold(True)

        italic_entry = QListWidgetItem()
        self.font_italic = italic_entry.font()
        self.font_italic.setItalic(True)

        bold_italic_entry = QListWidgetItem()
        self.font_bold_italic = bold_italic_entry.font()
        self.font_bold_italic.setBold(True)
        self.font_bold_italic.setItalic(True)


    def handleReloadShow(self):
        self.main_settings.form.loadShowSettingsById(self.currently_selected_show_id)
        self.extra_settings.form.loadShowSettingsById(self.currently_selected_show_id)
        self.common_settings.loadShowSettingsById(self.currently_selected_show_id)
        self.setShowEdited(False)
        # TODO warn if not saved


    def selectRow(self, index: int):
        self.shows_list.setCurrentRow(index)
        self.currently_selected_show_id = self.all_shows_objects[index].showId
        self.handleReloadShow()
        if hasattr(self, "button_box"):
            self.setShowEdited(False)
        print("selectRow " + str(index))
        # TODO warn if not saved


    def handleShowsEditListSelected(self):
        """Switch the settings to the selected show. Warn if not saved.
        """
        if len(self.shows_list) == 0:
            return

        self.selectRow(self.shows_list.currentIndex().row())


    def setShowEdited(self, edited: bool = True):
        if self.edited_unsaved == edited:
            return
        self.edited_unsaved = edited

        if self.edited_unsaved:
            self.button_box.hide()
            self.button_box = QDialogButtonBox(
                QDialogButtonBox.Save
                | QDialogButtonBox.Cancel,
                Qt.Horizontal
            )
            self.button_box.accepted.connect(self.handleSaveShow)
            self.button_box.rejected.connect(self.handleReloadShow)
            self.v_box_settings_all.addWidget(self.button_box)
        else:
            self.button_box.hide()
            self.button_box = QDialogButtonBox(
                QDialogButtonBox.Save,
                Qt.Horizontal
            )
            self.button_box.accepted.connect(self.handleSaveShow)
            self.v_box_settings_all.addWidget(self.button_box)


    def refreshWarningsBasepath(self):
        self.warned_isDir = False
        self.warned_hasFiles = False
        self.warned_duplicateDir = False
        self.setShowEdited(True)


    def refreshWarningsName(self):
        self.warned_duplicateName = False
        self.setShowEdited(True)


    def handleSaveShow(self):
        """Save show with the given attributes. Check validity before saving
        """
        name_state, name_text, name_pos = self.main_settings.form.name_validator.validate(self.main_settings.form.name_input.text(), 0)
        # print("Name input regex: " + str(name_state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable. name_text: " + name_text + ", name_pos: " + str(name_pos))
        if name_state != 2:
            showWarningDialog("Name Missing", "Enter A Name For The Show", "Every show needs a name. Enter something in the name input field and try again.")
            return

        path_state, path_text, path_pos = self.main_settings.form.path_box.path_validator.validate(self.main_settings.form.path_box.path_input.text(), 0)
        # print("Name input regex: " + str(path_state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable. path_text: " + path_text + ", path_pos: " + str(pos))
        if path_state != 2:
            showWarningDialog("Path Missing", "Choose A Directory For This Show's Path", "Use the 'Search' Button to find the directory with this show's files.")
            return

        use_base_path = self.main_setup.global_config.useBasePaths and not self.main_settings.form.no_basepath.isChecked()

        if use_base_path:
            complete_path = convert_path_slashes(os.path.join(self.main_settings.form.current_base_path, path_text))
        else:
            complete_path = convert_path_slashes(path_text)

        newName = whitespace_reduce(name_text)

        # check if directory exists (and contains files)
        if os.path.isdir(complete_path):
            recursion_level_input = self.extra_settings.form.recursionLevel_box.getValueNoneable()
            if not (
                self.warned_hasFiles or len(
                    get_files_from_directory(
                        complete_path,
                        recursionLevel=(recursion_level_input if recursion_level_input is not None else 0))
                ) > 0
            ):
                self.warned_hasFiles = True
                showWarningDialog("No Files Found", "Show Without Files?", "There seem to be no files in the directory for this show.\nYou can still save this show, if you want. Just try again.")
                return
        elif not self.warned_isDir:
            self.warned_isDir = True
            showWarningDialog("Directory Not Found", "Show Without Directory?", "The path specified for this show's files does not seem to be valid, directory cannot be found.\nYou can still save this show, if you want. Just try again.")
            return

        all_but_current_show = list(filter(lambda s: s.showId != self.currently_selected_show_id, self.main_setup.user_config.shows))

        if not self.warned_duplicateName and newName in list(map(lambda s: s.showName, all_but_current_show)):
            self.warned_duplicateName = True
            showWarningDialog("Duplicate Show Name", "Name Already In Use", "The name you specified for this show is already in use.\nYou can still save this show, if you want. Just try again.")
            return

        if not self.warned_duplicateDir and path_text in list(map(lambda s: s.directoryPath, all_but_current_show)):
            self.warned_duplicateDir = True
            index_dir_dup = list(map(lambda s: s.directoryPath, all_but_current_show)).index(path_text)
            show_name_dir_dup = all_but_current_show[index_dir_dup].showName
            showWarningDialog("Duplicate Show Path", "Directory Path Already In Use", f"The directory path you specified for this show is already in use for the show '{show_name_dir_dup}'.\nYou can still save this show, if you want. Just try again.")
            return


        save_show = self.main_setup.get_show_by_id(self.currently_selected_show_id)

        # main settings
        save_show.showName = newName
        save_show.isActive = self.main_settings.form.active_check.isChecked()
        save_show.doNotUseBasePath = self.main_settings.form.no_basepath.isChecked()
        save_show.directoryPath = path_text
        save_show.activeProfiles = self.main_settings.form.get_profiles_ids()
        save_show.firstEpisode = self.main_settings.form.episode_first_box.getValueNoneable()
        save_show.lastEpisode = self.main_settings.form.episode_last_box.getValueNoneable()
        save_show.currentEpisode = min(
            save_show.lastEpisode if save_show.lastEpisode is not None else save_show.lastEpisode_tmp if save_show.lastEpisode_tmp is not None else 99999,
            max(
                save_show.firstEpisode if save_show.firstEpisode is not None else save_show.firstEpisode_tmp if save_show.firstEpisode_tmp is not None else 1,
                self.main_settings.form.episode_current_input.value()
            )
        )
        save_show.tags = self.main_settings.form.get_tags()

        save_show.generate_first_and_last_episode_tmp_ints(self.main_setup.global_config, True)

        # extra and common settings
        save_show.showSettings = self.get_show_ec_settings()

        save_current_user_config_complete(self.main_setup)
        print("saved show")

        self.setShowEdited(False)
        self.updateListContents()
        updatedShowUpdateGui()


    def get_show_ec_settings(self) -> ConfigUserShowSettings:
        return ConfigUserShowSettings(
            self.extra_settings.form.random_box.getRadioBool(),
            self.extra_settings.form.multipleEpisodes_box.getValueNoneable(),
            self.extra_settings.form.probability_box.getValueNoneable(),
            self.extra_settings.form.noCommercialsAfter_box.getRadioBool(),
            self.extra_settings.form.noCommercialsBefore_box.getRadioBool(),
            self.extra_settings.form.commercialWeight_box.getValueNoneable(),
            self.extra_settings.form.commercialMax_box.getValueNoneable(),
            self.extra_settings.form.commercialMin_box.getValueNoneable(),
            self.extra_settings.form.videoTrack_box.getValueNoneable(),
            self.extra_settings.form.audioTrack_box.getValueNoneable(),
            self.extra_settings.form.subtitleTrack_box.getValueNoneable(),
            self.extra_settings.form.recursionLevel_box.getValueNoneable(),

            self.common_settings.opening_box.getRadioBool(),
            self.common_settings.ending_box.getRadioBool(),
            # self.common_settings.,
            self.common_settings.volume_box.getValueNoneable(),
            self.common_settings.speed_box.getValueNoneable(),
            self.common_settings.jump_box.getValueNoneable(),
            self.common_settings.end_box.getValueNoneable(),
            self.common_settings.pause_box.getRadioBool(),
            self.common_settings.mute_box.getRadioBool(),
            self.common_settings.fullscreen_box.getRadioBool(),
            # self.common_settings.,
            self.common_settings.suspendShow_box.getRadioBool(),
            self.common_settings.suspendQueue_box.getRadioBool(),
            self.common_settings.noRerun_box.getRadioBool(),
            # self.common_settings.,
            # self.common_settings.,
            self.common_settings.debugPlayerJumps_box.getRadioBool(),
        )


    def addShowDialog(self):
        self.dialog_new_show = UiShowsNewDialog(self.main_setup)
        self.dialog_new_show.show()


    def removeShowDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete show '" + self.shows_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText("This action cannot be undone. This show will be deleted from all profiles.")
        msg.setWindowTitle("Delete Show")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeShowConfirmed)
        msg.exec_()

    def removeShowConfirmed(self):
        selected_index = self.shows_list.currentIndex().row()
        show_to_delete = self.all_shows_objects[selected_index]

        self.main_setup.delete_show(show_to_delete)
        if not self.main_setup.global_config.noAutosave:
            save_current_user_config_complete(self.main_setup)

        print("deleted " + show_to_delete.showName)


    def duplicateShowDialog(self):
        selected_index = self.shows_list.currentIndex().row()
        show_to_duplicate = self.all_shows_objects[selected_index]

        text, okPressed = QInputDialog.getText(self, "Duplicate Show", "Name of copy of '" + show_to_duplicate.showName + "':", QLineEdit.Normal, "")
        if okPressed:
            self.main_setup.user_config.duplicate_show(show_to_duplicate, whitespace_reduce(text))
            save_current_user_config_complete(self.main_setup)

            print("duplicated " + show_to_duplicate.showName)


    def handleShowSort(self):
        if len(self.all_shows_objects) == 0:
            showWarningDialog("There Are No Shows", "There Don't Seem To Be Any Shows", "You don't have any shows saved. Add a new show before trying to sort existing shows.")
            return

        item, okPressed = QInputDialog.getItem(
            self.shows_buttons.sort,
            "Sort Shows",
            "Sort by:",
            list(map(SortingState.toString, SortingState.getValues(True))),
            (0 if self.main_setup.global_config.windowSettings.sortShowsCmsEdit is None else SortingState.getValues().index(self.main_setup.global_config.windowSettings.sortShowsCmsEdit)),
            False
        )
        if okPressed and item:
            self.main_setup.global_config.windowSettings.sortShowsCmsEdit = SortingState.fromString(item)
            self.updateListContents()


    def handleShowSortChange(self):
        current_ascending = self.main_setup.global_config.windowSettings.sortShowsCmsEditAscending
        current_ascending = True if current_ascending is None else current_ascending
        self.main_setup.global_config.windowSettings.sortShowsCmsEditAscending = not current_ascending
        self.updateListContents()





class UiShowsEditButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new User.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected User.", True, True)
        self.duplicate = UiManagementToolButton(ICON_DUPLICATE, "Duplicate the current User.", True, True)
        self.sort = UiManagementToolButton(ICON_SORT_THIN, "Select sort mode.", True, True)
        self.sort_change = UiManagementToolButton(ICON_SORT_CHANGE_THIN, "Toggle between ascending and descending sorting.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.duplicate)
        self.addWidget(self.sort)
        self.addWidget(self.sort_change)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)





class UiPlaybackControlVBox(QVBoxLayout):

    def __init__(
            self,
            window_style: QStyle,
            mediaplayer: vlc.MediaPlayer,
            main_setup: MainSetup,
            equalizer: EqualizerBar,
            # open_file_dialog,
            play_start_gui,
            timer_equalizer_start,
            timer_equalizer_stop,
            toggle_media_area_state,
            set_media_area_state,
            get_media_area_state,
            handle_media_area_state,
            showTextOnMedia,
            toggleManagementTopRowEnabled,
            loadMainSetupUserCurrent,
            check_upcoming_show_playable
    ):
        super().__init__()

        self.mediaplayer = mediaplayer
        self.main_setup = main_setup
        self.equalizer = equalizer
        self.play_start_gui = play_start_gui
        self.timer_equalizer_start = timer_equalizer_start
        self.timer_equalizer_stop = timer_equalizer_stop
        self.toggle_media_area_state = toggle_media_area_state
        self.set_media_area_state = set_media_area_state
        self.get_media_area_state = get_media_area_state
        self.handle_media_area_state = handle_media_area_state
        self.showTextOnMedia = showTextOnMedia
        self.toggleManagementTopRowEnabled = toggleManagementTopRowEnabled
        self.loadMainSetupUserCurrent = loadMainSetupUserCurrent
        self.check_upcoming_show_playable = check_upcoming_show_playable


        # create playback timer
        self.timer_playback = QTimer()
        self.timer_playback.setInterval(TIMER_PLAYBACK_UPDATE_MS)
        self.currently_playing_show_queue_id = None
        self.timer_playback.timeout.connect(self.updatePlaybackUI)


        # time label and slider layout
        self.time_progress_label = QLabel("00:00")
        self.time_progress_label.setStatusTip("Playback time.")

        self.time_progress_slider = QSlider(Qt.Horizontal)
        self.time_progress_slider.setStatusTip("Playback position.")
        self.time_progress_slider.setMaximum(10000)
        self.time_progress_slider.setFixedHeight(30)
        self.time_progress_slider.sliderPressed.connect(self.handleTimesliderPressed)
        self.time_progress_slider.sliderMoved.connect(self.handleTimesliderMoved)
        self.time_progress_slider.sliderReleased.connect(self.handleTimesliderReleased)

        self.time_progress_slider_moving = False
        self.time_progress_slider_new_position = False

        self.h_box_time = QHBoxLayout()
        self.h_box_time.addWidget(self.time_progress_label)
        self.h_box_time.setAlignment(self.time_progress_label, Qt.AlignCenter)
        self.h_box_time.addWidget(self.time_progress_slider)
        self.h_box_time.setContentsMargins(5, 0, 0, 0)


        self.show_extra_lines = False # TODO read from global-config

        # standard (first) line
        self.play_pause_button = QToolButton()
        self.play_pause_button.setIcon(QIcon(ICON_PLAY_START))
        self.play_pause_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.play_pause_button.setStatusTip("Start the current session.")
        self.play_pause_button.clicked.connect(self.handlePlayPausePlayback)
        self.play_pause_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.stop_button = QToolButton()
        self.stop_button.setPopupMode(QToolButton.DelayedPopup)
        self.stop_button.setIcon(QIcon(ICON_STOP))
        self.stop_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.stop_button.setStatusTip("Stop session. Hold Left Click to choose from other options.")
        self.stop_button.clicked.connect(self.handleStopPlayback)
        self.stop_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE + 8, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.menu_stop_options = UiMenuStopOptions()
        self.stop_button.setMenu(self.menu_stop_options.menu)


        self.skip_button = QToolButton()
        self.skip_button.setPopupMode(QToolButton.DelayedPopup)
        self.skip_button.setIcon(QIcon(ICON_SKIP))
        self.skip_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.skip_button.setStatusTip("Skip current media. Hold Left Click to choose from other options.")
        self.skip_button.clicked.connect(self.handleSkipPlayback)
        self.skip_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE + 8, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.menu_skip_options = UiMenuSkipOptions()
        self.skip_button.setMenu(self.menu_skip_options.menu)
        self.menu_skip_options.menu_skip.triggered.connect(self.handleSkipPlayback)
        self.menu_skip_options.menu_skip_to_cms.triggered.connect(self.handleSkipToCommercials)
        self.menu_skip_options.menu_skip_all.triggered.connect(self.handleSkipHardPlayback)


        self.jump_back_button = QToolButton()
        self.jump_back_button.setIcon(QIcon(ICON_BACKWARD))
        self.jump_back_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.jump_back_button.setStatusTip("Jump back 5 seconds.") #TODO: adjust seconds amount
        self.jump_back_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)
        self.jump_back_button.clicked.connect(self.handleBackPlayback)

        self.jump_forward_button = QToolButton()
        self.jump_forward_button.setIcon(QIcon(ICON_FORWARD))
        self.jump_forward_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.jump_forward_button.setStatusTip("Jump forward 5 seconds.") #TODO: adjust seconds amount
        self.jump_forward_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)
        self.jump_forward_button.clicked.connect(self.handleForwardPlayback)


        self.mute_button = QToolButton()
        self.mute_button.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.mute_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.mute_button.setStatusTip("Toggle mute.")
        self.mute_button.setCheckable(True)
        self.mute_button.clicked.connect(self.handleMutePlayback)
        self.mute_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.volume_label = QLabel("100")
        self.volume_label.setContentsMargins(0, 0, 0, 0)
        # self.volume_label.setFont(QFont("Arial", 5))

        self.volume_slider = QSlider(Qt.Horizontal)
        self.volume_slider.setMaximum(VOLUME_LIMIT_UPPER)
        self.volume_slider.setValue(100)
        self.volume_slider.setStatusTip("Volume")
        self.volume_slider.setContentsMargins(0, 0, 0, 0)
        self.volume_slider.valueChanged.connect(self.handleSetVolume)
        self.volume_slider.setMinimumWidth(75)
        self.volume_slider.setMaximumWidth(230)

        self.volume_slider_show_dynamic = QSlider(Qt.Horizontal)
        self.volume_slider_show_dynamic.setMaximum(VOLUME_LIMIT_UPPER)
        self.volume_slider_show_dynamic.setValue(100)
        self.volume_slider_show_dynamic.setStatusTip("Dynamic Show Volume")
        self.volume_slider_show_dynamic.setContentsMargins(0, 0, 0, 0)
        self.volume_slider_show_dynamic.valueChanged.connect(self.handleSetVolume)
        self.volume_slider_show_dynamic.setMinimumWidth(75)
        self.volume_slider_show_dynamic.setMaximumWidth(230)

        self.v_box_volume = QVBoxLayout()
        self.v_box_volume.addWidget(self.volume_label)
        self.v_box_volume.setAlignment(self.volume_label, Qt.AlignCenter)
        self.v_box_volume.addWidget(self.volume_slider)
        # self.v_box_volume.addWidget(self.volume_slider_show_dynamic) # TODO CONTINUE add if statement for dynamic settings

        self.fullscreen_button = QPushButton()
        self.fullscreen_button.setIcon(QIcon(ICON_FULLSCREEN))
        self.fullscreen_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.fullscreen_button.setStatusTip("Toggle full screen.")
        self.fullscreen_button.setCheckable(True)
        self.fullscreen_button.clicked.connect(self.handle_toggle_fullscreen)
        self.fullscreen_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.toggle_media_area_button = QToolButton()
        self.toggle_media_area_button.setArrowType(Qt.UpArrow)
        self.toggle_media_area_button.setPopupMode(QToolButton.DelayedPopup)
        self.menu_media_area_states = UiMenuUiMediaAreaStates(self.toggle_media_area_button, self.set_media_area_state, self.get_media_area_state)
        self.toggle_media_area_button.setMenu(self.menu_media_area_states.menu)
        self.toggle_media_area_button.setStatusTip("Toggle media area. Hold Left Click to choose state.")
        # self.toggle_media_area_button.clicked.connect(self.toggle_media_area_state())
        self.toggle_media_area_button.clicked.connect(lambda: self.menu_media_area_states.selectedOption(UiMediaAreaState.next_value(self.get_media_area_state()))) # takes care of checking the right one in the popup, else self.toggle_media_area_state() would be enough
        self.toggle_media_area_button.setFixedSize(int(BUTTON_PLAYBACK_SQUARE_SIZE / 2), int(BUTTON_PLAYBACK_SQUARE_SIZE / 2) - 3)

        self.toggle_extras_button = QToolButton()
        self.toggle_extras_button.setArrowType(Qt.DownArrow)
        self.toggle_extras_button.setStatusTip("Toggle extra buttons.")
        self.toggle_extras_button.setCheckable(True)
        self.toggle_extras_button.clicked.connect(self.handleToggleExtraButtons)
        self.toggle_extras_button.setFixedSize(int(BUTTON_PLAYBACK_SQUARE_SIZE / 2), int(BUTTON_PLAYBACK_SQUARE_SIZE / 2) - 3)

        self.v_box_arrow_buttons = QVBoxLayout()
        self.v_box_arrow_buttons.addWidget(self.toggle_media_area_button)
        self.v_box_arrow_buttons.addWidget(self.toggle_extras_button)


        # extra (second, usually hidden) line

        self.open_dir_button = QToolButton()
        self.open_dir_button.setIcon(QIcon(ICON_FOLDER))
        self.open_dir_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.open_dir_button.setStatusTip("Open folder containing the currently played media.")
        self.open_dir_button.clicked.connect(self.handleOpenDirCurrentMedia)
        self.open_dir_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.loop_button = QToolButton()
        self.loop_button.setIcon(QIcon(ICON_LOOP))
        self.loop_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.loop_button.setStatusTip("Loop current media.")
        self.loop_button.setCheckable(True)
        self.loop_button.clicked.connect(self.handleLoopPlayback)
        self.loop_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.timer_button = QToolButton()
        self.timer_button.setIcon(QIcon(ICON_TIMER))
        self.timer_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.timer_button.setStatusTip("Set turn off countdown.")
        self.timer_button.clicked.connect(self.handleShutdownEpisodesLimitDialog)
        self.timer_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.faster_button = QToolButton()
        self.faster_button.setIcon(QIcon(ICON_FASTER))
        self.faster_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.faster_button.setStatusTip("Speed up playback.")
        self.faster_button.clicked.connect(self.handleFasterPlayback)
        self.faster_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.slower_button = QToolButton()
        self.slower_button.setIcon(QIcon(ICON_SLOWER))
        self.slower_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.slower_button.setStatusTip("Slow down playback.")
        self.slower_button.clicked.connect(self.handleSlowerPlayback)
        self.slower_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        # combine all extra buttons
        self.h_box_playback_buttons_extra = QHBoxLayout()
        self.h_box_playback_buttons_extra.addWidget(self.open_dir_button)
        self.h_box_playback_buttons_extra.addWidget(self.loop_button)
        self.h_box_playback_buttons_extra.addWidget(self.timer_button)
        self.h_box_playback_buttons_extra.addWidget(self.faster_button)
        self.h_box_playback_buttons_extra.addWidget(self.slower_button)
        self.h_box_playback_buttons_extra.addStretch()
        self.h_box_playback_buttons_extra.setContentsMargins(0, 0, 0, 0)


        # put extra buttons in a frame that can be easily hidden
        self.h_box_playback_buttons_extra_frame_side = QFrame()
        self.h_box_playback_buttons_extra_frame_side.setLayout(self.h_box_playback_buttons_extra)
        # self.h_box_playback_buttons_extra_frame_side.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum) #FIXME: i have no idea why this works (not necessary anymore after alignment was set to bottom
        self.h_box_playback_buttons_extra_frame_side.setHidden(True)

        # disable some buttons until media playback starts
        self.handlePlaybackControlsEnabled(False)


        # combine all standard buttons
        self.h_box_playback_buttons_main = QHBoxLayout()
        self.h_box_playback_buttons_main.addWidget(self.play_pause_button)
        self.h_box_playback_buttons_main.addWidget(self.stop_button)
        self.h_box_playback_buttons_main.addWidget(self.skip_button)
        self.h_box_playback_buttons_main.addWidget(self.jump_back_button)
        self.h_box_playback_buttons_main.addWidget(self.jump_forward_button)
        self.h_box_playback_buttons_main.addStretch()
        self.h_box_playback_buttons_main.addWidget(self.h_box_playback_buttons_extra_frame_side)
        self.h_box_playback_buttons_main.addStretch()
        self.h_box_playback_buttons_main.addWidget(self.mute_button)
        self.h_box_playback_buttons_main.addLayout(self.v_box_volume)
        self.h_box_playback_buttons_main.addWidget(self.fullscreen_button)
        self.h_box_playback_buttons_main.addLayout(self.v_box_arrow_buttons)


        # combine both lines
        self.addLayout(self.h_box_time)
        self.addLayout(self.h_box_playback_buttons_main)
        # self.addWidget(self.h_box_playback_buttons_extra_frame_bottom)
        self.setContentsMargins(10, 0, 10, 0)



    def handlePlaybackControlsEnabled(self, enabled: bool): # TODO make sure all buttons are here
        self.time_progress_slider.setEnabled(enabled)

        self.stop_button.setEnabled(enabled)
        self.skip_button.setEnabled(enabled)
        self.jump_back_button.setEnabled(enabled)
        self.jump_forward_button.setEnabled(enabled)
        self.loop_button.setEnabled(enabled)
        self.mute_button.setEnabled(enabled) # TODO: should always be enabled, needs to be handled properly later
        self.volume_slider.setEnabled(enabled) # TODO: should always be enabled, needs to be handled properly later
        self.open_dir_button.setEnabled(enabled)
        self.loop_button.setEnabled(enabled)
        self.timer_button.setEnabled(enabled)
        self.slower_button.setEnabled(enabled)
        self.faster_button.setEnabled(enabled)
        self.fullscreen_button.setEnabled(enabled)


    def updatePlaybackUI(self):
        """updates the user interface"""
        if self.currently_playing_show_queue_id != self.main_setup.queue_shows.queue[0]:
            self.currently_playing_show_queue_id = self.main_setup.queue_shows.queue[0]
            self.check_upcoming_show_playable()

        if not self.mediaplayer.is_playing():
            return

        if gui_event_media_area_update_player_Playing.is_set() or (
            self.mediaplayer.get_position() * self.mediaplayer.get_length() < 500
        ):
            gui_event_media_area_update_player_Playing.clear()
            self.handle_media_area_state()

        # setting the slider or the playback to the desired position
        if self.time_progress_slider_moving:
            if self.time_progress_slider_new_position:
                new_position = self.time_progress_slider.value() / 10000.0
                media_length = self.mediaplayer.get_length()
                max_position = (media_length - (GUI_END_BLOCKED_TIME_ZONE_SEC * 1000)) / media_length
                self.mediaplayer.set_position(min(new_position, max_position))
                self.time_progress_slider_new_position = False
        else:
            self.media_length_string = get_h_m_s_from_milliseconds(self.mediaplayer.get_length())  # TODO: move into player handle options post play
            self.time_progress_slider.setValue(floor(self.mediaplayer.get_position() * 10000))
            self.time_progress_label.setText(get_h_m_s_from_milliseconds(self.mediaplayer.get_time()) + " / " + self.media_length_string)



    # handle button presses

    def handlePlayPausePlayback(self):
        """Toggle play/pause status
        """
        if len(self.main_setup.get_all_active_show_ids_for_current_profile()) == 0:
            return

        if self.mediaplayer.is_playing():
            self.mediaplayer.pause()
            self.play_pause_button.setIcon(QIcon(ICON_PLAY))
            self.play_pause_button.setStatusTip("Play")
            # self.isPaused = True
            self.timer_equalizer_stop()
        else:
            # if self.mediaplayer.play() == -1:
            if not session_running_event.is_set():

                # check if upcoming episode can be played
                if not self.check_upcoming_show_playable():
                    return

                # trigger session start
                self.toggleManagementTopRowEnabled(False)
                self.play_start_gui()
                event_player_Playing.wait()
                # print("playing!!!")
                sleep(0.31415)
                self.handle_media_area_state()
            else:
                self.mediaplayer.play()
            self.handlePlaybackControlsEnabled(True)
            self.play_pause_button.setIcon(QIcon(ICON_PAUSE))
            self.play_pause_button.setStatusTip("Pause")
            self.timer_playback.start()
            if not self.equalizer.isVisible():
                self.timer_equalizer_start()
            # self.isPaused = False


    # def check_upcoming_show_playable(self) -> bool:
    #     upcoming_show = get_next_show(self.main_setup)
    #     show_episodes_paths = show_generate_all_episodes_paths(upcoming_show, self.main_setup.global_config)
    #     if show_episodes_paths is None:
    #         showWarningDialog("Directory Not Found", "Upcoming show directory not found.",
    #                           "Directory for the show queued up next cannot be found. Maybe the wrong base path was selected?")
    #         if session_running_event.is_set():
    #             self.handleStopPlayback()
    #         return False
    #
    #     elif len(show_episodes_paths) == 0:
    #         showWarningDialog("No Files Found", "No files found for upcoming show.",
    #                           "Directory found for the upcoming show, but no files in there...")
    #         if session_running_event.is_set():
    #             self.handleStopPlayback()
    #         return False
    #
    #     elif len(show_episodes_paths) < upcoming_show.currentEpisode:
    #         showWarningDialog("'Current Episode' Too High", "Upcoming show has not enough files.",
    #                           "The 'Current Episode' is more than the amount of episodes itself... Could be caused by a manually set 'Last Episode' that is more than the amount of files in the directory.")
    #         if session_running_event.is_set():
    #             self.handleStopPlayback()
    #         return False
    #
    #     elif not os.path.isfile(show_generate_full_episode_path(upcoming_show, self.main_setup.global_config)):
    #         showWarningDialog("Upcoming Show Episode Not Found", "Upcoming show file not found.", "What the hell.")
    #         if session_running_event.is_set():
    #             self.handleStopPlayback()
    #         return False


    def handleStopPlayback(self):
        """Stop player
        """
        input_handle_exit(self.mediaplayer)
        self.toggleManagementTopRowEnabled(True)
        self.timer_playback.stop()
        self.timer_equalizer_stop()
        self.time_progress_slider.setValue(0)
        self.time_progress_label.setText("00:00")
        self.play_pause_button.setIcon(QIcon(ICON_PLAY_START))
        self.handlePlaybackControlsEnabled(False)

        sleep(0.31415) # TODO FIXME: sleep for exactly as long as all saving to json config files takes, not more or less

        self.handle_media_area_state()



    def handleSkipPlayback(self):
        """Skip this media
        """
        input_handle_player_next_no_save(self.mediaplayer, self.main_setup)


    def handleSkipHardPlayback(self):
        """Skip to the next episode (or its opening)
        """
        input_handle_skip_full_episode(self.mediaplayer)


    def handleSkipToCommercials(self):
        """Skip to commercials
        """
        input_handle_skip_to_commercials(self.mediaplayer)


    def handleBackPlayback(self):
        """Skip back a few seconds
        """
        input_handle_back_x(self.mediaplayer, 5)


    def handleForwardPlayback(self):
        """Skip ahead a few seconds
        """
        input_handle_forward_x(self.mediaplayer, 5)


    def handleSlowerPlayback(self): # TODO
        """Speed down playback
        """


    def handleFasterPlayback(self): # TODO
        """Speed up playback
        """


    def handleShutdownEpisodesLimitDialog(self): # TODO
        """Implement old 'shutdown' and 'last' command as gui
        """


    def handleOpenDirCurrentMedia(self):
        """Open directory of currently playing media
        """
        if self.main_setup.currently_running_show_fullpath is not None:
            open_file(os.path.dirname(self.main_setup.currently_running_show_fullpath))


    def handleLoopPlayback(self): # TODO
        """Loop current media forever (toggle)
        """
        if self.loop_button.isChecked():
            self.loop_button.setIcon(QIcon(ICON_LOOPING))
            self.main_setup.session_config.loop = True
        else:
            self.loop_button.setIcon(QIcon(ICON_LOOP))
            self.main_setup.session_config.loop = False


    def handleSetVolume(self, volume):
        """Set the volume
        """
        self.mediaplayer.audio_set_volume(volume) # TODO replace by proper volume handling
        self.volume_label.setText(str(volume))


    def handleMutePlayback(self):
        """Mute the playback (toggle)
        """
        # TODO replace by proper mute handling
        # TODO correct possible button desync in update_ui ??
        mute_state = self.mediaplayer.audio_get_mute()
        if mute_state == 0:  # 0 if not muted
            mute_bool = True
            self.mute_button.setIcon(QIcon(ICON_SPEAKER_MUTED))
        else:
            mute_bool = False
            self.mute_button.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.mediaplayer.audio_set_mute(mute_bool)


    def handle_toggle_fullscreen(self):
        input_handle_queue_show_display(self.main_setup)

        self.showTextOnMedia("Test hey sped up" + str(random.randint(1, 17)), 2500) # TODO use at appropriate places
        # equalizerAdjustByMedia(self.equalizer, "test1" + str(random.randint(1, 2000)), random.randint(3, 30), False) #TODO delete

        newBool = self.fullscreen_button.isChecked()
        self.mediaplayer.set_fullscreen(newBool)
        # print("fullscreen: " + str(newBool) + "! " + str(self.mediaplayer.get_fullscreen()))
        # print("TODO not working yet")


    def handleTimesliderPressed(self):
        """Deactivate updating of slider, else dragged Slider bugs around
        """
        # self.timer_playback.stop()
        self.time_progress_slider_moving = True


    def handleTimesliderReleased(self):
        """Reactivate updating of slider, else dragged Slider bugs around
        """
        if self.time_progress_slider_new_position:
            new_position = self.time_progress_slider.value() / 10000.0
            media_length = self.mediaplayer.get_length()
            max_position = (media_length - (GUI_END_BLOCKED_TIME_ZONE_SEC * 1000)) / media_length
            self.mediaplayer.set_position(min(new_position, max_position))
        self.time_progress_slider_new_position = False
        # self.timer_playback.start()
        self.time_progress_slider_moving = False


    def handleTimesliderMoved(self, position):
        """Set the playback time with the slider
        """
        # setting the position to where the slider was dragged
        # self.mediaplayer.set_position(position / 10000.0)
        self.time_progress_slider_new_position = True
        media_length = self.mediaplayer.get_length()
        self.time_progress_label.setText(get_h_m_s_from_milliseconds(math.floor(min(media_length * (position / 10000.0), media_length - GUI_END_BLOCKED_TIME_ZONE_SEC * 1000))) + " / " + self.media_length_string) # TODO: cache get_length somewhere
        # the vlc MediaPlayer needs a float value between 0 and 1, Qt
        # uses integer variables, so you need a factor; the higher the
        # factor, the more precise are the results
        # (10000 should be enough)


    def handleToggleExtraButtons(self):
        if self.show_extra_lines:
            # self.h_box_playback_buttons_extra_frame_bottom.setHidden(True)
            self.h_box_playback_buttons_extra_frame_side.setHidden(True)
        else:
            # self.h_box_playback_buttons_extra_frame_bottom.setHidden(False)
            self.h_box_playback_buttons_extra_frame_side.setHidden(False)
        self.show_extra_lines = not self.show_extra_lines


    def handleToggleMediaArea(self):
        pass # TODO




class UiMenubarPlayer(QMenuBar): # TODO: add logic behind all menu actions

    def __init__(
            self,
            main_setup: MainSetup,
            mediaplayer: vlc.MediaPlayer,
            ui_playback_control: UiPlaybackControlVBox,
            control_top_row: UiManagementTopRow
    ):
        super().__init__()

        self.main_setup = main_setup
        self.mediaplayer = mediaplayer
        self.ui_playback_control = ui_playback_control
        self.control_top_row = control_top_row

        #
        # # File TODO: delete
        # self.menubar_file = self.addMenu("&File") # TODO delete
        #
        # self.menu_open = QAction("&Open")
        # # self.menu_open.setShortcut("Ctrl+O")
        # self.menu_open.setStatusTip("Open a new File")
        # self.menu_open.triggered.connect(open_file_dialog)
        # self.menubar_file.addAction(self.menu_open)
        #
        #
        # self.menubar_file.addSeparator()
        #
        #
        # self.menu_exit = QAction("&Exit")
        # self.menu_exit.setShortcut("Ctrl+E")
        # self.menu_exit.setStatusTip("Close the program")
        # self.menu_exit.triggered.connect(sys.exit) # TODO: if session is playing, stop it and show warning: please stop the session first yourself to avoid problems when saving the session. Maybe add checkbox "Ignore the risk and don't show this again".
        # # TODO ^ move this into clicking the X of the window. This "File"-menu will be deleted
        # self.menubar_file.addAction(self.menu_exit)



        # Management
        self.menubar_management = self.addMenu("&Manage")

        self.menu_save = QAction("&Save")
        self.menu_save.setStatusTip("Save all settings. Can't save when session is running.")
        self.menu_save.setIcon(QIcon(ICON_SAVE))
        self.menu_save.setShortcut("Ctrl+S")
        self.menu_save.triggered.connect(lambda: self.control_top_row.user_profiles_paths_save_button.click())
        self.menubar_management.addAction(self.menu_save)

        self.menu_shows = QAction("&Shows")
        self.menu_shows.setStatusTip("Manage shows.")
        # self.menu_shows.triggered.connect(self.showDialogShowsEdit) # TODO
        self.menubar_management.addAction(self.menu_shows)

        self.menu_profiles = QAction("&Profiles")
        self.menu_profiles.setStatusTip("Manage profiles, users and base paths.")
        # self.menu_profiles.triggered.connect(self.showDialogShowsEdit) # TODO
        self.menubar_management.addAction(self.menu_profiles)



        # Playback
        self.menubar_playback = self.addMenu("&Playback")

        self.menu_play_pause = QAction("&Play")
        self.menu_play_pause.setIcon(QIcon(ICON_PLAY_START)) # TODO use blue and pause icons later
        self.menu_play_pause.setShortcut("Ctrl+P")
        self.menu_play_pause.setStatusTip("TODO: use variables for all status tips, maybe put into new module. Also good for possible translation later.")
        # self.menu_play_pause.triggered.connect() # TODO
        self.menubar_playback.addAction(self.menu_play_pause)

        self.menu_stop = QMenu("&Stop")
        self.menu_stop.setIcon(QIcon(ICON_STOP))
        # self.menu_stop.setStatusTip("")
        self.menubar_playback.addMenu(self.menu_stop)

        self.menu_stop_queue_time = QAction("&Suspend Session")
        self.menu_stop_queue_time.setShortcut("Ctrl+Q") # TODO: set shortcut on the stop variant that is selected in the settings
        # self.menu_stop_queue_time.setStatusTip("")
        self.menu_stop_queue_time.triggered.connect(lambda: self.ui_playback_control.menu_stop_options.menu_stop_queue_time.trigger())
        self.menu_stop.addAction(self.menu_stop_queue_time)

        self.menu_stop_time = QAction("&Suspend Show")
        # self.menu_stop_time.setShortcut("Ctrl+Q")
        # self.menu_stop_time.setStatusTip("")
        self.menu_stop_time.triggered.connect(lambda: self.ui_playback_control.menu_stop_options.menu_stop_time.trigger())
        self.menu_stop.addAction(self.menu_stop_time)

        self.menu_stop_queue = QAction("&Suspend Queue")
        # self.menu_stop_queue.setShortcut("Ctrl+Q")
        # self.menu_stop_queue.setStatusTip("")
        self.menu_stop_queue.triggered.connect(lambda: self.ui_playback_control.menu_stop_options.menu_stop_queue.trigger())
        self.menu_stop.addAction(self.menu_stop_queue)

        self.menu_stop_simple = QAction("&Stop")
        # self.menu_stop_simple.setShortcut("Ctrl+Q")
        # self.menu_stop_simple.setStatusTip("")
        self.menu_stop_simple.triggered.connect(lambda: self.ui_playback_control.menu_stop_options.menu_stop_simple.trigger())
        self.menu_stop.addAction(self.menu_stop_simple)


        self.menu_skip = QMenu("&Skip")
        self.menu_skip.setIcon(QIcon(ICON_SKIP))
        # self.menu_skip.setStatusTip("")
        self.menubar_playback.addMenu(self.menu_skip)

        self.menu_skip_simple = QAction("&Skip")
        self.menu_skip_simple.setShortcut("Ctrl+N")
        # self.menu_skip_simple.setStatusTip("")
        self.menu_skip_simple.triggered.connect(lambda: self.ui_playback_control.menu_skip_options.menu_skip.trigger())
        self.menu_skip.addAction(self.menu_skip_simple)

        self.menu_skip_to_cms = QAction("&Skip to CMs")
        # self.menu_skip_to_cms.setStatusTip("")
        self.menu_skip_to_cms.triggered.connect(lambda: self.ui_playback_control.menu_skip_options.menu_skip_to_cms.trigger())
        self.menu_skip.addAction(self.menu_skip_to_cms)

        self.menu_skip_all = QAction("&Skip All")
        # self.menu_skip_all.setStatusTip("")
        self.menu_skip_all.triggered.connect(lambda: self.ui_playback_control.menu_skip_options.menu_skip_all.trigger())
        self.menu_skip.addAction(self.menu_skip_all)

        self.menu_jump_back = QAction("&Jump Back")
        self.menu_jump_back.setIcon(QIcon(ICON_BACKWARD))
        self.menu_jump_back.setShortcut("Ctrl+B")
        # self.menu_jump_back.setStatusTip("")
        self.menu_jump_back.triggered.connect(lambda: self.ui_playback_control.jump_back_button.click())
        self.menubar_playback.addAction(self.menu_jump_back)

        self.menu_jump_forward = QAction("&Jump Forward")
        self.menu_jump_forward.setIcon(QIcon(ICON_FORWARD))
        self.menu_jump_forward.setShortcut("Ctrl+F")
        # self.menu_jump_forward.setStatusTip("")
        self.menu_jump_forward.triggered.connect(lambda: self.ui_playback_control.jump_forward_button.click())
        self.menubar_playback.addAction(self.menu_jump_forward)


        self.menubar_playback.addSeparator()


        self.menu_mute = QAction("&Mute") # TODO toggleable
        self.menu_mute.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.menu_mute.setShortcut("Ctrl+M")
        # self.menu_mute.setCheckable(True)
        # self.menu_mute.setStatusTip("")
        self.menu_mute.triggered.connect(lambda: self.ui_playback_control.mute_button.click())
        self.menubar_playback.addAction(self.menu_mute)


        self.menu_fullscreen = QAction("&Fullscreen") # TODO toggleable??
        self.menu_fullscreen.setIcon(QIcon(ICON_FULLSCREEN))
        self.menu_fullscreen.setDisabled(True) # TODO set all disabled just like the buttons and enable them at the same times each
        self.menu_fullscreen.setShortcut("Ctrl+F")
        # self.menu_fullscreen.setStatusTip("")
        self.menu_fullscreen.triggered.connect(lambda: self.ui_playback_control.fullscreen_button.click())
        self.menubar_playback.addAction(self.menu_fullscreen)


        self.menubar_playback.addSeparator()


        self.menu_open_dir = QAction("&Open media directory")
        self.menu_open_dir.setIcon(QIcon(ICON_FOLDER))
        self.menu_open_dir.setShortcut("Ctrl+O")
        # self.menu_open_dir.setStatusTip("")
        self.menu_open_dir.triggered.connect(lambda: self.ui_playback_control.open_dir_button.click())
        self.menubar_playback.addAction(self.menu_open_dir)

        self.menu_loop = QAction("&Loop On") # TODO toggleable
        self.menu_loop.setIcon(QIcon(ICON_LOOP))
        self.menu_loop.setShortcut("Ctrl+L")
        # self.menu_loop.setCheckable(True)
        # self.menu_loop.setStatusTip("")
        self.menu_loop.triggered.connect(lambda: self.ui_playback_control.loop_button.click())
        self.menubar_playback.addAction(self.menu_loop)

        self.menu_timer = QAction("&Timer")
        self.menu_timer.setIcon(QIcon(ICON_TIMER)) # TODO toggleable
        self.menu_timer.setShortcut("Ctrl+T")
        self.menu_timer.setCheckable(True)
        # self.menu_timer.setStatusTip("")
        self.menu_timer.triggered.connect(lambda: self.ui_playback_control.timer_button.click())
        self.menubar_playback.addAction(self.menu_timer)

        self.menu_speed = QMenu("&Speed")
        # self.menu_speed.setStatusTip("")
        self.menubar_playback.addMenu(self.menu_speed)

        self.menu_speed_faster = QAction("&Faster")
        self.menu_speed_faster.setIcon(QIcon(ICON_FASTER))
        # self.menu_speed_faster.setStatusTip("")
        self.menu_speed_faster.triggered.connect(lambda: self.ui_playback_control.faster_button.click())
        self.menu_speed.addAction(self.menu_speed_faster)

        self.menu_speed_normal = QAction("&Normal Speed")
        # self.menu_speed_normal.setStatusTip("")
        # self.menu_speed_normal.triggered.connect() # TODO
        self.menu_speed.addAction(self.menu_speed_normal)

        self.menu_speed_slower = QAction("&Slower")
        self.menu_speed_slower.setIcon(QIcon(ICON_SLOWER))
        # self.menu_speed_slower.setStatusTip("")
        self.menu_speed_slower.triggered.connect(lambda: self.ui_playback_control.slower_button.click())
        self.menu_speed.addAction(self.menu_speed_slower)


        self.menubar_playback.addSeparator()


        self.menu_track = QMenu("&Tracks")
        # self.menu_track.setStatusTip("")
        self.menubar_playback.addMenu(self.menu_track)

        self.menu_track_video = QAction("&Video")
        # self.menu_track_video.setStatusTip("")
        # self.menu_track_video.triggered.connect() # TODO
        self.menu_track.addAction(self.menu_track_video)

        self.menu_track_audio = QAction("&Audio")
        # self.menu_track_audio.setStatusTip("")
        # self.menu_track_audio.triggered.connect() # TODO
        self.menu_track.addAction(self.menu_track_audio)

        self.menu_track_subtitles = QAction("&Subtitles")
        # self.menu_track_subtitles.setStatusTip("")
        # self.menu_track_subtitles.triggered.connect() # TODO
        self.menu_track.addAction(self.menu_track_subtitles)



        # Extra
        self.menubar_extra = self.addMenu("&Extra")

        self.menu_mp3merge = QAction("&Mp3Merge")
        self.menu_mp3merge.setStatusTip("Create condensed anime episodes from 'subs2srs' Anki (.apkg) files.")
        self.menu_mp3merge.triggered.connect(self.showDialogMp3Merge) # TODO
        self.menubar_extra.addAction(self.menu_mp3merge)

        self.menu_debug_player_jumps = QAction("&Debug Player Jumps")
        self.menu_debug_player_jumps.setStatusTip("Experimental setting trying to fix buggy media files.")
        self.menu_debug_player_jumps.triggered.connect(self.showDialogDebugPlayerJumps)
        self.menubar_extra.addAction(self.menu_debug_player_jumps)

        self.menu_analysis = QAction("&Analysis")
        self.menu_analysis.setStatusTip("Show some stats about available shows and some estimated usage values.")
        # self.menu_analysis.triggered.connect(self.showDialogAnalysis) # TODO
        self.menubar_extra.addAction(self.menu_analysis)



        # Help
        self.menubar_help = self.addMenu("&Help")

        self.menu_about = QAction("&About")
        self.menu_about.setStatusTip("About this program")
        self.menu_about.triggered.connect(self.showDialogAbout) # TODO
        self.menubar_help.addAction(self.menu_about)

        self.menu_tutorial = QAction("&How To...")
        self.menu_tutorial.setStatusTip("How to use this program")
        # self.menu_tutorial.triggered.connect(self.showDialogAbout) # TODO
        self.menubar_help.addAction(self.menu_tutorial)

        self.menu_about_qt = QAction("&GUI")
        self.menu_about_qt.setStatusTip("This user interface was programmed using PyQt, Qt for Python.")
        self.menu_about_qt.triggered.connect(lambda: QMessageBox.aboutQt(self))
        self.menubar_help.addAction(self.menu_about_qt)




    @staticmethod # TODO: move somewhere else
    def showDialogAbout(self):
        about = QMessageBox()
        about.setWindowTitle("About")
        about.setText("""Python script with GUI for customizable ordered playback of a randomly picked series of predefined media files from your local computer.""")
        about.setInformativeText("""TODO finish this, put into own variable.

python-vlc is used to play the media files, see https://www.olivieraubert.net/vlc/python-ctypes/

This whole script was written with only basic python knowledge and basically no media handling knowledge. Feel free to improve coding and functionality.

Source Code: gitlab.com/CptMaister/ranimetv
Contact: alex.mai@posteo.net""")
        about.setIcon(QMessageBox.Information)

        # about.setStandardButtons(QMessageBox.Close|QMessageBox.Ok)
        about.setStandardButtons(QMessageBox.Close)
        about.exec_()


    def showDialogDebugPlayerJumps(self):
        debugRunning = debug_player_jumps_running_event.is_set()

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(("Deactivate" if debugRunning else "Activate") + " 'Debug Player Jumps'?")
        msg.setInformativeText("When creating 'condensed anime immersion' like with the 'Mp3Merge'-feature, the files will sometimes have errors that will jumble the playback up at certain playback positions.\n\nWith this option activated, any anomalies of the playback length will be automatically detected as error and the media first restarted, then the playback position set to 1 second after the error was encountered.")
        msg.setWindowTitle("Debug Player Jumps")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg.accepted.connect(lambda: self.deactivateDebugPlayerJumps() if debugRunning else self.activateDebugPlayerJumps())
        # msg.rejected.connect(self.deactivateDebugPlayerJumps) // do nothing
        msg.exec_()


    def activateDebugPlayerJumps(self):
        print("activate Debug Player Jumps")
        debug_player_jumps_on(self.mediaplayer)


    def deactivateDebugPlayerJumps(self):
        print("deactivate Debug Player Jumps")
        debug_player_jumps_off()


    def showDialogMp3Merge(self): # TODO CONTINUE !!!!!
        self.m3m_dialog = QDialog()
        self.m3m_dialog.setWindowTitle("Mp3Merge")
        self.m3m_dialog.setWindowModality(Qt.ApplicationModal)
        self.m3m_dialog.setMinimumWidth(500)

        path_box = UiPathBox(search_file=True)

        button_box = QDialogButtonBox(
            QDialogButtonBox.Ok,
            Qt.Horizontal
        )
        button_box.accepted.connect(lambda: print("Start conversion: " + path_box.path_input.text())) # TODO handle save button

        # combine path box and buttons
        layout_all = QVBoxLayout()
        layout_all.addLayout(path_box)
        layout_all.addWidget(button_box)

        self.m3m_dialog.setLayout(layout_all)
        self.m3m_dialog.show()




# TODO: add functionality
class UiMenuStopOptions:

    def __init__(
            self
    ):

        self.menu = QMenu()

        self.menu_stop_queue_time = self.menu.addAction("&Suspend Session")
        self.menu_stop_queue_time.setStatusTip("Stop, continue here next time with current queue.")
        self.menu_stop_queue_time.setCheckable(True)
        self.menu_stop_queue_time.setChecked(True) # TODO: read checked from settings
        self.menu_stop_queue_time.triggered.connect(lambda: print("test TODO DELETE")) # TODO

        self.menu_stop_time = self.menu.addAction("&Suspend Show")
        self.menu_stop_time.setStatusTip("Stop, continue here next time, don't save current queue.")
        # self.menu_stop_time.triggered.connect() # TODO

        self.menu_stop_queue = self.menu.addAction("&Suspend Queue")
        self.menu_stop_queue.setStatusTip("Stop, save current queue, but not current playback position.")
        # self.menu_queue.triggered.connect() # TODO

        self.menu_stop_simple = self.menu.addAction("&Stop")
        self.menu_stop_simple.setStatusTip("Stop, discard current position and queue.")
        # self.menu_stop_simple.triggered.connect() # TODO


# TODO: add functionality
class UiMenuSkipOptions:

    def __init__(
            self
    ):

        self.menu = QMenu()

        self.menu_skip = self.menu.addAction("&Skip")
        self.menu_skip.setStatusTip("Skip current media.")

        self.menu_skip_to_cms = self.menu.addAction("&Skip to CMs")
        self.menu_skip_to_cms.setStatusTip("Skip to commercials (if you have any).")

        self.menu_skip_all = self.menu.addAction("&Skip All")
        self.menu_skip_all.setStatusTip("Skip to next show's episode.")



class UiMenuUiMediaAreaStates:

    def __init__(
            self,
            toggle_media_area_button,
            set_media_area_state,
            get_media_area_state
    ):
        self.toggle_media_area_button = toggle_media_area_button
        self.set_media_area_state = set_media_area_state
        self.get_media_area_state = get_media_area_state

        self.menu = QMenu()

        self.menu_vid_equ = self.menu.addAction("&Normal")
        self.menu_vid_equ.setStatusTip("Show video, equalizer if audio, management if nothing playing.")
        self.menu_vid_equ.setCheckable(True)
        self.menu_vid_equ.setChecked(True) # TODO: read checked from settings
        self.menu_vid_equ.triggered.connect(lambda: self.selectedOption(UiMediaAreaState.VIDEO_EQUALIZER))

        self.menu_no_eq = self.menu.addAction("&No Equalizer")
        self.menu_no_eq.setStatusTip("Show Management during audio playback.")
        self.menu_no_eq.setCheckable(True)
        self.menu_no_eq.triggered.connect(lambda: self.selectedOption(UiMediaAreaState.VIDEO_NOEQUALIZER))

        self.menu_no_vid = self.menu.addAction("&No Video")
        self.menu_no_vid.setStatusTip("Show audio equalizer for video and audio playback.")
        self.menu_no_vid.setCheckable(True)
        self.menu_no_vid.triggered.connect(lambda: self.selectedOption(UiMediaAreaState.EQUALIZERONLY))

        self.menu_manage_only = self.menu.addAction("&Management only")
        self.menu_manage_only.setStatusTip("Show Management even during media playback.")
        self.menu_manage_only.setCheckable(True)
        self.menu_manage_only.triggered.connect(lambda: self.selectedOption(UiMediaAreaState.MANAGEMENT_ONLY))

        self.menu_nothing = self.menu.addAction("&Nothing")
        self.menu_nothing.setStatusTip("Show nothing in media area.")
        self.menu_nothing.setCheckable(True)
        self.menu_nothing.triggered.connect(lambda: self.selectedOption(UiMediaAreaState.NOTHING))


    def selectedOption(self, state: UiMediaAreaState):
        self.set_media_area_state(state)

        menu_vid_equ_checked = False
        menu_no_eq_checked = False
        menu_no_vid_checked = False
        menu_manage_only_checked = False
        menu_nothing_checked = False

        if state == UiMediaAreaState.VIDEO_EQUALIZER:
            menu_vid_equ_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Normal. Hold Left Click to choose state.")
        elif state == UiMediaAreaState.VIDEO_NOEQUALIZER:
            menu_no_eq_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: No Equalizer. Hold Left Click to choose state.")
        elif state == UiMediaAreaState.EQUALIZERONLY:
            menu_no_vid_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: No Video. Hold Left Click to choose state.")
        elif state == UiMediaAreaState.MANAGEMENT_ONLY:
            menu_manage_only_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Management only. Hold Left Click to choose state.")
        elif state == UiMediaAreaState.NOTHING:
            menu_nothing_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Nothing. Hold Left Click to choose state.")

        self.menu_vid_equ.setChecked(menu_vid_equ_checked)
        self.menu_no_eq.setChecked(menu_no_eq_checked)
        self.menu_no_vid.setChecked(menu_no_vid_checked)
        self.menu_manage_only.setChecked(menu_manage_only_checked)
        self.menu_nothing.setChecked(menu_nothing_checked)



def handlePrintTest():
    print("Hello! :-)")



def selectRowsInTableView(table: QTableView, row_indexes: [int]):
    if row_indexes is None or len(row_indexes) == 0:
        return

    rows_selection = table.selectionModel()
    model = rows_selection.model()
    selection = QItemSelection()

    for i in row_indexes:
        model_index = model.index(i, 0)
        selection.select(model_index, model_index)

    mode = QItemSelectionModel.Select | QItemSelectionModel.Rows
    rows_selection.clear()
    rows_selection.select(selection, mode)



def vlcPlayerTextActivate(
        mediaplayer: vlc.MediaPlayer,
        text: str,
        position: VlcPlayerTextPosition = VlcPlayerTextPosition.TOP_LEFT,
        duration: int = 2000
):
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Enable, 1)
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Size, int(mediaplayer.video_get_height() / 16))  # pixels
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Position, position.value)
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Timeout, duration)  # millisec, 0==forever
    # mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Refresh, 1000)  # millisec (or sec?)

    mediaplayer.video_set_marquee_string(vlc.VideoMarqueeOption.Text, vlc.str_to_bytes("^\n.   " + text))


def equalizerAdjustByMedia(
        equalizer: EqualizerBar,
        title: str,
        episodeAmount: int,
        lightBackground: bool = False,
):
    # TODO: adjust text on equalizer if bg is white (currently not used)
    equalizer.setBackgroundColor("white" if lightBackground else "black")

    # create hash, will stay the same for a title, but be different for different titles
    sha256_hash_hex = hashlib.sha256(title.encode()).hexdigest()

    # create 3 colors from the hash, first and third full saturation, middle one lighter
    color_1_hsv = (getValueFromHash(sha256_hash_hex, 1), (0.15 if lightBackground else 1.0), 1.0)
    color_2_hsv = (getValueFromHash(sha256_hash_hex, 2), (1.0 if lightBackground else 0.15), 1.0)
    color_3_hsv = (getValueFromHash(sha256_hash_hex, 3), (0.15 if lightBackground else 1.0), 1.0)

    bars = math.ceil(math.log(episodeAmount, 1.5) * 1.5) + 2

    # interpolate colors between
    colors_interpolated = getInterpolatedColors([color_1_hsv, color_2_hsv, color_3_hsv], bars)

    # print("getValueFromHash")
    # print("hash: " + sha256_hash_hex)
    # print("color value 1: " + str(color_1_hsv[0]))
    # print("color value 2: " + str(color_2_hsv[0]))
    # print("color value 3: " + str(color_3_hsv[0]))

    equalizer.setColors(list(map(hsv2rgb, colors_interpolated)))


def getInterpolatedColors(
        colors_hsv: list,
        amount: int
) -> list:

    if len(colors_hsv) == 1:
        return colors_hsv[0]

    colors_list_out = []

    separators = amount - 1
    len_colors = len(colors_hsv)
    for i in range(separators):
        color_a = colors_hsv[floor(i * ((len_colors - 1) / separators))]
        color_b = colors_hsv[floor(i * ((len_colors - 1) / separators)) + 1]
        interpolation_factor = (i * ((len_colors - 1) / separators)) % 1
        h = color_a[0] * (1 - interpolation_factor) + color_b[0] * interpolation_factor
        s = color_a[1] * (1 - interpolation_factor) + color_b[1] * interpolation_factor
        v = 1
        colors_list_out.append((min(1, h), min(1, s), v))

    colors_list_out.append(colors_hsv[-1])

    return colors_list_out


def getValueFromHash(hash: str, position: int = 0) -> float:
    return int(hash[(5 * position): (5 * position + 2)], 16) / 256


def hsv2rgb(hsv_tuple: tuple) -> tuple:
    return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(hsv_tuple[0], hsv_tuple[1], hsv_tuple[2]))


def showWarningDialog(title: str, text: str, informativeText: str):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Warning)
    msg.setWindowTitle(title.title())
    msg.setText(text)
    msg.setInformativeText(informativeText)
    msg.setStandardButtons(QMessageBox.Ok)
    # msg.accepted.connect(lambda: print("hey"))
    msg.exec_()
