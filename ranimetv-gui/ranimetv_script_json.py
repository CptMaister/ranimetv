#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Python script for customizable ordered playback of a randomly picked series
# of predefined media files from your local computer.
#
# ranimetv
# Copyright (C) 2021  Alexander Mai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Python script for customizable ordered playback of a randomly picked
series of predefined media files from your local computer.

To run properly it only needs a config file specifying all media files and
preferences. Just start up the script in your terminal and read the
instructions.

python-vlc is used to play the media files, see https://www.olivieraubert.net/vlc/python-ctypes/

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.

Source Code, Contact:
gitlab.com/CptMaister/ranimetv, alex.mai@posteo.net
"""
import threading
from re import sub  # substitute string parts with regex search, used to reduce whitespace
from queue import Queue # used for input processing
from threading import Thread, Event
from time import sleep
import os, sys # file path processing, handle args, detect current operating system, open dir in explorer
from subprocess import call, run, PIPE, STDOUT  # open directory in explorer with call, run things like in a terminal, used to get file's length with ffprobe
from copy import deepcopy
from math import floor
from random import randint, shuffle     # random ints, randomized order of elements in lists
from urllib.parse import unquote    # proper printing of commercial paths in print status, replaces %20 with a space and handles all other kinds of weird symbols
import shortuuid # see https://github.com/skorokithakis/shortuuid
import vlc

import PySimpleGUI as sg
from json import (load as jsonload, dump as jsondump)
from ranimetv_constants import *

from typing import List

PROGRAM_NAME = "RAnimeTV"
CONTACT_EMAIL = "alex.mai@posteo.net"
LINK_TO_GITLAB_REPO = "https://gitlab.com/CptMaister/ranimetv"
PROGRAM_VERSION = "0.11.0" # JSON Rework: config file is now saved as json files, preparation for GUI
#PROGRAM_VERSION = "0.10.21" # Windows compatibility, multiple config files, restructured code, few new commands and bug fixing
#PROGRAM_VERSION = "0.9.15" # massive bug fixes, options and new commands, code improvements
#PROGRAM_VERSION = "0.8" commercials, directory path line, queue, search alg
#PROGRAM_VERSION = "0.7" opening and ending, using only one player
#PROGRAM_VERSION = "0.6" player-jumps-debugger implemented, added license
#PROGRAM_VERSION = "0.5" using ShowEpisode and option classes, prepare future options and commands
#PROGRAM_VERSION = "0.4" help texts, error handling and monitoring
#PROGRAM_VERSION = "0.3" all basic commands
#PROGRAM_VERSION = "0.2" config file getting updated
#PROGRAM_VERSION = "0.1" main functionality, no config file updates


MESSAGE_ABOUT = f"""
Python script for customizable ordered playback of a randomly picked series of predefined media files from your local computer.

{PROGRAM_NAME} v{PROGRAM_VERSION}, Copyright (C) 2020  Alexander Mai, alex.mai@posteo.net

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.

Source Code: gitlab.com/CptMaister/ranimetv


References
colored and bold terminal output see https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
vlc MediaPlayer examples with python-vlc https://stackoverflow.com/questions/3595649/vlc-python-eventmanager-callback-type
python-vlc see https://wiki.videolan.org/Python_bindings/
waiting with threading for the player to finish at "The Good #2" in https://blog.miguelgrinberg.com/post/how-to-make-python-wait
input stuff with parallel threads https://stackoverflow.com/questions/5404068/how-to-read-keyboard-input/53344690#53344690
PySimpleGUI examples/inspiration from https://pysimplegui.trinket.io/demo-programs#/demo-programs/menu-bar
PySimpleGUI see https://pysimplegui.readthedocs.io/en/latest/
"""



# class for better terminal output, see https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
class color:
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    LIGHTGRAY = '\033[37m'

    BLACK_BG = '\033[40m'
    RED_BG = '\033[41m'
    GREEN_BG = '\033[42m'
    YELLOW_BG = '\033[43m'
    BLUE_BG = '\033[44m'
    PURPLE_BG = '\033[45m'
    CYAN_BG = '\033[46m'
    LIGHTGRAY_BG = '\033[47m'

    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

    HIGHLIGHT = BOLD + CYAN
    INPUT_ANSWER = BOLD
    PATH_PLAYBACK = "  " + GREEN
    WARNING = BOLD + YELLOW
    ERROR = BOLD + RED
# bold text: print(color.BOLD + 'Hello World !' + color.END)


if sys.platform == "win32":
    os.system('color') # this magically makes colors possible on windows 10 terminal / command prompt


def printlog(printstr: str):
    print(f'    {color.CYAN}{printstr}{color.END}')


def printdev(printstr: str):
    print(f'    {color.CYAN}{printstr}{color.END}')


def printunexpected(printstr: str):
    print(f'    {color.PURPLE}{printstr}{color.END}')



##### Set Limits, Standard values etc #####

VOLUME_LIMIT_LOWER = 0
VOLUME_LIMIT_UPPER = 200
SPEED_LIMIT_LOWER = 40
SPEED_LIMIT_UPPER = 300
MONITOR_LIMIT_LOWER = 1
PROBABILITY_LIMIT_LOWER = 5
PROBABILITY_LIMIT_UPPER = 100
QUEUE_LOAD_LIMIT_UPPER = 25


QUEUE_SIZE_STANDARD = 3
BACK_PLAYBACK_SIZE_STANDARD = 5
FORWARD_PLAYBACK_SIZE_STANDARD = 5
JUMP_PLAYBACK_BUMPER_SIZE_STANDARD = 20
SUSPEND_BACK_SIZE_STANDARD = 10
SUSPENDED_OPENING_TIME = 60
END_PLAYBACK_SIZE_STANDARD = 5
DEBUG_PLAYER_JUMPS_ANOMALY_SENSITIVITY_SIZE_STANDARD = 2
DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD = 1
MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD = 3



##### Config Handling Setup #####

########################################## Load/Save Settings File ##########################################

# current_path = os.getcwd()
CONFIG_LOCATION = "ranimetv-config"
CONFIG_GLOBAL_PATH = "global-config"
CONFIG_GLOBAL_FULL_PATH = os.path.join(CONFIG_LOCATION, CONFIG_GLOBAL_PATH + ".json")
CONFIG_USER_PATH = "user-config-"
ENCODING_CONFIG_FILE = "utf-8"

def get_config_user_full_path(userUuid: str) -> str:
    return os.path.join(CONFIG_LOCATION, CONFIG_USER_PATH + userUuid + ".json")

#
# ### config file data organisation #TODO: delete
# CONFIG_FIELD_SEPARATOR = ";"
# CONFIG_POSITION_SHOW_ALIAS = 0
# CONFIG_POSITION_SHOW_FIRST_EP = 1
# CONFIG_POSITION_SHOW_LAST_EP = 2
# CONFIG_POSITION_SHOW_CURRENT_EP = 3
#
# LEFT_OFF_AT_SEPARATOR = "|"
# CONFIG_GLOBAL_OPTIONS_LINE_STARTER = ">>>"
# CONFIG_SUSPEND_QUEUE_LINE_STARTER = "<<<"
#
# CONFIG_OPTIONS_YES_OPTIONS = ["true", "yes", "on", "y", "t"]
# CONFIG_OPTIONS_NO_OPTIONS = ["false", "no", "off", "n", "f"]


class JsonKeys:
    GCurrentUserUuid = 'currentUserUuid'
    # GNextUserId = 'nextUserId'
    GCurrentBasePath_Index = 'currentBasePathIndex'
    GUseBasePaths_Bool = 'useBasePaths'
    GUsers_Dicts = 'users'
    GUsersUuid = 'userUuid'
    GUsersName_String = 'userName'
    GBasePaths_StrArray = 'basePaths'            #default False
    GWindowSettings_Dict = 'windowSettings'
    GExcludeFilesEndingWith = 'excludeFilesEndingWith'
    GNoAutosave_Bool = 'noAutosave'

    GWindowSettingsMediaAreaState_Int = "mediaAreaState"    #default 0
    GWindowSettingsSortShows_Int = "sortShows"              #default 0
    GWindowSettingsSortShowsAscending_Boolean = "sortShowsAscending"              #default 0
    GWindowSettingsSortCommercials_Int = "sortCommercials"  #default 0
    GWindowSettingsSortCommercialsAscending_Boolean = "sortCommercialsAscending"  #default 0
    GWindowSettingsSortShowsCmsEdit_Int = "sortShowsCmsEdit" #default 0
    GWindowSettingsSortShowsCmsEditAscending_Boolean = "sortShowsCmsEditAscending" #default 0
    GWindowSettingsDynamicShowVolume_Boolean = "dynamicShowVolume"  #default false
    GWindowSettingsGuiTheme_String = 'guiTheme'     #default TODO
    GWindowSettingsLanguage_String = 'language'     #default "en"
    GWindowSettingsFontSize_Int = 'fontSize'        #default TODO 
    
    UCurrentProfile_Id = 'currentProfileId'
    UIdManagement_Dict = 'idManagement'
    UIdManagementNextProfileId = 'nextProfileId'
    UIdManagementNextShowId = 'nextShowId'

    UProfiles_Dicts = 'profiles'
    UProfilesId = 'profileId'
    UProfilesName_String = 'profileName'
    UProfilesSettings_Dict = 'profileSettings'
    UProfilesStartWithShows_IdArray = 'startWithShows'
    UProfilesSuspendedQueue_IdArray = 'suspendedQueue'
    UProfilesPlayAllShows_Bool = 'playAllShows'      #default False, overrides include and exclude
    UProfilesInclude_IdArray = 'profilesInclude'             #default []
    UProfilesExclude_IdArray = 'profilesExclude'             #default [], overpowers include
    
    # former user (global) player settings, profile settings:
    UPPSCommercialsPlay_Bool = 'commercialsPlay'           #default True
    UPPSCommercialsStartWeight_Value = 'commercialsStartWeight'    #default 0
    UPPSCommercialsBreakWeight_Value = 'commercialsBreakWeight'    #default 0
    UPPSProbability_Value = 'probability'           #default 100
    UPPSShutdown_Value = 'shutdown'                 #default None
    UPPSShowsLimit_Value = 'showsLimit'             #default None   # formerly called 'lastEpisodes'
    UPPSUniqueShows_Bool = 'uniqueShows'            #default False
    UPPSHardUniqueShows_Bool = 'hardUniqueShows'    #default False

    UShows_Dicts = 'shows'
    UShowsId = 'showId'
    UShowsName_String = 'showName'
    UShowsIsActive_Bool = "isActive"
    UShowsDoNotUseBasePath_Bool = 'doNotUseBasePath'    #default False
    UShowsDirectoryPath_String = 'directoryPath'
    UShowsActiveProfiles_IdArray = 'activeProfiles'
    UShowsFirstEpisode_Int = 'firstEpisode'
    UShowsLastEpisode_Int = 'lastEpisode'
    UShowsCurrentEpisode_Int = 'currentEpisode'
    UShowsSuspendedTimeSeconds_Int = 'suspendedTimeSeconds'
    UShowsTags_StrArray = 'tags'
    UShowsSettings_Dict = 'showSettings'

    # user show player settings:
    USPSRandom_Bool = 'random'                          #default False
    USPSMultipleEpisodes_Value = 'multipleEpisodes'     #default 1
    USPSProbability_Value = 'probability'               #default None
    USPSNoCommercialsAfter_Bool = 'noCommercialsAfter'    #default False
    USPSNoCommercialsBefore_Bool = 'noCommercialsBefore'  #default False
    USPSCommercialWeight_Value = 'commercialWeight'     #default None
    USPSCommercialMax_Value = 'commercialMax'           #default None
    USPSCommercialMin_Value = 'commercialMin'           #default 0
    USPSVideoTrack_Int = 'videoTrack'
    USPSAudioTrack_Int = 'audioTrack'
    USPSSubtitleTrack_Int = 'subtitleTrack'
    USPSRecursionLevel_Int = 'recursionLevel'

    # common player settings (settings that exist for profiles and for single shows):
    UCPSOpening_Bool = 'opening'            #default False
    UCPSEnding_Bool = 'ending'              #default False
    UCPSStatus_Bool = 'status'              #default False  #TODO GUI not needed
    UCPSVolume_Value = 'volume'             #default 100
    UCPSSpeed_Value = 'speed'               #default 100
    UCPSJump_Value = 'jump'                 #default 0
    UCPSEnd_Value = 'end'                   #default 0
    UCPSPause_Bool = 'pause'                #default False
    UCPSMute_Bool = 'mute'                  #default False
    UCPSFullscreen_Bool = 'fullscreen'      #default False
    UCPSQueueDisplay_Bool = 'queueDisplay'  #default False  #TODO: GUI not needed
    UCPSSuspend_Bool = 'suspend'            #default False  #TODO: make default True
    UCPSSuspendQueue_Bool = 'suspendQueue'  #default False  #TODO: make default True
    UCPSNoRerun_Bool = 'noRerun'            #default False
    UCPSMonitor_Value = 'monitor'           #default False  #TODO: GUI not needed
    UCPSMonitorJump_Bool = 'monitorJump'    #default False  #TODO: GUI put into planned status bar??
    UCPSDebugPlayerJumps_Bool = 'debugPlayerJumps'  #default False  #TODO: make default True

    # # MainSetup Session Dict Keys
    # SVolume_Value = 'volumeValue'
    # SSpeed_Value = 'speedValue'
    # SMute_Bool = 'muteBool'
    # SMonitor_Seconds = 'monitorValue'
    # SCommercialWeight_Value = 'commercialWeightValueGlobal'

    # # MainSetup Queue Dict Keys
    # QQueue_IdArray = 'queue'
    # QAlreadyPlayed_IdArray = 'alreadyPlayedIds'
    # QStartedPlaybacksPaths_StrArray = 'startedPlaybacksPaths'


class ConfigGlobalUser:
    def __init__(
        self,
        userUuid: str,
        userName: str,
    ):
        self.userUuid: str = userUuid
        self.userName: str = userName

    @staticmethod
    def create_from_dict(gu_json: dict):
        return ConfigGlobalUser(
            gu_json.get(JsonKeys.GUsersUuid, None),
            gu_json.get(JsonKeys.GUsersName_String, None)
        )

    def get_name(self):
        return self.userName





class ConfigGlobalWindowSettings:

    def __init__(
        self,
        mediaAreaState: str,
        sortShows: str,
        sortShowsAscending: bool,
        sortCommercials: str,
        sortCommercialsAscending: bool,
        sortShowsCmsEdit: str,
        sortShowsCmsEditAscending: bool,
        dynamicShowVolume: bool,
        guiTheme: str = None,
        language: str = None,
        fontSize: int = None
    ):
        self.mediaAreaState: str = mediaAreaState
        self.sortShows: str = sortShows
        self.sortShowsAscending: bool = sortShowsAscending
        self.sortCommercials: str = sortCommercials
        self.sortCommercialsAscending: bool = sortCommercialsAscending
        self.sortShowsCmsEdit: str = sortShowsCmsEdit
        self.sortShowsCmsEditAscending: bool = sortShowsCmsEditAscending
        self.dynamicShowVolume: bool = dynamicShowVolume
        self.guiTheme: str = guiTheme
        self.language: str = language
        self.fontSize: int = fontSize


    @staticmethod
    def create_from_dict(gw_json: dict):
        return ConfigGlobalWindowSettings(
            gw_json.get(JsonKeys.GWindowSettingsMediaAreaState_Int, None),
            gw_json.get(JsonKeys.GWindowSettingsSortShows_Int, None),
            gw_json.get(JsonKeys.GWindowSettingsSortShowsAscending_Boolean, None),
            gw_json.get(JsonKeys.GWindowSettingsSortCommercials_Int, None),
            gw_json.get(JsonKeys.GWindowSettingsSortCommercialsAscending_Boolean, None),
            gw_json.get(JsonKeys.GWindowSettingsSortShowsCmsEdit_Int, None),
            gw_json.get(JsonKeys.GWindowSettingsSortShowsCmsEditAscending_Boolean, None),
            gw_json.get(JsonKeys.GWindowSettingsDynamicShowVolume_Boolean, None),
            gw_json.get(JsonKeys.GWindowSettingsGuiTheme_String, None),
            gw_json.get(JsonKeys.GWindowSettingsLanguage_String, None),
            gw_json.get(JsonKeys.GWindowSettingsFontSize_Int, None)
        )




class ConfigGlobal:

    def __init__(
        self,
        currentUserUuid: str,
        currentBasePathIndex: int,
        useBasePaths: bool,
        users: List[ConfigGlobalUser],
        basePaths: List[str],
        windowSettings: ConfigGlobalWindowSettings,
        excludeFilesEndingWith: List[str] = None,
        noAutosave: bool = None
    ):
        self.currentUserUuid: str = currentUserUuid
        # self.nextUserId: int = nextUserId
        self.currentBasePathIndex: int = currentBasePathIndex
        self.useBasePaths: bool = useBasePaths

        self.users: List[ConfigGlobalUser] = users
        self.basePaths: List[str] = basePaths
        self.windowSettings: ConfigGlobalWindowSettings = windowSettings
        self.excludeFilesEndingWith: List[str] = excludeFilesEndingWith
        self.noAutosave: bool = noAutosave

        if self.currentBasePathIndex is not None:
            if len(self.basePaths) == 0:
                self.currentBasePathIndex = None
                self.useBasePaths = False
            else:
                self.currentBasePathIndex = min(self.currentBasePathIndex, len(self.basePaths) - 1)


    @staticmethod
    def create_from_dict(g_json: dict):
        return ConfigGlobal(
            g_json.get(JsonKeys.GCurrentUserUuid, None),
            g_json.get(JsonKeys.GCurrentBasePath_Index, None),
            g_json.get(JsonKeys.GUseBasePaths_Bool, False),
            list(map(ConfigGlobalUser.create_from_dict, g_json.get(JsonKeys.GUsers_Dicts, []))),
            g_json.get(JsonKeys.GBasePaths_StrArray, []),
            ConfigGlobalWindowSettings.create_from_dict(g_json.get(JsonKeys.GWindowSettings_Dict, dict())),
            g_json.get(JsonKeys.GExcludeFilesEndingWith, []),
            g_json.get(JsonKeys.GNoAutosave_Bool, None)
        )


    def get_user_by_uuid(self, user_uuid: str) -> ConfigGlobalUser:
        return next((item for item in self.users if item.userUuid == user_uuid), None)


    def create_new_user(
        self,
        userName: str,
        userUuid: str = None
    ) ->  ConfigGlobalUser:
        # new_user_id = self.nextUserId
        # self.nextUserId += 1

        new_user_uuid = userUuid
        if new_user_uuid is None:
            new_user_uuid = shortuuid.uuid()
            while os.path.isfile(get_config_user_full_path(new_user_uuid)):
                new_user_uuid = shortuuid.uuid()

        new_user = ConfigGlobalUser(
            new_user_uuid,
            userName
        )

        create_default_user_config_file(new_user.userUuid)
        self.users.append(new_user)
        save_global_config_complete(self)
        return new_user




class ConfigUserIdManagement:

    def __init__(
        self,
        nextProfileId,
        nextShowId
    ):
        self.nextProfileId: int = nextProfileId
        self.nextShowId: int = nextShowId

    @staticmethod
    def create_from_dict(ui_json: dict):
        return ConfigUserIdManagement(
            ui_json.get(JsonKeys.UIdManagementNextProfileId, None),
            ui_json.get(JsonKeys.UIdManagementNextShowId, None)
        )



class ConfigUserProfileSettings:

    def __init__(
        self,
        commercialsPlay: bool,
        commercialsStartWeight: int,
        commercialsBreakWeight: int,
        probability: int,
        shutdown: int,
        showsLimit: int,
        uniqueShows: bool,
        hardUniqueShows: bool,

        # common player settings (settings that exist for profiles and for single shows):
        opening: bool,
        ending: bool,
        # status: bool,
        volume: int,
        speed: int,
        jump: int,
        end: int,
        pause: bool,
        mute: bool,
        fullscreen: bool,
        # queueDisplay: bool,
        suspend: bool,
        suspendQueue: bool,
        noRerun: bool,
        # monitor: int,
        # monitorJump: bool,
        debugPlayerJumps: bool
    ):
        self.commercialsPlay: bool = commercialsPlay    #default True
        self.commercialsStartWeight: int = commercialsStartWeight   #default 0
        self.commercialsBreakWeight: int = commercialsBreakWeight   #default 0
        self.probability: int = probability     #default 100
        self.shutdown: int = shutdown           #default None
        self.showsLimit: int = showsLimit       #default None   # formerly called lastEpisodes
        self.uniqueShows: bool = uniqueShows            #default False
        self.hardUniqueShows: bool = hardUniqueShows    #default False

        # common player settings (settings that exist for profiles and for single shows):
        self.opening: bool = opening    #default False # TODO make default True in Profile Settings, but handle missing op / ed files without crashing
        self.ending: bool = ending      #default False # TODO make default True in Profile Settings, but handle missing op / ed files without crashing
        # self.status: bool = status      #default False  #TODO GUI not needed
        self.volume: int = volume       #default 100
        self.speed: int = speed         #default 100
        self.jump: int = jump           #default 0
        self.end: int = end             #default 0 # TODO implement
        self.pause: bool = pause        #default False
        self.mute: bool = mute          #default False
        self.fullscreen: bool = fullscreen      #default False
        # self.queueDisplay: bool = queueDisplay  #default False  #TODO: GUI not needed
        self.suspend: bool = suspend            #default False  #TODO: make default True
        self.suspendQueue: bool = suspendQueue  #default False  #TODO: make default True
        self.noRerun: bool = noRerun            #default False
        # self.monitor: int = monitor             #default False  #TODO: GUI not needed
        # self.monitorJump: bool = monitorJump    #default False  #TODO: GUI put into planned status bar??
        self.debugPlayerJumps: bool = debugPlayerJumps  #default False  #TODO: make default True


    @staticmethod
    def create_from_dict(ups_json: dict):
        return ConfigUserProfileSettings(
            ups_json.get(JsonKeys.UPPSCommercialsPlay_Bool, None),
            ups_json.get(JsonKeys.UPPSCommercialsStartWeight_Value, None),
            ups_json.get(JsonKeys.UPPSCommercialsBreakWeight_Value, None),
            ups_json.get(JsonKeys.UPPSProbability_Value, None),
            ups_json.get(JsonKeys.UPPSShutdown_Value, None),
            ups_json.get(JsonKeys.UPPSShowsLimit_Value, None),
            ups_json.get(JsonKeys.UPPSUniqueShows_Bool, None),
            ups_json.get(JsonKeys.UPPSHardUniqueShows_Bool, None),

            # common player settings (settings that exist for profiles and for single shows):
            ups_json.get(JsonKeys.UCPSOpening_Bool, None),
            ups_json.get(JsonKeys.UCPSEnding_Bool, None),
            # ups_json.get(JsonKeys.UCPSStatus_Bool, None),
            ups_json.get(JsonKeys.UCPSVolume_Value, None),
            ups_json.get(JsonKeys.UCPSSpeed_Value, None),
            ups_json.get(JsonKeys.UCPSJump_Value, None),
            ups_json.get(JsonKeys.UCPSEnd_Value, None),
            ups_json.get(JsonKeys.UCPSPause_Bool, None),
            ups_json.get(JsonKeys.UCPSMute_Bool, None),
            ups_json.get(JsonKeys.UCPSFullscreen_Bool, None),
            # ups_json.get(JsonKeys.UCPSQueueDisplay_Bool, None),
            ups_json.get(JsonKeys.UCPSSuspend_Bool, None),
            ups_json.get(JsonKeys.UCPSSuspendQueue_Bool, None),
            ups_json.get(JsonKeys.UCPSNoRerun_Bool, None),
            # ups_json.get(JsonKeys.UCPSMonitor_Value, None),
            # ups_json.get(JsonKeys.UCPSMonitorJump_Bool,None),
            ups_json.get(JsonKeys.UCPSDebugPlayerJumps_Bool, None)
        )




class ConfigUserProfile:

    def __init__(
            self,
            profileId: int,
            profileName: str,
            startWithShows: List[int] = [],
            suspendedQueue: List[int] = [],
            playAllShows: bool = None,
            profilesInclude: List[int] = [],
            profilesExclude: List[int] = [],
            profileSettings: ConfigUserProfileSettings = ConfigUserProfileSettings.create_from_dict(dict())
    ):
        self.profileId: int = profileId
        self.profileName: str = profileName
        self.startWithShows: List[int] = startWithShows
        self.suspendedQueue: List[int] = suspendedQueue
        self.playAllShows: bool = playAllShows
        self.profilesInclude: List[int] = None if profilesInclude is None else list(set(profilesInclude))
        self.profilesExclude: List[int] = None if profilesExclude is None else list(set(profilesExclude))

        self.profileSettings: ConfigUserProfileSettings = profileSettings

    @staticmethod
    def create_from_dict(up_json: dict):
        return ConfigUserProfile(
            up_json.get(JsonKeys.UProfilesId, None),
            up_json.get(JsonKeys.UProfilesName_String, None),
            up_json.get(JsonKeys.UProfilesStartWithShows_IdArray, []),
            # default [], kinda overrides suspendedQueue, ignores playAllShows, profileInclude, profileExclude
            up_json.get(JsonKeys.UProfilesSuspendedQueue_IdArray, []),
            up_json.get(JsonKeys.UProfilesPlayAllShows_Bool, None),
            # default False, overrides include and exclude
            up_json.get(JsonKeys.UProfilesInclude_IdArray, []),  # default []
            up_json.get(JsonKeys.UProfilesExclude_IdArray, []),  # default [], overpowers include

            ConfigUserProfileSettings.create_from_dict(up_json.get(JsonKeys.UProfilesSettings_Dict, dict()))
        )

    # def get_name(self):
    #     return self.profileName

    def __str__(self):
        return_string = "{"
        for property, value in vars(self).items():
            if value is not None:
                return_string += (property + ": " + str(value) + "; ")
        return return_string + "}"




class ConfigUserShowSettings:

    def __init__(
        self,
        random: bool,
        multipleEpisodes: int,
        probability: int,
        noCommercialsAfter: bool,
        noCommercialsBefore: bool,
        commercialWeight: int,
        commercialMax: int,
        commercialMin: int,
        videoTrack: int,
        audioTrack: int,
        subtitleTrack: int,
        recursionLevel: int,

        opening: bool,
        ending: bool,
        # status: bool,
        volume: int,
        speed: int,
        jump: int,
        end: int,
        pause: bool,
        mute: bool,
        fullscreen: bool,
        # queueDisplay: bool,
        suspend: bool,
        suspendQueue: bool,
        noRerun: bool,
        # monitor: int,
        # monitorJump: bool,
        debugPlayerJumps: bool,
    ):
        self.random: bool = random
        self.multipleEpisodes: int = multipleEpisodes
        self.probability: int = probability
        self.noCommercialsAfter: bool = noCommercialsAfter
        self.noCommercialsBefore: bool = noCommercialsBefore
        self.commercialWeight: int = commercialWeight
        self.commercialMax: int = commercialMax
        self.commercialMin: int = commercialMin
        self.videoTrack: int = videoTrack
        self.audioTrack: int = audioTrack
        self.subtitleTrack: int = subtitleTrack
        self.recursionLevel: int = recursionLevel

        # common player settings (settings that exist for profiles and for single shows):
        self.opening: bool = opening
        self.ending: bool = ending
        # self.status: bool = None
        self.volume: int = volume
        self.speed: int = speed
        self.jump: int = jump
        self.end: int = end
        self.pause: bool = pause
        self.mute: bool = mute
        self.fullscreen: bool = fullscreen
        # self.queueDisplay: bool = None
        self.suspend: bool = suspend
        self.suspendQueue: bool = suspendQueue
        self.noRerun: bool = noRerun
        # self.monitor: int = None
        # self.monitorJump: bool = None
        self.debugPlayerJumps: bool = debugPlayerJumps


    @staticmethod
    def create_from_dict(uss_json: dict):
        return ConfigUserShowSettings(
            uss_json.get(JsonKeys.USPSRandom_Bool, None),
            uss_json.get(JsonKeys.USPSMultipleEpisodes_Value, None),
            uss_json.get(JsonKeys.USPSProbability_Value, None),
            uss_json.get(JsonKeys.USPSNoCommercialsAfter_Bool, None),
            uss_json.get(JsonKeys.USPSNoCommercialsBefore_Bool, None),
            uss_json.get(JsonKeys.USPSCommercialWeight_Value, None),
            uss_json.get(JsonKeys.USPSCommercialMax_Value, None),
            uss_json.get(JsonKeys.USPSCommercialMin_Value, None),
            uss_json.get(JsonKeys.USPSVideoTrack_Int, None),
            uss_json.get(JsonKeys.USPSAudioTrack_Int, None),
            uss_json.get(JsonKeys.USPSSubtitleTrack_Int, None),
            uss_json.get(JsonKeys.USPSRecursionLevel_Int, None),

            # common player settings (settings that exist for profiles and for single shows):
            uss_json.get(JsonKeys.UCPSOpening_Bool, None),
            uss_json.get(JsonKeys.UCPSEnding_Bool, None),
            # uss_json.get(JsonKeys.UCPSStatus_Bool, None),
            uss_json.get(JsonKeys.UCPSVolume_Value, None),
            uss_json.get(JsonKeys.UCPSSpeed_Value, None),
            uss_json.get(JsonKeys.UCPSJump_Value, None),
            uss_json.get(JsonKeys.UCPSEnd_Value, None),
            uss_json.get(JsonKeys.UCPSPause_Bool, None),
            uss_json.get(JsonKeys.UCPSMute_Bool, None),
            uss_json.get(JsonKeys.UCPSFullscreen_Bool, None),
            # uss_json.get(JsonKeys.UCPSQueueDisplay_Bool, None),
            uss_json.get(JsonKeys.UCPSSuspend_Bool, None),
            uss_json.get(JsonKeys.UCPSSuspendQueue_Bool, None),
            uss_json.get(JsonKeys.UCPSNoRerun_Bool, None),
            # uss_json.get(JsonKeys.UCPSMonitor_Value, None),
            # uss_json.get(JsonKeys.UCPSMonitorJump_Bool, None),
            uss_json.get(JsonKeys.UCPSDebugPlayerJumps_Bool, None)
        )


    @staticmethod
    def create_empty():
        return ConfigUserShowSettings(
            None, None, None, None, None, None, None, None, None, None, None, None,
            None, None, None, None, None, None, None, None, None, None, None, None, None
        )

    def __str__(self):
        return_string = "{"
        for property, value in vars(self).items():
            if value is not None:
                return_string += (property + ": " + str(value) + "; ")
        return return_string + "}"



class ConfigUserShow:

    def __init__(
        self,
        showId: int,
        showName: str,
        isActive: bool,
        doNotUseBasePath: bool,
        directoryPath: str,
        activeProfiles: List[int],
        firstEpisode: int,
        lastEpisode: int,
        currentEpisode: int,
        suspendedTimeSeconds: int,
        tags: List[str],
        showSettings: ConfigUserShowSettings
    ):
        self.showId: int = showId
        self.showName: str = showName
        self.isActive: bool = isActive
        self.doNotUseBasePath: bool = doNotUseBasePath
        self.directoryPath: str = directoryPath
        self.activeProfiles: List[int] = activeProfiles
        self.firstEpisode: int = firstEpisode
        self.lastEpisode: int = lastEpisode
        self.currentEpisode: int = currentEpisode
        self.suspendedTimeSeconds: int = suspendedTimeSeconds
        self.tags: List[str] = tags

        self.showSettings: ConfigUserShowSettings = showSettings


    @staticmethod
    def create_from_dict(us_json: dict):
        return ConfigUserShow(
            us_json.get(JsonKeys.UShowsId, None),
            us_json.get(JsonKeys.UShowsName_String, None),
            us_json.get(JsonKeys.UShowsIsActive_Bool, None),
            us_json.get(JsonKeys.UShowsDoNotUseBasePath_Bool, None),
            us_json.get(JsonKeys.UShowsDirectoryPath_String, None),
            list(us_json.get(JsonKeys.UShowsActiveProfiles_IdArray, [])),
            us_json.get(JsonKeys.UShowsFirstEpisode_Int, None),
            us_json.get(JsonKeys.UShowsLastEpisode_Int, None),
            us_json.get(JsonKeys.UShowsCurrentEpisode_Int, None),
            us_json.get(JsonKeys.UShowsSuspendedTimeSeconds_Int, None),
            us_json.get(JsonKeys.UShowsTags_StrArray, []),

            ConfigUserShowSettings.create_from_dict(us_json.get(JsonKeys.UShowsSettings_Dict, dict()))
        )


    def to_string(self, episode_number: int = None) -> str:
        if episode_number == None:
            episode_number = self.currentEpisode
        return f'{self.showName} episode {episode_number}'
    
    def __str__(self):
        return_string = ""
        for property, value in vars(self).items():
            if isinstance(value, ConfigUserShowSettings):
                return_string += (property + ":\n\t" + str(value))
            else:
                return_string += (property + ": " + str(value) + ";\n")
        return return_string
        # return self.to_string(self.currentEpisode)
    
    def generate_first_and_last_episode_tmp_ints(self, global_config: ConfigGlobal, force_recalculate = True):
        self.firstEpisode_tmp = self.firstEpisode if self.firstEpisode != None else 1
        if self.lastEpisode is not None:
            self.lastEpisode_tmp = self.lastEpisode
        elif hasattr(self, "lastEpisode_tmp") and not force_recalculate:
            return
        else:
            self.lastEpisode_tmp = self.lastEpisode if self.lastEpisode != None else show_get_last_episode_calculated(self, global_config)
            self.lastEpisode_tmp = self.firstEpisode_tmp if self.lastEpisode_tmp is None else self.lastEpisode_tmp

    def get_episodes_amount_tmp(self, global_config: ConfigGlobal, force_recalculate = True):
        self.generate_first_and_last_episode_tmp_ints(global_config, force_recalculate)
        return self.lastEpisode_tmp - self.firstEpisode_tmp

    def randomize_current_ep(self, global_config: ConfigGlobal):
        self.generate_first_and_last_episode_tmp_ints(global_config)
        self.currentEpisode = randint(self.firstEpisode_tmp, self.lastEpisode_tmp)
    
    # needed only for commercials:
    def generate_instance_next_episode(self, global_config: ConfigGlobal):
        self.generate_first_and_last_episode_tmp_ints(global_config)
        return_instance = deepcopy(self)
        if return_instance.showSettings.random:
            return_instance.randomize_current_ep(global_config)
        else:
            return_instance.currentEpisode = self.get_next_episode_number(self.firstEpisode_tmp, self.lastEpisode_tmp, self.currentEpisode)
        return return_instance

    # needed only for commercials:
    def generate_next_episode(self, global_config: ConfigGlobal) -> int:
        self.generate_first_and_last_episode_tmp_ints(global_config)
        if self.currentEpisode >= self.lastEpisode_tmp:
            return self.firstEpisode_tmp
        else:
            return self.currentEpisode + 1

    # needed only for commercials:
    def get_next_episode_number(self, first, last, current) -> int:
        if current >= last:
            next_ep = first
        else:
            next_ep = current + 1
        return next_ep
    
    def get_is_commercial(self):
        return self.showSettings.commercialWeight is not None
    
    def get_is_show(self):
        return not self.get_is_commercial()


    def get_tags_frequency_highest(self, tmp_tags_list_counted):
        amounts = [0]
        tags_only = list(map(lambda x: x[0], tmp_tags_list_counted))

        for tag in self.tags:
            try:
                index = tags_only.index(tag)
                amounts.append(tmp_tags_list_counted[index][1])
            except ValueError:
                pass

        return max(amounts)

    def get_tags_frequency_sum(self, tmp_tags_list_counted):
        amounts = [0]
        tags_only = list(map(lambda x: x[0], tmp_tags_list_counted))

        for tag in self.tags:
            try:
                index = tags_only.index(tag)
                amounts.append(tmp_tags_list_counted[index][1])
            except ValueError:
                pass

        return sum(amounts)



class ConfigUser:

    def __init__(
        self,
        currentProfileId: int,
        idManagement: ConfigUserIdManagement,
        profiles: List[ConfigUserProfile],
        shows: List[ConfigUserShow]
    ):
        self.currentProfileId: int = currentProfileId
        self.idManagement: ConfigUserIdManagement = idManagement
        self.profiles: List[ConfigUserProfile] = profiles
        self.shows: List[ConfigUserShow] = shows

        self.tmp_all_tags_even_unused_of_session = set()
        self.tmp_tags_list = list()
        self.tmp_tags_list_counted: List[str, int] = list()
        self.tmp_tags_list_distinct = list()


    @staticmethod
    def create_from_dict(u_json: dict):
        return ConfigUser(
            u_json.get(JsonKeys.UCurrentProfile_Id, None),
            ConfigUserIdManagement.create_from_dict(u_json.get(JsonKeys.UIdManagement_Dict, None)),
            list(map(ConfigUserProfile.create_from_dict, u_json.get(JsonKeys.UProfiles_Dicts, []))),
            list(map(ConfigUserShow.create_from_dict, u_json.get(JsonKeys.UShows_Dicts, [])))
        )

    def index_of_show_by_id(self, show_id: int):
        for show in self.shows:
            if show.showId == show_id:
                return self.shows.index(show)
        else:
            print(f'    {color.GREEN}ER{color.END}{color.BOLD}{color.BLUE}RO{color.END}{color.BOLD}{color.RED}R P{color.END}{color.BOLD}{color.PURPLE}LA{color.END}{color.BOLD}{color.CYAN}YE{color.END}{color.BOLD}{color.YELLOW}R{color.END} oh shiat') # this should never happen
            return -1
    
    def index_of_profile_by_id(self, profile_id: int):
        for profile in self.profiles:
            if profile.profileId == profile_id:
                return self.profiles.index(profile)
        else:
            print(f'    {color.GREEN}ER{color.END}{color.BOLD}{color.BLUE}RO{color.END}{color.BOLD}{color.RED}R P{color.END}{color.BOLD}{color.PURPLE}LA{color.END}{color.BOLD}{color.CYAN}YE{color.END}{color.BOLD}{color.YELLOW}R{color.END} oh sheeet') # this should never happen
            return -1

    def get_profile_by_id(self, profileId: int) -> ConfigUserProfile:
        return_profile = next((item for item in self.profiles if item.profileId == profileId), None)
        return return_profile

    def get_profiles_by_ids(self, profile_ids: [int]) -> List[ConfigUserProfile]:
        selected_profiles = []
        for profile_id in profile_ids:
            nextProfile = self.get_profile_by_id(profile_id)
            if nextProfile is not None:
                selected_profiles.append(nextProfile)
        return selected_profiles

    def get_profile_by_name(self, profileName: str) -> ConfigUserProfile:
        return next((item for item in self.profiles if item.profileName == profileName), None)

    def get_current_profile(self) -> ConfigUserProfile:
        loaded_profile = self.get_profile_by_id(self.currentProfileId)
        if loaded_profile is None:
            self.currentProfileId = self.profiles[0].profileId
            loaded_profile = self.get_profile_by_id(self.currentProfileId)
        return loaded_profile

    def get_show_by_id(self, showId: int) -> ConfigUserShow:
        return next((item for item in self.shows if item.showId == showId), None)

    def get_shows_by_ids(self, show_ids: [int]) -> List[ConfigUserShow]:
        selected_shows = []
        for show_id in show_ids:
            nextShow = self.get_show_by_id(show_id)
            if nextShow is not None:
                selected_shows.append(nextShow)
        return selected_shows

    def get_show_by_name(self, showName: str) -> ConfigUserShow:
        return next((item for item in self.shows if item.showName == showName), None)

    def get_suspended_shows_by_profile_id(self, profileId: int) -> List[ConfigUserShow]:
        return self.get_shows_by_ids(self.get_profile_by_id(profileId).suspendedQueue)

    def get_start_with_shows_by_profile_id(self, profileId: int) -> List[ConfigUserShow]:
        return self.get_shows_by_ids(self.get_profile_by_id(profileId).startWithShows)

    def load_all_tags(self):
        self.tmp_tags_list = []
        for show in self.shows:
            self.tmp_tags_list = self.tmp_tags_list + show.tags
        self.tmp_tags_list = sorted(self.tmp_tags_list)

        self.tmp_tags_list_distinct = sorted(set(self.tmp_tags_list))

        self.tmp_tags_list_counted = []
        all_tags_with_duplicates_tmp = deepcopy(self.tmp_tags_list)
        while len(all_tags_with_duplicates_tmp) > 0:
            next_tag = all_tags_with_duplicates_tmp[0]
            tag_count = all_tags_with_duplicates_tmp.count(next_tag)
            self.tmp_tags_list_counted.append((next_tag, tag_count))
            while next_tag in all_tags_with_duplicates_tmp:
                all_tags_with_duplicates_tmp.remove(next_tag)
        for tag in self.tmp_all_tags_even_unused_of_session:
            if tag not in self.tmp_tags_list_distinct:
                self.tmp_tags_list_counted.append((tag, 0))

        self.tmp_all_tags_even_unused_of_session = set(list(self.tmp_all_tags_even_unused_of_session) + self.tmp_tags_list_distinct)


    def add_new_tag(self, new_tag: str):
        if new_tag in self.tmp_all_tags_even_unused_of_session:
            return

        self.tmp_tags_list.append(new_tag)
        self.tmp_tags_list_counted.append((new_tag, 0))
        self.tmp_tags_list_distinct = sorted(set(self.tmp_tags_list))
        self.tmp_all_tags_even_unused_of_session.add(new_tag)

    def create_new_profile(
        self,
        profileName: str
    ) ->  ConfigUserProfile:
        new_profile_id = self.idManagement.nextProfileId
        self.idManagement.nextProfileId += 1

        new_profile = ConfigUserProfile(
            new_profile_id,
            profileName
        )

        self.profiles.append(new_profile)
        return new_profile


    def create_new_show(
        self,
        # showId: int,
        showName: str,
        isActive: bool,
        doNotUseBasePath: bool,
        directoryPath: str,
        activeProfiles: List[int],
        firstEpisode: int,
        lastEpisode: int,
        currentEpisode: int,
        # suspendedTimeSeconds: int,
        tags: List[str],
        # showSettings: ConfigUserShowSettings
    ) ->  ConfigUserShow:
        new_show_id = self.idManagement.nextShowId
        self.idManagement.nextShowId += 1

        new_show = ConfigUserShow(
            new_show_id,
            showName,
            isActive,
            doNotUseBasePath,
            directoryPath,
            activeProfiles,
            firstEpisode,
            lastEpisode,
            currentEpisode,
            None,
            tags,
            ConfigUserShowSettings.create_empty()
        )

        self.shows.append(new_show)

        return new_show


    def duplicate_profile(self, profile: ConfigUserProfile, new_name: str):
        new_profile = deepcopy(profile)
        new_profile.profileName = new_name

        new_profile_id = self.idManagement.nextProfileId
        self.idManagement.nextProfileId += 1
        new_profile.profileId = new_profile_id

        self.profiles.append(new_profile)

        for show in self.shows:
            if profile.profileId in show.activeProfiles:
                show.activeProfiles.append(new_profile_id)

        gui_event_profiles_update_combo.set()

        # return new_profile

    def duplicate_show(self, show: ConfigUserShow, new_name: str):
        new_show = deepcopy(show)
        new_show.showName = new_name

        new_show_id = self.idManagement.nextShowId
        self.idManagement.nextShowId += 1
        new_show.showId = new_show_id

        self.shows.append(new_show)

        gui_event_shows_update_table.set()
        gui_event_cms_update_table.set()
        gui_event_shows_cms_edit_list_update_list.set()

        # return new_show








class MainSetup:
    """ Holds the global_config, user_config, session_config and the queue_shows.
    """

    def __init__(self, config_global_full_path: str, user_uuid: str = None):
        self.global_config: ConfigGlobal = ConfigGlobal.create_from_dict(load_json_to_dict(config_global_full_path))
        if user_uuid is not None:
            self.global_config.currentUserUuid = user_uuid
        self.load_current_user()


    def load_current_user(self):
        self.load_user(self.global_config.currentUserUuid)


    def load_user(self, user_uuid: str):
        self.user_config: ConfigUser = ConfigUser.create_from_dict(load_json_to_dict(get_config_user_full_path(user_uuid)))
        self.currently_running_show: ConfigUserShow = None
        self.currently_running_show_fullpath: str = None
        self.load_current_profile()


    def load_current_profile(self):
        self.session_config: MainSetupSession = MainSetupSession()
        if hasattr(self, "queue_shows"):
            self.queue_shows: MainSetupQueue = MainSetupQueue(self.queue_shows.startedPlaybacks)
        else:
            self.queue_shows: MainSetupQueue = MainSetupQueue()

        self.populate_suspended_queue()
        printdev("current queue ids suspended: " + str(self.queue_shows.queue))
        self.add_start_with_shows()
        self.remove_broken_show_ids()
        self.populate_automatic()
        printdev("current queue ids post automatic: " + str(self.queue_shows.queue))

        gui_event_queue_update_table.set()
        gui_event_shows_update_table.set()
        gui_event_history_update_table.set()
        gui_event_cms_update_table.set()
        gui_event_shows_cms_edit_list_update_list.set()

        self.currently_running_show: ConfigUserShow = None
        self.currently_running_show_fullpath: str = None


    def load_profile(self, profile_id: int):
        self.user_config.currentProfileId = profile_id
        printdev("noe profileId: " + str(self.user_config.currentProfileId))
        self.load_current_profile()


    def populate_suspended_queue(self):
        """Call this when the queue is created before doing anything else.
        """
        current_profile = self.get_current_profile()
        self.queue_shows.queue = deepcopy(current_profile.suspendedQueue)
        # current_profile.suspendedQueue = [] # do this on play start instead of here


    def add_start_with_shows(self):
        """Call this after populate_suspended_queue.
        """
        current_profile = self.get_current_profile()
        if current_profile.startWithShows is None or current_profile.startWithShows == [] or len(current_profile.startWithShows) == 0:
            return

        shows_to_add = deepcopy(current_profile.startWithShows)

        if self.queue_shows.queue is None or self.queue_shows.queue == [] or len(self.queue_shows.queue) == 0:
            self.queue_shows.queue = shows_to_add
        else:
            print("ssuspended queue")
            print(self.queue_shows.queue)

            # avoid duplicate startShowsWith from last session
            check_until = min(len(shows_to_add), len(self.queue_shows.queue))
            best_guess_amount = 0
            for i in range(check_until):
                if self.queue_shows.queue[i] == shows_to_add[-1]:
                    print("last show found! at index " + str(i))
                    for k in range(i):
                        if self.queue_shows.queue[i - k] != shows_to_add[-1 - k]:
                            break
                    else: # loop was successfully passed completely without a break-statement stopping it, so some end part of shows_to_add is at the beginning of the queue
                        best_guess_amount = i + 1

            if best_guess_amount == 0:
                print("add all")
                final_shows_to_add = shows_to_add
            else:
                print("selection: " + str(best_guess_amount))
                final_shows_to_add = shows_to_add[0: -best_guess_amount]

            print("final_shows_to_add")
            print(final_shows_to_add)

            self.queue_shows.queue = final_shows_to_add + self.queue_shows.queue


    def remove_broken_profile_ids(self):
        for profile in self.user_config.profiles:

            remove_ids = []
            for profile_id in profile.profilesInclude:
                possible_profile = self.get_profile_by_id(profile_id)
                if possible_profile is None:
                    remove_ids.append(profile_id)
            for remove_id in remove_ids:
                profile.profilesInclude.remove(remove_id)

            remove_ids = []
            for profile_id in profile.profilesExclude:
                possible_profile = self.get_profile_by_id(profile_id)
                if possible_profile is None:
                    remove_ids.append(profile_id)
            for remove_id in remove_ids:
                profile.profilesExclude.remove(remove_id)

    def remove_broken_show_ids(self):
        remove_ids = []
        for show_id in self.queue_shows.queue:
            possible_show = self.get_show_by_id(show_id)
            if possible_show is None:
                remove_ids.append(show_id)
        for remove_id in remove_ids:
            self.queue_shows.queue.remove(remove_id)

    def remove_broken_show_ids_in_profiles(self):
        for profile in self.user_config.profiles:

            remove_ids = []
            for show_id in profile.suspendedQueue:
                possible_show = self.get_show_by_id(show_id)
                if possible_show is None:
                    remove_ids.append(show_id)
            for remove_id in remove_ids:
                profile.suspendedQueue.remove(remove_id)

            remove_ids = []
            for show_id in profile.startWithShows:
                possible_show = self.get_show_by_id(show_id)
                if possible_show is None:
                    remove_ids.append(show_id)
            for remove_id in remove_ids:
                profile.startWithShows.remove(remove_id)


    def get_profile_by_id(self, profile_id: int) -> ConfigUserProfile:
        return self.user_config.get_profile_by_id(profile_id)


    def get_current_profile_and_id(self) -> (ConfigUserProfile, int):
        current_profile_id = self.user_config.currentProfileId
        current_profile = self.user_config.get_current_profile()
        return (current_profile, current_profile_id)


    def get_current_profile(self) -> ConfigUserProfile:
        return self.user_config.get_current_profile()


    def get_current_profile_settings(self) -> ConfigUserProfileSettings:
        return self.user_config.get_current_profile().profileSettings


    def populate_automatic(self, amount: int = None, update_gui_event_set: bool = True):
        """Populates queue with shows to be played.

        If no amount given, it will be filled up until it holds QUEUE_SIZE_STANDARD many episodes.
        """

        if amount == None and len(self.queue_shows.queue) >= QUEUE_SIZE_STANDARD:
            return

        # get all shows that can currently be played for this profile, exclude commercials
        shows_current_profile = self.get_all_active_show_ids_for_current_profile()
        current_profile = self.get_current_profile()

        if len(shows_current_profile) == 0:
            #TODO?: handle no shows for this profile (maybe done with this)
            return

        # ids to choose from are filtered if 'unique' global option is set
        unique_hard_bool = self.get_current_profile_settings().hardUniqueShows
        unique_soft_bool = self.get_current_profile_settings().uniqueShows

        if unique_hard_bool or unique_soft_bool:
            # don't allow shows where an episode already played
            filtered_shows = []
            for show_id in shows_current_profile:
                if not show_id in self.queue_shows.queue:
                    filtered_shows.append(show_id)
            shows_current_profile = filtered_shows
        # TODO: GUI: if unique (or hard unique), show already played shows in GUI as greyed out??

        queue_id_array = self.queue_shows.queue
        initial_queue_size = len(queue_id_array)
        while True:
            if amount == None and len(queue_id_array) >= QUEUE_SIZE_STANDARD:
                # return # done filling the queue
                break
            elif amount != None and len(queue_id_array) >= initial_queue_size + amount:
                # return # done filling the queue
                break
            elif len(shows_current_profile) == 0: # not done with the queue, but no shows left
                if unique_hard_bool:
                    message = f'QUEUE CAN\'T BE FILLED ANY MORE - global option \'{"HARD_UNIQUE_SHOWS_OPTION_GLOBAL"}\' doesn\'t allow shows to be played more than once in a session.'
                    print(message)
                    break # no aliases left means the config is messed up or the 'unique' or 'uunique' global option is set and there are no shows left for now
                elif amount != None and unique_soft_bool:
                    shows_current_profile = self.get_all_active_show_ids_for_current_profile()
                    self.queue_shows.alreadyPlayed = [] # empty the alreadyPlayed array
                    # TODO: check later during playback if unique that queue entries shouldn't play if id present in alreadyPlayed Array
                else:
                    shows_current_profile = self.get_all_active_show_ids_for_current_profile()

            # the return value here might carry the error message -1
            chosen_show_id = self.add_entry_from_show_ids(shows_current_profile, add_to_front=False, probability_dropping=True, probability_global=current_profile.profileSettings.probability)

            if chosen_show_id == -1: #show was probability-dropped
                continue

            shows_current_profile.remove(chosen_show_id)

        if update_gui_event_set:
            gui_event_queue_update_table.set()


    def get_all_active_show_ids_for_current_profile(self) -> [int]:
        return self.get_all_active_show_or_cm_ids_for_current_profile(False)

    def get_all_active_shows_for_current_profile(self) -> List[ConfigUserShow]:
        return list(
            map(
                self.get_show_by_id,
                self.get_all_active_show_ids_for_current_profile()
            )
        )


    def get_all_active_cm_ids_for_current_profile(self) -> [int]:
        return self.get_all_active_show_or_cm_ids_for_current_profile(True)

    def get_all_active_cms_for_current_profile(self) -> [ConfigUserShow]:
        return list(
            map(
                self.get_show_by_id,
                self.get_all_active_cm_ids_for_current_profile()
            )
        )


    def get_all_active_show_and_cm_ids_for_current_profile(self) -> [int]:
        return self.get_all_active_show_or_cm_ids_for_current_profile(None)

    def get_all_active_shows_and_cms_for_current_profile(self) -> [ConfigUserShow]:
        return list(
            map(
                self.get_show_by_id,
                self.get_all_active_show_and_cm_ids_for_current_profile()
            )
        )


    def get_all_active_show_or_cm_ids_for_current_profile(self, commercials_instead_of_shows: bool = False) -> [int]:
        """Get all showIds playing in the current profile
        """

        current_profile, current_profile_id = self.get_current_profile_and_id()

        # prepare to handle profileInclude, profileExclude, playAllShows
        profile_include_ids = set()
        profile_exclude_ids = set()
        if not current_profile.playAllShows:
            profile_include_ids = set(current_profile.profilesInclude)
            profile_exclude_ids = set(current_profile.profilesExclude)
        profile_include_ids.add(current_profile_id)

        filtered_show_ids = []

        for show_obj in self.user_config.shows:
            # remove shows that are not active
            if not show_obj.isActive:
                continue

            # remove commercials, or shows, depends on specified boolean parameter
            show_commercial_weight = show_obj.showSettings.commercialWeight
            show_cm_decider = (
                (
                    commercials_instead_of_shows == False
                    and show_commercial_weight == None
                )
                or (
                    commercials_instead_of_shows == True
                    and show_commercial_weight != None
                )
                or commercials_instead_of_shows == None
            )
            if show_cm_decider:
                # handle activeProfiles, playAllShows, profileInclude, profileExclude
                show_active_profiles = set(show_obj.activeProfiles)
                sp_included = show_active_profiles.intersection(profile_include_ids)
                sp_excluded = show_active_profiles.intersection(profile_exclude_ids)
                if (
                    show_active_profiles == {0}
                    or current_profile.playAllShows
                    or (
                        len(sp_included) > 0
                        and len(sp_excluded) == 0
                    )
                ):
                    filtered_show_ids.append(show_obj.showId)

        return filtered_show_ids
    

    def get_all_active_shows_or_cms_for_current_profile(self, commercials_instead_of_shows: bool = False) -> List[ConfigUserShow]:
        """Get all shows playing in the current profile. This implementation is a striaght copy of get_all_active_show_or_cm_ids_for_current_profile with one small change in the second to last line and some renaming.
        """

        current_profile, current_profile_id = self.get_current_profile_and_id()

        # prepare to handle profileInclude, profileExclude, playAllShows
        profile_include_ids = set()
        profile_exclude_ids = set()
        if not current_profile.playAllShows:
            profile_include_ids = set(current_profile.profilesInclude)
            profile_exclude_ids = set(current_profile.profilesExclude)
        profile_include_ids.add(current_profile_id)

        filtered_shows = []

        for show_obj in self.user_config.shows:
            # remove shows that are not active
            if not show_obj.isActive:
                continue

            # remove commercials, or shows, depends on specified boolean parameter
            show_commercial_weight = show_obj.showSettings.commercialWeight
            show_cm_decider = (
                (
                    commercials_instead_of_shows == False
                    and show_commercial_weight == None
                )
                or (
                    commercials_instead_of_shows == True
                    and show_commercial_weight != None
                )
                or commercials_instead_of_shows == None
            )
            if show_cm_decider:
                # handle activeProfiles, playAllShows, profileInclude, profileExclude
                show_active_profiles = set(show_obj.activeProfiles)
                sp_included = show_active_profiles.intersection(profile_include_ids)
                sp_excluded = show_active_profiles.intersection(profile_exclude_ids)
                if (
                    show_active_profiles == {0}
                    or current_profile.playAllShows
                    or (
                        len(sp_included) > 0
                        and len(sp_excluded) == 0
                    )
                ):
                    filtered_shows.append(show_obj)

        return filtered_shows


    def add_entry_from_show_ids(self, show_ids: [int], add_to_front: bool=False, probability_dropping: bool=False, probability_global: int = None) -> int:
        if probability_global == None:
            probability_global = 100

        # get random show from passed show_ids
        show_id = get_random_id_from_array(show_ids)

        if probability_dropping:
            show_obj = self.get_show_by_id(show_id)
            show_probability = show_obj.showSettings.probability

            if show_probability != None or probability_global < 100:
                if show_probability == None:
                    prob = probability_global # use global if no show probability set
                else:
                    prob = show_probability
                random_int = randint(1, 100)
                if prob < random_int:
                    return -1

        if add_to_front:
            self.queue_shows.queue.insert(0, show_id)
        else:
            self.queue_shows.queue.append(show_id)
        return show_id


    def get_user_by_uuid(self, user_uuid: str) -> ConfigGlobalUser:
        return self.global_config.get_user_by_uuid(user_uuid)


    def get_show_by_id(self, show_id: int) -> ConfigUserShow:
        return self.user_config.get_show_by_id(show_id)


    def get_shows_by_ids(self, show_ids: [int]) -> List[ConfigUserShow]:
        return self.user_config.get_shows_by_ids(show_ids)


    def get_next_show_id(self) -> int:
        # if len(self.queue_dict) > 0 and not episode_full_restart_event.is_set() and not skip_straight_to_next_episode_event.is_set():
        #     self.queue_dict[JsonKeys.QQueue_IdArray].pop(0) #first entry in the queue is the episode that is playing at the moment, so the one in the first place was just played in this case
        # ^ pop queue entry 0 at end of episode instead of here

        self.populate_automatic()

        printdev("self.queue_shows.queue " + str(self.queue_shows.queue))

        if len(self.queue_shows.queue) == 0:
            return None

        next_show_id = self.queue_shows.queue[0]

        return next_show_id


    def delete_show(self, show: ConfigUserShow):
        if show is None:
            printunexpected("show is None")
        self.user_config.shows.remove(show)
        self.remove_broken_show_ids()
        self.remove_broken_show_ids_in_profiles()
        self.populate_automatic()
        updatedShowUpdateGui()

    def delete_show_by_id(self, showId: int):
        self.delete_show(self.get_show_by_id(showId), None)


    def delete_profile(self, profile: ConfigUserProfile):
        if profile is None:
            printunexpected("profile is None")
        self.user_config.profiles.remove(profile)
        if self.user_config.currentProfileId is profile.profileId:
            self.user_config.currentProfileId = self.user_config.profiles[0].profileId
            self.load_current_profile()
        self.remove_broken_profile_ids()

    def delete_profile_by_id(self, profileId: int):
        self.delete_profile(self.get_profile_by_id(profileId), None)


    def delete_user_by_uuid(self, userUuid: str):
        # delete file
        if not self.global_config.noAutosave:
            delete_user_config_by_uuid(userUuid)

        # delete in ConfigGlobal
        delete_user = next((u for u in self.global_config.users if u.userUuid == userUuid), None)
        self.global_config.users.remove(delete_user)

        # switch to some other user in gui
        self.global_config.currentUserUuid = self.global_config.users[0].userUuid
        self.load_current_user()

        if not self.global_config.noAutosave:
            save_global_config_complete(self.global_config)


    def clear_queue(self):
        """Removes all but the first element of the queue. Also removes last one if playing commercial.

        This is because the first entry is supposed to be the show currently playing."""
        while len(self.queue_shows.queue) > 1:
            self.queue_shows.queue.pop(1)
        if playing_commercials_event.is_set():
            self.queue_shows.queue.pop(0)

        gui_event_queue_update_table.set()


    def pop_queue(self, index_pop_list: [int] = None, update_gui_event_set: bool = True):
        """Removes last element of the queue if no index given, else entry at index. No effect if only one remaining or if index is 0. except if playing commercial.

        This is because the first entry is supposed to be the show currently playing."""
        can_remove_index_zero = playing_commercials_event.is_set() or not session_running_event.is_set()

        if index_pop_list is None or len(index_pop_list) == 0:
            if len(self.queue_shows.queue) > 1:
                self.queue_shows.queue.pop(-1)
            elif len(self.queue_shows.queue) == 1 and can_remove_index_zero:
                self.queue_shows.queue.pop(-1)
        elif len(index_pop_list) == 1 and index_pop_list[0] < len(self.queue_shows.queue):
            if index_pop_list[0] > 0:
                self.queue_shows.queue.pop(index_pop_list[0])
            if index_pop_list[0] == 0 and can_remove_index_zero:
                self.queue_shows.queue.pop(0)
        else: # len(index_pop_list) > 1
            reverse_sorted_indexes = sorted(index_pop_list)
            reverse_sorted_indexes.reverse()
            # print(reverse_sorted_indexes)
            for index in reverse_sorted_indexes:
                # print(index)
                if index > 0:
                    self.queue_shows.queue.pop(index)
                if index == 0 and can_remove_index_zero:
                    self.queue_shows.queue.pop(0)

        if update_gui_event_set:
            gui_event_queue_update_table.set()


    def queue_move_up(self, move_up_list: List[int] = None, update_gui_event_set: bool = True) -> List[int]:
        """Moves selected elements up by one, bringing them closer to playback."""
        can_remove_index_zero = playing_commercials_event.is_set() or not session_running_event.is_set()

        if move_up_list is None or len(move_up_list) == 0:
            return []

        # avoid moving a selection at the very top
        dropped_indexes = []
        limit = 0
        if not can_remove_index_zero:
            limit = 1
            if 0 in move_up_list:
                move_up_list.remove(0)
        for i in range(limit, len(self.queue_shows.queue)):
            if i not in move_up_list:
                break
            else:
                dropped_indexes.append(i)
                move_up_list.remove(i)


        if len(move_up_list) == 0:
            return dropped_indexes

        # move
        sorted_indexes = sorted(move_up_list)
        # print(sorted_indexes)
        for index in sorted_indexes:
            # print(index)
            self.queue_shows.queue.insert(index - 1, self.queue_shows.queue[index])
            self.queue_shows.queue.pop(index + 1)

        if update_gui_event_set:
            gui_event_queue_update_table.set()

        return dropped_indexes + list(map(lambda x: x - 1, sorted_indexes))


    def queue_move_down(self, move_down_list: List[int] = None, update_gui_event_set: bool = True):
        """Moves selected elements down by one, bringing them further away from playback."""
        can_remove_index_zero = playing_commercials_event.is_set() or not session_running_event.is_set()

        if move_down_list is None or len(move_down_list) == 0:
            return

        # avoid moving a selection at the very bottom
        dropped_indexes = []
        if not can_remove_index_zero and 0 in move_down_list:
            move_down_list.remove(0)
        for i in reversed(range(len(self.queue_shows.queue))):
            if i not in move_down_list:
                break
            else:
                dropped_indexes.append(i)
                move_down_list.remove(i)

        if len(move_down_list) == 0:
            return dropped_indexes

        # move
        reversed_sorted_indexes = sorted(move_down_list)
        reversed_sorted_indexes.reverse()
        # print(reversed_sorted_indexes)
        for index in reversed_sorted_indexes:
            # print(index)
            self.queue_shows.queue.insert(index, self.queue_shows.queue[index + 1])
            self.queue_shows.queue.pop(index + 2)

        if update_gui_event_set:
            gui_event_queue_update_table.set()

        return dropped_indexes + list(map(lambda x: x + 1, reversed_sorted_indexes))


    def get_queue_table(self):
        shows_in_queue = self.get_shows_by_ids(self.queue_shows.queue)
        if len(shows_in_queue) == 0:
            message = 'Currently the queue is empty.'
        else:
            max_width = 0 # for pretty formatting
            for show in shows_in_queue:
                max_width = max(max_width, len(show.showName))

            queue_strings = []
            queue_strings.append(f'{color.BOLD}Current queue:{color.END}')

            for entry in shows_in_queue:
                entry_string = f'{entry.showName.ljust(max_width)}\t{entry.currentEpisode}'

                if entry.showSettings.opening:
                    entry_string = entry_string + "\top"
                else:
                    entry_string = entry_string + "\t"

                if entry.showSettings.ending:
                    entry_string = entry_string + "\ted"

                queue_strings.append(entry_string)

            message = "\n\t".join(queue_strings)
        return message


def load_user_current():
    return MainSetup(CONFIG_GLOBAL_FULL_PATH)

# def load_user(user_uuid: str):
#     return MainSetup(CONFIG_GLOBAL_FULL_PATH, user_uuid)






class MainSetupSession:
    def __init__(self):
        self.volume: int = None
        self.speed: int = None
        self.mute: bool = None
        # self.monitor: int = None
        self.commercialsBreakWeight: int = None
        self.loop: bool = False


class MainSetupQueue:
    def __init__(
        self,
        startedPlaybacks = None
    ):
        self.queue: List[int] = list() # a queue-like list holding the next few show Ids of the shows to be played and displayed with the 'queue' command, populated in generate_next_show_object or by using the queue command. Not actually using real python queues
        self.alreadyPlayed: List[(int, int, str)] = list() # tuples of show_id and episode_number and showName, needed for global options 'unique' and 'uunique'. Every entry consists of the show's id and ep number. Don't use this for history of played shows
        self.startedPlaybacks: List[(str, str, int)] = startedPlaybacks if startedPlaybacks is not None else list() # path strs full, and without base path, used for 'history' command, also the length in milliseconds from mediaplayer.get_length()



class ObjToDict: #to get the dict, take the resulting object 'thing' like this to really get it as dict: thing.__dict__
    def __init__(self, in_obj: object):
        assert isinstance(in_obj, object)
        for key, val in in_obj.__dict__.items():
            if isinstance(val, (list, set)):
                self.__dict__[key] = [ObjToDict(x).__dict__ if isinstance(x, (ConfigGlobalUser, ConfigGlobalWindowSettings, ConfigUserIdManagement, ConfigUserProfile, ConfigUserProfileSettings, ConfigUserShow, ConfigUserShowSettings)) else x for x in val]
            else:
                if val is not None:
                    self.__dict__[key] = ObjToDict(val).__dict__ if isinstance(val, (ConfigGlobalUser, ConfigGlobalWindowSettings, ConfigUserIdManagement, ConfigUserProfile, ConfigUserProfileSettings, ConfigUserShow, ConfigUserShowSettings)) else val

        # temporary attributes that shouldn't be saved
        if self.__dict__.get("noAutosave", None) is not None:
            self.__dict__.pop("noAutosave")
        if self.__dict__.get("firstEpisode_tmp", None):
            self.__dict__.pop("firstEpisode_tmp")
        if self.__dict__.get("lastEpisode_tmp", None):
            self.__dict__.pop("lastEpisode_tmp")
        if self.__dict__.get("tmp_all_tags_even_unused_of_session", None) is not None:
            self.__dict__.pop("tmp_all_tags_even_unused_of_session")
        if self.__dict__.get("tmp_tags_list", None) is not None:
            self.__dict__.pop("tmp_tags_list")
        if self.__dict__.get("tmp_tags_list_counted", None) is not None:
            self.__dict__.pop("tmp_tags_list_counted")
        if self.__dict__.get("tmp_tags_list_distinct", None) is not None:
            self.__dict__.pop("tmp_tags_list_distinct")




# DEFAULT_GLOBAL_SETTINGS = {
#         JsonKeys.GCurrentUserUuid: 1,
#         # JsonKeys.GNextUserId: 2, # TODO UUID
#         JsonKeys.GUsers_Dicts: [
#             {
#                 JsonKeys.GUsersUuid: 1,
#                 JsonKeys.GUsersName_String: "Userino",
#             },
#         ],
#         JsonKeys.GBasePaths_StrArray: [],
#         JsonKeys.GUseBasePaths_Bool: False,
#         JsonKeys.GWindowSettings_Dict: {
#             JsonKeys.GWindowSettingsMediaAreaState_Int: 0,
#             JsonKeys.GWindowSettingsGuiTheme_String: "Default",
#             JsonKeys.GWindowSettingsLanguage_String: "en",
#             JsonKeys.GWindowSettingsFontSize_Int: None,
#         },
#     }


# DEFAULT_USER_SETTINGS = {
#         JsonKeys.UCurrentProfile_Id: 1,
#         JsonKeys.UIdManagement_Dict: {
#             JsonKeys.UIdManagementNextProfileId: 2,
#             JsonKeys.UIdManagementNextShowId: 1,
#         },
#         JsonKeys.UProfiles_Dicts: [
#             {
#                 JsonKeys.UProfilesId: 1,
#                 JsonKeys.UProfilesName_String: "Profiler",
#                 # JsonKeys.UProfilesSettings_Dict: {
#                 #     JsonKeys.UCPSNoRerun_Bool: True,
#                 # }
#             },
#         ],
#         JsonKeys.UShows_Dicts: [],
#     }


def create_default_global_config_with_file(new_user_uuid: str) -> ConfigGlobal:
    new_global_config = ConfigGlobal(
        new_user_uuid,
        None,
        False,
        [],
        [],
        ConfigGlobalWindowSettings(0, None, None, None, None, None, None, None)
    )

    save_json_complete(CONFIG_GLOBAL_FULL_PATH, ObjToDict(new_global_config).__dict__)

    return new_global_config


def create_default_user_config_file(user_uuid: str):
    new_user_config = ConfigUser(
        1,
        ConfigUserIdManagement(nextProfileId=2, nextShowId=1),
        list([
            ConfigUserProfile(
                1,
                "My First Profile"
            )
        ]),
        list()
    )

    save_json_complete(get_config_user_full_path(user_uuid), ObjToDict(new_user_config).__dict__)


JSON_SAVE_INDENT = 4


def load_json_to_dict(json_file_path) -> dict:
    try:
        with open(json_file_path, 'r', encoding=ENCODING_CONFIG_FILE) as f:
            dict_from_json = jsonload(f)
    except Exception as e:
        return None
        # # TODO: handle unexpected missing files
        # sg.popup_quick_message(f'exception {e}', f'No json file found at {json_file_path}... please do not change the json-files manually from outside the GUI. If this happened unexpectedly, please contact me: {CONTACT_EMAIL} ...or try to fix the code yourself, see {LINK_TO_GITLAB_REPO}', keep_on_top=True, background_color='red', text_color='white')
        # sleep(3)
    return dict_from_json


def save_json_complete(json_file_path: str, dict_to_json: dict):
    with open(json_file_path, 'w', encoding=ENCODING_CONFIG_FILE) as f:
        jsondump(dict_to_json, f, indent=JSON_SAVE_INDENT, ensure_ascii=False)
    printdev("save_json_complete erledigt")
    
    
def save_main_setup_complete(main_setup: MainSetup):
    if main_setup.global_config.noAutosave:
        return
    save_global_config_complete(main_setup.global_config)
    save_user_config_complete(main_setup.user_config, main_setup.global_config.currentUserUuid)


def save_global_config_complete(global_config: ConfigGlobal, json_file_path: str = None):
    if global_config.noAutosave:
        return
    if json_file_path == None:
        json_file_path = CONFIG_GLOBAL_FULL_PATH
    with open(json_file_path, 'w', encoding=ENCODING_CONFIG_FILE) as f:
        jsondump(ObjToDict(global_config).__dict__, f, indent=JSON_SAVE_INDENT, ensure_ascii=False)
    printdev("Global Config saved in: " + json_file_path)
    

def save_user_config_complete(user_config: ConfigUser, user_uuid: str, json_file_path: str = None):
    if json_file_path == None:
        json_file_path = get_config_user_full_path(user_uuid)
    with open(json_file_path, 'w', encoding=ENCODING_CONFIG_FILE) as f:
        jsondump(ObjToDict(user_config).__dict__, f, indent=JSON_SAVE_INDENT, ensure_ascii=False)
    printdev("User Config saved in: " + json_file_path)

def delete_user_config_by_uuid(user_uuid: str):
    json_file_path = get_config_user_full_path(user_uuid)
    if os.path.isfile(json_file_path):
        os.remove(json_file_path)
        printdev("User file deleted: " + json_file_path)
    else:
        printdev("User file for uuid " + user_uuid + " not found, nothing deleted.")


def save_current_user_config_complete(main_setup: MainSetup, json_file_path: str = None):
    if main_setup.global_config.noAutosave:
        return
    save_user_config_complete(main_setup.user_config, main_setup.global_config.currentUserUuid, json_file_path)


# def dven(dictIn: dict, keyIn: str): #this is just a stupid short-name method of dict_value_else_none
#     return dict_value_else_none(dictIn, keyIn)

# def dict_value_else_none(dictIn: dict, keyIn: str):
#     try:
#         return dictIn[keyIn]
#     except Exception as e:
#         return None


def find_dict_in_list_by_value_else_none(listOfDicts: list, key: str, value) -> dict:
    return next((item for item in listOfDicts if item[key] == value), None)


# TODO: check if this is always the case for windows, check for mac?
def get_slash(getOpposite: bool = False):
    if sys.platform == "win32":
        if getOpposite:
            return "/"
        else:
            return "\\"
    else:
        if getOpposite:
            return "\\"
        else:
            return "/"


def convert_path_slashes(dir_path: str) -> str:
    return dir_path.replace(get_slash(True), get_slash())


########################################## ShowEpisode (old class) Replacements ##########################################

#TODO CONTINUE: reorder methods in this codes

def show_get_episode_file_name(show: ConfigUserShow, config_global: ConfigGlobal, episode_number: int = None) -> str:
    file_full_path = show_generate_full_episode_path(show, config_global, episode_number)
    file_name = os.path.basename(file_full_path)
    if "." in file_name:
        file_name = file_name[:file_name.rfind(".")]
    return file_name


def show_generate_full_episode_path(show: ConfigUserShow, config_global: ConfigGlobal, episode_number: int = None) -> str:
    files_full_paths = show_generate_all_episodes_paths(show, config_global)
    if episode_number == None:
        episode_number = show.currentEpisode
    show.generate_first_and_last_episode_tmp_ints(config_global)
    show.currentEpisode = max( min(show.currentEpisode, show.lastEpisode_tmp), show.firstEpisode_tmp )
    return files_full_paths[show.currentEpisode - 1] # remember, index starts at 0, but episode_number starts at 1 # TODO CONTINUE if that many files don't exist anymore what then? Can also happen with ne excludeFilesEndingWith-restrictions


def show_generate_full_op_ed_path(show: ConfigUserShow, config_global: ConfigGlobal, specifier: str, episode_number: int = None):
    if specifier != "opening" and specifier != "ending":
        raise Exception("Error in code, show_generate_full_op_ed_path's specifier has to be 'opening' or 'ending'.")

    if specifier == "opening":
        spec = "op"
    else:
        spec = "ed"

    full_path = ""

    # path_starter = f'{os.path.join(show_generate_directory_path(show, config_global), spec)}'
    path_starter = spec
    
    files_full_paths = show_generate_all_op_ed_paths(show, config_global)

    if episode_number == None:
        episode_number = show.currentEpisode

    # now pick most fitting file, realizes functionality of different openings for different episode numbers
    full_path = helper_pick_best_op_ed_file(path_starter, episode_number, files_full_paths)
    
    return full_path


def helper_pick_best_op_ed_file(path_starter: str, episode_number: int, files_full_paths: [str]) -> str:
    """Returns full file path to be played.

    path_starter should be 'op' or 'ed'.

    Was before:
    path_starter    home/PATH/TO/FILE/op
    ..... (too lazy)

    returns example:    home/PATH/TO/FILE/op123.mp3
    """

    filtered_files = []
    for f in files_full_paths:
        if os.path.basename(f).startswith(path_starter):
            filtered_files.append(f)

    file_tuple = ("", 0) # hold currently best fitting file and its int from which episode on in it supposed to be played, 0 if no int (standard op/ed file), best fitting int is highest but lower than episode_number
    for f in filtered_files:
        f_name = os.path.basename(f)
        if f_name.startswith(path_starter + ".") and file_tuple[1] == 0:
            file_tuple = (f, 0)
        else: # check if file is PATH + INT + '.SOME_FILETYPE'
            potential_file_path_number = f_name[len(path_starter):f_name.find(".", len(path_starter))]
            if potential_file_path_number != "":
                try:
                    episode_start_int = int(potential_file_path_number)
                    if file_tuple[1] < episode_start_int <= episode_number:
                        file_tuple = (f, episode_start_int)
                except ValueError:
                    pass

    return file_tuple[0]



# TODO add python caching! seen video recently about that, maybe need to implement an "equals" method in ConfigUserShow and ConfigUserGlobal
def show_generate_all_episodes_paths(show: ConfigUserShow, config_global: ConfigGlobal) -> [str]:
    dir_path = show_generate_directory_path(show, config_global)
    return show_generate_all_episodes_paths_from_dir_path(
        dir_path,
        config_global.excludeFilesEndingWith,
        False,
        show.showSettings.recursionLevel if show.showSettings.recursionLevel else 0
    )

# TODO add python caching! seen video recently about that
def show_get_last_episode_calculated(show: ConfigUserShow, global_config: ConfigGlobal):
    episode_paths = show_generate_all_episodes_paths(show, global_config)
    return None if episode_paths is None else len(episode_paths)


def show_generate_directory_path(show: ConfigUserShow, config_global: ConfigGlobal) -> str:
    dir_path = show.directoryPath
    dir_path = convert_path_slashes(dir_path)

    amount_basepaths = len(config_global.basePaths)
    if config_global.useBasePaths and not show.doNotUseBasePath and amount_basepaths > 0:
        if amount_basepaths < config_global.currentBasePathIndex:
            config_global.currentBasePathIndex = amount_basepaths - 1
        base_path_index = config_global.currentBasePathIndex
        base_path = convert_path_slashes(config_global.basePaths[base_path_index])
        dir_path = os.path.join(base_path, dir_path)
    
    return dir_path


def show_generate_all_op_ed_paths(show: ConfigUserShow, config_global: ConfigGlobal) -> [str]:
    dir_path = show_generate_directory_path(show, config_global)
    return show_generate_all_episodes_paths_from_dir_path(
        dir_path,
        config_global.excludeFilesEndingWith,
        True,
        show.showSettings.recursionLevel if show.showSettings.recursionLevel else 0
    )


# TODO CONTINUE: handle None returned here if dir not found
def show_generate_all_episodes_paths_from_dir_path(dir_path: str, excludeFilesEndingWith: [str], return_ops_eds: bool = False, recursionLevel: int = 0) -> [str]:
    dir_path = convert_path_slashes(dir_path)
    files_full_paths = get_files_from_directory(dir_path, excludeFilesEndingWith, True, recursionLevel=recursionLevel, removeDuplicates=True)

    if files_full_paths is None:
        return None

    filtered_episodes_full_paths_list = []

    for full_path in files_full_paths:
        file_name = full_path[full_path.rfind(get_slash())+1:]
        if not "op" in file_name and not "ed" in file_name:
            if not return_ops_eds:
                filtered_episodes_full_paths_list.append(full_path)
        else:
            if "op" in file_name:
                check_op_ed = "op"
            else:
                check_op_ed = "ed"
            try:
                # this next line pretty much checks if the file name is "op" or "ed" and then a number
                # and then a file ending, like 'path/to/file/op123.mp3'
                maybe_number = file_name[2 : file_name.rfind(".")]
                if maybe_number == "":
                    pass
                else:
                    episode_start_int = int(maybe_number)

                if return_ops_eds:
                    filtered_episodes_full_paths_list.append(full_path)
                # if no ValueError thrown, then the file is an opening/ending file like 'home/PATH/TO/EPISODES/op123.mp4'
            except ValueError:
                if not return_ops_eds:
                    filtered_episodes_full_paths_list.append(full_path)

    filtered_episodes_full_paths_list.sort()
    return filtered_episodes_full_paths_list


def get_files_from_directory(dir_path: str, excludeFilesEndingWith: [str] = [], full_paths: bool = True, recursionLevel: int = 0, removeDuplicates: bool = True) -> [str]:
    files_list = []

    try:
        with os.scandir(dir_path) as entries:
            for entry in entries:
                if entry.is_file():
                    if full_paths:
                        files_list.append(os.path.join(dir_path, entry.name)) #TODO try replacing "os.path.join(dir_path, entry.name)"" by "entry.path"
                    else:
                        files_list.append(entry.name)
                elif recursionLevel > 0:
                    files_list.extend(get_files_from_directory(os.path.join(dir_path, entry.name), excludeFilesEndingWith, full_paths, recursionLevel - 1))
    except FileNotFoundError as e:
        return None

    return_files_list = []
    for path in files_list:
        for ending in excludeFilesEndingWith:
            if path.endswith(ending):
                break
        else:
            return_files_list.append(path)

    if removeDuplicates:
        return list(set(return_files_list))  # set usually destroys order, but this list should be sorted later if needed
    else:
        return return_files_list



##### Commands, Options and Help Text Setup #####

### Commands

# ## command specifiers
# HELP_COMMAND = "help"
# HELP_FULL_COMMAND = "HELP"
# STATUS_COMMAND = "status"
# PAUSE_COMMAND = "pause"
# VOLUME_COMMAND = "volume"
# JUMP_COMMAND = "jump"
# BACK_COMMAND = "back"
# FORWARD_COMMAND = "forward"
# RESTART_COMMAND = "restart"
# SKIP_EPISODE_COMMAND = "skip"
# FINISH_EPISODE_COMMAND = "finish"
# SHOW_CONFIG_COMMAND = "config"
# CONFIG_HELP_COMMAND = "config-help"
# EXIT_COMMAND = "exit"
#
# OPEN_PLAYBACK_DIRECTORY_COMMAND = "open"
# OPEN_CONFIG_COMMAND = "cconfig"
# OPTIONS_HELP_COMMAND = "options-help"
# DISPLAY_CURRENT_OPTIONS_COMMAND = "options"
# SPEED_COMMAND = "speed"
# HARD_VOLUME_COMMAND = "vvolume"
# MUTE_COMMAND = "mute"
# FULLSCREEN_COMMAND = "fullscreen"
# DISPLAY_SHOWS_COMMAND = "shows"
# QUEUE_SHOW_COMMAND = "queue"
# QUEUE_SHOW_LOAD_COMMAND = "queue-load"
# QUEUE_SHOW_CLEAR_COMMAND = "queue-clear"
# QUEUE_SHOW_POP_COMMAND = "queue-pop"
# PLAY_SHOW_COMMAND = "play"
# #PLAY_EPISODE_COMMAND = "episode"
# #PREVIOUS_EPISODE_COMMAND = "previous"
# #NEXT_EPISODE_COMMAND = "next"
# #LOOP_EPISODE_COMMAND = "loop"
# END_COMMAND = "eend"
# SUSPEND_COMMAND = "suspend"
# SUSPEND_QUEUE_COMMAND = "ssuspend"
# SHUTDOWN_COMMAND = "shutdown"
# LAST_EPISODE_COMMAND = "last"
# SKIP_FULL_EPISODE_COMMAND = "sskip"
# FINISH_FULL_EPISODE_COMMAND = "ffinish"
# SKIP_TO_COMMERCIALS_COMMAND = "commercial"
# COMMERCIALS_GLOBAL_OVERRIDE_COMMAND = "commercials"
# HARD_PLAY_COMMAND = "resume"
# HARD_PAUSE_COMMAND = "ppause"
# VIDEO_TRACK_COMMAND = "video"
# AUDIO_TRACK_COMMAND = "audio"
# SUBTITLE_TRACK_COMMAND = "subtitle"
# HISTORY_COMMAND = "history"
# MONITOR_COMMAND = "monitor"
# MONITOR_PLAYER_JUMPS_COMMAND = "monitor-jump"
# MONITOR_OFF_COMMAND = "mon"
# DEBUG_PLAYER_JUMPS_COMMAND = "debug"
# MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND = "analysis"
# MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND = "analysis-full"
# ABOUT_COMMAND = "about"
#
# COMMANDS_LIST = [HELP_COMMAND, HELP_FULL_COMMAND, STATUS_COMMAND, PAUSE_COMMAND, VOLUME_COMMAND, JUMP_COMMAND, BACK_COMMAND, FORWARD_COMMAND, RESTART_COMMAND, SKIP_EPISODE_COMMAND, FINISH_EPISODE_COMMAND, SHOW_CONFIG_COMMAND, CONFIG_HELP_COMMAND, EXIT_COMMAND, OPEN_PLAYBACK_DIRECTORY_COMMAND, OPEN_CONFIG_COMMAND, OPTIONS_HELP_COMMAND, DISPLAY_CURRENT_OPTIONS_COMMAND, SPEED_COMMAND, HARD_VOLUME_COMMAND, MUTE_COMMAND, FULLSCREEN_COMMAND, DISPLAY_SHOWS_COMMAND, QUEUE_SHOW_COMMAND, QUEUE_SHOW_LOAD_COMMAND, QUEUE_SHOW_CLEAR_COMMAND, QUEUE_SHOW_POP_COMMAND, PLAY_SHOW_COMMAND, END_COMMAND, SUSPEND_COMMAND, SUSPEND_QUEUE_COMMAND, SHUTDOWN_COMMAND, LAST_EPISODE_COMMAND, SKIP_FULL_EPISODE_COMMAND, FINISH_FULL_EPISODE_COMMAND, SKIP_TO_COMMERCIALS_COMMAND, COMMERCIALS_GLOBAL_OVERRIDE_COMMAND, HARD_PLAY_COMMAND, HARD_PAUSE_COMMAND, VIDEO_TRACK_COMMAND, AUDIO_TRACK_COMMAND, SUBTITLE_TRACK_COMMAND, HISTORY_COMMAND, MONITOR_COMMAND, MONITOR_PLAYER_JUMPS_COMMAND, MONITOR_OFF_COMMAND, DEBUG_PLAYER_JUMPS_COMMAND, MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND, MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND, ABOUT_COMMAND] #formerly used for input auto completion
#
#
# # short command names (if in use)
# HELP_C = "h"
# HELP_FULL_C = "H"
# STATUS_C = "s"
# PAUSE_C = "p"
# VOLUME_C = "v"
# JUMP_C = "j"
# BACK_C = "b"
# FORWARD_C = "f"
# RESTART_C = "r"
# SHOW_CONFIG_C = "c"
# EXIT_C = "e"
# EXIT_C_2 = "q"
#
# MUTE_C = "m"
# QUEUE_SHOW_C = "qu"
# QUEUE_SHOW_LOAD_C = "ql"
# QUEUE_SHOW_CLEAR_C = "qc"
# QUEUE_SHOW_POP_C = "qp"
# #PREVIOUS_EPISODE_C = "prev"
# SKIP_TO_COMMERCIALS_C = "cm"
# COMMERCIALS_GLOBAL_OVERRIDE_C = "cms"
#
#
#
#
# ### Options
# # option specifiers
# # option specifiers that have no corresponding command
# OPENING_OPTION = "opening"              # bool, with custom file type
# ENDING_OPTION = "ending"                # bool, with custom file type
# RANDOM_OPTION_SHOW = "random"           # bool, current_ep will be randomized
# MULTIPLE_EPISODES_OPTION_SHOW = "episodes"
# PROBABILITY_OPTION_SHOW = "probability" # int 0-100
# PROBABILITY_OPTION_GLOBAL = "probability"   # int 1-100, sets default probability for all shows that don't use the probability option
# COMMERCIALS_PLAY_OPTION_GLOBAL = "commercials" # bool <- play shows, that are commercials (option set). Usually no show with commercial option will be randomly picked, only if user intentionally gets it with command 'play' or 'queue'
# COMMERCIAL_WEIGHT_OPTION_GLOBAL = "commercial" # int <- weight how much to play
# COMMERCIAL_WEIGHT_OPTION_SHOW = "commercial"   # int <- weight of each cm here
# COMMERCIAL_MAX_OPTION_SHOW = "commercialMax"   # int <- weight of each cm here
# COMMERCIAL_MIN_OPTION_SHOW = "commercialMin"   # int <- weight of each cm here
# UNIQUE_SHOWS_OPTION_GLOBAL = "unique"   # bool, avoid playing a show more than once in a session
# HARD_UNIQUE_SHOWS_OPTION_GLOBAL = "uunique"     # bool, plays every show not more than once, end ranimetv if every show played once
# NO_RERUN_OPTION = "noRerun"
#
# STATUS_OPTION = STATUS_COMMAND  # bool, display status on player start
# PAUSE_OPTION = PAUSE_COMMAND    # bool, pause player on player start
# VOLUME_OPTION = VOLUME_COMMAND  # int, volume in percent, limitted
# JUMP_OPTION = JUMP_COMMAND      # int, jumps to int seconds when episode starts
# SPEED_OPTION = SPEED_COMMAND    # int, episode speed in percent
# MUTE_OPTION = MUTE_COMMAND      # bool, mute when start, global only on startup
# FULLSCREEN_OPTION = FULLSCREEN_COMMAND  # bool
# QUEUE_SHOW_DISPLAY_OPTION = QUEUE_SHOW_COMMAND  # bool, display queue on start
# #LOOP_EPISODE_OPTION = LOOP_EPISODE_COMMAND      # int, loop amount
# SUSPEND_OPTION = SUSPEND_COMMAND                # bool
# SUSPEND_QUEUE_OPTION = SUSPEND_QUEUE_COMMAND    # bool
# SHUTDOWN_OPTION_GLOBAL = SHUTDOWN_COMMAND       # int, turn off after int sec
# LAST_EPISODE_OPTION_GLOBAL = LAST_EPISODE_COMMAND # int, turn off after int many finished shows
# VIDEO_TRACK_OPTION_SHOW = VIDEO_TRACK_COMMAND   # int, choose track
# AUDIO_TRACK_OPTION_SHOW = AUDIO_TRACK_COMMAND   # int, choose track
# SUBTITLE_TRACK_OPTION_SHOW = SUBTITLE_TRACK_COMMAND # int, choose track
# MONITOR_OPTION = MONITOR_COMMAND    # int, print full status every int seconds
# MONITOR_PLAYER_JUMPS_OPTION = MONITOR_PLAYER_JUMPS_COMMAND  # bool, activate
# DEBUG_PLAYER_JUMPS_OPTION = DEBUG_PLAYER_JUMPS_COMMAND  # bool, experimental auto restarter on encountered player length or time changing player behaviour
#
# OPTIONS_LIST = [OPENING_OPTION, ENDING_OPTION, RANDOM_OPTION_SHOW, MULTIPLE_EPISODES_OPTION_SHOW, PROBABILITY_OPTION_SHOW, COMMERCIALS_PLAY_OPTION_GLOBAL, COMMERCIAL_WEIGHT_OPTION_SHOW, COMMERCIAL_MAX_OPTION_SHOW, COMMERCIAL_MIN_OPTION_SHOW, UNIQUE_SHOWS_OPTION_GLOBAL, HARD_UNIQUE_SHOWS_OPTION_GLOBAL, NO_RERUN_OPTION, STATUS_OPTION, PAUSE_OPTION, VOLUME_OPTION, JUMP_OPTION, SPEED_OPTION, MUTE_OPTION, FULLSCREEN_OPTION, QUEUE_SHOW_DISPLAY_OPTION, SUSPEND_OPTION, SUSPEND_QUEUE_OPTION, SHUTDOWN_OPTION_GLOBAL, LAST_EPISODE_OPTION_GLOBAL, VIDEO_TRACK_OPTION_SHOW, AUDIO_TRACK_OPTION_SHOW, SUBTITLE_TRACK_OPTION_SHOW, MONITOR_OPTION, MONITOR_PLAYER_JUMPS_OPTION, DEBUG_PLAYER_JUMPS_OPTION]
#
# def showcase_commands(): # only used for a video explaining the program
#     counter = 0
#     message = ""
#     for command in COMMANDS_LIST:
#         counter += 1
#         if len(command) < 8:
#             message = message + command + "\t\t"
#         else:
#             message = message + command + "\t"
#         if counter % 5 == 0:
#             message = message + "\n"
#     print(color.BOLD + message + color.END)
#
# #showcase_commands()
#
# def showcase_options(): # only used for a video explaining the program
#     counter = 0
#     message = ""
#     for option in OPTIONS_LIST:
#         counter += 1
#         if len(option) < 8:
#             message = message + option + "\t\t"
#         else:
#             message = message + option + "\t"
#         if counter % 5 == 0:
#             message = message + "\n"
#     print(color.BOLD + message + color.END)
#
# #showcase_options()
#
#
#
#
# ## Command Explanations
# # explanations in short help string
# HELP_COMMAND_DESCR = "display this help text."
# HELP_FULL_COMMAND_DESCR = "more detailed help with more commands."
# STATUS_COMMAND_DESCR = "display information about current playback."
# PAUSE_COMMAND_DESCR = "toggle pause."
# VOLUME_COMMAND_DESCR = "set volume."
# JUMP_COMMAND_DESCR = "jump to second X."
# BACK_COMMAND_DESCR = f"go X seconds back. Type '{BACK_COMMAND}' to go {BACK_PLAYBACK_SIZE_STANDARD} seconds back."
# FORWARD_COMMAND_DESCR = f"go X seconds forward. Type '{FORWARD_COMMAND}' to go {FORWARD_PLAYBACK_SIZE_STANDARD} seconds forward."
# RESTART_COMMAND_DESCR = "restart current episode."
# SKIP_EPISODE_COMMAND_DESCR = "play something else, don't update config."
# FINISH_EPISODE_COMMAND_DESCR = "play something else, and update config."
# SHOW_CONFIG_COMMAND_DESCR = "display path and contents of config."
# CONFIG_HELP_COMMAND_DESCR = "display how to create a config file."
# EXIT_COMMAND_DESCR = "exit this program."
#
# # explanations in long help string
# HELP_COMMAND_DESCRIPTION = "display shorter help text."
# HELP_FULL_COMMAND_DESCRIPTION = "display this more detailed help text with debugging commands, more details about implementation and handling of bad or out of bounds input."
# STATUS_COMMAND_DESCRIPTION = "display information about current playback."
# PAUSE_COMMAND_DESCRIPTION = "pauses a playing episode or resumes a paused episode."
# VOLUME_COMMAND_DESCRIPTION = f"set volume to X percent. Lower limit is {VOLUME_LIMIT_LOWER}, upper limit is {VOLUME_LIMIT_UPPER}. This affects the whole session and acts as a multiplier, just like the show's config and global config volume. See '{HARD_VOLUME_COMMAND}' to change volume for the current playback, ignoring all config volume settings. Type '{VOLUME_COMMAND}' to see current volume and volume settings."
# JUMP_COMMAND_DESCRIPTION = f"jump to second X of playback, see below for possibilities of X. If goal time is beyond episode's length, it will jump {JUMP_PLAYBACK_BUMPER_SIZE_STANDARD} seconds close to end or do nothing if already closer than {JUMP_PLAYBACK_BUMPER_SIZE_STANDARD} seconds to end."
# BACK_COMMAND_DESCRIPTION = f"go X seconds back. Same as '{JUMP_COMMAND} 0' if going back more seconds than current playback already passed. Type '{BACK_COMMAND}' to go {BACK_PLAYBACK_SIZE_STANDARD} seconds back."
# FORWARD_COMMAND_DESCRIPTION = f"go X seconds forward. Same behaviour as with '{JUMP_COMMAND}' if goal time is too high. Type '{FORWARD_COMMAND}' to go {FORWARD_PLAYBACK_SIZE_STANDARD} seconds forward."
# RESTART_COMMAND_DESCRIPTION = "restart playback of current episode, will call player.stop(), player.play(), using the skipped flag to avoid config file changes."
# SKIP_EPISODE_COMMAND_DESCRIPTION = f"play next media, don't update the current episode in your config file if this command is used during episode playback. If current playback is an opening, ending or commercial, then there is no effect on if the config will be changed or not. Use '{SKIP_FULL_EPISODE_COMMAND}' to skip all components of this episode (including opening, ending and commercials) and play the next show."
# FINISH_EPISODE_COMMAND_DESCRIPTION = f"play something else, and update the current episode in config file, increasing it by one. Use '{FINISH_FULL_EPISODE_COMMAND}' to finish all components of this episode (including opening, ending and commercials) and play the next show. Episode will be updated in config (if used during opening or episode), commercials won't be. If used during a commercial, then this commercial will be updated."
# SHOW_CONFIG_COMMAND_DESCRIPTION = "show path and contents of config file."
# CONFIG_HELP_COMMAND_DESCRIPTION = "show how to create a proper config file with examples and all necessary and possible arguments and lines."
# EXIT_COMMAND_DESCRIPTION = "exit this program, will stop playback, input and everything else."
#
#
# OPEN_PLAYBACK_DIRECTORY_COMMAND_DESCRIPTION = "open directory of currently playing file."
# OPEN_CONFIG_COMMAND_DESCRIPTION = "open config file with your system's standard text file application."
# OPTIONS_HELP_COMMAND_DESCRIPTION = "show all global and local options available."
# DISPLAY_CURRENT_OPTIONS_COMMAND_DESCRIPTION = "show all current options."
# SPEED_COMMAND_DESCRIPTION = f"set playback speed to X percent. Limited to 300 up, 20 down. Lower limit is {SPEED_LIMIT_LOWER} upper limit is {SPEED_LIMIT_UPPER}. Type '{SPEED_COMMAND}' to see current speed."
# HARD_VOLUME_COMMAND_DESCRIPTION = "set current player's volume to X, ignoring all settings in config and ignoring limits."
# MUTE_COMMAND_DESCRIPTION = "toggle mute."
# FULLSCREEN_COMMAND_DESCRIPTION = "toggle fullscreen."
# DISPLAY_SHOWS_COMMAND_DESCRIPTION = "display all shows from your config. This does not check if your show lines are valid or if they have valid path lines or if the files exist on your system at all."
# QUEUE_SHOW_COMMAND_DESCRIPTION = f"add a specific show to the end of the current show queue. S has to be a show alias. Type '{QUEUE_SHOW_COMMAND}' to show the current queue. The queue will hold at least {QUEUE_SIZE_STANDARD} shows when a new episode starts, reloading new ones when there aren't enough when a new episode is started."
# QUEUE_SHOW_LOAD_COMMAND_DESCRIPTION = f"add X random shows to the show queue. Type '{QUEUE_SHOW_LOAD_COMMAND}' or '{QUEUE_SHOW_LOAD_C}' to load {QUEUE_SIZE_STANDARD} shows."
# QUEUE_SHOW_CLEAR_COMMAND_DESCRIPTION = "clear the current show queue."
# QUEUE_SHOW_POP_COMMAND_DESCRIPTION = "remove the last item in the current show queue."
# PLAY_SHOW_COMMAND_DESCRIPTION = "play a specific show now. S has to be a show alias. This will play one episode of the show, as if it was the current entry in the show queue. This will push all other shows in the queue back by one, so the show that played just now will be played next."
# #LOOP_EPISODE_COMMAND_DESCRIPTION = f"loop every episode X times. This loops the current and every other episode in this session X times. If looping is not turned on and you type '{LOOP_EPISODE_COMMAND}', it will loop the current episode forever. Input '{LOOP_EPISODE_COMMAND}' again to stop any looping. If you go to another episode with commands like '{PLAY_SHOW_COMMAND}' or '{SKIP_EPISODE_COMMAND}', it will not turn off looping."
# END_COMMAND_DESCRIPTION = f"jump X seconds close to end, X has to be an integer. Type '{END_COMMAND}' to jump {END_PLAYBACK_SIZE_STANDARD} seconds close to end."
# SUSPEND_COMMAND_DESCRIPTION = f"turn off {PROGRAM_NAME}, but also save in config where you left off. {PROGRAM_NAME} will continue where you left off when you start it again, going back {SUSPEND_BACK_SIZE_STANDARD} seconds."
# SUSPEND_QUEUE_COMMAND_DESCRIPTION = f"same as '{SUSPEND_COMMAND}', but also saves the current queue in config. The aliases of the shows in the current queue will be saved at the top of your config, starting with '{CONFIG_SUSPEND_QUEUE_LINE_STARTER}' and separated by semicolons (;). If a show is currently running, then it is not saved in this line, as its playback position if already saved like when using '{SUSPEND_COMMAND}'."
# SHUTDOWN_COMMAND_DESCRIPTION = f"turn off {PROGRAM_NAME} in X minutes. Type '{SHUTDOWN_COMMAND}' again to turn off the timer."
# LAST_EPISODE_COMMAND_DESCRIPTION = f"turn of {PROGRAM_NAME} after X shows (including currently running) are done playing. Not counting down if episode is skipped. Type '{LAST_EPISODE_COMMAND}' to cancel if currently activated, or type it when deactivated to end after the current playback (also works on playbacks that are not a show's episode)."
# SKIP_FULL_EPISODE_COMMAND_DESCRIPTION = f"skips all components of the episode and plays the next show. Same as '{SKIP_EPISODE_COMMAND}' if you have no opening, ending and commercial settings. Use '{SKIP_EPISODE_COMMAND}' if you only want to skip the currently playing media."
# FINISH_FULL_EPISODE_COMMAND_DESCRIPTION = f"skips all components of the episode and plays the next show. Same as '{SKIP_FULL_EPISODE_COMMAND}' but updates this show's current episode in config if it is used during opening or episode."
# SKIP_TO_COMMERCIALS_COMMAND_DESCRIPTION = f"skips all components of the episode and plays the commercials, if any are set. If there are no commercials set in config, then same as '{SKIP_FULL_EPISODE_COMMAND}'."
# COMMERCIALS_GLOBAL_OVERRIDE_COMMAND_DESCRIPTION = f"overrides the global commercial weight config option '{COMMERCIAL_WEIGHT_OPTION_GLOBAL}'."
# HARD_PLAY_COMMAND_DESCRIPTION = "resume playback. No effect if playback already running."
# HARD_PAUSE_COMMAND_DESCRIPTION = "pause playback. No effect if playback already paused."
# VIDEO_TRACK_COMMAND_DESCRIPTION = f"set the video to video track X. Type '{VIDEO_TRACK_COMMAND}' to display current track and total amount of video tracks."
# AUDIO_TRACK_COMMAND_DESCRIPTION = f"set the audio to audio track X. Type '{AUDIO_TRACK_COMMAND}' to display current track and total amount of audio tracks."
# SUBTITLE_TRACK_COMMAND_DESCRIPTION = f"set the subtitles to subtitle track X. Type '{SUBTITLE_TRACK_COMMAND}' to display current track and total amount of subtitle tracks."
# HISTORY_COMMAND_DESCRIPTION = "display all files that have been played and all episodes finished so far."
# MONITOR_COMMAND_DESCRIPTION = "toggle normal monitor. Prints all sorts of player information every X seconds."
# MONITOR_PLAYER_JUMPS_COMMAND_DESCRIPTION = "toggle Jump Monitor. Checks every 0.1 seconds if the player did an unusual jump in playback time. Prints results if anything notable happens."
# MONITOR_OFF_COMMAND_DESCRIPTION = "turn off all monitors. No effect if none was turned on."
# DEBUG_PLAYER_JUMPS_COMMAND_DESCRIPTION = f"experimental debugger that recognizes if the playback is corrupted. More precisely if the player length changes, it will restart the playback and jump to {DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD} second after the player_length was changed."
# MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND_DESCRIPTION = f"only works if ffmpeg is installed. Displays a table with approximations of all the shows' amount of episodes, average length, total length and the total length of all shows togehter. Excluding opening and ending files. Lengths are only approximated by getting the length of {MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD} files per show and then multiplying with (amount of episodes / {MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD}). This method works especially well, if the episodes within a show all have pretty much the same length. Use {MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND} for a thorough analysis."
# MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND_DESCRIPTION = "WARNING: only use this if you are ready for your computer to do a lot of reading and writing on your files to pretty much check all the files from your config. Takes a while to finish, around 30 seconds per 50h of files. Can slow your computer down. Displays a table with all the shows' amount of episodes, average length, total length and the total length of all shows togehter. Excluding opening and ending files."
# ABOUT_COMMAND_DESCRIPTION = "display various information about the program."




#
# def format_help_commands(commands: [str]):
#     message_unformatted = " " + ", ".join(commands)
#     message_formatted = f'{message_unformatted: <15}'
#     return color.BOLD + message_formatted + color.END
#
# MESSAGE_HELP_SHORT = f"""{color.BOLD}---*** {PROGRAM_NAME} Help ***---{color.END}
#
# Available commands:
#  {format_help_commands([HELP_C, HELP_COMMAND])} {HELP_COMMAND_DESCR}
#  {format_help_commands([HELP_FULL_C, HELP_FULL_COMMAND])} {HELP_FULL_COMMAND_DESCR}
#  {format_help_commands([STATUS_C, STATUS_COMMAND])} {STATUS_COMMAND_DESCR}
#  {format_help_commands([PAUSE_C, PAUSE_COMMAND])} {PAUSE_COMMAND_DESCR}
#  {format_help_commands([VOLUME_C + " X", VOLUME_COMMAND + " X"])} {VOLUME_COMMAND_DESCR}
#  {format_help_commands([JUMP_C + " X", JUMP_COMMAND + " X"])} {JUMP_COMMAND_DESCR}
#  {format_help_commands([BACK_C + " X", BACK_COMMAND + " X"])} {BACK_COMMAND_DESCR}
#  {format_help_commands([FORWARD_C + " X", FORWARD_COMMAND + " X"])} {FORWARD_COMMAND_DESCR}
#  {format_help_commands([RESTART_C, RESTART_COMMAND])} {RESTART_COMMAND_DESCR}
#  {format_help_commands([SKIP_EPISODE_COMMAND])} {SKIP_EPISODE_COMMAND_DESCR}
#  {format_help_commands([FINISH_EPISODE_COMMAND])} {FINISH_EPISODE_COMMAND_DESCR}
#  {format_help_commands([SHOW_CONFIG_C, SHOW_CONFIG_COMMAND])} {SHOW_CONFIG_COMMAND_DESCR}
#  {format_help_commands([CONFIG_HELP_COMMAND])} {CONFIG_HELP_COMMAND_DESCR}
#  {format_help_commands([EXIT_C_2, EXIT_C, EXIT_COMMAND])} {EXIT_COMMAND_DESCR}
#
# Enter {color.CYAN}{color.BOLD}{HELP_FULL_COMMAND}{color.END} to see extra commands meant for troubleshooting.
# source code: {color.YELLOW}{color.BOLD}gitlab.com/CptMaister/ranimetv{color.END}
# ---*** End of {PROGRAM_NAME} Help ***---"""
#
#
#
# MESSAGE_HELP_FULL = f"""{color.BOLD}---*** {PROGRAM_NAME} HELP ***---{color.END}
#
# Available commands:
#  {format_help_commands([HELP_C, HELP_COMMAND])} {HELP_COMMAND_DESCRIPTION}
#  {format_help_commands([HELP_FULL_C, HELP_FULL_COMMAND])} {HELP_FULL_COMMAND_DESCRIPTION}
#  {format_help_commands([STATUS_C, STATUS_COMMAND])} {STATUS_COMMAND_DESCRIPTION}
#  {format_help_commands([PAUSE_C, PAUSE_COMMAND])} {PAUSE_COMMAND_DESCRIPTION}
#  {format_help_commands([VOLUME_C + " X", VOLUME_COMMAND + " X"])} {VOLUME_COMMAND_DESCRIPTION}
#  {format_help_commands([JUMP_C + " X", JUMP_COMMAND + " X"])} {JUMP_COMMAND_DESCRIPTION}
#  {format_help_commands([BACK_C + " X", BACK_COMMAND + " X"])} {BACK_COMMAND_DESCRIPTION}
#  {format_help_commands([FORWARD_C + " X", FORWARD_COMMAND + " X"])} {FORWARD_COMMAND_DESCRIPTION}
#  {format_help_commands([RESTART_C, RESTART_COMMAND])} {RESTART_COMMAND_DESCRIPTION}
#  {format_help_commands([SKIP_EPISODE_COMMAND])} {SKIP_EPISODE_COMMAND_DESCRIPTION}
#  {format_help_commands([FINISH_EPISODE_COMMAND])} {FINISH_EPISODE_COMMAND_DESCRIPTION}
#  {format_help_commands([SHOW_CONFIG_C, SHOW_CONFIG_COMMAND])} {SHOW_CONFIG_COMMAND_DESCRIPTION}
#  {format_help_commands([CONFIG_HELP_COMMAND])} {CONFIG_HELP_COMMAND_DESCRIPTION}
#  {format_help_commands([EXIT_C, EXIT_COMMAND])} {EXIT_COMMAND_DESCRIPTION}
#
# Whenever a command takes an integer X for seconds, you can also input hh:mm:ss or mm:ss for X, like 4:20 to get 260 seconds.
#
# These commands are either considered too niche to appear in the main help or were originally meant for debugging and testing of {PROGRAM_NAME} and have not been extensively tested:
#  {format_help_commands([OPEN_PLAYBACK_DIRECTORY_COMMAND])} {OPEN_PLAYBACK_DIRECTORY_COMMAND_DESCRIPTION}
#  {format_help_commands([OPEN_CONFIG_COMMAND])} {OPEN_CONFIG_COMMAND_DESCRIPTION}
#  {format_help_commands([OPTIONS_HELP_COMMAND])} {OPTIONS_HELP_COMMAND_DESCRIPTION}
#  {format_help_commands([DISPLAY_CURRENT_OPTIONS_COMMAND])} {DISPLAY_CURRENT_OPTIONS_COMMAND_DESCRIPTION}
#  {format_help_commands([SPEED_COMMAND + " X"])} {SPEED_COMMAND_DESCRIPTION}
#  {format_help_commands([HARD_VOLUME_COMMAND + " X"])} {HARD_VOLUME_COMMAND_DESCRIPTION}
#  {format_help_commands([MUTE_C, MUTE_COMMAND])} {MUTE_COMMAND_DESCRIPTION}
#  {format_help_commands([FULLSCREEN_COMMAND])} {FULLSCREEN_COMMAND_DESCRIPTION}
#  {format_help_commands([DISPLAY_SHOWS_COMMAND])} {DISPLAY_SHOWS_COMMAND_DESCRIPTION}
#  {format_help_commands([QUEUE_SHOW_C + " S", QUEUE_SHOW_COMMAND + " S"])} {QUEUE_SHOW_COMMAND_DESCRIPTION}
#  {format_help_commands([QUEUE_SHOW_LOAD_C + " X"])} {QUEUE_SHOW_LOAD_COMMAND_DESCRIPTION}
#  {format_help_commands([QUEUE_SHOW_CLEAR_C, QUEUE_SHOW_CLEAR_COMMAND])} {QUEUE_SHOW_CLEAR_COMMAND_DESCRIPTION}
#  {format_help_commands([QUEUE_SHOW_POP_C, QUEUE_SHOW_POP_COMMAND])} {QUEUE_SHOW_POP_COMMAND_DESCRIPTION}
#  {format_help_commands([PLAY_SHOW_COMMAND + " S"])} {PLAY_SHOW_COMMAND_DESCRIPTION}
#  {format_help_commands([END_COMMAND + " X"])} {END_COMMAND_DESCRIPTION}
#  {format_help_commands([SUSPEND_COMMAND])} {SUSPEND_COMMAND_DESCRIPTION}
#  {format_help_commands([SUSPEND_QUEUE_COMMAND])} {SUSPEND_QUEUE_COMMAND_DESCRIPTION}
#  {format_help_commands([SHUTDOWN_COMMAND + " X"])} {SHUTDOWN_COMMAND_DESCRIPTION}
#  {format_help_commands([LAST_EPISODE_COMMAND + " X"])} {LAST_EPISODE_COMMAND_DESCRIPTION}
#  {format_help_commands([SKIP_FULL_EPISODE_COMMAND])} {SKIP_FULL_EPISODE_COMMAND_DESCRIPTION}
#  {format_help_commands([FINISH_FULL_EPISODE_COMMAND])} {FINISH_FULL_EPISODE_COMMAND_DESCRIPTION}
#  {format_help_commands([SKIP_TO_COMMERCIALS_C, SKIP_TO_COMMERCIALS_COMMAND])} {SKIP_TO_COMMERCIALS_COMMAND_DESCRIPTION}
#  {format_help_commands([COMMERCIALS_GLOBAL_OVERRIDE_C, COMMERCIALS_GLOBAL_OVERRIDE_COMMAND])} {COMMERCIALS_GLOBAL_OVERRIDE_COMMAND_DESCRIPTION}
#  {format_help_commands([HARD_PLAY_COMMAND])} {HARD_PLAY_COMMAND_DESCRIPTION}
#  {format_help_commands([HARD_PAUSE_COMMAND])} {HARD_PAUSE_COMMAND_DESCRIPTION}
#  {format_help_commands([VIDEO_TRACK_COMMAND + " X"])} {VIDEO_TRACK_COMMAND_DESCRIPTION}
#  {format_help_commands([AUDIO_TRACK_COMMAND + " X"])} {AUDIO_TRACK_COMMAND_DESCRIPTION}
#  {format_help_commands([SUBTITLE_TRACK_COMMAND + " X"])} {SUBTITLE_TRACK_COMMAND_DESCRIPTION}
#  {format_help_commands([HISTORY_COMMAND])} {HISTORY_COMMAND_DESCRIPTION}
#  {format_help_commands([MONITOR_COMMAND + " X"])} {MONITOR_COMMAND_DESCRIPTION}
#  {format_help_commands([MONITOR_PLAYER_JUMPS_COMMAND])} {MONITOR_PLAYER_JUMPS_COMMAND_DESCRIPTION}
#  {format_help_commands([MONITOR_OFF_COMMAND])} {MONITOR_OFF_COMMAND_DESCRIPTION}
#  {format_help_commands([DEBUG_PLAYER_JUMPS_COMMAND])} {DEBUG_PLAYER_JUMPS_COMMAND_DESCRIPTION}
#  {format_help_commands([MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND])} {MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND_DESCRIPTION}
#  {format_help_commands([MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND])} {MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND_DESCRIPTION}
#  {format_help_commands([ABOUT_COMMAND])} {ABOUT_COMMAND_DESCRIPTION}
#
# Suggestions for new functionalities and improvements for {PROGRAM_NAME} are always welcome at {color.YELLOW}{color.BOLD}gitlab.com/CptMaister/ranimetv{color.END} or {color.GREEN}{color.BOLD}alex.mai@posteo.net{color.END} :-)
# ---*** End of {PROGRAM_NAME} HELP ***---"""


#
# MESSAGE_CONFIG_HELP = f"""{color.BOLD}---*** Config Help ***{color.BOLD}---
#     {color.BOLD}{color.YELLOW}{"Simple Examples".upper()}{color.END}
#
#     {color.YELLOW}Example 1 - required arguments for 2 shows:{color.END}
# {color.BOLD}Death Note;            01; 37;    13
# JoJo's 5: Golden Wind; 22; 39;    22
#
# Death Note = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
# JoJo's 5: Golden Wind = /home/user/Desktop/JoJo/Part 5/JJBA_GW_ ; .mp4{color.END}
#
#     {color.YELLOW}Explanation:{color.END}
# On the next run {PROGRAM_NAME} will randomly pick one of these 2 Animes and will either try to play the file
# /home/user/Videos/Favorite Anime/DeathNote/Death-Note-13-rip.mp3
# or
# /home/user/Desktop/JoJo/Part 5/JJBA_GW_22.mp4
#
#
#     {color.YELLOW}Example 2 - three shows and some optional arguments:{color.END}
# {color.BOLD}{CONFIG_GLOBAL_OPTIONS_LINE_STARTER} ending:true; speed:90; volume: 115
# DeathNote; 01; 37; 13; ending:false; pause:true;
# JoJo5; 22; 39; 22; opening:true.mp3; jump:10;
# Samurai Champloo; 3; 8; 3
#
# DeathNote = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
# JoJo5 = /home/user/Desktop/JoJo/Part 5/JJBA_GW_ ; .mp4
# Samurai Champloo = '/home/user/Videos/oldanime/samurai champloo ' ; ' hq.avi'{color.END}
#
#     {color.YELLOW}Explanation:{color.END}
# Global options are set and will be applied to all episodes, if they don't set other values for the specified options:
#   play ending after each episode
#   playback speed is at 90%
#   global volume multiplier 115% (or 1.15)
# On the next run {PROGRAM_NAME} will randomly pick one of these 3 Animes and apply some extra show specific options in the first two cases, if one of them is picked. The episodes to be played would be
# /home/user/Videos/Favorite Anime/DeathNote/Death-Note-13-rip.mp3
# or
# /home/user/Desktop/JoJo/Part 5/JJBA_GW_22.mp4
# or
# /home/user/Videos/oldanime/samurai champloo 3 hq.avi
#
# The Death Note episode would start in a paused state. When the episode is finished, no ending is played because the ending:false overrides the global ending option value.
#
# Before the JoJo 5 episode starts, the file
# /home/user/Desktop/JoJo/Part 5/JJBA_GW_op.mp3
# will be played, afterwards the episode, but it will start at 10 seconds.
#
# The Samurai Champloo episode has no individual show options so only the global options will take effect.
#
#
#     {color.YELLOW}Example 3 - directory path lines and commercials:{color.END}
# {color.BOLD}{CONFIG_GLOBAL_OPTIONS_LINE_STARTER} debug: true, unique: true; commercial:13
#
# # Animes
# DeathNote;  01;  37;    13;  opening:true; ending:true; episodes: 2
# JoJo5;      8;   last;  22   #get opening and ending
# Dr. Stone;  1;   12;    3;   random:true
#
# # my commercials
# Nintendo Trailer;  1;   last;  17;  random:True; commercial:6; commercial-max: 1
# Nintendo Teaser;   11;  20;    15;  random:True; commercial:2
#
# # path lines
# DeathNote = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
# Dr. Stone = /home/user/Videos/last season/dr-stone/
# JoJo5 = /home/user/Desktop/JoJo/Part 5
# Nintendo Trailer = /home/user/Gaming/Nintendo/introductions
# Nintendo Teaser = /home/user/Gaming/Nintendo/short trailers{color.END}
#
#     {color.YELLOW}Explanation:{color.END}
#   Global options
# For every playback the experimental debugger will be on. It detects changes in playback length and tries to restart the playback at {DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD} seconds after the detected length change.
# The unique option will avoid getting shows into the queue more than once as long as there are shows that haven't been played during this session.
# The commercial option with value 13 sets the global commercial weight to 13. This means that between every episode (including opening and ending) there will be commercials played, if there are show lines with a commercial weight lower than the global commercial weight. Type '{OPTIONS_HELP_COMMAND}' to learn more about these options.
#
#   Show and path lines
# As you can see, there is a lot different in this example. Let's look at one funcitonality at a time.
#
# Directory path lines:
# Instead of writing the whole path with the episode number missing, you can write the directory (also known as 'folder') where the files are located in. In this example, all but one show use this way of declaring the path.
# This way you can have files with wildly differing names played as one show in {PROGRAM_NAME} without having to rename them. They just have to be in their own directory.
# The files will be sorted by {PROGRAM_NAME} alphabetically.
# When you use a directory path instead of an exact path, you can use the word 'last' in place of the last episode. As seen in the example, this sets the last file of the directory as the last episode. This is especially useful if you add files to that directory frequently.
#
# Show Options:
# Death Note will always play 2 episodes in a row, without an opening and ending in between.
# JoJo 5 has no options, just a comment. Everything after a '#' in a line will be ignored. Use this to get some structure into your config file for better readability, as seen in this example.
# Dr. Stone episodes will be in a randomized order.
# Nintendo Trailer and Nintendo Teaser will be excluded from normal playback and will not be automatically put into the queue, as they have a commercial value assigned to them. They will be played between episodes. Their commercial weight is used to determine how many commercials to play. In this example we have a global commercial weight of 13, so there could be 3 Nintendo Teasers and 1 Nintendo Trailer, with a total weight of 12. Or 6 Nintendo Teasers and 0 Nintendo Trailers. Although 2 Nintendo Trailers would be within the global commercial weight, the {COMMERCIAL_MAX_OPTION_SHOW} command limits Nintendo Trailers to appear at most once during the commercials between two episodes.
#
#
#     {color.BOLD}{color.GREEN}{"Allowed Config lines".upper()}{color.END}
#
# The order of the lines is usually completely irrelevant (only exceptions: two Path Lines with the same ALIAS, and if there are two Global Config Lines. The first one is always picked, see below). {PROGRAM_NAME} looks for special characters to determine a line's role in the config.
# Comment like in programming with '#'.
# Global Options Line starts with '{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}'.
# Suspended queue line starts with '{CONFIG_SUSPEND_QUEUE_LINE_STARTER}', see command '{SUSPEND_QUEUE_COMMAND}' for more info.
# Path Lines contain a '='.
# Empty lines or lines consisting of only whitespace are ignored.
# All other lines are considered Show Lines.
#
#
#     {color.GREEN}Comments with #{color.END}
# No matter which line, everything after the first appearance of a '#' in a line will be cut off, including the '#' itself. A line starting with (whitespaces and then a) # will be ignored completely.
#
#
#     {color.GREEN}Global Options Line (not required):{color.END}
# {color.BOLD}{CONFIG_GLOBAL_OPTIONS_LINE_STARTER} OPTION:VALUE; OPTION:VALUE; ....{color.END}
# Set optional global options here that are applied to every show in the config file.
# Start the line with '{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}', then list the options, separated by ; (semicolon).
# An option conists of the option name, then the option value, separated by : (colon).
#
# Type '{color.BOLD}{OPTIONS_HELP_COMMAND}{color.END}' to see all global and local options available.
#
#
#     {color.GREEN}Show Lines - Necessary Arguments:{color.END}
# {color.BOLD}ALIAS; FIRST; LAST; CURRENT [; OPTIONS]{color.END}
# ALIAS is a nickname you give your show. It has to be the same as the ALIAS in the corresponding Path Line (see below).
# FIRST and LAST are integers specifying the range of episodes that are to be played by {PROGRAM_NAME}.
# CURRENT is an integer indicating the episode to play when this show line is picked for playback. When using a directory path line instead of an exact path line (see below), you can use 'last' as a special keyword for the last file in the directory.
#
# A show line has to consist of these first 4 arguments at least, each separated by a single ; (semicolon). OPTIONS are again separated from the necessary arguments by a ; and are optional and explained below.
#
# Every line in your config is considered a Show Line if it doesn't contain a '=' (equals sign) or the global options line specifier '{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}'.
#
# An incorrect Show Line can be picked up for playback and will result in an Error if it is either incorrect in its necessary arguments, or has no correct corresponding Path Line. In this case, {PROGRAM_NAME} will skip this show and try another. Be sure to check the terminal for errors or you might miss out on a show without realizing.
#
# Leading and trailing whitespaces (Spaces, Tabs etc) are ignored, but of course shouldn't randomly appear inside the arguments: '17' is a valid integer, but '1 7' isn't. ALIAS 'DeathNote' will not be interpreted as the same as 'Death Note' because of the added whitespace. But multiple spaces and tabs are reduces to a single space, so 'Death   \tNote' (3 spaces and a tab) is the same as 'Death Note' (1 space). As a consequence, the paths on your local computer can only have a single space as whitespaces in a row.
#
#
#     {color.GREEN}Show Lines - Optional Options (not required):{color.END}
# {color.BOLD}OPTION:VALUE; OPTION:VALUE; ....{color.END}
# All options, that are not specified here, will default to your global options or the default options of {PROGRAM_NAME}.
# Just like the necessary arguments in a show line, each option has to be separated by ; (semicolon) and make sure to have a semicolon between the last necessary Show Line argument CURRENT and the first option.
# An option conists of the option name, then the option value, separated by : (colon).
#
# Type '{color.BOLD}{OPTIONS_HELP_COMMAND}{color.END}' to see all global and local options available.
#
#
#     {color.GREEN}Path Lines - connect an ALIAS to a real path on your system:{color.END}
# {color.BOLD}ALIAS = PATH_UP_TO_EPISODE_NUMBER ; PATH_AFTER_EPISODE_NUMBER{color.END}
# or
# {color.BOLD}ALIAS = PATH_TO_DIRECTORY{color.END}
# ALIAS is supposed to be an ALIAS that exists in a Show Line. When a Show Line is picked for playback, {PROGRAM_NAME} will try to find its corresponding Path Line. It will pick the first in the list that has the same ALIAS, so any Path Lines with duplicate ALIASes will be ignored after the first one.
# You can either specify the exact path to the episodes with just the episode number missing or you can specify the path to the directory where the files are located.
#
# Exact Path:
# PATH_UP_TO_EPISODE_NUMBER is the full path of the show's files on your system up to the episode number.
# PATH_AFTER_EPISODE_NUMBER is the rest of the path after the episode number, including the file type, like '.mp3'.
#
# Directory Path:
# PATH_TO_DIRECTORY is the path including the directory where the files are located in. It shouldn't matter if there is a slash at the end or not.
#
# {color.BOLD}---*** End of Config Help ***---{color.END}"""
#
#
#
# def format_options_help_specifiers(specifier: str, value_repr: str, value_extra: str=None) -> str:
#
#     # build string to return
#     if value_repr == "int":
#         pure_string = f'{specifier}:INT'
#     elif value_repr == "bool":
#         pure_string = f'{specifier}:BOOL'
#     else:
#         pure_string = f'{specifier}:{value_repr}'
#
#     if value_extra != None:
#         pure_string = f'{pure_string}.{value_extra}'
#
#     # add formatting to string:
#     return f' {color.BOLD}{pure_string}{color.END}'
#
#
# # TODO gui add explanatory texts to options settings help in gui
# MESSAGE_OPTIONS_HELP = f"""{color.BOLD}---*** Options Help ***---{color.END}
#
#     {color.BOLD}{color.YELLOW}About options{color.END}
# Options can appear in a show line after the necessary show line arguments (see config help, type '{CONFIG_HELP_COMMAND}') and in a single global options line. Depending on show or global options, there are different options, sometimes with same or similar names but different effects. See this list for all options.
#
# Default values for all options are usually False or 0, {VOLUME_OPTION} and {SPEED_OPTION} default values are 100, video, audio and subtitle track defaults are whatever the player picks, which is ususally track 1.
#
#
#     {color.BOLD}{color.YELLOW}How to read the options list{color.END}
# Each option consists of a specifier and a value. Options are separated by '{CONFIG_FIELD_SEPARATOR}' while a specifier is separated by its value by ':', for example
# {UNIQUE_SHOWS_OPTION_GLOBAL}:True; {VOLUME_OPTION}:115
#
# Almost all options take a positive integer (like 0, 17, 130) or a True/False as value. Some few options also take extra values, which have to be separated with '.' from the main value, like 'opening:True.mp3'. The video, audio and subtitle track options also take negative integers.
#
# In the following list INT is a placeholder for an integer and BOOL a placeholder for True or False.
#
#
#     {color.BOLD}{color.YELLOW}About formatting{color.END}
# - All option specifiers and values are case-insensitive, so you can write capital letters wherever you want.
# - All whitespaces are ignored, as long as they don't appear within specifiers or values, which would separate them.
# - Instead of True you can write any of these: {', '.join(CONFIG_OPTIONS_YES_OPTIONS)}
# - Instead of False you can write any of these: {', '.join(CONFIG_OPTIONS_NO_OPTIONS)}
# - All of these options are equal for {PROGRAM_NAME}:
#   opening:True; random:False
#   OPENing: y; rANDOM: NO
#      opening  : On  ; random : off
#   OPENING:t; RANDOM:f
#
#
#     {color.BOLD}{color.YELLOW}About overriding and duplicate options{color.END}
# Most options exist with the exact same specifier for global and show options. In this case, the show option overrides the global option in most cases, especially if not further specified in the options list below.
#
#
#     {color.BOLD}{color.GREEN}Global Options{color.END}
# {format_options_help_specifiers(OPENING_OPTION, "bool", "FILETYPE")}
#     Play opening before episode.
#     - If show's exact path is given in path line, then the opening file to be played should have the same name like the episode, but replace the episode number with 'op', like Death-Note-op-rip.mp3 instead of Death-Note-7-rip.mp3
#     - If not show's exact path, but only directory given in path line, then the opening file to be played should be named op.mp3 or op.avi or any file type, the first found one will be picked.
#     - FILETYPE is optional and can be something like 'mp3' or 'avi' to play an opening with a filetype different from the episode files.
#     - You can have multiple opening files to simulate different show seasons. If you want to play a different opening from episode 23 on, replace 'op' with 'op23' in the filename. This file will then be played for all episodes from episode 23, as long as there is no higher opening file that is still equal or lower than your current episode.
# {format_options_help_specifiers(ENDING_OPTION, "bool", "FILETYPE")}
#     Play ending after episode.
#     - Same explanations as with opening, but replace 'op' with 'ed'.
# {format_options_help_specifiers(STATUS_OPTION, "bool")}
#     Display full status on every playback start.
# {format_options_help_specifiers(PAUSE_OPTION, "bool")}
#     Pause player on every playback start.
# {format_options_help_specifiers(VOLUME_OPTION, "int")}
#     INT {VOLUME_LIMIT_LOWER}-{VOLUME_LIMIT_UPPER}, volume multiplier in percent.
#     - Is multiplied with the show volume option and the session volume option for the total playback volume.
# {format_options_help_specifiers(JUMP_OPTION, "int")}
#     Jump to INT seconds on every playback start, gets overridden by show jump option.
# {format_options_help_specifiers(SPEED_OPTION, "int")}
#     INT {SPEED_LIMIT_LOWER}-{SPEED_LIMIT_UPPER}, playback speed for every played file, gets overridden by show speed option.
# {format_options_help_specifiers(MUTE_OPTION, "bool")}
#     Mute player of first playback when starting {PROGRAM_NAME}, no other playback will be muted by this option.
# {format_options_help_specifiers(FULLSCREEN_OPTION, "bool")}
#     Fullscreen player, no effect if no video track available or currently chosen.
# {format_options_help_specifiers(QUEUE_SHOW_DISPLAY_OPTION, "bool")}
#     Display queue on episode playback start.
# {format_options_help_specifiers(SUSPEND_OPTION, "bool")}
#     If {SUSPEND_OPTION} is activated, then exiting {PROGRAM_NAME} using '{EXIT_COMMAND}' is the same as using '{SUSPEND_COMMAND}'. Global option '{SUSPEND_OPTION}' is compatible with global option'{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}', but using '{SUSPEND_OPTION}' as a show option is not compatible with option '{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}'.
# {format_options_help_specifiers(SUSPEND_QUEUE_OPTION, "bool")}
#     If {SUSPEND_QUEUE_COMMAND} is activated, then exiting {PROGRAM_NAME} using '{EXIT_COMMAND}' is the same as using '{SUSPEND_QUEUE_COMMAND}'. Overrides option '{SUSPEND_OPTION}'. Global option '{SUSPEND_QUEUE_OPTION}' is compatible with global option'{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}', but using '{SUSPEND_QUEUE_OPTION}' as a show option is not compatible with option '{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}'.
# {format_options_help_specifiers(NO_RERUN_OPTION, "bool")}
#     Disable a show line in config file if last episode was played by adding a '#' at the beginning. This makes the show line a comment, which is completely ignored by {PROGRAM_NAME} afterwards. Current episode in config will still be set to first episode. To enable a show line again, just delete the '#' at the beginning of the show line in the config file. This has no effect on shows with option '{RANDOM_OPTION_SHOW}'.
# {format_options_help_specifiers(MONITOR_OPTION, "int")}
#     Print full player status every INT seconds.
# {format_options_help_specifiers(MONITOR_PLAYER_JUMPS_OPTION, "bool")}
#     Print full player status if the playback length or current playback position changed more than it should with normal playback. This also triggers when you intentionally change the playback position and was mainly used for analysing the debug player jumps functionality.
# {format_options_help_specifiers(DEBUG_PLAYER_JUMPS_OPTION, "bool")}
#     Experimental auto restarter, triggers on drastic encountered player length or position changes, then restarts the current playback and jumps to 1 second after the change was registered.
#
# {format_options_help_specifiers(PROBABILITY_OPTION_GLOBAL, "int")}
#     INT {PROBABILITY_LIMIT_LOWER}-{PROBABILITY_LIMIT_UPPER}, sets default probability for all shows that don't use the probability option themselves.
# {format_options_help_specifiers(COMMERCIALS_PLAY_OPTION_GLOBAL, "int")}
#     If set, then show option '{COMMERCIAL_WEIGHT_OPTION_SHOW}' is ignored, so commercials will be played like normal shows.
#     - Usually no show with commercial option will be randomly picked for a show, only if user intentionally gets it with commands '{PLAY_SHOW_COMMAND}' or '{QUEUE_SHOW_COMMAND}'.
# {format_options_help_specifiers(COMMERCIAL_WEIGHT_OPTION_GLOBAL, "int")}
#     Sets the global commercial weight to INT.
#     - If global commercial weight is higher than 0, then commercials will be played between episodes, if any are set. A simple search and fill algorithm will pick shows that are marked as commercials until the global commercial weight is full and then play them.
# {format_options_help_specifiers(SHUTDOWN_OPTION_GLOBAL, "int")}
#     Activates automatic shutdown with INT minutes, turning off {PROGRAM_NAME} in INT minutes. Can be manually disabled with '{SHUTDOWN_COMMAND} 0', behaves like the '{SHUTDOWN_COMMAND}' command.
# {format_options_help_specifiers(LAST_EPISODE_OPTION_GLOBAL, "int")}
#     Turn off {PROGRAM_NAME} after INT finished episodes.
# {format_options_help_specifiers(UNIQUE_SHOWS_OPTION_GLOBAL, "bool")}
#     Will avoid playing shows more than once during a session. Once every show was finished once, every show can be played again. Gets overridden by user choices like '{PLAY_SHOW_COMMAND}' and '{QUEUE_SHOW_COMMAND}'.
# {format_options_help_specifiers(HARD_UNIQUE_SHOWS_OPTION_GLOBAL, "bool")}
#     Will prevent playing shows more than once during a session. End {PROGRAM_NAME} if every show finished once. Queue will not be automatically filled anymore by shows that have been finished before. Gets overridden by user choices like '{PLAY_SHOW_COMMAND}' and '{QUEUE_SHOW_COMMAND}'.
#
#
#     {color.BOLD}{color.GREEN}Show Options{color.END}
# {format_options_help_specifiers(OPENING_OPTION, "bool", "FILETYPE")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(ENDING_OPTION, "bool", "FILETYPE")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(STATUS_OPTION, "bool")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(PAUSE_OPTION, "bool")}
#     Same as global option, but only for this show's episode.
# {format_options_help_specifiers(VOLUME_OPTION, "int")}
#     INT {VOLUME_LIMIT_LOWER}-{VOLUME_LIMIT_UPPER}, volume multiplier in percent.
#     - Is multiplied with the global volume option and the session volume option for the total playback volume.
# {format_options_help_specifiers(JUMP_OPTION, "int")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(SPEED_OPTION, "int")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(MUTE_OPTION, "bool")}
#     Mute player when this show's episode starts.
# {format_options_help_specifiers(FULLSCREEN_OPTION, "bool")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(QUEUE_SHOW_DISPLAY_OPTION, "bool")}
#     Display queue on this show's episode playback start.
# {format_options_help_specifiers(SUSPEND_OPTION, "bool")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(SUSPEND_QUEUE_OPTION, "bool")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(NO_RERUN_OPTION, "bool")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(MONITOR_OPTION, "int")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(MONITOR_PLAYER_JUMPS_OPTION, "bool")}
#     Same as global option, but only for this show.
# {format_options_help_specifiers(DEBUG_PLAYER_JUMPS_OPTION, "bool")}
#     Same as global option, but only for this show.
#
# {format_options_help_specifiers(RANDOM_OPTION_SHOW, "bool")}
#     Randomizes which episode to play.
# {format_options_help_specifiers(MULTIPLE_EPISODES_OPTION_SHOW, "int")}
#     Play INT episodes of this show in a row, skipping endings, commercials and openings in between.
#     - Ignored if INT is 0 or 1.
# {format_options_help_specifiers(PROBABILITY_OPTION_SHOW, "int")}
#     INT {PROBABILITY_LIMIT_LOWER}-{PROBABILITY_LIMIT_UPPER}, sets probability for this show.
#     - When a show is randomly picked for the queue, then the chance for it to be added to the queue is this probability, if set. Else default probability is 100 or set by global probability option.
# {format_options_help_specifiers(COMMERCIAL_WEIGHT_OPTION_SHOW, "int")}
#     Sets show's commercial weight.
#     - This will classify this show as a commercial and exclude it from automatic addition to queue.
#     - This commercial can be picked for commercial playback between episodes, if the global commercial weight is at least as this commercial's weight.
# {format_options_help_specifiers(COMMERCIAL_MAX_OPTION_SHOW, "int")}
#     Maximum amount of times this show is played as a commercial between episodes.
#     - No effect if no commercial weight set on this show.
# {format_options_help_specifiers(COMMERCIAL_MIN_OPTION_SHOW, "int")}
#     Minimum amount of times this show is played as a commercial between episodes by giving it priority at being picked until minimum amount is satisfied.
#     - Does not override the global commercial weight.
#     - No effect if no commercial weight set on this show.
# {format_options_help_specifiers(VIDEO_TRACK_OPTION_SHOW, "int")}
#     Sets video track for this show's episode.
# {format_options_help_specifiers(AUDIO_TRACK_OPTION_SHOW, "int")}
#     Sets audio track for this show's episode.
# {format_options_help_specifiers(SUBTITLE_TRACK_OPTION_SHOW, "int")}
#     Sets subtitle track for this show's episode.
#
# {color.BOLD}---*** End of Options Help ***---{color.END}"""
#
#
# MESSAGE_REPORT_BUGS = f'Please report bugs to {color.BOLD}{color.GREEN}alex.mai@posteo.net{color.END} or at {color.BOLD}{color.YELLOW}gitlab.com/CptMaister/ranimetv{color.END}\n'
#




##### Other Setup #####

# some threading.Events
session_running_event = Event()

event_player_Stopped = Event()
playing_opening_event = Event()
playing_episode_event = Event()
playing_ending_event = Event()
playing_commercials_event = Event()

episode_finished_event = Event()
episode_skipped_event = Event()
op_or_ed_or_cm_skipped_event = Event()
episode_stop_all_playback_event = Event()
episode_skip_to_commercial_event = Event()
episode_error_event = Event()
player_restart_event = Event()
episode_full_restart_event = Event()

exit_program_event = Event()
end_session_event = Event()
input_registered_event = Event()
input_handled_event = Event()

shutdown_running_event = Event() # used for 'shutdown' option and command
# monitoring_running_event = Event() # used for 'monitor' option and command
# monitoring_player_jumps_running_event = Event() # used for 'monitor-jump' option and command

# used for 'debug' command and option
event_player_Playing = Event()
media_changed_counter = 0
event_player_LengthChanged_event = Event()
debug_player_jumps_running_event = Event()
debug_player_last_time = 0
DEBUG_PLAYER_JUMPS_INTERVALL = 0.1

# used for suspend command and option
suspend_event = Event()
suspend_queue_event = Event()
left_off_at_seconds = 0

# used for 'episodes' show option
skip_straight_to_next_episode_event = Event()
skip_straight_to_next_episode_counter = 0

# used for 'last' command and global option
last_episodes_for_today_counter = 0

# used for 'shutdown' command and option
shutdown_minutes = 1

# used for 'analysis' and 'analysis-full' command
analysis_running_event = Event()


all_events = []
all_events.append(session_running_event)
all_events.append(event_player_Stopped)
all_events.append(playing_opening_event)
all_events.append(playing_episode_event)
all_events.append(playing_ending_event)
all_events.append(playing_commercials_event)
all_events.append(episode_finished_event)
all_events.append(episode_skipped_event)
all_events.append(op_or_ed_or_cm_skipped_event)
all_events.append(episode_stop_all_playback_event)
all_events.append(episode_skip_to_commercial_event)
all_events.append(episode_error_event)
all_events.append(player_restart_event)
all_events.append(episode_full_restart_event)
all_events.append(exit_program_event)
all_events.append(end_session_event)
all_events.append(input_registered_event)
all_events.append(input_handled_event)
all_events.append(shutdown_running_event)
# all_events.append(monitoring_running_event)
# all_events.append(monitoring_player_jumps_running_event)
all_events.append(event_player_Playing)
all_events.append(event_player_LengthChanged_event)
all_events.append(debug_player_jumps_running_event)
all_events.append(suspend_event)
all_events.append(suspend_queue_event)
all_events.append(skip_straight_to_next_episode_event)
all_events.append(analysis_running_event)



### gui update events

gui_event_media_area_update_player_Playing = Event()

# management top row
gui_event_users_update_combo = Event()
gui_event_profiles_update_combo = Event()
gui_event_basepaths_update_combo = Event()

# tables
gui_event_shows_update_table = Event()
gui_event_cms_update_table = Event()
gui_event_queue_update_table = Event()
gui_event_history_update_table = Event()

# lists
gui_event_shows_cms_edit_list_update_list = Event()

gui_event_media_changed_update_equalizer_visuals = Event()


all_gui_update_events = []
all_gui_update_events.append(gui_event_media_area_update_player_Playing)

all_gui_update_events.append(gui_event_users_update_combo)
all_gui_update_events.append(gui_event_profiles_update_combo)
all_gui_update_events.append(gui_event_basepaths_update_combo)

all_gui_update_events.append(gui_event_shows_update_table)
all_gui_update_events.append(gui_event_cms_update_table)
all_gui_update_events.append(gui_event_queue_update_table)
all_gui_update_events.append(gui_event_history_update_table)

all_gui_update_events.append(gui_event_shows_cms_edit_list_update_list)
all_gui_update_events.append(gui_event_media_changed_update_equalizer_visuals)

def updateAllGui():
    for e in all_gui_update_events:
        e.set()

def updatedShowUpdateGui():
    gui_event_shows_update_table.set()
    gui_event_cms_update_table.set()
    gui_event_queue_update_table.set()
    gui_event_shows_cms_edit_list_update_list.set()
    gui_event_media_changed_update_equalizer_visuals.set()



#
# ### Input Handling ###
# # see https://stackoverflow.com/questions/5404068/how-to-read-keyboard-input/53344690#53344690
# inputQueue = Queue()
#
# def read_keyboard_input():
#     global inputQueue
#     event_player_Playing.wait() # wait for player to start playing for the first time
#     message_input_startup = f'Ready for keyboard input. Type \'{HELP_COMMAND}\' to see all commands :-)'
#     print(message_input_startup)
#
#     input_program_dead_counter = 0
#     input_handled_event.set()
#     while True:
#         input_str = input()
#         inputQueue.queue.clear() # in case someone is typing really fast
#         inputQueue.put(input_str)
#
#         input_registered_event.set() # this will activate handle_input() in the playThread
#
#         # make sure program is handling inputs, else print warning to terminal
#         if not input_handled_event.is_set():
#             input_program_dead_counter = input_program_dead_counter + 1
#         else:
#             input_handled_event.clear()
#             input_program_dead_counter = 0
#
#         if input_program_dead_counter == 1:
#             message = f'\n{color.ERROR}Input can\'t be handled.{color.END} The program probably crashed somewhere.\nPress Ctrl+C to force quit.'
#             print(message)
#             print(MESSAGE_REPORT_BUGS)
#
#         if input_program_dead_counter >= 5:
#             message = f'\n{color.ERROR}Input still can\'t be handled.{color.END} Exiting'
#             print(message)
#             exit_program_event.set()
#
#
def player_playing_wait():
    event_player_Stopped.wait()

# def player_wait_input(player: vlc.MediaPlayer, show: ConfigUserShow, main_setup: MainSetup):
#     # input handling, looping as long as the player hasn't stopped
#     event_player_Playing.wait()
#     printdev("HEYOOOOOO delete at player_wait_input")
#     while not event_player_Stopped.is_set():
#         input_registered_event.wait() # this is set in the inputThread when input is registered

        # input_handled_event.set() # tells the input that program is alive
        #
        # if not event_player_Stopped.is_set():
        #     try:
        #         handle_input(player, show, main_setup)
        #     except Exception as e:
        #         print(e)
        #         message = f'{color.RED}Something went wrong with the input.{color.END} If it doesn\'t respond, close the terminal or press Ctrl+C to force quit.'
        #         print(message)
        #         print(MESSAGE_REPORT_BUGS)
        #
        # input_registered_event.clear()


# def handle_input(player: vlc.MediaPlayer, show: ConfigUserShow, main_setup: MainSetup):
#     global inputQueue
#
#     if (inputQueue.qsize() > 0):
#         input_str = inputQueue.get().strip()
#
#         if (input_str.lower() == HELP_COMMAND or input_str == HELP_C) and input_str != HELP_FULL_COMMAND:
#             input_handle_help()
#
#         elif input_str == HELP_FULL_COMMAND or input_str == HELP_FULL_C:
#             input_handle_help_full()
#
#         elif input_str.lower() == STATUS_COMMAND or input_str == STATUS_C:
#             input_handle_status(player, show)
#
#         elif input_str.lower() == PAUSE_COMMAND or input_str == PAUSE_C:
#             input_handle_pause(player)
#
#         elif input_str.lower() == VOLUME_COMMAND or input_str == VOLUME_C:
#             input_handle_volume_display(player, show, main_setup)
#
#         elif input_str.lower().startswith(VOLUME_COMMAND + " ") or input_str.startswith(VOLUME_C + " "):
#             input_handle_volume_x(player, show, main_setup, input_str)
#
#         elif input_str.lower().startswith(JUMP_COMMAND + " ") or input_str.startswith(JUMP_C + " "):
#             input_handle_jump_x(player, input_str)
#
#         elif input_str.lower() == BACK_COMMAND or input_str == BACK_C:
#             input_handle_back(player)
#
#         elif input_str.lower().startswith(BACK_COMMAND + " ") or input_str.startswith(BACK_C + " "):
#             input_handle_back_x(player, input_str)
#
#         elif input_str.lower() == FORWARD_COMMAND or input_str == FORWARD_C:
#             input_handle_forward(player)
#
#         elif input_str.lower().startswith(FORWARD_COMMAND + " ") or input_str.startswith(FORWARD_C + " "):
#             input_handle_forward_x(player, input_str)
#
#         elif input_str.lower() == RESTART_COMMAND or input_str == RESTART_C:
#             input_handle_restart(player)
#
#         elif input_str.lower() == SKIP_EPISODE_COMMAND:
#             input_handle_player_next_no_save(player, main_setup)
#
#         elif input_str.lower() == FINISH_EPISODE_COMMAND:
#             input_handle_player_next_with_save(player, show, main_setup)
#
#         elif input_str.lower() == SHOW_CONFIG_COMMAND or input_str == SHOW_CONFIG_C:
#             input_handle_show_config(show)
#
#         elif input_str.lower() == CONFIG_HELP_COMMAND:
#             input_handle_config_help()
#
#         elif input_str.lower() == EXIT_COMMAND or input_str == EXIT_C or input_str == EXIT_C_2:
#             input_handle_exit(player, option_handle_bools(show.showSettings.suspend, main_setup.get_current_profile_settings().suspend), option_handle_bools(show.showSettings.suspendQueue, main_setup.get_current_profile_settings().suspendQueue))
#
#
#         ## extra commands
#         elif input_str.lower() == OPEN_PLAYBACK_DIRECTORY_COMMAND:
#             input_handle_open_playback_directory(player)
#
#         elif input_str.lower() == OPEN_CONFIG_COMMAND:
#             input_handle_open_config()
#
#         elif input_str.lower() == OPTIONS_HELP_COMMAND:
#             input_handle_options_help()
#
#         # gui not needed
#         # elif input_str.lower() == DISPLAY_CURRENT_OPTIONS_COMMAND:
#         #     input_handle_display_current_options(show, main_setup)
#
#         elif input_str.lower() == SPEED_COMMAND:
#             input_handle_speed_display(player)
#
#         elif input_str.lower().startswith(SPEED_COMMAND + " "):
#             input_handle_speed_x(player, main_setup, input_str)
#
#         elif input_str.lower().startswith(HARD_VOLUME_COMMAND + " "):
#             input_handle_hard_volume_x(player, show, main_setup, input_str)
#
#         elif input_str.lower() == MUTE_COMMAND or input_str == MUTE_C:
#             input_handle_mute(player, main_setup)
#
#         elif input_str.lower() == FULLSCREEN_COMMAND:
#             input_handle_fullscreen_toggle(player)
#
#         elif input_str.lower() == DISPLAY_SHOWS_COMMAND:
#             input_handle_display_shows(main_setup)
#
#         # TODO gui redo
#         elif input_str.lower() == QUEUE_SHOW_COMMAND or input_str == QUEUE_SHOW_C:
#             input_handle_queue_show_display(main_setup)
#
#         # TODO gui redo
#         elif input_str.lower().startswith(QUEUE_SHOW_COMMAND + " ") or input_str.startswith(QUEUE_SHOW_C + " "):
#             input_handle_queue_show_x(main_setup, input_str)
#
#         # TODO gui redo button click
#         elif input_str.lower() == QUEUE_SHOW_LOAD_COMMAND or input_str == QUEUE_SHOW_LOAD_C:
#             input_handle_queue_show_load(main_setup)
#
#         # TODO json redo, gui button click
#         elif input_str.lower().startswith(QUEUE_SHOW_LOAD_COMMAND + " ") or input_str.startswith(QUEUE_SHOW_LOAD_C + " "):
#             input_handle_queue_show_load_x(main_setup, input_str)
#
#         elif input_str.lower() == QUEUE_SHOW_CLEAR_COMMAND or input_str == QUEUE_SHOW_CLEAR_C:
#             input_handle_queue_show_clear(main_setup)
#
#         # TODO gui redo specify which queue entry to pop/delete
#         elif input_str.lower() == QUEUE_SHOW_POP_COMMAND or input_str == QUEUE_SHOW_POP_C:
#             input_handle_queue_show_pop(main_setup)
#
#         elif input_str.lower().startswith(PLAY_SHOW_COMMAND + " "):
#             input_handle_play_show_x(player, show, main_setup, input_str)
#
#         #elif input_str.lower() == PLAY_EPISODE_COMMAND:
#         #    input_handle_play_episode_current(player)
#         #elif input_str.lower().startswith(PLAY_EPISODE_COMMAND + " "):
#         #    input_handle_play_episode_x(player, input_str)
#         #elif input_str.lower() == PREVIOUS_EPISODE_COMMAND or input_str.lower() == PREVIOUS_EPISODE_C:
#         #    input_handle_previous_episode()
#         #elif input_str.lower() == NEXT_EPISODE_COMMAND:
#         #    input_handle_next_episode()
#         #elif input_str.lower() == LOOP_EPISODE_COMMAND:
#         #    input_handle_loop_episode() #reset debug_player_last_timereseten
#         #elif input_str.lower().startswith(LOOP_EPISODE_COMMAND + " "):
#         #    input_handle_loop_x_episode(input_str) #reset debug_player_last_time
#
#         elif input_str.lower() == END_COMMAND:
#             input_handle_end_of_episode(player)
#
#         elif input_str.lower().startswith(END_COMMAND + " "):
#             input_handle_end_x_of_episode(player, input_str)
#
#         elif input_str.lower() == SUSPEND_COMMAND:
#             input_handle_suspend(player)
#
#         elif input_str.lower() == SUSPEND_QUEUE_COMMAND:
#             input_handle_suspend_queue(player)
#
#         elif input_str.lower() == SHUTDOWN_COMMAND:
#             input_handle_shutdown()
#
#         elif input_str.lower().startswith(SHUTDOWN_COMMAND + " "):
#             input_handle_shutdown_x(player, main_setup.get_current_profile_settings(), input_str)
#
#         elif input_str.lower() == LAST_EPISODE_COMMAND:
#             input_handle_last_episode()
#
#         elif input_str.lower().startswith(LAST_EPISODE_COMMAND + " "):
#             input_handle_last_episode_x(input_str)
#
#         elif input_str.lower() == SKIP_FULL_EPISODE_COMMAND:
#             input_handle_skip_full_episode(player)
#
#         elif input_str.lower() == FINISH_FULL_EPISODE_COMMAND:
#             input_handle_finish_full_episode(player)
#
#         elif input_str.lower() == SKIP_TO_COMMERCIALS_COMMAND or input_str == SKIP_TO_COMMERCIALS_C:
#             input_handle_skip_to_commercials(player)
#
#         elif input_str.lower().startswith(COMMERCIALS_GLOBAL_OVERRIDE_COMMAND + " ") or input_str.startswith(COMMERCIALS_GLOBAL_OVERRIDE_C + " "):
#             input_handle_commercials_global_override_x(main_setup, input_str)
#
#         elif input_str.lower() == HARD_PLAY_COMMAND:
#             input_handle_resume(player)
#
#         elif input_str.lower() == HARD_PAUSE_COMMAND:
#             input_handle_hard_pause(player)
#
#         elif input_str.lower().startswith(VIDEO_TRACK_COMMAND + " "):
#             input_handle_video_track_x(player, show, input_str)
#
#         elif input_str.lower() == VIDEO_TRACK_COMMAND:
#             input_handle_display_video_track(player)
#
#         elif input_str.lower().startswith(AUDIO_TRACK_COMMAND + " "):
#             input_handle_audio_track_x(player, show, input_str)
#
#         elif input_str.lower() == AUDIO_TRACK_COMMAND:
#             input_handle_display_audio_track(player)
#
#         elif input_str.lower().startswith(SUBTITLE_TRACK_COMMAND + " "):
#             input_handle_subtitle_track_x(player, show, input_str)
#
#         elif input_str.lower() == SUBTITLE_TRACK_COMMAND :
#             input_handle_display_subtitle_track(player)
#
#         elif input_str.lower() == HISTORY_COMMAND:
#             input_handle_history(main_setup)
#
#         elif input_str.lower().startswith(MONITOR_COMMAND + " "):
#             input_handle_monitor_x(player, main_setup, show, input_str)
#
#         elif input_str.lower() == MONITOR_PLAYER_JUMPS_COMMAND:
#             input_handle_monitor_jumps(player, show)
#
#         elif input_str.lower() == MONITOR_OFF_COMMAND:
#             input_handle_monitors_off()
#
#         elif input_str.lower() == DEBUG_PLAYER_JUMPS_COMMAND:
#             input_handle_debug_player_jumps(player)
#
#         elif input_str.lower() == MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND:
#             input_handle_media_lengths_analysis_quick(main_setup)
#
#         elif input_str.lower() == MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND:
#             input_handle_media_lengths_analysis_full(main_setup)
#
#         elif input_str.lower() == ABOUT_COMMAND:
#             input_handle_about()
#
#         else:
#             if input_str.strip() != "":
#                 message = f'Unknown command: {input_str}'
#                 print(message)
#
#         # print empty line for better readability
#         print(" ")




##### METHODS FROM INPUT #####
#
# def input_handle_help():
#     print("Help")
#     # print(MESSAGE_HELP_SHORT)
#
#
# def input_handle_help_full():
#     print("HELP")
#     # print(MESSAGE_HELP_FULL)
#
#
# def input_handle_status(player: vlc.MediaPlayer, ep_instance: ConfigUserShow):
#     message = f'{color.INPUT_ANSWER}STATUS{color.END} - show status.'
#     print(message)
#     print_full_player_status(player, ep_instance, full_status=False)
#
#
# def print_full_player_status(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, full_status: bool):
#     milli = player.get_time()
#     sec = str(floor(milli / 1000))
#
#     milli_length = player.get_length()
#     sec_length = str(floor(milli_length / 1000))
#
#     if playing_opening_event.is_set():
#         status_what_is_playing = f'{ep_instance.showName} opening'
#     elif playing_ending_event.is_set():
#         status_what_is_playing = f'{ep_instance.showName} ending'
#     elif playing_commercials_event.is_set():
#         commercial_path = player.get_media().get_mrl()
#         commercial_name = os.path.basename(os.path.normpath(commercial_path)) # see https://stackoverflow.com/questions/3925096/how-to-get-only-the-last-part-of-a-path-in-python
#         status_what_is_playing = f'commercial {unquote(commercial_name)}'
#     else:
#         status_what_is_playing = f'{str(ep_instance)}'
#
#     status_playing = f'Playing {status_what_is_playing} at {get_h_m_s_from_milliseconds(milli)} / {get_h_m_s_from_milliseconds(milli_length)}'
#     status_seconds = f'(second {sec}/{sec_length}).'
#     status_state = f'State: {str(player.get_state())[6:]}.'
#     status_output = f'Audio output device:{str(player.audio_output_device_get())}.'
#     status_muted = f'Muted: {("Yes" if player.audio_get_mute() else "No")}.'
#     status_volume = f'Volume: {player.audio_get_volume()}%.'
#     status_speed = f'Speed: {int(player.get_rate() * 100)}%'
#     status_rest = f'Track: {player.audio_get_track()}. Channel: {str(player.audio_get_channel())}. Delay: {str(player.audio_get_delay())}. Role: {str(player.get_role())}.'
#
#     if full_status:
#         print("   ", status_playing, status_seconds, status_state, status_muted, status_volume, status_speed, status_output, status_rest)
#     else:
#         print(status_playing, status_seconds, status_state, status_muted, status_volume, status_speed, status_output)


def get_h_m_s_from_milliseconds(milli: int):
    """Get milliseconds converted to hh:mm:ss. If hh == 0, then mm:ss"""
    sec = floor(milli / 1000)
    s = sec % 60
    m = floor(sec / 60) % 60
    h = floor(sec / (60 * 60))
    if h > 0:
        return f'{"{0:0=2d}".format(h)}:{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'
    else:
        return f'{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'


# def input_handle_pause(player: vlc.MediaPlayer):
#     if player.is_playing() == 1:
#         # message = f'{color.INPUT_ANSWER}PAUSING{color.END} - pause playback. Enter \'{PAUSE_COMMAND}\' again to resume playback.'
#         # print(message)
#         player.set_pause(1)
#     else:
#         # message = f'{color.INPUT_ANSWER}RESUMING{color.END} - resume playback.'
#         # print(message)
#         player.set_pause(0)
#
#     milli = player.get_time()
#     milli_length = player.get_length()
#     message = f'Currently at {get_h_m_s_from_milliseconds(milli)} ({floor(milli / 1000)}s) / {get_h_m_s_from_milliseconds(milli_length)} ({floor(milli_length / 1000)}s).'
#     print(message)

#
# def input_handle_volume_display(player, ep: ConfigUserShow, setup: MainSetup):
#     message = f"""{color.BOLD}Player volume {player.audio_get_volume()}%{color.END}
#      Session: {setup.session_config.volume if setup.session_config.volume else 100}%
#       Global: {setup.get_current_profile_settings().volume if setup.get_current_profile_settings().volume else 100}%
#         Show: {ep.showSettings.volume if ep.showSettings.volume else 100}%"""
#     print(message)


def input_handle_volume_x(player: vlc.MediaPlayer, ep: ConfigUserShow, main_setup: MainSetup, volume_percent: int):
    # volume_to = input_handle_number_input(input_str, VOLUME_COMMAND, VOLUME_C, VOLUME_LIMIT_LOWER, VOLUME_LIMIT_UPPER)
    volume_to = volume_percent

    if volume_to != None:

        message = f'{color.INPUT_ANSWER}VOLUME {volume_to}{color.END} - Set volume to {volume_to}%.'
        print(message)
        main_setup.session_config.volume = volume_to
        player.audio_set_volume(option_handle_volume(main_setup.session_config.volume, ep.showSettings.volume, main_setup.get_current_profile_settings().volume))


def input_handle_jump_x(player: vlc.MediaPlayer, jump_to_sec: int):
    # jump_to = input_handle_number_timestamp_input(input_str, JUMP_COMMAND, JUMP_C)
    jump_to = jump_to_sec

    if jump_to != -1:

        current_time = player.get_time()
        message = f'{color.INPUT_ANSWER}JUMP {jump_to}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) to {get_h_m_s_from_milliseconds(jump_to * 1000)} ({jump_to}s).'
        print(message)

        # handle jumping over episode end
        goal_time = jump_to * 1000

        handle_jump_forward_helper(player, goal_time)

        # print_now_at_time(player.get_time())


def handle_jump_forward_helper(player: vlc.MediaPlayer, goal_milli: int):
    global debug_player_last_time

    ep_len = player.get_length() # audio length in milliseconds
    current_time = player.get_time()
    if ep_len < (goal_milli):
        if ep_len - (JUMP_PLAYBACK_BUMPER_SIZE_STANDARD* 1000) > current_time:
            message = f'File is only {get_h_m_s_from_milliseconds(ep_len)} long, but trying to go to {get_h_m_s_from_milliseconds(goal_milli)}. Jumping {JUMP_PLAYBACK_BUMPER_SIZE_STANDARD} seconds close to end instead.'
            print(message)
            goal_milli = ep_len - (JUMP_PLAYBACK_BUMPER_SIZE_STANDARD * 1000) # jump 20 seconds close to end
        else:
            message = f'File is only {get_h_m_s_from_milliseconds(ep_len)} long, but trying to go to {get_h_m_s_from_milliseconds(goal_milli)}. Nothing happened.'
            print(message)
            return
    debug_player_last_time = goal_milli
    player.set_time(goal_milli)


# def print_now_at_time(milli: int):
#     message = f'Now at {get_h_m_s_from_milliseconds(milli)} ({floor(milli / 1000)} seconds).'
#     print(message)


# def input_handle_number_timestamp_input(input_str: str, command: str, comm="") -> int:
#     """Get int from input, can be timestamp.
#
#     Tries to fetch an int or timestamp like mm:ss or h:m:s from an input_str and returns as seconds. Returns -1 if not successful. Pass the input_str, the long command and the short command, if existing.
#
#     For example for the command 'jump' you have to pass an integer or timestamp afterwards. Anything like 'jump 123', 'jump 4:20', 'jump 00:12:03' is possible, as well as typing 'j' instead of 'jump'. The result of these examples will be the intergers 123, 260 and 723 respectively.
#     Negative numbers anywhere are turned to 0.
#     If an error occurs parsing the command, like when typing 'jump 4m20s', then the method will return -1.
#     """
#     try:
#         if input_str.lower().startswith(command + " "):
#             number_str = input_str[(len(command) + 1):]
#         elif comm:
#             number_str = input_str[(len(comm) + 1):]
#         else:
#             return -1
#
#         if ":" in number_str:
#             split = number_str.split(":")
#             h = 0
#             if len(split) == 3:
#                 h = max(int(split[0]), 0)
#                 m = max(int(split[1]), 0)
#                 s = max(int(split[2]), 0)
#             else:
#                 m = max(int(split[0]), 0)
#                 s = max(int(split[1]), 0)
#             number = h * 3600 + m * 60 + s
#         else:
#             number = int(number_str)
#             number = max(number, 0)
#         return number
#     except ValueError:
#         message = f'Bad input! Enter a positive number after \'{command}\', like: \'{command} 83\', or a timestamp, like: \'{command} 12:34\''
#         print(message)
#         return -1 # arbitrary error integer, only positive integers are correct as output
#
#
# def input_handle_number_input(input_str: str, command: str, comm: str = "", lower_limit: int = None, upper_limit: int = None) -> int:
#     """Get int from input.
#
#     Tries to fetch an int from an input_str and returns as integer bounded by lower_limit and upper_limit. Returns None if not successful. Pass the input_str, the long command and the short command, if existing.
#
#     For example for the command 'volume' you have to pass an integer afterwards. Anything like 'volume 123' is possible, as well as typing 'v' instead of 'volume'.
#     Negative numbers are turned to lower_limit.
#     If an error occurs parsing the command, like when typing 'volume a420s', then the method will return None.
#     """
#     try:
#         if input_str.lower().startswith(command + " "):
#             number_str = input_str[(len(command) + 1):]
#         elif comm:
#             number_str = input_str[(len(comm) + 1):]
#         else:
#             return None
#
#         number = int(number_str)
#         if lower_limit != None:
#             number = max(number, lower_limit)
#         if upper_limit != None:
#             number = min(number, upper_limit)
#         return number
#     except ValueError:
#         message = f'Bad input! Enter a positive number after \'{command}\', like: \'{command} 83\''
#         print(message)
#         return None


def input_handle_back(player: vlc.MediaPlayer):
    # message = f'{color.INPUT_ANSWER}BACK{color.END} - go back {BACK_PLAYBACK_SIZE_STANDARD} seconds.'
    # print(message)

    input_handle_back_by_helper(player, BACK_PLAYBACK_SIZE_STANDARD)


def input_handle_back_x(player: vlc.MediaPlayer, back_sec: int):
    # back_by = input_handle_number_timestamp_input(input_str, BACK_COMMAND, BACK_C)
    back_by = back_sec

    if back_by != -1:
        current_time = player.get_time()
        # message = f'{color.INPUT_ANSWER}BACK {back_by}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) back by {back_by}s.'
        # print(message)

        input_handle_back_by_helper(player, back_by)


def input_handle_back_by_helper(player: vlc.MediaPlayer, seconds: int):
    global debug_player_last_time

    current_time = player.get_time()
    if (current_time - seconds * 1000) < 100:
        goal_milli = 0
    else:
        goal_milli = current_time - seconds * 1000

    debug_player_last_time = goal_milli
    player.set_time(goal_milli)
    # print_now_at_time(player.get_time())


def input_handle_forward(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}FORWARD{color.END} - go forward {FORWARD_PLAYBACK_SIZE_STANDARD} seconds.'
    print(message)
    player.set_time(player.get_time() + (FORWARD_PLAYBACK_SIZE_STANDARD * 1000))
    # print_now_at_time(player.get_time())


def input_handle_forward_x(player: vlc.MediaPlayer, forward_sec: int):
    forward_by = forward_sec

    if forward_by != -1:

        current_time = player.get_time()
        # message = f'{color.INPUT_ANSWER}FORWARD {forward_by}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) forward by {forward_by}s.'
        # print(message)

        # handle jumping over episode end
        goal_time = current_time + forward_by * 1000

        handle_jump_forward_helper(player, goal_time)

        # print_now_at_time(player.get_time())


def input_handle_restart(player: vlc.MediaPlayer):
    player_was_playing = (player.is_playing() == 1)
    if not player_was_playing:
        message_extra = f' Playback is still paused, enter \'{"PAUSE_COMMAND"}\' to resume playback.'
    else:
        message_extra = ""

    if playing_opening_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this opening.{message_extra}'
    elif playing_episode_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this episode.{message_extra}'
    elif playing_ending_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this ending.{message_extra}'
    elif playing_commercials_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this commercial.{message_extra}'
    else:
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart whatever.{message_extra}'

    print(message)

    player_restart_event.set()
    player.stop()
    player.play()
    event_player_Playing.wait()
    if not player_was_playing:
        player.set_pause(1) # pause again if player was paused before restart
    player_restart_event.clear()


def input_handle_player_next_no_save(player: vlc.MediaPlayer, setup: MainSetup):
    handle_left_off_at_seconds(player)

    if playing_opening_event.is_set():
        op_or_ed_or_cm_skipped_event.set() # this only exists to break a looping playback
        message = f'{color.INPUT_ANSWER}SKIPPING OPENING{color.END} - go to episode.\n'
    elif playing_episode_event.is_set():
        if (
            (
                setup.get_current_profile_settings().commercialsBreakWeight == None
                or setup.get_current_profile_settings().commercialsBreakWeight == 0
                or setup.get_current_profile_settings().commercialsPlay == False
            )
            and setup.session_config.commercialsBreakWeight == None
        ):
            message = f'{color.INPUT_ANSWER}SKIPPING EPISODE{color.END} - stop this episode without marking it as watched, play next show.'
        else:
            message = f'{color.INPUT_ANSWER}SKIPPING EPISODE{color.END} - stop this episode without marking it as watched, play commercials.'
        episode_skipped_event.set()
    elif playing_ending_event.is_set():
        op_or_ed_or_cm_skipped_event.set() # this only exists to break a looping playback
        if (
            (
                setup.get_current_profile_settings().commercialsBreakWeight == None
                or setup.get_current_profile_settings().commercialsBreakWeight == 0
                or setup.get_current_profile_settings().commercialsPlay == False
            )
            and setup.session_config.commercialsBreakWeight == None
        ):
            message = f'{color.INPUT_ANSWER}SKIPPING ENDING{color.END} - play next show.\n'
        else:
            message = f'{color.INPUT_ANSWER}SKIPPING ENDING{color.END} - play commercials.\n'
    elif playing_commercials_event.is_set():
        op_or_ed_or_cm_skipped_event.set() # this only exists to break a looping playback
        message = f'{color.INPUT_ANSWER}SKIPPING THIS COMMERCIAL{color.END} - play next thing. If there are more cms and you want to skip all of them at once, use \'{"SKIP_FULL_EPISODE_COMMAND"}\'.\n'
    else:
        message = f'{color.INPUT_ANSWER}SKIPPING WHATEVER{color.END} - play whatever next.\n'


    print(message, end="")
    player.stop()


def input_handle_player_next_with_save(player: vlc.MediaPlayer, ep: ConfigUserShow, setup: MainSetup):
    if playing_episode_event.is_set():
        message = f'{color.INPUT_ANSWER}FINISHING EPISODE{color.END} - mark this episode as watched, play next show.'
        episode_finished_event.set()
        print(message, end="")
        player.stop()
    else:
        input_handle_player_next_no_save(player, setup)


# def input_handle_show_config(ep_instance: ConfigUserShow):
#     message = f'{color.INPUT_ANSWER}SHOW CONFIG{color.END} - show path and contents of current config file.'
#     print(message)
#     with open(CONFIG_GLOBAL_FULL_PATH, encoding=ENCODING_CONFIG_FILE) as f:
#         content = f.readlines()
#     print(CONFIG_GLOBAL_FULL_PATH)
#     for line in content:
#         print(line, end="")


# def input_handle_config_help():
#     print(MESSAGE_CONFIG_HELP)


def input_handle_exit(player):
    # suspend_bool = option_handle_bools(show.showSettings.suspend, main_setup.get_current_profile_settings().suspend)
    # suspend_queue_bool = option_handle_bools(show.showSettings.suspendQueue, main_setup.get_current_profile_settings().suspendQueue)

    handle_left_off_at_seconds(player)

    # if suspend_queue_bool and suspend_bool:
    #     input_handle_suspend_queue_and_time(player)
    # elif suspend_queue_bool:
    #     input_handle_suspend_queue(player)
    # elif suspend_bool:
    #     input_handle_suspend(player)
    # else:
    #     helper_exit_program(player)
    helper_exit_program(player)


def handle_left_off_at_seconds(player: vlc.MediaPlayer):
    global left_off_at_seconds

    if playing_episode_event.is_set():
        left_off_at_seconds = int(player.get_time() / 1000)
    elif playing_opening_event.is_set():
        left_off_at_seconds = 0
    else:
        left_off_at_seconds = -1


def helper_exit_program(player):
    # global left_off_at_seconds
    #
    # # stop player
    # player_restart_event.set() # spaghetti way of avoiding player.stop() to trigger next playback
    # episode_stop_all_playback_event.set()
    # episode_skipped_event.set()
    # left_off_at_seconds = int(player.get_time() / 1000)

    # print and end
    message = f'{color.INPUT_ANSWER}EXITING{color.END} - end program.'
    print(message)
    handle_end_session(player) # this will make the main() end and with it the whole program



##### Extra Commands Handle Methods

def input_handle_open_playback_directory(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}OPEN PLAYBACK DIRECTORY{color.END} - a new directory explorer window with the current playback file should be opened.'
    print(message)
    playback_path = os.path.dirname(player.get_media().get_mrl())
    open_file(playback_path)


def input_handle_open_config():
    message = f'{color.INPUT_ANSWER}OPEN CONFIG{color.END} - a new window with the config file should be opened.'
    print(message)
    open_file(CONFIG_GLOBAL_FULL_PATH)


# see https://stackoverflow.com/questions/17317219/is-there-an-platform-independent-equivalent-of-os-startfile
def open_file(filename: str):
    """Opens a file or directory with system's standard program.
    """

    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        call([opener, filename])


# def input_handle_options_help():
#     print(MESSAGE_OPTIONS_HELP)


# gui not needed
# def input_handle_display_current_options(ep: ConfigUserShow, setup: MainSetup):
#     show_options = generate_config_show_global_line_options_list(ep)
#     if len(show_options) == 0:
#         show_options = ["None"]
#     show_message = f'{color.BOLD}Show Options:{color.END}'
#     show_options.insert(0, show_message)

#     global_options = generate_config_show_global_line_options_list(og)
#     if len(global_options) == 0:
#         global_options = ["None"]
#     global_message = f'{color.BOLD}Global Options:{color.END}'
#     global_options.insert(0, global_message)

#     print("\n\t".join(show_options))
#     print("\n\t".join(global_options))


# def input_handle_speed_display(player: vlc.MediaPlayer):
#     message = f'{color.BOLD}Player speed {int(player.get_rate() * 100)}%{color.END}'
#     print(message)


def input_handle_speed_x(player: vlc.MediaPlayer, main_setup: MainSetup, speed_percent: int):
    # speed_to = input_handle_number_input(input_str, SPEED_COMMAND, None, SPEED_LIMIT_LOWER, SPEED_LIMIT_UPPER)
    speed_to = speed_percent

    if speed_to != None:

        message = f'{color.INPUT_ANSWER}SPEED {speed_to}{color.END} - Set playback speed to {speed_to}%.'
        print(message)

        main_setup.session_config.speed = speed_to
        player.set_rate(speed_to / 100)


def input_handle_hard_volume_x(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, main_setup: MainSetup, volume_percent: int):
    # volume_to = input_handle_number_input(input_str, HARD_VOLUME_COMMAND, None, 0, 1000)
    volume_to = volume_percent

    if volume_to != None:

        message = f"{color.INPUT_ANSWER}VOLUME {volume_to}{color.END} - Set player's volume to {volume_to}%, ignoring settings."
        print(message)

        ep_volume_mult = (ep_instance.showSettings.volume / 100) if ep_instance.showSettings.volume else 1
        og_volume_mult = (main_setup.get_current_profile_settings().volume / 100) if main_setup.get_current_profile_settings().volume else 1

        if ep_volume_mult != 0 and og_volume_mult != 0:
            sesh_volume_value = volume_to * (1 / ep_volume_mult) * (1 / og_volume_mult)
            main_setup.session_config.volume = int(sesh_volume_value)

        player.audio_set_volume(volume_to)


def input_handle_mute(player: vlc.MediaPlayer, setup: MainSetup):
    mute_state = player.audio_get_mute()
    if mute_state == 0: # 0 if not muted
        message = f'{color.INPUT_ANSWER}MUTE{color.END}'
        mute_bool = True
    else:
        message = f'{color.INPUT_ANSWER}UNMUTE{color.END}'
        mute_bool = False
    print(message)
    setup.session_config.mute = mute_bool
    player.audio_set_mute(mute_bool)


def input_handle_fullscreen_toggle(player: vlc.MediaPlayer):
    media_has_fullscreen = player.has_vout() # 1 if has video track, 0 if none
    if media_has_fullscreen == 0:
        message = f'{color.INPUT_ANSWER}FULLSCREEN{color.END} - but this media has no video track.'
    else:
        media_is_fullscreen = player.get_fullscreen() # 1 if is fullscreen, else 0
        if media_is_fullscreen == 0:
            message = f'{color.INPUT_ANSWER}FULLSCREEN{color.END} - enable.'
            player.set_fullscreen(1)
        else:
            message = f'{color.INPUT_ANSWER}FULLSCREEN{color.END} - disable.'
            player.set_fullscreen(0)
    print(message)


# def input_handle_display_shows(main_setup: MainSetup):
#     all_shows = main_setup.get_all_active_shows_or_cms_for_current_profile(commercials_instead_of_shows=False)
#     all_show_names = list(map(show_get_name, all_shows))
#     if len(all_shows) > 0:
#         all_show_names.insert(0, "\nShows:")
#         print("\n\t".join(all_show_names))
#     else:
#         print("No shows found in config.")
#
#     all_commercials = main_setup.get_all_active_shows_or_cms_for_current_profile(commercials_instead_of_shows=True)
#     all_commercials_names = list(map(show_get_name, all_commercials))
#     if len(all_commercials_names) > 0:
#         all_commercials_names.insert(0, "\nCommercials:")
#         print("\n\t".join(all_commercials_names))
#     else:
#         print("No commercials found in config.")
#
#
# def show_get_name(show: ConfigUserShow) -> str:
#     return show.showName
#
#
def input_handle_queue_show_display(main_setup: MainSetup):
    message = main_setup.get_queue_table()
    print(message)


def input_handle_queue_show_x(main_setup: MainSetup, show_id: int, update_gui_event_set: bool = True):
    # show_id = input_handle_number_input(input_str, QUEUE_SHOW_COMMAND, QUEUE_SHOW_C, 0)

    if show_id != None and show_id >= 0:

        # show_name = main_setup.get_show_by_id(show_id).showName
        main_setup.queue_shows.queue.append(show_id)
        if update_gui_event_set:
            gui_event_queue_update_table.set()
        # message = f'{color.INPUT_ANSWER}QUEUE {show_id}{color.END} - added {show_name} to end of queue.'
        # print(message)


def input_handle_queue_show_load(m_setup: MainSetup):
    m_setup.populate_automatic(QUEUE_SIZE_STANDARD)
    message = f'{color.INPUT_ANSWER}QUEUE LOAD{color.END} - loaded {QUEUE_SIZE_STANDARD} new entries into queue.'
    print(message)


def input_handle_queue_show_load_x(m_setup: MainSetup, load_size: int):
    # load_size = input_handle_number_input(input_str, QUEUE_SHOW_LOAD_COMMAND, QUEUE_SHOW_LOAD_C, 0, QUEUE_LOAD_LIMIT_UPPER)

    if load_size != None:

        m_setup.populate_automatic(load_size)
        message = f'{color.INPUT_ANSWER}QUEUE LOAD {load_size}{color.END} - loaded {load_size} new entries into queue.'
        print(message)


def input_handle_queue_show_clear(m_setup: MainSetup):
    m_setup.clear_queue()
    if playing_commercials_event.is_set():
        message = f'{color.INPUT_ANSWER}QUEUE CLEARED{color.END} - deleted all entries in queue.'
    else:
        message = f'{color.INPUT_ANSWER}QUEUE CLEARED{color.END} - deleted all entries in queue except for current episode.'
    print(message)


def input_handle_queue_show_pop(main_setup: MainSetup):
    queue_size_before = len(main_setup.queue_shows.queue)
    main_setup.pop_queue()
    if len(main_setup.queue_shows.queue) == queue_size_before: # nothing was removed
        message = f'{color.INPUT_ANSWER}QUEUE POP{color.END} - nothing removed, queue already too small.'
    else:
        message = f'{color.INPUT_ANSWER}QUEUE POP{color.END} - deleted last entry in queue.'
    print(message)


def input_handle_play_show_x(player: vlc.MediaPlayer, show: ConfigUserShow, setup: MainSetup, show_id: int, update_gui_event_set: bool = True):
    # show_id = input_handle_number_input(input_str, PLAY_SHOW_COMMAND, None)

    if show_id != None and show_id >= 0:

        show_found_id = setup.add_entry_from_show_ids([show_id], add_to_front=True, probability_dropping=False)
        if update_gui_event_set:
            gui_event_queue_update_table.set()

        message = f'{color.INPUT_ANSWER}PLAY {show_found_id}{color.END} - playing now.'
        print(message)
        episode_full_restart_event.set() # with this event, nothing is printed and nothing happens when the episode finishes. Also it prevents deleting the first entry of the queue
        skip_straight_to_next_episode_event.clear() # avoid option 'episodes' causing bugs

        if show.showSettings.suspend != False and playing_episode_event.is_set():
            show_index = setup.user_config.index_of_show_by_id(show.showId)

            setup.user_config.shows[show_index].suspendedTimeSeconds = suspend_safe_value(int(player.get_time() / 1000), setup.user_config.shows[show_index].suspendedTimeSeconds)

            save_current_user_config_complete(setup)
            printdev("Saved timestamp.")

        player.stop()
    
    else:
        message = f'{color.INPUT_ANSWER}PLAY FAILED{color.END} - can\'t find show with that id.'
        print(message)



#def input_handle_play_episode_current(player: vlc.MediaPlayer):
#    pass
#def input_handle_play_episode_x(player: vlc.MediaPlayer, input_str: str):
#    pass
#def input_handle_previous_episode():
#    pass
#def input_handle_next_episode():
#    pass
#def input_handle_loop_episode():
#    pass
#def input_handle_loop_x_episode(input_str: str):
#    pass


def input_handle_end_of_episode(player: vlc.MediaPlayer):
    global debug_player_last_time

    current_time = player.get_time()
    message = f'{color.INPUT_ANSWER}END{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) to 5s close to end of episode.'
    print(message)

    goal_milli = player.get_length() - 5000
    debug_player_last_time = goal_milli
    player.set_time(goal_milli)
    # print_now_at_time(player.get_time())


def input_handle_end_x_of_episode(player: vlc.MediaPlayer, end_to_sec: int):
    global debug_player_last_time

    # end_to = input_handle_number_input(input_str, END_COMMAND, 0)
    end_to = end_to_sec

    if end_to != None:

        current_time = player.get_time()
        message = f'{color.INPUT_ANSWER}END {end_to}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) to {end_to}s close to end of episode.'
        print(message)

        goal_milli = player.get_length() - (end_to * 1000)
        debug_player_last_time = goal_milli
        player.set_time(goal_milli)
        sleep(0.3)
        # print_now_at_time(player.get_time())


def input_handle_suspend(player):
    handle_suspend(player)

def handle_suspend(player): #TODO CONTINUE: user json keeps getting incompletely saved
    suspend_event.set()
    handle_left_off_at_seconds(player)
    handle_end_session(player)


def input_handle_suspend_queue(player):
    handle_suspend_queue(player)

def handle_suspend_queue(player):
    suspend_queue_event.set()
    handle_left_off_at_seconds(player)
    handle_end_session(player)


def input_handle_suspend_queue_and_time(player):
    handle_suspend_queue_and_time(player)

def handle_suspend_queue_and_time(player):
    suspend_queue_event.set()
    suspend_event.set()
    handle_left_off_at_seconds(player)
    handle_end_session(player)


def input_handle_shutdown():
    if shutdown_running_event.is_set():
        message = f"{color.INPUT_ANSWER}SHUTDOWN{color.END} - {shutdown_minutes} minutes left before automatic end of {PROGRAM_NAME}."
    else:
        message = f"{color.INPUT_ANSWER}SHUTDOWN{color.END} - missing argument, type '{'SHUTDOWN_COMMAND'} 7' to activate shutdown timer with 7 minutes, turning off {PROGRAM_NAME} in 7 minutes."
    print(message)


def input_handle_shutdown_x(player, profile_settings: ConfigUserProfileSettings, shutdown_min: int):
    global shutdown_minutes

    # shutdown_min = input_handle_number_timestamp_input(input_str, SHUTDOWN_COMMAND)

    if shutdown_min != -1:

        if shutdown_min == 0:
            if shutdown_running_event.is_set():
                shutdown_running_event.clear()
                message = f'{color.INPUT_ANSWER}SHUTDOWN CLEAR{color.END} - turned off shutdown, was at {shutdown_minutes} minutes left.'
                shutdown_running_event.clear()
            else:
                message = f"{color.INPUT_ANSWER}SHUTDOWN CLEAR{color.END} - but the shutdown wasn't even turned on."

        # typed 'shutdown X', activate shutdown with X minutes, if X is int
        else:
            if shutdown_running_event.is_set():
                shutdown_minutes = shutdown_min
                message = f"{color.INPUT_ANSWER}SHUTDOWN {shutdown_minutes}{color.END} - updated shutdown timer to {shutdown_minutes} minutes. Type '{'SHUTDOWN_COMMAND'} 0' to cancel."
            else:
                handle_shutdown_x(player, shutdown_min, profile_settings.suspend, profile_settings.suspendQueue)
                message = ""

        if message:
            print(message)


def handle_shutdown_x(player, shutdown_mins, suspend_bool, suspend_queue_bool):
    global shutdown_minutes
    shutdown_minutes = shutdown_mins
    shutdown_thread = Thread(target=shutdown_start, args=(player, suspend_bool, suspend_queue_bool), daemon=True)
    shutdown_thread.start()
    shutdown_running_event.set()


def shutdown_start(player: vlc.MediaPlayer, global_suspend_bool: bool, global_suspend_queue_bool: bool):
    global shutdown_minutes
    shutdown_running_event.wait()

    if shutdown_minutes > 1:
        message = f"{color.WARNING}SHUTDOWN IN {shutdown_minutes} MINUTES{color.END} - {PROGRAM_NAME} automatically ends in {shutdown_minutes} minutes. Type '{'SHUTDOWN_COMMAND'} 0' to cancel. Type '{'SHUTDOWN_COMMAND'}' to display remaining time.\n"
        print(message)
        while shutdown_minutes > 1:

            for i in range(60): #sleep for a minute
                sleep(1)
                if not shutdown_running_event.is_set():
                    break
            shutdown_minutes -= 1

            if not shutdown_running_event.is_set():
                break

    if not shutdown_running_event.is_set():
        return

    message = f"{color.WARNING}SHUTDOWN IN 1 MINUTE{color.END} - {PROGRAM_NAME} automatically ends in 1 minute. Type '{'SHUTDOWN_COMMAND'} 0' to cancel.\n"
    print(message)

    for i in range(60): #sleep for a minute
        sleep(1)
        if not shutdown_running_event.is_set():
            break
    shutdown_minutes -= 1

    if not shutdown_running_event.is_set():
        return

    message = f"{color.WARNING}SHUTDOWN{color.END} - timer finished. Have a great day :-)\n"
    print(message)

    if global_suspend_queue_bool != False: #TODO CONTINUE: test, once ssuspend and suspend are fixed
        handle_suspend_queue(player)
    elif global_suspend_bool != False:
        handle_suspend(player)
    else:
        handle_end_session(player)


def input_handle_last_episode():
    global last_episodes_for_today_counter

    if last_episodes_for_today_counter == 0: # was not turned on
        message = f"LAST - canceled, but was not turned on. Type '{'LAST_EPISODE_COMMAND'} 3' to automatically end {PROGRAM_NAME} after 3 episodes."
    else:
        message = f'LAST - canceled, was at {last_episodes_for_today_counter}.'
        last_episodes_for_today_counter = 0
    print(message)


def input_handle_last_episode_x(last_int: int):
    global last_episodes_for_today_counter

    # last_int = input_handle_number_input(input_str, LAST_EPISODE_COMMAND, "", 0)

    if last_int != None:
        if last_int > 0:
            if last_int == 1:
                if playing_commercials_event.is_set():
                    message = f"{color.INPUT_ANSWER}LAST 1{color.END} - turn off {PROGRAM_NAME} after next episode."
                else:
                    message = f"{color.INPUT_ANSWER}LAST 1{color.END} - turn off {PROGRAM_NAME} after current episode."
            else:
                if playing_commercials_event.is_set():
                    message = f"{color.INPUT_ANSWER}LAST {last_int}{color.END} - turn off {PROGRAM_NAME} after {last_int} episodes."
                else:
                    message = f"{color.INPUT_ANSWER}LAST {last_int}{color.END} - turn off {PROGRAM_NAME} after {last_int} episodes, including currently running."
            last_episodes_for_today_counter = last_int
        else:
            message = f'LAST 0 - is ignored, use at least 1.'
        print(message)


def input_handle_skip_full_episode(player: vlc.MediaPlayer):
    global skip_straight_to_next_episode_counter

    message = f'{color.INPUT_ANSWER}SKIP FULL EPISODE{color.END} - skipping everything up to the next show\'s episode playback.'
    print(message)
    episode_stop_all_playback_event.set()
    episode_skipped_event.set()

    skip_straight_to_next_episode_event.clear() # cancel 'episodes' option
    skip_straight_to_next_episode_counter = 0 # cancel 'episodes' option

    player.stop()


def input_handle_finish_full_episode(player: vlc.MediaPlayer):
    global skip_straight_to_next_episode_counter

    if playing_opening_event.is_set() or playing_episode_event.is_set():
        message = f'{color.INPUT_ANSWER}FINISH FULL EPISODE{color.END} - skipping everything up to the next show\'s episode playback and updating config.'
    else:
        message = f'{color.INPUT_ANSWER}SKIP FULL EPISODE{color.END} - skipping everything up to the next show\'s episode playback.'
    print(message)
    episode_stop_all_playback_event.set()
    episode_finished_event.set()

    skip_straight_to_next_episode_event.clear() # cancel 'episodes' option
    skip_straight_to_next_episode_counter = 0 # cancel 'episodes' option

    player.stop()


def input_handle_skip_to_commercials(player: vlc.MediaPlayer):
    global skip_straight_to_next_episode_counter

    if playing_commercials_event.is_set():
        message = f'SKIP TO COMMERCIALS - already playing commercials, nothing happened.'
        print(message)
    else:
        message = f'SKIP TO COMMERCIALS - skipping current episode playback'
        print(message)
        episode_skipped_event.set() # this avoids changing current episode in the config
        episode_skip_to_commercial_event.set() # skips opening, episode and ending

        skip_straight_to_next_episode_event.clear() # cancel 'episodes' option
        skip_straight_to_next_episode_counter = 0 # cancel 'episodes' option

        player.stop()


def input_handle_commercials_global_override_x(setup: MainSetup, cms_weight: int):
    # cms_to = input_handle_number_input(input_str, COMMERCIALS_GLOBAL_OVERRIDE_COMMAND, COMMERCIALS_GLOBAL_OVERRIDE_C, 0)
    cms_to = cms_weight

    if cms_to != None:

        message = f"{color.INPUT_ANSWER}COMMERCIALS {cms_to}{color.END} - Set global commercials weight to {cms_to} for this session, overriding config's global option commercials value."
        print(message)

        setup.session_config.commercialsBreakWeight = cms_to


def input_handle_resume(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}RESUME PLAYBACK{color.END} - resume playback. No effect if playback is already running.'
    print(message)
    player.set_pause(0)


def input_handle_hard_pause(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}HARD PAUSE{color.END} - pause playback. No effect if playback is already paused.'
    print(message)
    player.set_pause(1)


def input_handle_video_track_x(player: vlc.MediaPlayer, ep: ConfigUserShow, track_int: int):
    # track_int = input_handle_number_input(input_str, VIDEO_TRACK_COMMAND)

    if track_int != None:
        message = f'{color.INPUT_ANSWER}VIDEO {track_int}{color.END} - change video track of current playback to {track_int}.'
        print(message)

        if (player.video_set_track(track_int) != 0): # returns 0 if success, else -1
            message = f'{ep.showName} episode {ep.currentEpisode} has no video track number {track_int}, command has no effect.'
            print(message)


# def input_handle_display_video_track(player: vlc.MediaPlayer):
#     track_int = player.video_get_track()
#     track_amount = player.video_get_track_count()
#     if track_amount > 2:
#         message = f'{color.INPUT_ANSWER}VIDEO{color.END} - player uses video track {track_int}, media has {track_amount - 1} video tracks.' # subtract one because the 'turned-off-track' -1 is also counted if at least one other track exists
#     elif track_amount == 2:
#         message = f'{color.INPUT_ANSWER}VIDEO{color.END} - player uses video track {track_int}, media has 1 video track.'
#     else:
#         message = f'{color.INPUT_ANSWER}VIDEO{color.END} - player uses video track {track_int}, media has {track_amount} video tracks.'
#
#     print(message)


def input_handle_audio_track_x(player: vlc.MediaPlayer, ep: ConfigUserShow, track_int: int):
    # track_int = input_handle_number_input(input_str, AUDIO_TRACK_COMMAND)

    if track_int != None:
        message = f'{color.INPUT_ANSWER}AUDIO {track_int}{color.END} - change audio track of current playback to {track_int}.'
        print(message)

        if (player.audio_set_track(track_int) != 0): # returns 0 if success, else -1
            message = f'{ep.showName} episode {ep.currentEpisode} has no audio track number {track_int}, command has no effect.'
            print(message)


# def input_handle_display_audio_track(player: vlc.MediaPlayer):
#     track_int = player.audio_get_track()
#     track_amount = player.audio_get_track_count()
#     if track_amount > 2:
#         message = f'{color.INPUT_ANSWER}AUDIO{color.END} - player uses audio track {track_int}, media has {track_amount - 1} audio tracks.' # subtract one because the 'turned-off-track' -1 is also counted if at least one other track exists
#     elif track_amount == 2:
#         message = f'{color.INPUT_ANSWER}AUDIO{color.END} - player uses audio track {track_int}, media has 1 audio track.'
#     else:
#         message = f'{color.INPUT_ANSWER}AUDIO{color.END} - player uses audio track {track_int}, media has {track_amount} audio tracks.'
#     print(message)


def input_handle_subtitle_track_x(player: vlc.MediaPlayer, ep: ConfigUserShow, track_int: int):
    # track_int = input_handle_number_input(input_str, SUBTITLE_TRACK_COMMAND)

    if track_int != None:
        message = f'{color.INPUT_ANSWER}SUBTITLE {track_int}{color.END} - change subtitle track of current playback to {track_int}.'
        print(message)

        if (player.video_set_spu(track_int) != 0): # returns 0 if success, else -1
            message = f'{ep.showName} episode {ep.currentEpisode} has no subtitle track number {track_int}, command has no effect.'
            print(message)


# def input_handle_display_subtitle_track(player: vlc.MediaPlayer):
#     track_int = player.video_get_spu()
#     track_amount = player.video_get_spu_count()
#     if track_amount > 2:
#         message = f'{color.INPUT_ANSWER}SUBTITLE{color.END} - player uses subtitle track {track_int}, media has {track_amount - 1} subtitle tracks.' # subtract one because the 'turned-off-track' -1 is also counted if at least one other track exists
#     elif track_amount == 2:
#         message = f'{color.INPUT_ANSWER}SUBTITLE{color.END} - player uses subtitle track {track_int}, media has 1 subtitle track.'
#     else:
#         message = f'{color.INPUT_ANSWER}SUBTITLE{color.END} - player uses subtitle track {track_int}, media has {track_amount} subtitle tracks.'
#     print(message)


# def input_handle_history(main_setup: MainSetup):
#     message_playbacks = deepcopy(main_setup.queue_shows.startedPlaybacks)
#     message_playbacks.insert(0, f"{color.BOLD}Files played so far:{color.END}")
#     print("\n\t".join(message_playbacks))
#
#     message_finished = [f"{x[2]} ep {x[1]}" for x in main_setup.queue_shows.alreadyPlayed]
#     message_finished.insert(0, f"{color.BOLD}Episodes finished so far:{color.END}")
#     print("\n\t".join(message_finished))


# def input_handle_monitor_x(player: vlc.MediaPlayer, main_setup: MainSetup, ep_instance: ConfigUserShow, monitor_interval: int):
#     if not monitoring_running_event.is_set():
#
#         # monitor_interval = input_handle_number_input(input_str, MONITOR_COMMAND, MONITOR_LIMIT_LOWER)
#
#         if monitor_interval != None:
#
#             message = f'{color.INPUT_ANSWER}TOGGLE MONITOR: ON{color.END} - prints modified status every {monitor_interval} seconds.'
#             print(message)
#             main_setup.session_config.monitor = monitor_interval
#             monitoring_player_on(player, ep_instance, monitor_interval)
#
#     else:
#         monitoring_player_off()
#         message = f'{color.INPUT_ANSWER}TOGGLE MONITOR: OFF{color.END}'
#         print(message)
#
#
# def monitoring_player_on(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, interval: int):
#     message = f'ACTIVATE MONITORING'
#     print(message)
#     monitoring_running_event.set()
#     monitorThread = Thread(target=monitoring_player, args=(player, ep_instance, interval), daemon=True)
#     monitorThread.start()
#
#
# def monitoring_player_off():
#     monitoring_running_event.clear()
#
#
# def monitoring_player(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, interval: int):
#     current_media_changed_counter = media_changed_counter
#     while monitoring_running_event.is_set():
#         if current_media_changed_counter == media_changed_counter:
#             print_full_player_status(player, ep_instance, True)
#             sleep(interval)
#         else:
#             break


# def input_handle_monitor_jumps(player: vlc.MediaPlayer, ep_instance: ConfigUserShow):
#     if not monitoring_player_jumps_running_event.is_set():
#         message = f'{color.INPUT_ANSWER}TOGGLE JUMP MONITOR: ON{color.END} - checks every 0.1 seconds if the player did an unusual jump in playback time. Prints results if anything notable happens.'
#         print(message)
#         monitoring_player_jumps_on(player, ep_instance)
#     else:
#         monitoring_player_jumps_off()
#         message = f'{color.INPUT_ANSWER}TOGGLE JUMP MONITOR: OFF{color.END}'
#         print(message)


# def monitoring_player_jumps_on(player: vlc.MediaPlayer, ep_instance: ConfigUserShow):
#     message = f'ACTIVATE JUMP MONITORING'
#     print(message)
#     monitoring_player_jumps_running_event.set()
#     monitorJumpsThread = Thread(target=monitoring_player_jumps, args=(player, ep_instance), daemon=True)
#     monitorJumpsThread.start()


# def monitoring_player_jumps_off():
#     monitoring_player_jumps_running_event.clear()


# def monitoring_player_jumps(player: vlc.MediaPlayer, ep_instance: ConfigUserShow):
#     # this monitor also reports "weird" playback time behaviour when there was an input, could have been one of the intentional playback time change commands like forward, back, end or jump
#     last_length = player.get_length()
#     last_time = player.get_time()
#     current_media_changed_counter = media_changed_counter
#     while monitoring_player_jumps_running_event.is_set():
#         if current_media_changed_counter == media_changed_counter:
#             sleep(0.1)
#             current_time = player.get_time()
#             current_length = player.get_length()
#             print_full = False
#             if current_time - last_time > 1000 or current_time < last_time:
#                 # something weird happened with current playback position
#                 message = f'Weird playback position: 0.1 seconds ago was {get_h_m_s_from_milliseconds(last_time)} / {last_time}ms, now is {get_h_m_s_from_milliseconds(current_time)} / {current_time}ms.'
#                 print(message)
#                 print_full = True
#             if current_length < last_length - 1000 or current_length > last_length + 1000:
#                 # something weird happened with the mp3 length
#                 message = f'Weird player length: 0.1 seconds ago was {get_h_m_s_from_milliseconds(last_length)} / {last_length}ms, now is {get_h_m_s_from_milliseconds(current_length)} / {current_length}ms.'
#                 print(message)
#                 print_full = True
#
#             # if print_full:
#                 # print_full_player_status(player, ep_instance, True)
#
#             last_length = current_length
#             last_time = current_time
#         else:
#             break


# def input_handle_monitors_off():
#     monitoring_running_event.clear()
#     monitoring_player_jumps_running_event.clear()
#     message = f'{color.INPUT_ANSWER}MONITORS OFF{color.END} - turned off all monitors'
#     print(message)


def input_handle_debug_player_jumps(player: vlc.MediaPlayer):
    if not debug_player_jumps_running_event.is_set():
        message = f'{color.BOLD}ACTIVATE DEBUG PLAYER JUMPS{color.END}'
        print(message)
        debug_player_jumps_on(player)
    else:
        message = f'{color.BOLD}DEACTIVATE DEBUG PLAYER JUMPS{color.END}'
        print(message)
        debug_player_jumps_off()


def debug_player_jumps_on(player: vlc.MediaPlayer):
    debug_player_jumps_running_event.set()
    debugPlayerJumpsThread = Thread(target=debug_player_jumps, args=(player,), daemon=True)
    debugPlayerJumpsThread.start()


def debug_player_jumps_off():
    message = f'{color.GREEN_BG}DEBUG PLAYER JUMPS OFF{color.END}'
    print(message)
    debug_player_jumps_running_event.clear()


def debug_player_jumps(player: vlc.MediaPlayer):
    global debug_player_last_time
    if player.get_state() == vlc.State.Playing:
        event_player_Stopped.clear()
        event_player_Playing.set()

    event_player_Playing.wait()
    message = f'{color.GREEN_BG}DEBUG PLAYER JUMPS READY{color.END}'
    print(message)

    while debug_player_jumps_running_event.is_set() and not end_session_event.is_set():
        event_player_Playing.wait()
        # setup
        event_player_LengthChanged_event.clear()
        debug_player_last_time = player.get_time()
        media_counter = media_changed_counter
        debugPlayerJumpsFulfillThread = Thread(target=debug_player_jumps_fulfill, args=(player,), daemon=True)
        debugPlayerJumpsFulfillThread.start()
        #message = f'{color.GREEN_BG}DEBUG PLAYER JUMPS READY{color.END}'
        #print(message)

        # update debug_player_last_time as long as the same file is still playing
        while debug_player_jumps_running_event.is_set() and not end_session_event.is_set():
            # this while loop breaks if the media was changed or debugging is turned off
            event_player_Playing.wait()
            if not end_session_event.is_set():
                if media_counter != media_changed_counter:
                    break
                debug_player_last_time = max(debug_player_last_time, player.get_time())
                sleep(DEBUG_PLAYER_JUMPS_INTERVALL)


def debug_player_jumps_fulfill(player):
    media_counter = media_changed_counter
    length_normal = player.get_length()
    while debug_player_jumps_running_event.is_set() and media_counter == media_changed_counter and not end_session_event.is_set():
        event_player_LengthChanged_event.wait()
        if not end_session_event.is_set():
            #ensure that media hasn't changed during wait. First condition is for media that has mildy fluctuating length all the time
            if (
                abs(length_normal - player.get_length()) > (DEBUG_PLAYER_JUMPS_ANOMALY_SENSITIVITY_SIZE_STANDARD * 1000)
                and player.get_state() == vlc.State.Playing and media_counter == media_changed_counter
            ):
                player_restart_event.set() # used to not change config, program handles player.stop() as episode finished and tries to set next episode in config
                player.stop()
                player.play()
                event_player_Playing.wait()
                player_restart_event.clear()
                player.set_time(debug_player_last_time + (DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD * 1000))
                print(f'{color.GREEN_BG}ENCOUNTERED PLAYER JUMP, RESTARTED AT {get_h_m_s_from_milliseconds(debug_player_last_time + 1000)}{color.END}')
            event_player_LengthChanged_event.clear()


def input_handle_media_lengths_analysis_quick(main_setup: MainSetup):
    if analysis_running_event.is_set():
        message= f"{color.INPUT_ANSWER}STOP RUNNING ANALYSIS{color.END}"
        print(message)
        analysis_running_event.clear()
    else:
        message = f"{color.INPUT_ANSWER}QUICK LENGTHS ANALYSIS{color.END} - this might take a while, depending on the amount of files and your computer's CPU performance. If this slows down your laptop, (force) quit {PROGRAM_NAME}. The results will be displayed in your console once they are ready."
        print(message)

        analysisThread = Thread(target=handle_media_lengths_analysis, args=(main_setup, MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD), daemon=True)
        analysisThread.start()
        analysis_running_event.set()


def input_handle_media_lengths_analysis_full(main_setup: MainSetup):
    if analysis_running_event.is_set():
        message= f"{color.INPUT_ANSWER}STOP RUNNING ANALYSIS{color.END}"
        print(message)
        analysis_running_event.clear()
    else:
        message = f"{color.INPUT_ANSWER}FULL LENGTHS ANALYSIS{color.END} - this might take a while, depending on the amount of files and your computer's CPU performance. If this slows down your laptop, (force) quit {PROGRAM_NAME}. The results will be displayed in your console once they are ready (~10 seconds per 50h of media)."
        print(message)

        analysisThread = Thread(target=handle_media_lengths_analysis, args=(main_setup,), daemon=True)
        analysisThread.start()
        analysis_running_event.set()


def handle_media_lengths_analysis(main_setup: MainSetup, quick_max_episodes: int=0):
    
    # get all shows and commercials of current profile
    all_shows = main_setup.get_all_active_shows_or_cms_for_current_profile(False)
    all_commercials = main_setup.get_all_active_shows_or_cms_for_current_profile(True)

    analysis_counter = 0
    lines_amount = len(all_shows) + len(all_commercials)

    total_shows_milli = 0
    total_commercials_milli = 0
    show_tuples = []
    commercial_tuples = []

    # loop through show lines, call media_lengths_analysis_show on the ShowEpisode instance to get the necessary info
    for show in all_shows:
        show_tuples.append(media_lengths_analysis_show(show, main_setup, quick_max_episodes))

        if not analysis_running_event.is_set():
            return

        analysis_counter += 1
        if analysis_counter % 5 == 0 or analysis_counter == lines_amount:
            printlog(f"Analysis progress: {analysis_counter}/{lines_amount}")

    # same for commercial lines
    for commercial in all_commercials:
        commercial_tuples.append(media_lengths_analysis_show(commercial, main_setup, quick_max_episodes))

        if not analysis_running_event.is_set():
            return

        analysis_counter += 1
        if analysis_counter % 5 == 0 or analysis_counter == lines_amount:
            printlog(f"Analysis progress: {analysis_counter}/{lines_amount}")

    # analysis finished, build messages to print afterwards:

    message_shows = ["Shows:", "NAME                 TOTAL     AVERAGE  EPISODES"]
    for s_t in show_tuples:
        alias = s_t[0]
        total_time = get_h_m_s_from_milliseconds(s_t[1])
        average_time = get_h_m_s_from_milliseconds(int(s_t[1] / s_t[2]))
        files_amount = s_t[2]
        message_shows.append(f"{alias: <20} {total_time: <9} {average_time: <8} {files_amount}")
        total_shows_milli += s_t[1]

    message_commercials = ["Commercials:"]
    for c_t in commercial_tuples:
        alias = c_t[0]
        total_time = get_h_m_s_from_milliseconds(c_t[1])
        average_time = get_h_m_s_from_milliseconds(int(c_t[1] / c_t[2]))
        files_amount = c_t[2]
        message_commercials.append(f"{alias: <20} {total_time: <9} {average_time: <8} {files_amount}")
        total_commercials_milli += c_t[1]

    # print messages
    print("\n  ".join(message_shows))
    print("\n  ".join(message_commercials))
    print(f"\nAll shows:       {get_h_m_s_from_milliseconds(total_shows_milli)}")
    print(f"All commercials: {get_h_m_s_from_milliseconds(total_commercials_milli)}")
    print(f"All files:       {get_h_m_s_from_milliseconds(total_shows_milli + total_commercials_milli)}")
    analysis_running_event.clear()


def media_lengths_analysis_show(show: ConfigUserShow, main_setup: MainSetup, quick_max_episodes: int=0) -> (str, int, int, int):
    """This method is useless if ffmpeg / ffprobe is not installed.
    """
    show_file_paths = show_generate_all_episodes_paths(show, main_setup.global_config)
    show_last_ep = show.lastEpisode if show.lastEpisode else len(show_file_paths)
    show_first_ep = show.firstEpisode if show.firstEpisode else 1
    episodes_amount = show_last_ep + 1 - show_first_ep
    length_milli_total = 0

    if quick_max_episodes == 0:
        analyze_episodes = range(show_first_ep, show_last_ep)
    else:
        # get quick_max_episodes many random ints from the possible episodes to analyze
        analyze_episodes = []
        while len(analyze_episodes) < quick_max_episodes:
            if len(analyze_episodes) == episodes_amount:
                break
            next_int = randint(show_first_ep, show_last_ep)
            if not next_int in analyze_episodes:
                analyze_episodes.append(next_int)

    for i in analyze_episodes:
        file_full_path = show_file_paths[i - 1]
        if file_full_path != "":
            length_milli = get_file_length_milli(file_full_path)
            if length_milli == -1:
                break
            elif length_milli == 0:
                episodes_amount -= 1
            else:
                length_milli_total += length_milli
        else:
            episodes_amount -= 1

    # extrapolate from lower analysis amount:
    if quick_max_episodes > 0 and len(analyze_episodes) < episodes_amount:
        length_milli_total = length_milli_total * (episodes_amount / len(analyze_episodes))

    return (show.showName, length_milli_total, episodes_amount)


# def input_handle_about():
#     print(MESSAGE_ABOUT)






##### PLAYER METHODS #####

### Main Player Methods ###

def play_start(main_setup: MainSetup, player: vlc.MediaPlayer):
    clear_all_threading_events_before_session_start()

    # creating a vlc player
    # player = vlc.Instance().media_player_new()

    # attach player events
    # player_attach_events(player)

    #process some global startup options
    # printdev("pre global_options_startup_handle")
    global_options_startup_handle(player, main_setup)
    # printdev("post global_options_startup_handle")

    session_running_event.set()

    current_profile = main_setup.get_current_profile()
    current_profile.suspendedQueue = []
    main_setup.remove_broken_show_ids()
    main_setup.populate_automatic()

    if (
        current_profile.profileSettings.commercialsStartWeight == None
        or current_profile.profileSettings.commercialsStartWeight == 0
        or current_profile.profileSettings.commercialsPlay == False
    ):
        pass
    else:
        set_playing_events("commercialsStart")
        play_commercials(player, main_setup, session_start_cms=True)

    while not end_session_event.is_set():

        # get next show
        next_show = get_next_show(main_setup)

        # here everything from the next show (episode) will be handled and played, including all show options
        play_episode_full(player, next_show, main_setup)

    session_running_event.clear()
    printdev("session ended")


def get_next_show(main_setup: MainSetup) -> ConfigUserShow:
    next_show_id = main_setup.get_next_show_id()
    if next_show_id == None:
        print("no queueable shows found, end session")
        end_session_event.set()
        return

    next_show = main_setup.get_show_by_id(next_show_id)

    while next_show is None:
        main_setup.queue_shows.queue.pop(0)
        main_setup.populate_automatic()

        next_show_id = main_setup.get_next_show_id()
        if next_show_id == None:
            print("no queueable shows found, end session")
            end_session_event.set()
            return None
        next_show = main_setup.get_show_by_id(next_show_id)

    return next_show



def play_episode_full(player: vlc.MediaPlayer, next_show: ConfigUserShow, main_setup: MainSetup):

    # clear some threading events in case they are still set
    clear_threading_events_before_play_start()

    # check if there are any episode files and if the current_episode file exists
    if not check_episode_files_exist(next_show, main_setup):
        return

    # randomize episode if random bool is True
    if next_show.showSettings.random:
        next_show.randomize_current_ep(main_setup.global_config)
    
    # announce episode
    message = f'\n{color.BOLD}{color.CYAN}Playing episode {next_show.currentEpisode} of {next_show.showName}.{color.END} Current Run: {next_show.firstEpisode if next_show.firstEpisode != None else "First"}-{next_show.lastEpisode if next_show.lastEpisode != None else "Last"}.'
    print(message)

    # playback flow: opening, episode, ending, commercials. with config handling after episode and ending
    play_opening_episode_ending(player, next_show, main_setup, "opening")
    play_opening_episode_ending(player, next_show, main_setup, "episode")

    handle_episode_finish(player, next_show, main_setup)

    play_opening_episode_ending(player, next_show, main_setup, "ending")

    handle_episode_finish_post_ending(player, main_setup)

    if next_show.showSettings.noCommercialsAfter or (
        len(main_setup.queue_shows.queue) > 1
        and main_setup.get_show_by_id(main_setup.queue_shows.queue[1]).showSettings.noCommercialsBefore
    ):
        pass
    else:
        play_commercials(player, main_setup)

    handle_episode_finish_post_commercials(player, next_show, main_setup)

    printdev("Test END play_episode_full")


def check_episode_files_exist(show: ConfigUserShow, main_setup: MainSetup):
    # check if there are any files for this show at all
    all_paths = show_generate_all_episodes_paths(show, main_setup.global_config)
    if all_paths is None or len(all_paths) == 0:
        handle_no_episodes_found(show_generate_directory_path(show, main_setup.global_config))
        main_setup.queue_shows.queue.pop(0)
        gui_event_queue_update_table.set()
        return False

    # check if episode file exists
    episode_file_full_path = show_generate_full_episode_path(show, main_setup.global_config)
    if not os.path.isfile(episode_file_full_path):
        handle_episode_not_found(episode_file_full_path)
        main_setup.queue_shows.queue.pop(0)
        gui_event_queue_update_table.set()
        return False

    return True


def handle_episode_not_found(episode_file_full_path: str):
    wait_seconds = 5
    message = f'{color.WARNING}Warning: no file found at {episode_file_full_path}{color.END}, skipping this episode, trying next show in {wait_seconds} seconds. Terminate program with Ctrl+C.\n'
    print(message)

    episode_error_event.set()
    sleep(wait_seconds)


def handle_no_episodes_found(dir_full_path: str):
    wait_seconds = 5
    message = f'{color.WARNING}Warning: no files found at {dir_full_path}{color.END}, skipping this show, trying next show in {wait_seconds} seconds. Terminate program with Ctrl+C.\n'
    print(message)

    episode_error_event.set()
    sleep(wait_seconds)


def play_opening_episode_ending(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, main_setup: MainSetup, specifier: str):
    if not specifier in ["episode", "opening", "ending"]:
        raise Exception("Error in program code, play_opening_episode_ending's specifier has to be 'episode', 'opening' or 'ending'.")

    if play_skip_bool_handle(specifier, main_setup.get_current_profile_settings(), show=ep_instance):
        return

    set_playing_events(specifier)
    if specifier == "episode":
        is_episode = True
        player_option_handle_episodes(ep_instance) # handle 'episodes' show option
    else:
        is_episode = False

    # get file path
    file_full_path = player_get_full_file_path(specifier, ep_instance, main_setup.global_config)
    if file_full_path == "":
        return

    message = f'{color.PATH_PLAYBACK}{specifier} path: {file_full_path}{color.END}'
    print(message)

    # tell player to play file from path
    media = vlc.Instance().media_new_path(file_full_path)
    main_setup.currently_running_show = ep_instance
    main_setup.currently_running_show_fullpath = file_full_path

    first_play = True # cheesy way of entering the while loop at least once
    while first_play or (main_setup.session_config.loop and not play_skip_bool_handle("loop")):
        player.set_media(media)

        # play the player and apply options. some can be applied before starting, some need the player to be running
        player_handle_options_pre_play(player, ep_instance, main_setup, is_episode)
        player.play()

        if (
            is_episode
            and first_play
            and ep_instance.suspendedTimeSeconds != None
            and ep_instance.suspendedTimeSeconds > SUSPENDED_OPENING_TIME
            and ep_instance.showSettings.suspend != False # TODO SUSPEND rework
        ):
            player_handle_suspend_resume(player, ep_instance)

        player_handle_options_post_play(player, ep_instance, main_setup, is_episode)

        # save playback path for 'history' command
        main_setup.queue_shows.startedPlaybacks.append((file_full_path, file_full_path[len(main_setup.global_config.basePaths[main_setup.global_config.currentBasePathIndex]):], player.get_length()))
        gui_event_history_update_table.set()

        gui_event_media_area_update_player_Playing.set()
        first_play = False

        # input handling and waiting while media is playing
        # player_wait_input(player, ep_instance, main_setup)
        player_playing_wait()

    main_setup.currently_running_show = None
    main_setup.currently_running_show_fullpath = None

    # clear events
    if suspend_event.is_set() or suspend_queue_event.is_set() or end_session_event.is_set():
        return # do this to keep the playing_xyz_event set
    else:
        playing_opening_event.clear()
        playing_episode_event.clear()
        playing_ending_event.clear()
        if is_episode and not episode_skipped_event.is_set():
            episode_finished_event.set()


def player_get_full_file_path(specifier: str, ep: ConfigUserShow, config_global: ConfigGlobal):
    # get file path
    if specifier == "episode":
        file_full_path = show_generate_full_episode_path(ep, config_global)
    elif specifier == "opening" or specifier == "ending":
        file_full_path = show_generate_full_op_ed_path(ep, config_global, specifier)
    else:
        raise ValueError("specifier in player_get_full_file_path is invalid") #TODO: add stack trace print or sth

    # check if returned path is empty
    if specifier != "episode":
        if file_full_path == "":
            message = f'{color.WARNING}Warning: no {specifier} file found for {ep.showName} ep {ep.currentEpisode}, skipping {specifier}.\n'
            print(message)
            return ""

        # check if file exists locally
        if not os.path.isfile(file_full_path):
            message = f'{color.WARNING}Warning: no file found at {file_full_path}{color.END}, skipping {specifier}.\n'
            print(message)
            return ""

    return file_full_path


def player_handle_suspend_resume(player: vlc.MediaPlayer, ep: ConfigUserShow):
    message = f"{color.WARNING}RESUME SUSPEND{color.END} - resuming playback of {ep.showName} ep {ep.currentEpisode} at {ep.suspendedTimeSeconds - SUSPEND_BACK_SIZE_STANDARD if ep.suspendedTimeSeconds >= 10 else 0} seconds."
    print(message)
    player.set_time((ep.suspendedTimeSeconds - SUSPEND_BACK_SIZE_STANDARD) * 1000)


def play_commercials(player: vlc.MediaPlayer, main_setup: MainSetup, session_start_cms: bool = False):
    """Disclaimer: this is the most spaghetti method in this whole program. This whole functionality was not intended at first and sloppily added at the end of the initial development while the only developer working on it had to do his taxes, change his bank account and insurances, get a new phone, change his official place of residence, write a bachelor's thesis and try not to die from the novel coronavirus during a worsening climate crisis. If you read this chuck me an e-mail if you want and tell me about your troubles.
    """

    if play_skip_bool_handle("commercial", main_setup.get_current_profile_settings(), session=main_setup.session_config):
        return

    set_playing_events("commercial")

    if session_start_cms:
        cm_global_weight = main_setup.get_current_profile_settings().commercialsStartWeight
    else:
        # sesh.commercial_weight_value_global overrides og.commercial_weight_value_global if existing
        if main_setup.session_config.commercialsBreakWeight != None:
            cm_global_weight = main_setup.session_config.commercialsBreakWeight
        else:
            cm_global_weight = main_setup.get_current_profile_settings().commercialsBreakWeight

    #cm_global_weight will be used later, do not change. cm_weight_global will be decremented along the way
    cm_weight_global = cm_global_weight

    # get cm lines and error handling
    all_commercials = main_setup.get_all_active_shows_or_cms_for_current_profile(True)
    if len(all_commercials) == 0:
        message = f"NO COMMERCIALS FOUND - global {'COMMERCIAL_WEIGHT_OPTION_GLOBAL'} weight is set but there are currently no show lines in your config that are marked as commercials. Can't play any commercials until this is fixed."
        print(message)
        return

    # generate ShowEpisode objects from cm_lines, store in list which is then sorted and chosen from. I feel like all of this method we are in right now is complete spaghetti and was not thought of when the whole ShowEpisode concept was designed
    cms_list_all = []
    for cm_instance in all_commercials:
        if cm_instance.showSettings.commercialWeight <= cm_weight_global:
            cm_tuple = (cm_instance.showSettings.commercialWeight, deepcopy(cm_instance))
            cms_list_all.append(cm_tuple)

    if len(cms_list_all) == 0:
        message = f"{color.ERROR}NO COMMERCIALS FOUND{color.END} - global {'COMMERCIAL_WEIGHT_OPTION_GLOBAL'} weight is set but there are currently no properly set up show lines in your config that are marked as commercials with commercial weight lower than global commercial weight. Can't play any commercials until this is fixed."
        print(message)
        return

    sorted_cms_all = sorted(cms_list_all, key=lambda tup: tup[0])

    # check if all commercials have weight 0, if so, then commercial global weight will never be exhausted, resulting in commercials forever. This doesn't happen if all cms are limited by commercial-max
    if sorted_cms_all[-1][0] == 0: # weight of last one in list is 0
        all_cms_limited = True

        for cm in sorted_cms_all: # print warning if a cm is included without commercial-max limit
            if cm[1].showSettings.commercialMax == None:
                message = f"{color.WARNING}ONLY COMMERCIALS WITH 0 WEIGHT{color.END} - this is ok, but you should know that this means that commercials will be played until you type '{'SKIP_FULL_EPISODE_COMMAND'}'."
                print(message)
                all_cms_limited = False
                break

        if all_cms_limited: # all cms have a commercial-max value
            weights_only_zero = False
            # now fill cms_chosen_list with commercials until limits commercial-max reached
            cms_chosen_list = []
            while len(sorted_cms_all) > 0:
                cms_chosen_list.append(sorted_cms_all[0])
                if sorted_cms_all[0][1].showSettings.commercialMax > 1:
                    sorted_cms_all[0][1].showSettings.commercialMax -= 1
                else:
                    sorted_cms_all.pop(0)

            shuffle(cms_chosen_list)
            helper_print_upcoming_commercials(cms_chosen_list, cm_global_weight, main_setup.global_config)

        else:
            weights_only_zero = True

            random_cm_int = randint(0, len(sorted_cms_all) - 1)
            cm_chosen = sorted_cms_all[random_cm_int]
            cms_chosen_list = [cm_chosen] # just add one, add one more later after every playback

    # not only commercials with weight 0:
    else:
        weights_only_zero = False

        # choose commercials for later playback
        cms_chosen_list = []
        drop_limit = 20
        sorted_cms_possible = deepcopy(sorted_cms_all)
        while cm_weight_global > 0 and drop_limit > 0:

            # only go through ones that have small enough weight, listed in sorted_cms_possible
            highest_possible_index = -1
            for i in range(0, len(sorted_cms_possible)):
                if sorted_cms_possible[i][0] <= cm_weight_global * 2:
                    highest_possible_index = i
                else:
                    break
            if highest_possible_index == -1:
                break
            while len(sorted_cms_possible) > highest_possible_index + 1:
                sorted_cms_possible.pop() #deletes last item from list

            # also drop all commercials that have their commercial-max value exhausted. this value is decremented every time the commercial is picked down below
            # REMINDER: cm lists here consist of cm_tuple = (cm_instance.commercial_weight_value_show, cm_instance)
            cms_to_remove = []
            for cm in sorted_cms_possible:
                if cm[1].showSettings.commercialMax != None:
                    if cm[1].showSettings.commercialMax < 1:
                        cms_to_remove.append(cm)
            for cm in cms_to_remove:
                sorted_cms_possible.remove(cm)
            if len(sorted_cms_possible) == 0:
                break

            # now choose commercials, but first prioritze commercials with a commercial_min_value_show, decrement this value whenever the commercial is picked, shuffle cms to not always pick first in sorted list first
            sorted_cms_possible_with_min_value = []
            cm_chosen = None
            for cm in sorted_cms_possible:
                if cm[1].showSettings.commercialMin != None:
                    if cm[1].showSettings.commercialMin > 0 and cm[1].showSettings.commercialWeight < cm_weight_global:
                        sorted_cms_possible_with_min_value.append(cm)

            if len(sorted_cms_possible_with_min_value) > 0:
                shuffle(sorted_cms_possible_with_min_value)
                cm_chosen = sorted_cms_possible_with_min_value[0]
                irremovable_bool = True

            if cm_chosen == None:
                # choose one randomly, if there was none chosen because of commercial_min_value_show
                random_cm_int = randint(0, len(sorted_cms_possible) - 1)

                cm_chosen = sorted_cms_possible[random_cm_int]

                # probability drop
                if main_setup.get_current_profile_settings().probability == None:
                    probability_global = 100
                else:
                    probability_global = main_setup.get_current_profile_settings().probability
                if cm_chosen[1].showSettings.probability != None or probability_global < 100:
                    if cm_chosen[1].showSettings.probability == None:
                        prob = probability_global # use global if no show probability set
                    else:
                        prob = cm_chosen[1].showSettings.probability
                    random_int = randint(1, 100)
                    if prob < random_int:
                        drop_limit -= 1
                        continue

                irremovable_bool = False

            # decrement cm_chosen's commercial_min_value_show and commercial_max_value_show in sorted_cms_possible
            cm_index = sorted_cms_possible.index(cm_chosen)
            if sorted_cms_possible[cm_index][1].showSettings.commercialMin != None:
                if sorted_cms_possible[cm_index][1].showSettings.commercialMin > 0:
                    sorted_cms_possible[cm_index][1].showSettings.commercialMin -= 1
            if sorted_cms_possible[cm_index][1].showSettings.commercialMax != None:
                if sorted_cms_possible[cm_index][1].showSettings.commercialMax > 0:
                    sorted_cms_possible[cm_index][1].showSettings.commercialMax -= 1

            cms_chosen_list.append((cm_chosen[0], cm_chosen[1], irremovable_bool)) # irremovable_bool == True means it can't be removed in the next phase
            cm_weight_global -= cm_chosen[0]

            drop_limit = 20


        # this happens if the last chosen commercial in the previous loop had a weight exceeding global weight limit. In this case remove the smallest one that fixes it at once if possible, but not the last one, else multiple
        if cm_weight_global < 0:

            sorted_cms_chosen = sorted(cms_chosen_list, key=lambda tup: tup[0])
            for i in range(1, len(sorted_cms_chosen)):
                if sorted_cms_chosen[i][0] > abs(cm_weight_global):
                    if sorted_cms_chosen[i-1][0] == 0:
                        cm_to_remove = sorted_cms_chosen[i]
                        if cm_to_remove[2] == True:
                            continue
                    else:
                        cm_to_remove = sorted_cms_chosen[i-1]
                        if cm_to_remove[2] == True:
                            continue
                    if cm_to_remove in cms_chosen_list:
                        cms_chosen_list.remove(cm_to_remove)
                        cm_weight_global += cm_to_remove[0]
            # none found to remove, so remove the last one after all
            if cm_weight_global < 0:
                cm_weight_global += cms_chosen_list[-1][0]
                cms_chosen_list.pop()

            if cm_weight_global > 0:
                #get all cms with weight above 0 and below cm_weight_global and add them if weight left
                cms_left_to_pick = []
                for cm in sorted_cms_all:
                    if cm[0] > 0 and cm[0] <= cm_weight_global:
                        cms_left_to_pick.append(cm)

                #now add them to the cms_chosen_list starting from reversed order
                while True and cm_weight_global > 0:
                    for i in reversed(range(0, len(cms_left_to_pick))):
                        if cms_left_to_pick[i][0] <= cm_weight_global:
                            cms_chosen_list.append(cms_left_to_pick[i])
                            cm_weight_global -= cms_left_to_pick[i][0]
                            break # start from top again to add higher_weight ones first
                    else: # if none left to add, exit while loop
                        break


        ## fix commercials that would play the same thing
        # first locate duplicates, for now remove from chosen commercials, add again later after they are changed
        cms_chosen_copy = deepcopy(cms_chosen_list)
        cms_chosen_duplicates = []
        duplicate_indexes = []
        for i in range(0, len(cms_chosen_list)):
            cm_tuple = cms_chosen_list[i]
            # first remove element from cms_chosen_copy
            for cm in cms_chosen_copy:
                if cm_tuple[1].showId == cm[1].showId:
                    cms_chosen_copy.remove(cm)
                    break
            # then if there is still an element same as cm_tuple in cms_chosen_copy, then add it to the duplicates list
            for cm in cms_chosen_copy:
                if cm_tuple[1].showId == cm[1].showId:
                    cms_chosen_duplicates.append(cm_tuple)
                    duplicate_indexes.append(i)
                    break

        # remove duplicates from cms_chosen_list for now, add later once the current_ep of cm_tuple has been changed
        for dupl_iter in reversed(duplicate_indexes):
            cms_chosen_list.pop(dupl_iter)

        # now apply spaghetti fix for duplicates, increasing their current episode until they are not longer duplicates (with loop limit to avoid infinite loop), then adding them back to cms_chosen_list
        prev_alias = ""
        prev_ep = 0
        for i in range(0, len(cms_chosen_duplicates)):
            cm_dupl = deepcopy(cms_chosen_duplicates[i])

            loop_limit = 0
            comp_list = helper_get_commercial_tuple_comparison_list(cms_chosen_list)

            while (cm_dupl[1].showId, cm_dupl[1].currentEpisode) in comp_list and loop_limit < 6:
                if cm_dupl[1].showSettings.random and loop_limit < 4:
                        cm_dupl[1].randomize_current_ep(main_setup.global_config)
                else:
                    if prev_alias == cm_dupl[1].showName:
                        # create new tuple consisting of ShowEpisode with current_ep from last one, then create one with next episode with the ep.method
                        cm_new_instance_temp = deepcopy(cm_dupl[1])
                        cm_new_instance_temp.currentEpisode = prev_ep
                        cm_dupl = (cm_dupl[0], cm_new_instance_temp.generate_instance_next_episode(main_setup.global_config), cm_dupl[2])
                        prev_ep = cm_dupl[1].currentEpisode
                    else:
                        cm_dupl = (cm_dupl[0], cm_dupl[1].generate_instance_next_episode(main_setup.global_config), cm_dupl[2])
                        prev_alias = cm_dupl[1].showName
                        prev_ep = cm_dupl[1].currentEpisode
                loop_limit += 1

            cms_chosen_list.append(cm_dupl)


        # shuffle the list, see https://stackoverflow.com/questions/976882/shuffling-a-list-of-objects
        shuffle(cms_chosen_list)

        # information on the upcoming commercials printing:
        helper_print_upcoming_commercials(cms_chosen_list, cm_global_weight, main_setup.global_config)


    dict_remember_highest_save_episode_of_show = { "dummyKey": 1 }
    dict_remember_inactive_norerun_show = { "dummyKey": True }

    ## play the commercials
    while len(cms_chosen_list) > 0:
        if episode_stop_all_playback_event.is_set() or episode_full_restart_event.is_set():
            break

        c_instance: ConfigUserShow = cms_chosen_list[0][1]
        cms_chosen_list.pop(0)

        # if c_instance.showSettings.random:
        #     c_instance.randomize_current_ep(main_setup.global_config) #TODO: delete this or is it needed?

        # get path and check if commercial file exists
        cm_file_full_path = show_generate_full_episode_path(c_instance, main_setup.global_config)
        if not os.path.isfile(cm_file_full_path):
            message = f'{color.WARNING}Warning: no file found at {cm_file_full_path}{color.END}, skipping this commercial.\n'
            print(message)
            continue

        message = f'{color.PATH_PLAYBACK}commercial path: {cm_file_full_path}{color.END}'
        print(message)

        media = vlc.Instance().media_new_path(cm_file_full_path)
        main_setup.currently_running_show = c_instance
        main_setup.currently_running_show_fullpath = cm_file_full_path

        first_play = True # cheesy way of entering the while loop at least once
        op_or_ed_or_cm_skipped_event.clear()
        while first_play or (main_setup.session_config.loop and not play_skip_bool_handle("loop")):
            player.set_media(media)

            player_handle_options_pre_play(player, c_instance, main_setup, True)
            player.play()
            player_handle_options_post_play(player, c_instance, main_setup, True)
    
            # save playback path for 'history' command
            main_setup.queue_shows.startedPlaybacks.append((cm_file_full_path, cm_file_full_path[len(main_setup.global_config.basePaths[main_setup.global_config.currentBasePathIndex]):], player.get_length()))
            gui_event_history_update_table.set()

            if first_play:
                # keep track of the highest episode number to save of a show, that ist not random, as multiple instances could be shuffled into the current selection, messing up the saving down below, like if episodes 29, 30, 31 are picked, they might play in the order 30, 31, 29 and then episode 30 will be saved in the end
                episode_number_save = c_instance.generate_next_episode(main_setup.global_config)
                dict_remember_highest_save_episode_of_show[str(c_instance.showId)] = max(episode_number_save, dict_remember_highest_save_episode_of_show.get(str(c_instance.showId), 0))

                # keep track of a commercial show reaching the last episode (only for shows that are not random)
                if episode_number_save == c_instance.firstEpisode_tmp and not c_instance.showSettings.random:
                    dict_remember_inactive_norerun_show[str(c_instance.showId)] = False

                # get index of show for saving, need to get the list element by index to update attributes
                cm_index = main_setup.user_config.index_of_show_by_id(c_instance.showId)
                # update currentEpisode
                main_setup.user_config.shows[cm_index].currentEpisode = episode_number_save if c_instance.showSettings.random else dict_remember_highest_save_episode_of_show[str(c_instance.showId)]
                # save isActive
                main_setup.user_config.shows[cm_index].isActive = dict_remember_inactive_norerun_show.get(str(c_instance.showId), True)
                save_current_user_config_complete(main_setup)
                printdev("Saved commercial episode just started.")

                if weights_only_zero:
                    random_cm_int = randint(0, len(sorted_cms_all) - 1)
                    cm_chosen = sorted_cms_all[random_cm_int]
                    cc_instance = cm_chosen[1].generate_instance_next_episode(main_setup.global_config)
                    if cc_instance.showSettings.random:
                        cc_instance.randomize_current_ep(main_setup.global_config)
                    cms_chosen_list = [(cc_instance.showSettings.commercialWeight, cc_instance)]

            gui_event_media_area_update_player_Playing.set()
            first_play = False

            # input handling and waiting while commercial is playing
            # player_wait_input(player, c_instance, main_setup)
            player_playing_wait()

        main_setup.currently_running_show = None
        main_setup.currently_running_show_fullpath = None

        if suspend_event.is_set() or suspend_queue_event.is_set() or end_session_event.is_set():
            break

    playing_commercials_event.clear()


def helper_get_commercial_tuple_comparison_list(cm_tuple_list: [(int, ConfigUserShow, bool)]) -> [(int, str)]:
    return_list = []
    for cm in cm_tuple_list:
        new_entry = (cm[1].showId, cm[1].currentEpisode)
        return_list.append(new_entry)
    return return_list


def helper_print_upcoming_commercials(cms_chosen_list, cm_global_weight, config_global: ConfigGlobal):
    message = []
    cm_total_length_milli = 0
    cm_total_weight = 0
    for entry in cms_chosen_list:
        cm_total_weight += entry[1].showSettings.commercialWeight
        try:
            file_length_milli = get_file_length_milli(show_generate_full_episode_path(entry[1], config_global))
            cm_total_length_milli += file_length_milli
            file_length = get_h_m_s_from_milliseconds(file_length_milli)

            commercial_name = show_get_episode_file_name(entry[1], config_global)
            message.append(f'{entry[0]} {file_length} {commercial_name}')

        except Exception:
            commercial_name = show_get_episode_file_name(entry[1], config_global)
            message.append(f'{entry[0]} {commercial_name}')

    if cm_total_length_milli != 0:
        start_message = f'{color.BOLD}Upcoming commercials {cm_total_weight}/{cm_global_weight} ({get_h_m_s_from_milliseconds(cm_total_length_milli)}):{color.END}'
    else:
        start_message = f'{color.BOLD}Upcoming commercials {cm_total_weight}/{cm_global_weight}:{color.END}'
    message.insert(0, start_message)

    print("\n\t".join(message))


def get_file_length_milli(filename):
    try:
        result = run(["ffprobe", "-v", "error", "-show_entries",
                                "format=duration", "-of",
                                "default=noprint_wrappers=1:nokey=1", filename],
            stdout=PIPE,
            stderr=STDOUT)
        return int(float(result.stdout) * 1000)
    except Exception:
        if os.path.isfile(filename):
            message = "Need to install ffmpeg to get the length of a file."
            print(message)
            return -1
        else:
            return 0




### Config Update / Episode Done Methods ###

def handle_episode_finish(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, main_setup: MainSetup):
    # if episode_full_restart_event.is_set() or suspend_queue_event.is_set():
    if episode_full_restart_event.is_set():
        return

    # if not (suspend_event.is_set() or suspend_queue_event.is_set()): # this condition fails if a suspend option was chosen manually, not just clicking default stop
    #     if option_handle_suspend_bools(ep_instance.showSettings.suspend, main_setup.get_current_profile_settings().suspend) and left_off_at_seconds != -1:
    #         suspend_event.set()
    #     if option_handle_suspend_bools(ep_instance.showSettings.suspendQueue, main_setup.get_current_profile_settings().suspendQueue):
    #         suspend_queue_event.set()

    show_index = main_setup.user_config.index_of_show_by_id(ep_instance.showId)

    # if suspend_event.is_set(): # moved into handle_episode_finish_post_commercials
    #     if playing_episode_event.is_set():
    #         message = f'{color.WARNING}SUSPEND{color.END} - saving where you left off.'
    #         print(message)
    #
    #         main_setup.user_config.shows[show_index].suspendedTimeSeconds = left_off_at_seconds
    #         save_current_user_config_complete(main_setup)
    #         printdev("Saved timestamp.")
    #     else:
    #         message = f'SUSPEND - no effect outside of an episode.'
    #         print(message)
    #
    #     handle_end_session(player)

    if episode_skipped_event.is_set() and not end_session_event.is_set(): #this should be checked before episode_finished_event as both might be set
        message = f'EPISODE SKIPPED'

        if skip_straight_to_next_episode_event.is_set():
            message = message + ' - play next episode.\n'
        else:
            if option_handle_suspend_bools(ep_instance.showSettings.suspend, main_setup.get_current_profile_settings().suspend):
                message = message + ' - save timestamp where you left off.\n'
                main_setup.user_config.shows[show_index].suspendedTimeSeconds = suspend_safe_value(left_off_at_seconds, main_setup.user_config.shows[show_index].suspendedTimeSeconds)

                save_current_user_config_complete(main_setup)
                printdev("Saved timestamp.")
            else:
                message = message + ' - play next show.\n'

        print(message)

    elif episode_error_event.is_set():
        message = f'PLAYER ERROR - an error caused the episode to stop. Trying to play next show in 5 seconds. Press {color.BOLD}Ctrl+C{color.END} or press ENTER a few times to terminate early.'
        print(message)
        sleep(5)
        episode_error_event.clear()

    elif episode_finished_event.is_set():
        message = f'EPISODE FINISHED - stop of player was signaled.'
        print(message)

        main_setup.queue_shows.alreadyPlayed.append((deepcopy(ep_instance.showId), deepcopy(ep_instance.currentEpisode), deepcopy(ep_instance.showName))) # used for global options 'uunique', 'unique' and 'history'

        last_episode = ep_instance.lastEpisode if ep_instance.lastEpisode != None else show_get_last_episode_calculated(ep_instance, main_setup.global_config)
        if ep_instance.currentEpisode == last_episode:

            # create boolean that decides if a show line is disabled in config file (by adding a # at the beginning) if the last episode just finished playing
            if option_handle_bools(ep_instance.showSettings.noRerun, main_setup.get_current_profile_settings().noRerun, False):
                if ep_instance.showSettings.random:
                    disable_show_bool = False
                else:
                    disable_show_bool = True
            else:
                disable_show_bool = False

            main_setup.user_config.shows[show_index].currentEpisode = ep_instance.firstEpisode if ep_instance.firstEpisode != None else 1

            update_message = f'Updated {ep_instance.showName} in config, was last episode, next episode will be {main_setup.get_show_by_id(ep_instance.showId).currentEpisode}.\n'

            if disable_show_bool:
                main_setup.user_config.shows[show_index].isActive = False

                update_message = f'Updated {ep_instance.showName} in config, was last episode, reruns are disabled, so show is deactivated.\n'

        else:
            main_setup.user_config.shows[show_index].currentEpisode += 1

            update_message = f'Updated {ep_instance.showName} in config, next episode will be {main_setup.get_show_by_id(ep_instance.showId).currentEpisode}.\n'

        main_setup.user_config.shows[show_index].suspendedTimeSeconds = None

        save_current_user_config_complete(main_setup)
        printdev("Saved finished episode, set next episode.")
        print(update_message)

    elif end_session_event.is_set():
        # if option_handle_suspend_bools(ep_instance.showSettings.suspend, main_setup.get_current_profile_settings().suspend):
        #     suspend_event.set()
        # if option_handle_suspend_bools(ep_instance.showSettings.suspendQueue, main_setup.get_current_profile_settings().suspendQueue):
        #     suspend_queue_event.set()
        message = "END SESSION"
        print(message)

    else:
        message = f'UNKNOWN - episode is finished, but what happened? Press {color.BOLD}Ctrl+C{color.END} or press ENTER a few times to terminate early. Playing next show in 3 seconds, I hope...'
        print(message)
        sleep(3)


def suspend_safe_value(left_off_at_secs: int, suspendedTimeSecondsBefore):
    if suspendedTimeSecondsBefore is not None and left_off_at_secs > suspendedTimeSecondsBefore - SUSPEND_BACK_SIZE_STANDARD:
        return max(
            left_off_at_secs,
            suspendedTimeSecondsBefore
        )
    else:
        return left_off_at_secs



def handle_episode_finish_post_ending(player: vlc.MediaPlayer, main_setup: MainSetup):
    global last_episodes_for_today_counter

    if skip_straight_to_next_episode_event.is_set():
        return # used for 'episodes' option


    # # remove show from queue that was just played
    # if episode_full_restart_event.is_set() or (suspend_queue_event.is_set() and left_off_at_seconds != -1) or (suspend_event.is_set() and left_off_at_seconds != -1):
    #     pass # made the condition easier to read
    # else:
    #     main_setup.queue_shows.queue.pop(0)


    # handle 'last' command and option
    if episode_finished_event.is_set(): # only count down if episode not skipped
        if last_episodes_for_today_counter > 0:
            last_episodes_for_today_counter -= 1
            if last_episodes_for_today_counter == 0:
                message = "Last episode for now. Have a great day :-)"
                print(message)
                handle_end_session(player)
            else:
                message = f"{last_episodes_for_today_counter} shows left to watch. Type '{'LAST_EPISODE_COMMAND'}' to cancel."
                print(message)

    profile_settings = main_setup.get_current_profile_settings()

    # handle 'unique' and 'uunique' option
    if profile_settings.uniqueShows or profile_settings.hardUniqueShows:

        # get all show_ids
        if profile_settings.commercialsPlay == False:
            all_ids_for_current_profile = main_setup.get_all_active_show_and_cm_ids_for_current_profile()
        else:
            all_ids_for_current_profile = main_setup.get_all_active_show_ids_for_current_profile()

        # check if all shows have already been played
        all_shows_played = False
        for show_id_ep_name_tuple in main_setup.queue_shows.alreadyPlayed:
            if show_id_ep_name_tuple[0] in all_ids_for_current_profile:
                all_ids_for_current_profile.remove(show_id_ep_name_tuple[0])
        if len(all_ids_for_current_profile) == 0:
            all_shows_played = True

        if all_shows_played:
            if profile_settings.hardUniqueShows:
                message = f"""ALL SHOWS PLAYED ONCE - option '{'HARD_UNIQUE_SHOWS_OPTION_GLOBAL'}' was set, ending {PROGRAM_NAME} now.
                Have a great day :-)"""
                print(message)
                handle_end_session()
            else: # unique_shows_bool_global True
                main_setup.queue_shows.alreadyPlayed.clear()


def handle_episode_finish_post_commercials(player: vlc.MediaPlayer, ep_instance: ConfigUserShow, main_setup: MainSetup):
    show_index = main_setup.user_config.index_of_show_by_id(ep_instance.showId)
    profile_index = main_setup.user_config.index_of_profile_by_id(main_setup.get_current_profile().profileId)

    printdev("Test START handle_episode_finish_post_commercials")

    if end_session_event.is_set() and not (suspend_event.is_set() or suspend_queue_event.is_set()): # this condition fails if a suspend option was chosen manually, not just clicking default stop
        printdev("TEST end and maybeeee suspend")
        if option_handle_suspend_bools(ep_instance.showSettings.suspend, main_setup.get_current_profile_settings().suspend) and left_off_at_seconds != -1:
            suspend_event.set()
        if option_handle_suspend_bools(ep_instance.showSettings.suspendQueue, main_setup.get_current_profile_settings().suspendQueue):
            suspend_queue_event.set()

    # remove show from queue that was just played
    if episode_full_restart_event.is_set() or (
        suspend_queue_event.is_set() and left_off_at_seconds != -1
    ) or (
        suspend_event.is_set() and left_off_at_seconds != -1
    ) or skip_straight_to_next_episode_counter > 0:
        pass # made the condition easier to read
    else:
        main_setup.queue_shows.queue.pop(0)
        gui_event_queue_update_table.set()


    if suspend_event.is_set() and playing_episode_event.is_set():
        message = f'{color.WARNING}SUSPEND TIME{color.END} - saving where you left off.'
        print(message)
        main_setup.user_config.shows[show_index].suspendedTimeSeconds = suspend_safe_value(left_off_at_seconds, main_setup.user_config.shows[show_index].suspendedTimeSeconds)
        main_setup.user_config.profiles[profile_index].suspendedQueue = [main_setup.queue_shows.queue[0]]

    if suspend_queue_event.is_set():
        message = f'{color.WARNING}SUSPEND QUEUE{color.END} - save upcoming shows.'
        print(message)
        main_setup.user_config.profiles[profile_index].suspendedQueue = main_setup.queue_shows.queue
    else:
        main_setup.user_config.profiles[profile_index].suspendedQueue = []

    save_current_user_config_complete(main_setup)
    # handle_end_session(player)


def handle_end_session(player: vlc.MediaPlayer):
    global skip_straight_to_next_episode_counter
    # global left_off_at_seconds

    skip_straight_to_next_episode_counter = 0
    # left_off_at_seconds = int(player.get_time() / 1000)

    # stop player
    player_restart_event.set() # spaghetti way of avoiding player.stop() to trigger next playback
    episode_stop_all_playback_event.set()
    episode_skipped_event.set()

    end_session_event.set()

    event_player_Playing.set() # (interrupt debug jumps)
    input_registered_event.set() # interrupt player_wait_input
    event_player_LengthChanged_event.set() # interrupt debug jumps

    event_player_Stopped.set()
    player.stop()





### Player Options handler methods ###
## all other options are handled somewhere else in the code

def global_options_startup_handle(player: vlc.MediaPlayer, main_setup: MainSetup):
    # global shutdown_minutes
    global last_episodes_for_today_counter

    debug_player_jumps = main_setup.get_current_profile_settings().debugPlayerJumps
    if debug_player_jumps == None or debug_player_jumps == True:
        debug_player_jumps_on(player)
    
    if main_setup.get_current_profile_settings().shutdown:
        handle_shutdown_x(player, main_setup.get_current_profile_settings().shutdown, main_setup.get_current_profile_settings().suspend, main_setup.get_current_profile_settings().suspendQueue)

    if main_setup.get_current_profile_settings().showsLimit:
        last_episodes_for_today_counter = main_setup.get_current_profile_settings().showsLimit


def player_option_handle_episodes(show_instance: ConfigUserShow):
    # handles 'episodes' show option
    global skip_straight_to_next_episode_counter

    if show_instance.showSettings.multipleEpisodes != None:
        if skip_straight_to_next_episode_counter > 0:
            show_instance.suspendedTimeSeconds = None
            skip_straight_to_next_episode_counter -= 1
            if skip_straight_to_next_episode_counter == 1: # done with this show, this will be the last time
                skip_straight_to_next_episode_counter = 0
                skip_straight_to_next_episode_event.clear()
        elif show_instance.showSettings.multipleEpisodes > 1:
            skip_straight_to_next_episode_counter = show_instance.showSettings.multipleEpisodes
            skip_straight_to_next_episode_event.set()
    else:
        skip_straight_to_next_episode_event.clear()


def player_handle_options_pre_play(player: vlc.MediaPlayer, show: ConfigUserShow, main_setup: MainSetup, is_episode: bool):
    player_option_handle_speed(player, show, main_setup)
    player_option_handle_mute(player, show, main_setup)
    player_option_handle_fullscreen(player, show, main_setup)
    # player_option_handle_queue_display(show, main_setup, is_episode)


def player_handle_options_post_play(player: vlc.MediaPlayer, show: ConfigUserShow, main_setup: MainSetup, is_episode: bool):
    event_player_Playing.wait()

    player_option_handle_volume(player, show, main_setup)
    player_option_handle_pause(player, show, main_setup.get_current_profile_settings())
    if is_episode:
        player_option_handle_jump(player, show, main_setup.get_current_profile_settings(), is_episode)
        player_option_handle_video_track(player, show, is_episode)
        player_option_handle_audio_track(player, show, is_episode)
        player_option_handle_subtitle_track(player, show, is_episode)
    # player_option_handle_monitor(player, show, main_setup)
    # player_option_handle_monitor_jump(player, show, main_setup.get_current_profile_settings())
    player_option_handle_debug_jumps(player, show, main_setup.get_current_profile_settings())
    # player_option_handle_status(player, show, main_setup.get_current_profile_settings())


def player_option_handle_speed(player, show: ConfigUserShow, main_setup: MainSetup):
    player.set_rate(option_handle_speed(show, main_setup))


def player_option_handle_mute(player, show: ConfigUserShow, main_setup: MainSetup):
    player.audio_set_mute(option_handle_mute(show, main_setup))


def player_option_handle_fullscreen(player, show: ConfigUserShow, main_setup: MainSetup):
    player.set_fullscreen(option_handle_fullscreen(show, main_setup))


# def player_option_handle_queue_display(show: ConfigUserShow, main_setup: MainSetup, is_episode: bool):
#     if is_episode and show.showSettings.commercialWeight == None:
#         if option_handle_bools(show.showSettings.queueDisplay, main_setup.get_current_profile().profileSettings.queueDisplay):
#             message = main_setup.get_queue_table()
#             print(message)


def player_option_handle_volume(player, show: ConfigUserShow, main_setup: MainSetup):
    set_volume = option_handle_volume(main_setup.session_config.volume, show.showSettings.volume, main_setup.get_current_profile().profileSettings.volume)
    sleep(0.05) # setting player volume immediately after player.play() will fail if the program doesn't wait here for at least 0.01-0.02 seconds. 0.02 will usually always be fine, but turned up to 0.05 just to be sure
    player.audio_set_volume(set_volume)


# def player_option_handle_status(player, show: ConfigUserShow, profile_settings: ConfigUserProfileSettings):
#     if option_handle_bools(show.showSettings.status, profile_settings.status):
#         sleep(0.1) #give status jump and suspend enough time to change playback position first
#         # print_full_player_status(player, show, True)


def player_option_handle_pause(player, show: ConfigUserShow, profile_settings: ConfigUserProfileSettings):
    if option_handle_bools(show.showSettings.pause, profile_settings.pause, False):
        player.set_pause(1)
    else:
        player.set_pause(0)


def player_option_handle_jump(player, show: ConfigUserShow, profile_settings: ConfigUserProfileSettings, is_episode: bool):
    if is_episode and show.suspendedTimeSeconds == None:
        if show.showSettings.jump:
            handle_jump_forward_helper(player, show.showSettings.jump * 1000)
        elif profile_settings.jump:
            handle_jump_forward_helper(player, profile_settings.jump * 1000)


def player_option_handle_video_track(player, show: ConfigUserShow, is_episode: bool):
    if show.showSettings.videoTrack != None and is_episode:
        if player.video_set_track(show.showSettings.videoTrack) != 0: # returns 0 if success, else -1
            message = f'{show.showName} episode {show.currentEpisode} has no video track number {show.showSettings.videoTrack}, option has no effect.'


def player_option_handle_audio_track(player, show: ConfigUserShow, is_episode: bool):
    if show.showSettings.audioTrack != None and is_episode:
        if player.audio_set_track(show.showSettings.audioTrack) != 0: # returns 0 if success, else -1
            message = f'{show.showName} episode {show.currentEpisode} has no audio track number {show.showSettings.audioTrack}, option has no effect.'


def player_option_handle_subtitle_track(player, show: ConfigUserShow, is_episode: bool):
    if show.showSettings.subtitleTrack != None and is_episode:
        if player.video_set_spu(show.showSettings.subtitleTrack) != 0: # returns 0 if success, else -1
            message = f'{show.showName} episode {show.currentEpisode} has no subtitle track number {show.showSettings.subtitleTrack}, option has no effect.'


# def player_option_handle_monitor(player, show: ConfigUserShow, main_setup: MainSetup):
#     if monitoring_running_event.is_set():
#         monitoring_player_off()
#         sleep(0.5)
#
#     if main_setup.session_config.monitor:
#         sleep(0.5) #give status jump and suspend enough time to change playback position first
#         monitoring_player_on(player, show, main_setup.session_config.monitor)
#     elif show.showSettings.monitor:
#         sleep(0.5)
#         monitoring_player_on(player, show, show.showSettings.monitor)
#     elif main_setup.get_current_profile_settings().monitor:
#         sleep(0.5)
#         monitoring_player_on(player, show, main_setup.get_current_profile_settings().monitor)


# def player_option_handle_monitor_jump(player, show: ConfigUserShow, profile_settings: ConfigUserProfileSettings):
#     if monitoring_player_jumps_running_event.is_set():
#         monitoring_player_jumps_off()
#         sleep(0.5)
#
#     if option_handle_bools(show.showSettings.monitorJump, profile_settings.monitorJump):
#         monitoring_player_jumps_on(player, show)
#     else:
#         monitoring_player_jumps_off()


def player_option_handle_debug_jumps(player, show: ConfigUserShow, profile_settings: ConfigUserProfileSettings):
    if option_handle_bools(show.showSettings.debugPlayerJumps, profile_settings.debugPlayerJumps, True):
        if debug_player_jumps_running_event.is_set():
            return
        else:
            message = f'{color.BOLD}ACTIVATE DEBUG PLAYER JUMPS{color.END}'
            print(message)
            debug_player_jumps_on(player)
    else:
        if not debug_player_jumps_running_event.is_set():
            return
        else:
            message = f'{color.BOLD}DEACTIVATE DEBUG PLAYER JUMPS{color.END}'
            print(message)
            debug_player_jumps_off()


def option_handle_bools(show_bool: bool, global_bool: bool, default: bool):
    if show_bool != None:
        return show_bool
    elif global_bool != None:
        return global_bool
    elif default != None:
        return default
    else:
        return False


def option_handle_suspend_bools(show_suspend: bool, profile_suspend: bool):
    return (
        show_suspend == True
        or (
            show_suspend == None and profile_suspend == True
        )
        or (
            show_suspend == None and profile_suspend == None
        )
    )


def option_handle_volume(sesh_volume: int, ep_volume: int, global_volume: int) -> int:
    if (sesh_volume != None) or (ep_volume != None) or (global_volume != None):
        sesh_volume_mult = (sesh_volume / 100) if sesh_volume else 1
        ep_volume_mult = (ep_volume / 100) if ep_volume else 1
        og_volume_mult = (global_volume / 100) if global_volume else 1
        result_volume = int(100 * sesh_volume_mult * ep_volume_mult * og_volume_mult)
        return result_volume
    else:
        return 100 # 100 is 100%


def option_handle_speed(show: ConfigUserShow, main_setup: MainSetup) -> float:
    if main_setup.session_config.speed:
        return (main_setup.session_config.speed / 100)
    elif show.showSettings.speed:
        return (show.showSettings.speed / 100)
    elif main_setup.get_current_profile_settings().speed:
        return (main_setup.get_current_profile_settings().speed / 100)
    else:
        return 1 # 1 is 100%


def option_handle_mute(show: ConfigUserShow, main_setup: MainSetup):
    if main_setup.session_config.mute or option_handle_bools(show.showSettings.mute, (main_setup.get_current_profile_settings().mute and len(main_setup.queue_shows.startedPlaybacks) == 1), False): # global mute option only affects first playback of the session
        return 1 # 1 activates mute, 0 turns off
    else:
        return 0


def option_handle_fullscreen(show: ConfigUserShow, main_setup: MainSetup):
    fullscreen_boolean = option_handle_bools(show.showSettings.fullscreen, main_setup.get_current_profile_settings().fullscreen, False)
    if fullscreen_boolean:
        return 1
    else:
        return 0




### Player Event Handling and Setup ###

def player_attach_events(player: vlc.MediaPlayer):
    events = player.event_manager()
    events.event_attach(vlc.EventType.MediaPlayerEncounteredError, player_event_EncounteredError)
    events.event_attach(vlc.EventType.MediaPlayerStopped, player_event_Stopped)
    # player events introduced and mainly used for debug player jumps mode:
    events.event_attach(vlc.EventType.MediaPlayerLengthChanged, player_event_LengthChanged)
    events.event_attach(vlc.EventType.MediaPlayerMediaChanged, player_event_MediaChanged)
    events.event_attach(vlc.EventType.MediaPlayerPaused, player_event_Paused)
    events.event_attach(vlc.EventType.MediaPlayerPlaying, player_event_Playing)

def clear_all_threading_events_before_session_start():
    for event in all_events:
        event.clear()

def clear_threading_events_before_play_start():
    playing_opening_event.clear()
    playing_episode_event.clear()
    playing_ending_event.clear()
    playing_commercials_event.clear()

    episode_finished_event.clear()
    episode_skipped_event.clear()
    op_or_ed_or_cm_skipped_event.clear()
    episode_stop_all_playback_event.clear()
    episode_skip_to_commercial_event.clear()
    episode_error_event.clear()
    player_restart_event.clear()
    episode_full_restart_event.clear()

    end_session_event.clear()


def play_skip_bool_handle(specifier: str, profile_settings: ConfigUserProfileSettings = None, session: MainSetupSession = None, show: ConfigUserShow = None):
    if specifier == "opening":
        return (
            end_session_event.is_set()
            or (not option_handle_bools(show.showSettings.opening, profile_settings.opening, False))
            or episode_stop_all_playback_event.is_set()
            or episode_error_event.is_set()
            or episode_full_restart_event.is_set()
            or skip_straight_to_next_episode_event.is_set()
            or suspend_event.is_set()
            or suspend_queue_event.is_set()
            or (
                    show.suspendedTimeSeconds != None
                    and show.suspendedTimeSeconds > SUSPENDED_OPENING_TIME
            )
        )
    elif specifier == "episode":
        return (
            end_session_event.is_set()
            or episode_stop_all_playback_event.is_set()
            or episode_error_event.is_set()
            or episode_full_restart_event.is_set()
            or episode_skip_to_commercial_event.is_set()
            or suspend_event.is_set()
            or suspend_queue_event.is_set()
        )
    elif specifier == "ending":
        return (
            end_session_event.is_set()
            or (not option_handle_bools(show.showSettings.ending, profile_settings.ending, False))
            or episode_stop_all_playback_event.is_set()
            or episode_skipped_event.is_set()
            or episode_error_event.is_set()
            or episode_full_restart_event.is_set()
            or episode_skip_to_commercial_event.is_set()
            or skip_straight_to_next_episode_event.is_set()
            or suspend_event.is_set()
            or suspend_queue_event.is_set()
        )
    elif specifier == "commercial":
        return (
            end_session_event.is_set()
            or (
                (
                    profile_settings.commercialsBreakWeight == None
                    or profile_settings.commercialsBreakWeight == 0
                    or profile_settings.commercialsPlay == False
                )
                and session.commercialsBreakWeight == None
            )
            or episode_stop_all_playback_event.is_set()
            or episode_error_event.is_set()
            or episode_full_restart_event.is_set()
            or skip_straight_to_next_episode_event.is_set()
            or suspend_event.is_set()
            or suspend_queue_event.is_set()
        )
    elif specifier == "loop":
        return (
            end_session_event.is_set()
            or episode_stop_all_playback_event.is_set()
            or episode_skipped_event.is_set()
            or op_or_ed_or_cm_skipped_event.is_set()
            or episode_error_event.is_set()
            or episode_full_restart_event.is_set()
            or episode_skip_to_commercial_event.is_set()
            or suspend_event.is_set()
            or suspend_queue_event.is_set()
        )


def set_playing_events(specifier: str):
    playing_opening_event.clear()
    playing_episode_event.clear()
    playing_ending_event.clear()
    playing_commercials_event.clear()

    if specifier == "opening":
        playing_opening_event.set()
    elif specifier == "episode":
        playing_episode_event.set()
    elif specifier == "ending":
        playing_ending_event.set()
    elif specifier == "commercial":
        playing_commercials_event.set()
    elif specifier == "commercialsStart": #TODO? needs separate event?
        playing_commercials_event.set()


def player_event_Stopped(event):
    event_player_Playing.clear() # used for debug player jumps
    if not player_restart_event.is_set(): # this event is set when player.stop(), player.play() is called to restart the episode
        event_player_Stopped.set()
        input_registered_event.set() # this is necessary to stop the loop with handle_input() from waiting
    # gui_event_media_area_update_player_Playing.set()


def player_event_EncounteredError(event):
    if playing_opening_event.is_set():
        pass
    elif playing_episode_event.is_set():
        episode_error_event.set()
    elif playing_ending_event.is_set():
        pass
    elif playing_commercials_event.is_set():
        pass
    print(f'    {color.GREEN} ER{color.END}{color.BOLD}{color.BLUE}RO{color.END}{color.BOLD}{color.RED}R P{color.END}{color.BOLD}{color.PURPLE}LA{color.END}{color.BOLD}{color.CYAN}YE{color.END}{color.BOLD}{color.YELLOW}R{color.END}') # this should never happen, i.e. if this happens, there is a bug or very corrupt media file


def player_event_LengthChanged(event):
    if event_player_Playing.is_set(): # only set event if state is playing, length changes before playing (loading file?)
        event_player_LengthChanged_event.set()


def player_event_MediaChanged(event):
    global media_changed_counter
    event_player_Playing.clear()
    media_changed_counter += 1
    gui_event_media_changed_update_equalizer_visuals.set()
    # gui_event_media_area_update_player_Playing.set()


def player_event_Paused(event):
    event_player_Playing.clear()


def player_event_Playing(event):
    event_player_Stopped.clear()
    event_player_Playing.set()
    gui_event_media_area_update_player_Playing.set()




##### Config Reading and Filtering #####

def get_random_id_from_array(id_array: [int]) -> int:

    random_index = randint(0, len(id_array) - 1)
    return id_array[random_index]




# # TODO reuse this in the gui??
# def search_alg_match_show_names(search_string: str, all_active_shows: List[ConfigUserShow]) -> List[ConfigUserShow]:
#     possible_shows = [] # search_string doesn't have to be the full showName. If search_string doesn't match with any show's name, then it will try to be matched to the words in the show's name
#     found_something_bool = False
#
#     search_phase = 1
#     # phases become looser with growing integer value, only increasing if nothing matched yet
#     ### This was the first concept for the search phases, might be changed since then by now, but this should give a basic idea of the search algorithms intentions:
#     # phase 1: try exact match, this is done for example by the automatic queue building, where the program had saved ShowEpisode instances created out of config show lines earlier. Can result in multiple matches if config contains duplicate show_aliases.
#     # phase 2: same as 1, but ignore case (small/capital letters).
#     # phase 3: split show lines' alias into words consisting only of latin letters (a-z, A-Z) and see if one of the words starts with search_string (if itself only consists of one such word). Ignores case.
#     # phase 4: same as phase 3, but now not with startswith, but only checks if contained in.
#     # phase 5: only if search_string string is more than one latin alphabet word. Same as phase 3, but iterating over all words of search_string string.
#     # phase 6: same as phase 5, but replace the "3" with a "4" in its description
#
#     if search_string.strip() == "":
#         return []
#
#     # these are the numbers of the search_phases where the strings are split and no non-alphabet chars are replaced. This is used to reduce redundancy and improve extendability
#     phases1 = [3, 4, 5, 6]
#     sizee = len(phases1)
#     phases2 = [x + sizee for x in phases1] # list comprehension (<- for reference)
#     phases_pairs = list(map(lambda a, b: [a, b], phases1, phases2))
#
#     # with every while-loop the possible_shows will only keep the ones with the best matches from the loop (beginning from search_phase 3), once a match was found
#     check_shows = deepcopy(all_active_shows)
#
#     while True:
#         matched_index_tuple_list = [[x] for x in check_shows]
#         for show_instance in check_shows:
#
#             show_name = show_instance.showName
#
#             # exact match 1: 1
#             if search_phase == 1 and search_string == show_name:
#                 possible_shows.append(show_instance)
#
#             # exact match 1: 1 ignore case
#             elif search_phase == 2 and search_string.lower() == show_name.lower():
#                 possible_shows.append(show_instance)
#
#             elif search_phase in phases1 + phases2:
#                 if search_phase in phases1:
#                     show_words = show_name.split()
#                     input_words = search_string.split()
#                 else:
#                     # replace all characters, that are not standard latin letters a-z or A-Z, with spaces, using regex
#                     show_words = sub("[^(a-zA-z]", " ",  show_name).split()
#                     input_words = sub("[^(a-zA-z]", " ",  search_string).split()
#
#
#                 for input_word in input_words:
#                     for show_word in show_words:
#
#                         # exact match n : n
#                         if search_phase in phases_pairs[0]:
#                             if show_word == input_word:
#
#                                 matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_shows, show_instance, search_phase)
#
#                         # exact match n : n ignore case
#                         elif search_phase in phases_pairs[1]:
#                             if show_word.lower() == input_word.lower():
#
#                                 matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_shows, show_instance, search_phase)
#
#                         # starts with n : n ignore case
#                         elif search_phase in phases_pairs[2]:
#                             if show_word.lower().startswith(input_word.lower()):
#
#                                 matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_shows, show_instance, search_phase)
#
#                         # is in n : n ignore case
#                         elif search_phase in phases_pairs[3]:
#                             if input_word.lower() in show_word.lower():
#
#                                 matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_shows, show_instance, search_phase)
#
#         # stop if there were exact matches during first two search phases
#         if search_phase < phases1[0] and len(possible_shows) > 0:
#             break
#
#         # remove elements without tuples
#         matched_index_tuple_list = [x for x in matched_index_tuple_list if len(x) > 1]
#
#         if len(matched_index_tuple_list) > 0:
#             found_something_bool = True
#             # evaluate weights, lower is better, then more tuples is better. Throw out worse matches here. This is kinda spaghetti
#             while True and len(matched_index_tuple_list) > 1:
#                 a = matched_index_tuple_list[0]
#                 len_a = len(a)
#
#                 for b in matched_index_tuple_list[1:]:
#                     temp_len = min(len_a, len(b))
#                     temp_ind = 1
#                     keep_str = ""
#
#                     while temp_ind < temp_len:
#                         if a[temp_ind][0] < b[temp_ind][0]:
#                             keep_str = "a"
#                             break
#                         if a[temp_ind][0] > b[temp_ind][0]:
#                             keep_str = "b"
#                             break
#                         if a[temp_ind][1] < b[temp_ind][1]:
#                             keep_str = "a"
#                             break
#                         if a[temp_ind][1] > b[temp_ind][1]:
#                             keep_str = "b"
#                             break
#                         temp_ind += 1
#                     else:
#                         if len_a > len(b):
#                             keep_str = "a"
#                         if len_a < len(b):
#                             keep_str = "b"
#
#                     if keep_str == "a":
#                         matched_index_tuple_list.remove(b)
#                         break
#                     if keep_str == "b":
#                         matched_index_tuple_list.remove(a)
#                         break
#                 else:
#                     break
#
#             # remove weight tuples
#             possible_shows = [x[0] for x in matched_index_tuple_list] #TODO?????????
#
#         # last search_phase is done
#         if search_phase == phases2[-1]:
#             break
#
#         if found_something_bool:
#             if len(possible_shows) == 1:
#                 break
#             if len(possible_shows) == 0:
#                 possible_shows = check_shows
#                 break
#             else:
#                 check_shows = possible_shows
#
#         search_phase += 1
#
#     return list(possible_shows)


# def helper_handle_matched_list(matched_index_tuple_list, input_words: [str], input_word: str, show_words: [str], show_word: str, check_shows: List[ConfigUserShow], show_instance: ConfigUserShow, search_phase):
#
#     index_input_word = input_words.index(input_word)
#     index_show_word = show_words.index(show_word)
#     index_tuple = (index_input_word, index_show_word)
#
#     index_show = check_shows.index(show_instance)
#
#     matched_index_tuple_list.insert(index_show, matched_index_tuple_list[index_show] + [index_tuple]) #TODO??????????? don't get this here
#     matched_index_tuple_list.pop(index_show + 1)
#
#     return matched_index_tuple_list




### Some Useful Small Methods ###

def whitespace_reduce(string: str) -> str:
    string = sub('\t', ' ', string)
    string = sub(' +', ' ', string).strip()
    return string


def create_config_if_none_existing():
    if not os.path.isfile(CONFIG_GLOBAL_FULL_PATH):
        new_user_uuid = shortuuid.uuid()
        while os.path.isfile(get_config_user_full_path(new_user_uuid)):
            new_user_uuid = shortuuid.uuid()

        new_config_global = create_default_global_config_with_file(new_user_uuid)
        new_config_global.create_new_user("User", new_user_uuid)
    else:
        global_config = ConfigGlobal.create_from_dict(load_json_to_dict(CONFIG_GLOBAL_FULL_PATH))
        if global_config.currentUserUuid is None or not os.path.isfile(get_config_user_full_path(global_config.currentUserUuid)):
            while len(global_config.users) > 0:
                next_user = global_config.users[0]
                global_config.currentUserUuid = next_user.userUuid
                if global_config.currentUserUuid is None or not os.path.isfile(get_config_user_full_path(global_config.currentUserUuid)):
                    global_config.users.remove(next_user)
                else:
                    user_config = ConfigUser.create_from_dict(load_json_to_dict(get_config_user_full_path(global_config.currentUserUuid)))
                    if user_config is None:
                        continue
                    else:
                        break
            else:
                new_user_uuid = shortuuid.uuid()
                while os.path.isfile(get_config_user_full_path(new_user_uuid)):
                    new_user_uuid = shortuuid.uuid()
                global_config.currentUserUuid = new_user_uuid
                global_config.create_new_user("User", new_user_uuid)

    # if not os.path.isfile(CONFIG_GLOBAL_FULL_PATH):
    #     save_json_complete(CONFIG_GLOBAL_FULL_PATH, DEFAULT_GLOBAL_SETTINGS)

    # if not os.path.isfile(get_config_user_full_path(1)):
    #     save_json_complete(get_config_user_full_path(1), DEFAULT_USER_SETTINGS)



##### Script Start #####
def main():

    create_config_if_none_existing()

    #TODO: check if shows and profiles available in selected user!

    # TODO make reloadable?? (necessary?), but don't handle errors coming from users fiddling with config files while everything is running!
    main_setup = MainSetup(CONFIG_GLOBAL_FULL_PATH)


    # input stuff TODO: gui delete
    # TODO: start player, "start" only command allowed before started, also add "stop"
    # to stop without closing, being able to "start" again
    # inputThread = Thread(target=read_keyboard_input, daemon=True)
    # inputThread.start()

    # start GUI here, invite user to create shows etc
    # TODO

    # main thread doing all the stuff, TODO: put into loop btw if stopped, can be restarted from gui
    playThread = Thread(target=play_start, args=(main_setup,), daemon=True)
    playThread.start()
    playThread.join()

    exit_program_event.set() # TODO: move once multiple sessions are allowed

    # this threading Event is set when the string EXIT_COMMAND for ending the program is passed during playback input
    exit_program_event.wait()

    end_message = f'{color.HIGHLIGHT}Turn Off {PROGRAM_NAME}{color.END}'
    print(end_message)


if __name__ == "__main__":
    main()
