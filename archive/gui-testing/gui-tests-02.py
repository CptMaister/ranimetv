from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys

# https://www.youtube.com/watch?v=Vde5SH8e1OQ&list=PLzMcBGfZo4-lB8MZfHPLTEHO9zJDDLpYj


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.initUI()
        self.counter = 0

        x_pos = 200
        y_pos = 200
        width = 300
        height = 300
        self.setGeometry(x_pos, y_pos, width, height)
        self.setWindowTitle("Test Window Title!")

    def initUI(self):
        self.button1 = QtWidgets.QPushButton(self)
        self.button1.setText("Click me")
        self.button1.clicked.connect(self.clicked)

        self.label = QtWidgets.QLabel(self)
        self.label.setText("My Label!")
        self.label.move(50, 100)

    def clicked(self):
        self.counter += 1
        self.label.setText("Clicked " + str(self.counter) + " times wohooooooooo!")
        self.label.adjustSize()


def window():
    app = QApplication(sys.argv)
    win = MyWindow()

    win.show()
    sys.exit(app.exec_())


window()
