from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys

# https://www.youtube.com/watch?v=Vde5SH8e1OQ&list=PLzMcBGfZo4-lB8MZfHPLTEHO9zJDDLpYj

def window():
    app = QApplication(sys.argv)
    win = QMainWindow()

    xpos = 200
    ypos = 200
    width = 300
    height = 300
    win.setGeometry(xpos, ypos, width, height)
    win.setWindowTitle("Test Window Title!")

    label = QtWidgets.QLabel(win)
    label.setText("My first Label!")
    label.move(50, 100)

    win.show()
    sys.exit(app.exec_())

window()