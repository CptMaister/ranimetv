#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Python script for customizable ordered playback of a randomly picked series
# of predefined media files from your local computer.
#
# ranimetv
# Copyright (C) 2020  Alexander Mai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Python script for customizable ordered playback of a randomly picked
series of predefined media files from your local computer.

To run properly it only needs a config file specifying all media files and
preferences. Just start up the script in your terminal and read the
instructions.

python-vlc is used to play the media files, see https://www.olivieraubert.net/vlc/python-ctypes/

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.

Source Code, Contact:
gitlab.com/CptMaister/ranimetv, alex.mai@posteo.net
"""


from re import sub  # substitute string parts with regex search, used to reduce whitespace
from queue import Queue # used for input processing
from threading import Thread, Event
from time import sleep
import os, sys # file path processing, handle args, detect current operating system, open dir in explorer
from subprocess import call, run, PIPE, STDOUT  # open directory in explorer with call, run things like in a terminal, used to get file's length with ffprobe
from copy import deepcopy
from math import floor
from random import randint, shuffle     # random ints, randomized order of elements in lists
from urllib.parse import unquote    # proper printing of commercial paths in print status, replaces %20 with a space and handles all other kinds of weird symbols
import vlc

PROGRAM_NAME = "RAnimeTV"
PROGRAM_VERSION = "0.10.21" # Windows compatibility, multiple config files, restructured code, few new commands and bug fixing
#PROGRAM_VERSION = "0.9.15" # massive bug fixes, options and new commands, code improvements
#PROGRAM_VERSION = "0.8" commercials, directory path line, queue, search alg
#PROGRAM_VERSION = "0.7" opening and ending, using only one player
#PROGRAM_VERSION = "0.6" player-jumps-debugger implemented, added license
#PROGRAM_VERSION = "0.5" using ShowEpisode and option classes, prepare future options and commands
#PROGRAM_VERSION = "0.4" help texts, error handling and monitoring
#PROGRAM_VERSION = "0.3" all basic commands
#PROGRAM_VERSION = "0.2" config file getting updated
#PROGRAM_VERSION = "0.1" main functionality, no config file updates


MESSAGE_ABOUT = f"""
Python script for customizable ordered playback of a randomly picked series of predefined media files from your local computer.

{PROGRAM_NAME} v{PROGRAM_VERSION}, Copyright (C) 2020  Alexander Mai, alex.mai@posteo.net

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.

Source Code: gitlab.com/CptMaister/ranimetv


References
colored and bold terminal output see https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
vlc MediaPlayer examples with python-vlc https://stackoverflow.com/questions/3595649/vlc-python-eventmanager-callback-type
waiting with threading for the player to finish at "The Good #2" in https://blog.miguelgrinberg.com/post/how-to-make-python-wait
input stuff with parallel threads https://stackoverflow.com/questions/5404068/how-to-read-keyboard-input/53344690#53344690
"""



# class for better terminal output, see https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
class color:
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    LIGHTGRAY = '\033[37m'

    BLACK_BG = '\033[40m'
    RED_BG = '\033[41m'
    GREEN_BG = '\033[42m'
    YELLOW_BG = '\033[43m'
    BLUE_BG = '\033[44m'
    PURPLE_BG = '\033[45m'
    CYAN_BG = '\033[46m'
    LIGHTGRAY_BG = '\033[47m'

    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

    HIGHLIGHT = BOLD + CYAN
    INPUT_ANSWER = BOLD
    PATH_PLAYBACK = "  " + GREEN
    WARNING = BOLD + YELLOW
    ERROR = BOLD + RED
# bold text: print(color.BOLD + 'Hello World !' + color.END)

if sys.platform == "win32":
    os.system('color') # this magically makes colors possible on windows 10

def printlog(printstr: str):
    print(f'    {color.CYAN}{printstr}{color.END}')



##### Set Limits, Standard values etc #####

VOLUME_LIMIT_LOWER = 0
VOLUME_LIMIT_UPPER = 200
SPEED_LIMIT_LOWER = 40
SPEED_LIMIT_UPPER = 300
MONITOR_LIMIT_LOWER = 1
PROBABILITY_LIMIT_LOWER = 5
PROBABILITY_LIMIT_UPPER = 100
QUEUE_LOAD_LIMIT_UPPER = 25


QUEUE_SIZE_STANDARD = 3
BACK_PLAYBACK_SIZE_STANDARD = 5
FORWARD_PLAYBACK_SIZE_STANDARD = 5
JUMP_PLAYBACK_BUMPER_SIZE_STANDARD = 20
SUSPEND_BACK_SIZE_STANDARD = 10
END_PLAYBACK_SIZE_STANDARD = 5
DEBUG_PLAYER_JUMPS_ANOMALY_SENSITIVITY_SIZE_STANDARD = 2
DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD = 1
MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD = 3



##### Config Handling Setup #####

# current_path = os.getcwd()
CONFIG_PATH = "ranimetv-config"

# ranimetv will take the first passed arg and add it to the config file name to enable using different config files swiftly. On Windows 10, these are not allowed, which is why they will be replaced here by a dash: \ / : * ? " < > |
FORBIDDEN_FILE_CHARS = ["\\", "/", ":", "*", "?", "\"", "<", ">", "|"]
config_path_extra = ""
if len(sys.argv) > 1:
    config_path_extra = sys.argv[1]
    for char in FORBIDDEN_FILE_CHARS:
        config_path_extra = config_path_extra.replace(char, "-")
    config_path_extra = "-" + config_path_extra.strip("-")

CONFIG_FULL_PATH = CONFIG_PATH + config_path_extra + ".txt"
ENCODING_CONFIG_FILE = "utf-8"

### config file data organisation
CONFIG_FIELD_SEPARATOR = ";"
CONFIG_POSITION_SHOW_ALIAS = 0
CONFIG_POSITION_SHOW_FIRST_EP = 1
CONFIG_POSITION_SHOW_LAST_EP = 2
CONFIG_POSITION_SHOW_CURRENT_EP = 3

LEFT_OFF_AT_SEPARATOR = "|"
CONFIG_GLOBAL_OPTIONS_LINE_STARTER = ">>>"
CONFIG_SUSPEND_QUEUE_LINE_STARTER = "<<<"

CONFIG_OPTIONS_YES_OPTIONS = ["true", "yes", "on", "y", "t"]
CONFIG_OPTIONS_NO_OPTIONS = ["false", "no", "off", "n", "f"]



##### Commands, Options and Help Text Setup #####

### Commands

## command specifiers
HELP_COMMAND = "help"
HELP_FULL_COMMAND = "HELP"
STATUS_COMMAND = "status"
PAUSE_COMMAND = "pause"
VOLUME_COMMAND = "volume"
JUMP_COMMAND = "jump"
BACK_COMMAND = "back"
FORWARD_COMMAND = "forward"
RESTART_COMMAND = "restart"
SKIP_EPISODE_COMMAND = "skip"
FINISH_EPISODE_COMMAND = "finish"
SHOW_CONFIG_COMMAND = "config"
CONFIG_HELP_COMMAND = "config-help"
EXIT_COMMAND = "exit"

OPEN_PLAYBACK_DIRECTORY_COMMAND = "open"
OPEN_CONFIG_COMMAND = "cconfig"
OPTIONS_HELP_COMMAND = "options-help"
DISPLAY_CURRENT_OPTIONS_COMMAND = "options"
SPEED_COMMAND = "speed"
HARD_VOLUME_COMMAND = "vvolume"
MUTE_COMMAND = "mute"
FULLSCREEN_COMMAND = "fullscreen"
DISPLAY_SHOWS_COMMAND = "shows"
QUEUE_SHOW_COMMAND = "queue"
QUEUE_SHOW_LOAD_COMMAND = "queue-load"
QUEUE_SHOW_CLEAR_COMMAND = "queue-clear"
QUEUE_SHOW_POP_COMMAND = "queue-pop"
PLAY_SHOW_COMMAND = "play"
#PLAY_EPISODE_COMMAND = "episode"
#PREVIOUS_EPISODE_COMMAND = "previous"
#NEXT_EPISODE_COMMAND = "next"
#LOOP_EPISODE_COMMAND = "loop"
END_COMMAND = "eend"
SUSPEND_COMMAND = "suspend"
SUSPEND_QUEUE_COMMAND = "ssuspend"
SHUTDOWN_COMMAND = "shutdown"
LAST_EPISODE_COMMAND = "last"
SKIP_FULL_EPISODE_COMMAND = "sskip"
FINISH_FULL_EPISODE_COMMAND = "ffinish"
SKIP_TO_COMMERCIALS_COMMAND = "commercial"
COMMERCIALS_GLOBAL_OVERRIDE_COMMAND = "commercials"
HARD_PLAY_COMMAND = "resume"
HARD_PAUSE_COMMAND = "ppause"
VIDEO_TRACK_COMMAND = "video"
AUDIO_TRACK_COMMAND = "audio"
SUBTITLE_TRACK_COMMAND = "subtitle"
HISTORY_COMMAND = "history"
MONITOR_COMMAND = "monitor"
MONITOR_PLAYER_JUMPS_COMMAND = "monitor-jump"
MONITOR_OFF_COMMAND = "mon"
DEBUG_PLAYER_JUMPS_COMMAND = "debug"
MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND = "analysis"
MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND = "analysis-full"
ABOUT_COMMAND = "about"

COMMANDS_LIST = [HELP_COMMAND, HELP_FULL_COMMAND, STATUS_COMMAND, PAUSE_COMMAND, VOLUME_COMMAND, JUMP_COMMAND, BACK_COMMAND, FORWARD_COMMAND, RESTART_COMMAND, SKIP_EPISODE_COMMAND, FINISH_EPISODE_COMMAND, SHOW_CONFIG_COMMAND, CONFIG_HELP_COMMAND, EXIT_COMMAND, OPEN_PLAYBACK_DIRECTORY_COMMAND, OPEN_CONFIG_COMMAND, OPTIONS_HELP_COMMAND, DISPLAY_CURRENT_OPTIONS_COMMAND, SPEED_COMMAND, HARD_VOLUME_COMMAND, MUTE_COMMAND, FULLSCREEN_COMMAND, DISPLAY_SHOWS_COMMAND, QUEUE_SHOW_COMMAND, QUEUE_SHOW_LOAD_COMMAND, QUEUE_SHOW_CLEAR_COMMAND, QUEUE_SHOW_POP_COMMAND, PLAY_SHOW_COMMAND, END_COMMAND, SUSPEND_COMMAND, SUSPEND_QUEUE_COMMAND, SHUTDOWN_COMMAND, LAST_EPISODE_COMMAND, SKIP_FULL_EPISODE_COMMAND, FINISH_FULL_EPISODE_COMMAND, SKIP_TO_COMMERCIALS_COMMAND, COMMERCIALS_GLOBAL_OVERRIDE_COMMAND, HARD_PLAY_COMMAND, HARD_PAUSE_COMMAND, VIDEO_TRACK_COMMAND, AUDIO_TRACK_COMMAND, SUBTITLE_TRACK_COMMAND, HISTORY_COMMAND, MONITOR_COMMAND, MONITOR_PLAYER_JUMPS_COMMAND, MONITOR_OFF_COMMAND, DEBUG_PLAYER_JUMPS_COMMAND, MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND, MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND, ABOUT_COMMAND] #formerly used for input auto completion


# short command names (if in use)
HELP_C = "h"
HELP_FULL_C = "H"
STATUS_C = "s"
PAUSE_C = "p"
VOLUME_C = "v"
JUMP_C = "j"
BACK_C = "b"
FORWARD_C = "f"
RESTART_C = "r"
SHOW_CONFIG_C = "c"
EXIT_C = "e"
EXIT_C_2 = "q"

MUTE_C = "m"
QUEUE_SHOW_C = "qu"
QUEUE_SHOW_LOAD_C = "ql"
QUEUE_SHOW_CLEAR_C = "qc"
QUEUE_SHOW_POP_C = "qp"
#PREVIOUS_EPISODE_C = "prev"
SKIP_TO_COMMERCIALS_C = "cm"
COMMERCIALS_GLOBAL_OVERRIDE_C = "cms"




### Options
# option specifiers
# option specifiers that have no corresponding command
OPENING_OPTION = "opening"              # bool, with custom file type
ENDING_OPTION = "ending"                # bool, with custom file type
RANDOM_OPTION_SHOW = "random"           # bool, current_ep will be randomized
MULTIPLE_EPISODES_OPTION_SHOW = "episodes"
PROBABILITY_OPTION_SHOW = "probability" # int 0-100
PROBABILITY_OPTION_GLOBAL = "probability"   # int 1-100, sets default probability for all shows that don't use the probability option
COMMERCIALS_PLAY_OPTION_GLOBAL = "commercials" # bool <- play shows, that are commercials (option set). Usually no show with commercial option will be randomly picked, only if user intentionally gets it with command 'play' or 'queue'
COMMERCIAL_WEIGHT_OPTION_GLOBAL = "commercial" # int <- weight how much to play
COMMERCIAL_WEIGHT_OPTION_SHOW = "commercial"   # int <- weight of each cm here
COMMERCIAL_MAX_OPTION_SHOW = "commercial-max"   # int <- weight of each cm here
COMMERCIAL_MIN_OPTION_SHOW = "commercial-min"   # int <- weight of each cm here
UNIQUE_SHOWS_OPTION_GLOBAL = "unique"   # bool, avoid playing a show more than once in a session
HARD_UNIQUE_SHOWS_OPTION_GLOBAL = "uunique"     # bool, plays every show not more than once, end ranimetv if every show played once
NO_RERUN_OPTION = "no-rerun"

STATUS_OPTION = STATUS_COMMAND  # bool, display status on player start
PAUSE_OPTION = PAUSE_COMMAND    # bool, pause player on player start
VOLUME_OPTION = VOLUME_COMMAND  # int, volume in percent, limitted
JUMP_OPTION = JUMP_COMMAND      # int, jumps to int seconds when episode starts
SPEED_OPTION = SPEED_COMMAND    # int, episode speed in percent
MUTE_OPTION = MUTE_COMMAND      # bool, mute when start, global only on startup
FULLSCREEN_OPTION = FULLSCREEN_COMMAND  # bool
QUEUE_SHOW_DISPLAY_OPTION = QUEUE_SHOW_COMMAND  # bool, display queue on start
#LOOP_EPISODE_OPTION = LOOP_EPISODE_COMMAND      # int, loop amount
SUSPEND_OPTION = SUSPEND_COMMAND                # bool
SUSPEND_QUEUE_OPTION = SUSPEND_QUEUE_COMMAND    # bool
SHUTDOWN_OPTION_GLOBAL = SHUTDOWN_COMMAND       # int, turn off after int sec
LAST_EPISODE_OPTION_GLOBAL = LAST_EPISODE_COMMAND # int, turn off after int many finished shows
VIDEO_TRACK_OPTION_SHOW = VIDEO_TRACK_COMMAND   # int, choose track
AUDIO_TRACK_OPTION_SHOW = AUDIO_TRACK_COMMAND   # int, choose track
SUBTITLE_TRACK_OPTION_SHOW = SUBTITLE_TRACK_COMMAND # int, choose track
MONITOR_OPTION = MONITOR_COMMAND    # int, print full status every int seconds
MONITOR_PLAYER_JUMPS_OPTION = MONITOR_PLAYER_JUMPS_COMMAND  # bool, activate
DEBUG_PLAYER_JUMPS_OPTION = DEBUG_PLAYER_JUMPS_COMMAND  # bool, experimental auto restarter on encountered player length or time changing player behaviour

OPTIONS_LIST = [OPENING_OPTION, ENDING_OPTION, RANDOM_OPTION_SHOW, MULTIPLE_EPISODES_OPTION_SHOW, PROBABILITY_OPTION_SHOW, COMMERCIALS_PLAY_OPTION_GLOBAL, COMMERCIAL_WEIGHT_OPTION_SHOW, COMMERCIAL_MAX_OPTION_SHOW, COMMERCIAL_MIN_OPTION_SHOW, UNIQUE_SHOWS_OPTION_GLOBAL, HARD_UNIQUE_SHOWS_OPTION_GLOBAL, NO_RERUN_OPTION, STATUS_OPTION, PAUSE_OPTION, VOLUME_OPTION, JUMP_OPTION, SPEED_OPTION, MUTE_OPTION, FULLSCREEN_OPTION, QUEUE_SHOW_DISPLAY_OPTION, SUSPEND_OPTION, SUSPEND_QUEUE_OPTION, SHUTDOWN_OPTION_GLOBAL, LAST_EPISODE_OPTION_GLOBAL, VIDEO_TRACK_OPTION_SHOW, AUDIO_TRACK_OPTION_SHOW, SUBTITLE_TRACK_OPTION_SHOW, MONITOR_OPTION, MONITOR_PLAYER_JUMPS_OPTION, DEBUG_PLAYER_JUMPS_OPTION]

def showcase_commands(): # only used for a video explaining the program
    counter = 0
    message = ""
    for command in COMMANDS_LIST:
        counter += 1
        if len(command) < 8:
            message = message + command + "\t\t"
        else:
            message = message + command + "\t"
        if counter % 5 == 0:
            message = message + "\n"
    print(color.BOLD + message + color.END)

#showcase_commands()

def showcase_options(): # only used for a video explaining the program
    counter = 0
    message = ""
    for option in OPTIONS_LIST:
        counter += 1
        if len(option) < 8:
            message = message + option + "\t\t"
        else:
            message = message + option + "\t"
        if counter % 5 == 0:
            message = message + "\n"
    print(color.BOLD + message + color.END)

#showcase_options()




## Command Explanations
# explanations in short help string
HELP_COMMAND_DESCR = "display this help text."
HELP_FULL_COMMAND_DESCR = "more detailed help with more commands."
STATUS_COMMAND_DESCR = "display information about current playback."
PAUSE_COMMAND_DESCR = "toggle pause."
VOLUME_COMMAND_DESCR = "set volume."
JUMP_COMMAND_DESCR = "jump to second X."
BACK_COMMAND_DESCR = f"go X seconds back. Type '{BACK_COMMAND}' to go {BACK_PLAYBACK_SIZE_STANDARD} seconds back."
FORWARD_COMMAND_DESCR = f"go X seconds forward. Type '{FORWARD_COMMAND}' to go {FORWARD_PLAYBACK_SIZE_STANDARD} seconds forward."
RESTART_COMMAND_DESCR = "restart current episode."
SKIP_EPISODE_COMMAND_DESCR = "play something else, don't update config."
FINISH_EPISODE_COMMAND_DESCR = "play something else, and update config."
SHOW_CONFIG_COMMAND_DESCR = "display path and contents of config."
CONFIG_HELP_COMMAND_DESCR = "display how to create a config file."
EXIT_COMMAND_DESCR = "exit this program."

# explanations in long help string
HELP_COMMAND_DESCRIPTION = "display shorter help text."
HELP_FULL_COMMAND_DESCRIPTION = "display this more detailed help text with debugging commands, more details about implementation and handling of bad or out of bounds input."
STATUS_COMMAND_DESCRIPTION = "display information about current playback."
PAUSE_COMMAND_DESCRIPTION = "pauses a playing episode or resumes a paused episode."
VOLUME_COMMAND_DESCRIPTION = f"set volume to X percent. Lower limit is {VOLUME_LIMIT_LOWER}, upper limit is {VOLUME_LIMIT_UPPER}. This affects the whole session and acts as a multiplier, just like the show's config and global config volume. See '{HARD_VOLUME_COMMAND}' to change volume for the current playback, ignoring all config volume settings. Type '{VOLUME_COMMAND}' to see current volume and volume settings."
JUMP_COMMAND_DESCRIPTION = f"jump to second X of playback, see below for possibilities of X. If goal time is beyond episode's length, it will jump {JUMP_PLAYBACK_BUMPER_SIZE_STANDARD} seconds close to end or do nothing if already closer than {JUMP_PLAYBACK_BUMPER_SIZE_STANDARD} seconds to end."
BACK_COMMAND_DESCRIPTION = f"go X seconds back. Same as '{JUMP_COMMAND} 0' if going back more seconds than current playback already passed. Type '{BACK_COMMAND}' to go {BACK_PLAYBACK_SIZE_STANDARD} seconds back."
FORWARD_COMMAND_DESCRIPTION = f"go X seconds forward. Same behaviour as with '{JUMP_COMMAND}' if goal time is too high. Type '{FORWARD_COMMAND}' to go {FORWARD_PLAYBACK_SIZE_STANDARD} seconds forward."
RESTART_COMMAND_DESCRIPTION = "restart playback of current episode, will call player.stop(), player.play(), using the skipped flag to avoid config file changes."
SKIP_EPISODE_COMMAND_DESCRIPTION = f"play next media, don't update the current episode in your config file if this command is used during episode playback. If current playback is an opening, ending or commercial, then there is no effect on if the config will be changed or not. Use '{SKIP_FULL_EPISODE_COMMAND}' to skip all components of this episode (including opening, ending and commercials) and play the next show."
FINISH_EPISODE_COMMAND_DESCRIPTION = f"play something else, and update the current episode in config file, increasing it by one. Use '{FINISH_FULL_EPISODE_COMMAND}' to finish all components of this episode (including opening, ending and commercials) and play the next show. Episode will be updated in config (if used during opening or episode), commercials won't be. If used during a commercial, then this commercial will be updated."
SHOW_CONFIG_COMMAND_DESCRIPTION = "show path and contents of config file."
CONFIG_HELP_COMMAND_DESCRIPTION = "show how to create a proper config file with examples and all necessary and possible arguments and lines."
EXIT_COMMAND_DESCRIPTION = "exit this program, will stop playback, input and everything else."


OPEN_PLAYBACK_DIRECTORY_COMMAND_DESCRIPTION = "open directory of currently playing file."
OPEN_CONFIG_COMMAND_DESCRIPTION = "open config file with your system's standard text file application."
OPTIONS_HELP_COMMAND_DESCRIPTION = "show all global and local options available."
DISPLAY_CURRENT_OPTIONS_COMMAND_DESCRIPTION = "show all current options."
SPEED_COMMAND_DESCRIPTION = f"set playback speed to X percent. Limited to 300 up, 20 down. Lower limit is {SPEED_LIMIT_LOWER} upper limit is {SPEED_LIMIT_UPPER}. Type '{SPEED_COMMAND}' to see current speed."
HARD_VOLUME_COMMAND_DESCRIPTION = "set current player's volume to X, ignoring all settings in config and ignoring limits."
MUTE_COMMAND_DESCRIPTION = "toggle mute."
FULLSCREEN_COMMAND_DESCRIPTION = "toggle fullscreen."
DISPLAY_SHOWS_COMMAND_DESCRIPTION = "display all shows from your config. This does not check if your show lines are valid or if they have valid path lines or if the files exist on your system at all."
QUEUE_SHOW_COMMAND_DESCRIPTION = f"add a specific show to the end of the current show queue. S has to be a show alias. Type '{QUEUE_SHOW_COMMAND}' to show the current queue. The queue will hold at least {QUEUE_SIZE_STANDARD} shows when a new episode starts, reloading new ones when there aren't enough when a new episode is started."
QUEUE_SHOW_LOAD_COMMAND_DESCRIPTION = f"add X random shows to the show queue. Type '{QUEUE_SHOW_LOAD_COMMAND}' or '{QUEUE_SHOW_LOAD_C}' to load {QUEUE_SIZE_STANDARD} shows."
QUEUE_SHOW_CLEAR_COMMAND_DESCRIPTION = "clear the current show queue."
QUEUE_SHOW_POP_COMMAND_DESCRIPTION = "remove the last item in the current show queue."
PLAY_SHOW_COMMAND_DESCRIPTION = "play a specific show now. S has to be a show alias. This will play one episode of the show, as if it was the current entry in the show queue. This will push all other shows in the queue back by one, so the show that played just now will be played next."
#PLAY_EPISODE_COMMAND_DESCRIPTION = f"DELETED play episode X of the currently running show. When playing an episode that is not the current episode according to the config file, then the config file won't be updated after watching this episode. To return to the current episode of the currently running show, type '{PLAY_EPISODE_COMMAND}'."
#PREVIOUS_EPISODE_COMMAND_DESCRIPTION = "DELETED play previous episode of the currently running show."
#NEXT_EPISODE_COMMAND_DESCRIPTION = "DELETED play next episode of the currently running show."
#LOOP_EPISODE_COMMAND_DESCRIPTION = f"loop every episode X times. This loops the current and every other episode in this session X times. If looping is not turned on and you type '{LOOP_EPISODE_COMMAND}', it will loop the current episode forever. Input '{LOOP_EPISODE_COMMAND}' again to stop any looping. If you go to another episode with commands like '{PLAY_SHOW_COMMAND}' or '{SKIP_EPISODE_COMMAND}', it will not turn off looping."
END_COMMAND_DESCRIPTION = f"jump X seconds close to end, X has to be an integer. Type '{END_COMMAND}' to jump {END_PLAYBACK_SIZE_STANDARD} seconds close to end."
SUSPEND_COMMAND_DESCRIPTION = f"turn off {PROGRAM_NAME}, but also save in config where you left off. {PROGRAM_NAME} will continue where you left off when you start it again, going back {SUSPEND_BACK_SIZE_STANDARD} seconds."
SUSPEND_QUEUE_COMMAND_DESCRIPTION = f"same as '{SUSPEND_COMMAND}', but also saves the current queue in config. The aliases of the shows in the current queue will be saved at the top of your config, starting with '{CONFIG_SUSPEND_QUEUE_LINE_STARTER}' and separated by semicolons (;). If a show is currently running, then it is not saved in this line, as its playback position if already saved like when using '{SUSPEND_COMMAND}'."
SHUTDOWN_COMMAND_DESCRIPTION = f"turn off {PROGRAM_NAME} in X minutes. Type '{SHUTDOWN_COMMAND}' again to turn off the timer."
LAST_EPISODE_COMMAND_DESCRIPTION = f"turn of {PROGRAM_NAME} after X shows (including currently running) are done playing. Not counting down if episode is skipped. Type '{LAST_EPISODE_COMMAND}' to cancel if currently activated, or type it when deactivated to end after the current playback (also works on playbacks that are not a show's episode)."
SKIP_FULL_EPISODE_COMMAND_DESCRIPTION = f"skips all components of the episode and plays the next show. Same as '{SKIP_EPISODE_COMMAND}' if you have no opening, ending and commercial settings. Use '{SKIP_EPISODE_COMMAND}' if you only want to skip the currently playing media."
FINISH_FULL_EPISODE_COMMAND_DESCRIPTION = f"skips all components of the episode and plays the next show. Same as '{SKIP_FULL_EPISODE_COMMAND}' but updates this show's current episode in config if it is used during opening or episode."
SKIP_TO_COMMERCIALS_COMMAND_DESCRIPTION = f"skips all components of the episode and plays the commercials, if any are set. If there are no commercials set in config, then same as '{SKIP_FULL_EPISODE_COMMAND}'."
COMMERCIALS_GLOBAL_OVERRIDE_COMMAND_DESCRIPTION = f"overrides the global commercial weight config option '{COMMERCIAL_WEIGHT_OPTION_GLOBAL}'."
HARD_PLAY_COMMAND_DESCRIPTION = "resume playback. No effect if playback already running."
HARD_PAUSE_COMMAND_DESCRIPTION = "pause playback. No effect if playback already paused."
VIDEO_TRACK_COMMAND_DESCRIPTION = f"set the video to video track X. Type '{VIDEO_TRACK_COMMAND}' to display current track and total amount of video tracks."
AUDIO_TRACK_COMMAND_DESCRIPTION = f"set the audio to audio track X. Type '{AUDIO_TRACK_COMMAND}' to display current track and total amount of audio tracks."
SUBTITLE_TRACK_COMMAND_DESCRIPTION = f"set the subtitles to subtitle track X. Type '{SUBTITLE_TRACK_COMMAND}' to display current track and total amount of subtitle tracks."
HISTORY_COMMAND_DESCRIPTION = "display all files that have been played and all episodes finished so far."
MONITOR_COMMAND_DESCRIPTION = "toggle normal monitor. Prints all sorts of player information every X seconds."
MONITOR_PLAYER_JUMPS_COMMAND_DESCRIPTION = "toggle Jump Monitor. Checks every 0.1 seconds if the player did an unusual jump in playback time. Prints results if anything notable happens."
MONITOR_OFF_COMMAND_DESCRIPTION = "turn off all monitors. No effect if none was turned on."
DEBUG_PLAYER_JUMPS_COMMAND_DESCRIPTION = f"experimental debugger that recognizes if the playback is corrupted. More precisely if the player length changes, it will restart the playback and jump to {DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD} second after the player_length was changed."
MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND_DESCRIPTION = f"only works if ffmpeg is installed. Displays a table with approximations of all the shows' amount of episodes, average length, total length and the total length of all shows togehter. Excluding opening and ending files. Lengths are only approximated by getting the length of {MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD} files per show and then multiplying with (amount of episodes / {MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD}). This method works especially well, if the episodes within a show all have pretty much the same length. Use {MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND} for a thorough analysis."
MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND_DESCRIPTION = "WARNING: only use this if you are ready for your computer to do a lot of reading and writing on your files to pretty much check all the files from your config. Takes a while to finish, around 30 seconds per 50h of files. Can slow your computer down. Displays a table with all the shows' amount of episodes, average length, total length and the total length of all shows togehter. Excluding opening and ending files."
ABOUT_COMMAND_DESCRIPTION = "display various information about the program."





def format_help_commands(commands: [str]):
    message_unformatted = " " + ", ".join(commands)
    message_formatted = f'{message_unformatted: <15}'
    return color.BOLD + message_formatted + color.END

MESSAGE_HELP_SHORT = f"""{color.BOLD}---*** {PROGRAM_NAME} Help ***---{color.END}

Available commands:
 {format_help_commands([HELP_C, HELP_COMMAND])} {HELP_COMMAND_DESCR}
 {format_help_commands([HELP_FULL_C, HELP_FULL_COMMAND])} {HELP_FULL_COMMAND_DESCR}
 {format_help_commands([STATUS_C, STATUS_COMMAND])} {STATUS_COMMAND_DESCR}
 {format_help_commands([PAUSE_C, PAUSE_COMMAND])} {PAUSE_COMMAND_DESCR}
 {format_help_commands([VOLUME_C + " X", VOLUME_COMMAND + " X"])} {VOLUME_COMMAND_DESCR}
 {format_help_commands([JUMP_C + " X", JUMP_COMMAND + " X"])} {JUMP_COMMAND_DESCR}
 {format_help_commands([BACK_C + " X", BACK_COMMAND + " X"])} {BACK_COMMAND_DESCR}
 {format_help_commands([FORWARD_C + " X", FORWARD_COMMAND + " X"])} {FORWARD_COMMAND_DESCR}
 {format_help_commands([RESTART_C, RESTART_COMMAND])} {RESTART_COMMAND_DESCR}
 {format_help_commands([SKIP_EPISODE_COMMAND])} {SKIP_EPISODE_COMMAND_DESCR}
 {format_help_commands([FINISH_EPISODE_COMMAND])} {FINISH_EPISODE_COMMAND_DESCR}
 {format_help_commands([SHOW_CONFIG_C, SHOW_CONFIG_COMMAND])} {SHOW_CONFIG_COMMAND_DESCR}
 {format_help_commands([CONFIG_HELP_COMMAND])} {CONFIG_HELP_COMMAND_DESCR}
 {format_help_commands([EXIT_C_2, EXIT_C, EXIT_COMMAND])} {EXIT_COMMAND_DESCR}

Enter {color.CYAN}{color.BOLD}{HELP_FULL_COMMAND}{color.END} to see extra commands meant for troubleshooting.
source code: {color.YELLOW}{color.BOLD}gitlab.com/CptMaister/ranimetv{color.END}
---*** End of {PROGRAM_NAME} Help ***---"""



MESSAGE_HELP_FULL = f"""{color.BOLD}---*** {PROGRAM_NAME} HELP ***---{color.END}

Available commands:
 {format_help_commands([HELP_C, HELP_COMMAND])} {HELP_COMMAND_DESCRIPTION}
 {format_help_commands([HELP_FULL_C, HELP_FULL_COMMAND])} {HELP_FULL_COMMAND_DESCRIPTION}
 {format_help_commands([STATUS_C, STATUS_COMMAND])} {STATUS_COMMAND_DESCRIPTION}
 {format_help_commands([PAUSE_C, PAUSE_COMMAND])} {PAUSE_COMMAND_DESCRIPTION}
 {format_help_commands([VOLUME_C + " X", VOLUME_COMMAND + " X"])} {VOLUME_COMMAND_DESCRIPTION}
 {format_help_commands([JUMP_C + " X", JUMP_COMMAND + " X"])} {JUMP_COMMAND_DESCRIPTION}
 {format_help_commands([BACK_C + " X", BACK_COMMAND + " X"])} {BACK_COMMAND_DESCRIPTION}
 {format_help_commands([FORWARD_C + " X", FORWARD_COMMAND + " X"])} {FORWARD_COMMAND_DESCRIPTION}
 {format_help_commands([RESTART_C, RESTART_COMMAND])} {RESTART_COMMAND_DESCRIPTION}
 {format_help_commands([SKIP_EPISODE_COMMAND])} {SKIP_EPISODE_COMMAND_DESCRIPTION}
 {format_help_commands([FINISH_EPISODE_COMMAND])} {FINISH_EPISODE_COMMAND_DESCRIPTION}
 {format_help_commands([SHOW_CONFIG_C, SHOW_CONFIG_COMMAND])} {SHOW_CONFIG_COMMAND_DESCRIPTION}
 {format_help_commands([CONFIG_HELP_COMMAND])} {CONFIG_HELP_COMMAND_DESCRIPTION}
 {format_help_commands([EXIT_C, EXIT_COMMAND])} {EXIT_COMMAND_DESCRIPTION}

Whenever a command takes an integer X for seconds, you can also input hh:mm:ss or mm:ss for X, like 4:20 to get 260 seconds.

These commands are either considered too niche to appear in the main help or were originally meant for debugging and testing of {PROGRAM_NAME} and have not been extensively tested:
 {format_help_commands([OPEN_PLAYBACK_DIRECTORY_COMMAND])} {OPEN_PLAYBACK_DIRECTORY_COMMAND_DESCRIPTION}
 {format_help_commands([OPEN_CONFIG_COMMAND])} {OPEN_CONFIG_COMMAND_DESCRIPTION}
 {format_help_commands([OPTIONS_HELP_COMMAND])} {OPTIONS_HELP_COMMAND_DESCRIPTION}
 {format_help_commands([DISPLAY_CURRENT_OPTIONS_COMMAND])} {DISPLAY_CURRENT_OPTIONS_COMMAND_DESCRIPTION}
 {format_help_commands([SPEED_COMMAND + " X"])} {SPEED_COMMAND_DESCRIPTION}
 {format_help_commands([HARD_VOLUME_COMMAND + " X"])} {HARD_VOLUME_COMMAND_DESCRIPTION}
 {format_help_commands([MUTE_C, MUTE_COMMAND])} {MUTE_COMMAND_DESCRIPTION}
 {format_help_commands([FULLSCREEN_COMMAND])} {FULLSCREEN_COMMAND_DESCRIPTION}
 {format_help_commands([DISPLAY_SHOWS_COMMAND])} {DISPLAY_SHOWS_COMMAND_DESCRIPTION}
 {format_help_commands([QUEUE_SHOW_C + " S", QUEUE_SHOW_COMMAND + " S"])} {QUEUE_SHOW_COMMAND_DESCRIPTION}
 {format_help_commands([QUEUE_SHOW_LOAD_C + " X"])} {QUEUE_SHOW_LOAD_COMMAND_DESCRIPTION}
 {format_help_commands([QUEUE_SHOW_CLEAR_C, QUEUE_SHOW_CLEAR_COMMAND])} {QUEUE_SHOW_CLEAR_COMMAND_DESCRIPTION}
 {format_help_commands([QUEUE_SHOW_POP_C, QUEUE_SHOW_POP_COMMAND])} {QUEUE_SHOW_POP_COMMAND_DESCRIPTION}
 {format_help_commands([PLAY_SHOW_COMMAND + " S"])} {PLAY_SHOW_COMMAND_DESCRIPTION}
 {format_help_commands([END_COMMAND + " X"])} {END_COMMAND_DESCRIPTION}
 {format_help_commands([SUSPEND_COMMAND])} {SUSPEND_COMMAND_DESCRIPTION}
 {format_help_commands([SUSPEND_QUEUE_COMMAND])} {SUSPEND_QUEUE_COMMAND_DESCRIPTION}
 {format_help_commands([SHUTDOWN_COMMAND + " X"])} {SHUTDOWN_COMMAND_DESCRIPTION}
 {format_help_commands([LAST_EPISODE_COMMAND + " X"])} {LAST_EPISODE_COMMAND_DESCRIPTION}
 {format_help_commands([SKIP_FULL_EPISODE_COMMAND])} {SKIP_FULL_EPISODE_COMMAND_DESCRIPTION}
 {format_help_commands([FINISH_FULL_EPISODE_COMMAND])} {FINISH_FULL_EPISODE_COMMAND_DESCRIPTION}
 {format_help_commands([SKIP_TO_COMMERCIALS_C, SKIP_TO_COMMERCIALS_COMMAND])} {SKIP_TO_COMMERCIALS_COMMAND_DESCRIPTION}
 {format_help_commands([COMMERCIALS_GLOBAL_OVERRIDE_C, COMMERCIALS_GLOBAL_OVERRIDE_COMMAND])} {COMMERCIALS_GLOBAL_OVERRIDE_COMMAND_DESCRIPTION}
 {format_help_commands([HARD_PLAY_COMMAND])} {HARD_PLAY_COMMAND_DESCRIPTION}
 {format_help_commands([HARD_PAUSE_COMMAND])} {HARD_PAUSE_COMMAND_DESCRIPTION}
 {format_help_commands([VIDEO_TRACK_COMMAND + " X"])} {VIDEO_TRACK_COMMAND_DESCRIPTION}
 {format_help_commands([AUDIO_TRACK_COMMAND + " X"])} {AUDIO_TRACK_COMMAND_DESCRIPTION}
 {format_help_commands([SUBTITLE_TRACK_COMMAND + " X"])} {SUBTITLE_TRACK_COMMAND_DESCRIPTION}
 {format_help_commands([HISTORY_COMMAND])} {HISTORY_COMMAND_DESCRIPTION}
 {format_help_commands([MONITOR_COMMAND + " X"])} {MONITOR_COMMAND_DESCRIPTION}
 {format_help_commands([MONITOR_PLAYER_JUMPS_COMMAND])} {MONITOR_PLAYER_JUMPS_COMMAND_DESCRIPTION}
 {format_help_commands([MONITOR_OFF_COMMAND])} {MONITOR_OFF_COMMAND_DESCRIPTION}
 {format_help_commands([DEBUG_PLAYER_JUMPS_COMMAND])} {DEBUG_PLAYER_JUMPS_COMMAND_DESCRIPTION}
 {format_help_commands([MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND])} {MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND_DESCRIPTION}
 {format_help_commands([MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND])} {MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND_DESCRIPTION}
 {format_help_commands([ABOUT_COMMAND])} {ABOUT_COMMAND_DESCRIPTION}

Suggestions for new functionalities and improvements for {PROGRAM_NAME} are always welcome at {color.YELLOW}{color.BOLD}gitlab.com/CptMaister/ranimetv{color.END} or {color.GREEN}{color.BOLD}alex.mai@posteo.net{color.END} :-)
---*** End of {PROGRAM_NAME} HELP ***---"""



MESSAGE_CONFIG_HELP = f"""{color.BOLD}---*** Config Help ***{color.BOLD}---
    {color.BOLD}{color.YELLOW}{"Simple Examples".upper()}{color.END}

    {color.YELLOW}Example 1 - required arguments for 2 shows:{color.END}
{color.BOLD}Death Note;            01; 37;    13
JoJo's 5: Golden Wind; 22; 39;    22

Death Note = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
JoJo's 5: Golden Wind = /home/user/Desktop/JoJo/Part 5/JJBA_GW_ ; .mp4{color.END}

    {color.YELLOW}Explanation:{color.END}
On the next run {PROGRAM_NAME} will randomly pick one of these 2 Animes and will either try to play the file
/home/user/Videos/Favorite Anime/DeathNote/Death-Note-13-rip.mp3
or
/home/user/Desktop/JoJo/Part 5/JJBA_GW_22.mp4


    {color.YELLOW}Example 2 - three shows and some optional arguments:{color.END}
{color.BOLD}{CONFIG_GLOBAL_OPTIONS_LINE_STARTER} ending:true; speed:90; volume: 115
DeathNote; 01; 37; 13; ending:false; pause:true;
JoJo5; 22; 39; 22; opening:true.mp3; jump:10;
Samurai Champloo; 3; 8; 3

DeathNote = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
JoJo5 = /home/user/Desktop/JoJo/Part 5/JJBA_GW_ ; .mp4
Samurai Champloo = '/home/user/Videos/oldanime/samurai champloo ' ; ' hq.avi'{color.END}

    {color.YELLOW}Explanation:{color.END}
Global options are set and will be applied to all episodes, if they don't set other values for the specified options:
  play ending after each episode
  playback speed is at 90%
  global volume multiplier 115% (or 1.15)
On the next run {PROGRAM_NAME} will randomly pick one of these 3 Animes and apply some extra show specific options in the first two cases, if one of them is picked. The episodes to be played would be
/home/user/Videos/Favorite Anime/DeathNote/Death-Note-13-rip.mp3
or
/home/user/Desktop/JoJo/Part 5/JJBA_GW_22.mp4
or
/home/user/Videos/oldanime/samurai champloo 3 hq.avi

The Death Note episode would start in a paused state. When the episode is finished, no ending is played because the ending:false overrides the global ending option value.

Before the JoJo 5 episode starts, the file
/home/user/Desktop/JoJo/Part 5/JJBA_GW_op.mp3
will be played, afterwards the episode, but it will start at 10 seconds.

The Samurai Champloo episode has no individual show options so only the global options will take effect.


    {color.YELLOW}Example 3 - directory path lines and commercials:{color.END}
{color.BOLD}{CONFIG_GLOBAL_OPTIONS_LINE_STARTER} debug: true, unique: true; commercial:13

# Animes
DeathNote;  01;  37;    13;  opening:true; ending:true; episodes: 2
JoJo5;      8;   last;  22   #get opening and ending
Dr. Stone;  1;   12;    3;   random:true

# my commercials
Nintendo Trailer;  1;   last;  17;  random:True; commercial:6; commercial-max: 1
Nintendo Teaser;   11;  20;    15;  random:True; commercial:2

# path lines
DeathNote = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
Dr. Stone = /home/user/Videos/last season/dr-stone/
JoJo5 = /home/user/Desktop/JoJo/Part 5
Nintendo Trailer = /home/user/Gaming/Nintendo/introductions
Nintendo Teaser = /home/user/Gaming/Nintendo/short trailers{color.END}

    {color.YELLOW}Explanation:{color.END}
  Global options
For every playback the experimental debugger will be on. It detects changes in playback length and tries to restart the playback at {DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD} seconds after the detected length change.
The unique option will avoid getting shows into the queue more than once as long as there are shows that haven't been played during this session.
The commercial option with value 13 sets the global commercial weight to 13. This means that between every episode (including opening and ending) there will be commercials played, if there are show lines with a commercial weight lower than the global commercial weight. Type '{OPTIONS_HELP_COMMAND}' to learn more about these options.

  Show and path lines
As you can see, there is a lot different in this example. Let's look at one funcitonality at a time.

Directory path lines:
Instead of writing the whole path with the episode number missing, you can write the directory (also known as 'folder') where the files are located in. In this example, all but one show use this way of declaring the path.
This way you can have files with wildly differing names played as one show in {PROGRAM_NAME} without having to rename them. They just have to be in their own directory.
The files will be sorted by {PROGRAM_NAME} alphabetically.
When you use a directory path instead of an exact path, you can use the word 'last' in place of the last episode. As seen in the example, this sets the last file of the directory as the last episode. This is especially useful if you add files to that directory frequently.

Show Options:
Death Note will always play 2 episodes in a row, without an opening and ending in between.
JoJo 5 has no options, just a comment. Everything after a '#' in a line will be ignored. Use this to get some structure into your config file for better readability, as seen in this example.
Dr. Stone episodes will be in a randomized order.
Nintendo Trailer and Nintendo Teaser will be excluded from normal playback and will not be automatically put into the queue, as they have a commercial value assigned to them. They will be played between episodes. Their commercial weight is used to determine how many commercials to play. In this example we have a global commercial weight of 13, so there could be 3 Nintendo Teasers and 1 Nintendo Trailer, with a total weight of 12. Or 6 Nintendo Teasers and 0 Nintendo Trailers. Although 2 Nintendo Trailers would be within the global commercial weight, the {COMMERCIAL_MAX_OPTION_SHOW} command limits Nintendo Trailers to appear at most once during the commercials between two episodes.


    {color.BOLD}{color.GREEN}{"Allowed Config lines".upper()}{color.END}

The order of the lines is usually completely irrelevant (only exceptions: two Path Lines with the same ALIAS, and if there are two Global Config Lines. The first one is always picked, see below). {PROGRAM_NAME} looks for special characters to determine a line's role in the config.
Comment like in programming with '#'.
Global Options Line starts with '{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}'.
Suspended queue line starts with '{CONFIG_SUSPEND_QUEUE_LINE_STARTER}', see command '{SUSPEND_QUEUE_COMMAND}' for more info.
Path Lines contain a '='.
Empty lines or lines consisting of only whitespace are ignored.
All other lines are considered Show Lines.


    {color.GREEN}Comments with #{color.END}
No matter which line, everything after the first appearance of a '#' in a line will be cut off, including the '#' itself. A line starting with (whitespaces and then a) # will be ignored completely.


    {color.GREEN}Global Options Line (not required):{color.END}
{color.BOLD}{CONFIG_GLOBAL_OPTIONS_LINE_STARTER} OPTION:VALUE; OPTION:VALUE; ....{color.END}
Set optional global options here that are applied to every show in the config file.
Start the line with '{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}', then list the options, separated by ; (semicolon).
An option conists of the option name, then the option value, separated by : (colon).

Type '{color.BOLD}{OPTIONS_HELP_COMMAND}{color.END}' to see all global and local options available.


    {color.GREEN}Show Lines - Necessary Arguments:{color.END}
{color.BOLD}ALIAS; FIRST; LAST; CURRENT [; OPTIONS]{color.END}
ALIAS is a nickname you give your show. It has to be the same as the ALIAS in the corresponding Path Line (see below).
FIRST and LAST are integers specifying the range of episodes that are to be played by {PROGRAM_NAME}.
CURRENT is an integer indicating the episode to play when this show line is picked for playback. When using a directory path line instead of an exact path line (see below), you can use 'last' as a special keyword for the last file in the directory.

A show line has to consist of these first 4 arguments at least, each separated by a single ; (semicolon). OPTIONS are again separated from the necessary arguments by a ; and are optional and explained below.

Every line in your config is considered a Show Line if it doesn't contain a '=' (equals sign) or the global options line specifier '{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}'.

An incorrect Show Line can be picked up for playback and will result in an Error if it is either incorrect in its necessary arguments, or has no correct corresponding Path Line. In this case, {PROGRAM_NAME} will skip this show and try another. Be sure to check the terminal for errors or you might miss out on a show without realizing.

Leading and trailing whitespaces (Spaces, Tabs etc) are ignored, but of course shouldn't randomly appear inside the arguments: '17' is a valid integer, but '1 7' isn't. ALIAS 'DeathNote' will not be interpreted as the same as 'Death Note' because of the added whitespace. But multiple spaces and tabs are reduces to a single space, so 'Death   \tNote' (3 spaces and a tab) is the same as 'Death Note' (1 space). As a consequence, the paths on your local computer can only have a single space as whitespaces in a row.


    {color.GREEN}Show Lines - Optional Options (not required):{color.END}
{color.BOLD}OPTION:VALUE; OPTION:VALUE; ....{color.END}
All options, that are not specified here, will default to your global options or the default options of {PROGRAM_NAME}.
Just like the necessary arguments in a show line, each option has to be separated by ; (semicolon) and make sure to have a semicolon between the last necessary Show Line argument CURRENT and the first option.
An option conists of the option name, then the option value, separated by : (colon).

Type '{color.BOLD}{OPTIONS_HELP_COMMAND}{color.END}' to see all global and local options available.


    {color.GREEN}Path Lines - connect an ALIAS to a real path on your system:{color.END}
{color.BOLD}ALIAS = PATH_UP_TO_EPISODE_NUMBER ; PATH_AFTER_EPISODE_NUMBER{color.END}
or
{color.BOLD}ALIAS = PATH_TO_DIRECTORY{color.END}
ALIAS is supposed to be an ALIAS that exists in a Show Line. When a Show Line is picked for playback, {PROGRAM_NAME} will try to find its corresponding Path Line. It will pick the first in the list that has the same ALIAS, so any Path Lines with duplicate ALIASes will be ignored after the first one.
You can either specify the exact path to the episodes with just the episode number missing or you can specify the path to the directory where the files are located.

Exact Path:
PATH_UP_TO_EPISODE_NUMBER is the full path of the show's files on your system up to the episode number.
PATH_AFTER_EPISODE_NUMBER is the rest of the path after the episode number, including the file type, like '.mp3'.

Directory Path:
PATH_TO_DIRECTORY is the path including the directory where the files are located in. It shouldn't matter if there is a slash at the end or not.

{color.BOLD}---*** End of Config Help ***---{color.END}"""



def format_options_help_specifiers(specifier: str, value_repr: str, value_extra: str=None) -> str:

    # build string to return
    if value_repr == "int":
        pure_string = f'{specifier}:INT'
    elif value_repr == "bool":
        pure_string = f'{specifier}:BOOL'
    else:
        pure_string = f'{specifier}:{value_repr}'

    if value_extra != None:
        pure_string = f'{pure_string}.{value_extra}'

    # add formatting to string:
    return f' {color.BOLD}{pure_string}{color.END}'


MESSAGE_OPTIONS_HELP = f"""{color.BOLD}---*** Options Help ***---{color.END}

    {color.BOLD}{color.YELLOW}About options{color.END}
Options can appear in a show line after the necessary show line arguments (see config help, type '{CONFIG_HELP_COMMAND}') and in a single global options line. Depending on show or global options, there are different options, sometimes with same or similar names but different effects. See this list for all options.

Default values for all options are usually False or 0, {VOLUME_OPTION} and {SPEED_OPTION} default values are 100, video, audio and subtitle track defaults are whatever the player picks, which is ususally track 1.


    {color.BOLD}{color.YELLOW}How to read the options list{color.END}
Each option consists of a specifier and a value. Options are separated by '{CONFIG_FIELD_SEPARATOR}' while a specifier is separated by its value by ':', for example
{UNIQUE_SHOWS_OPTION_GLOBAL}:True; {VOLUME_OPTION}:115

Almost all options take a positive integer (like 0, 17, 130) or a True/False as value. Some few options also take extra values, which have to be separated with '.' from the main value, like 'opening:True.mp3'. The video, audio and subtitle track options also take negative integers.

In the following list INT is a placeholder for an integer and BOOL a placeholder for True or False.


    {color.BOLD}{color.YELLOW}About formatting{color.END}
- All option specifiers and values are case-insensitive, so you can write capital letters wherever you want.
- All whitespaces are ignored, as long as they don't appear within specifiers or values, which would separate them.
- Instead of True you can write any of these: {', '.join(CONFIG_OPTIONS_YES_OPTIONS)}
- Instead of False you can write any of these: {', '.join(CONFIG_OPTIONS_NO_OPTIONS)}
- All of these options are equal for {PROGRAM_NAME}:
  opening:True; random:False
  OPENing: y; rANDOM: NO
     opening  : On  ; random : off
  OPENING:t; RANDOM:f


    {color.BOLD}{color.YELLOW}About overriding and duplicate options{color.END}
Most options exist with the exact same specifier for global and show options. In this case, the show option overrides the global option in most cases, especially if not further specified in the options list below.


    {color.BOLD}{color.GREEN}Global Options{color.END}
{format_options_help_specifiers(OPENING_OPTION, "bool", "FILETYPE")}
    Play opening before episode.
    - If show's exact path is given in path line, then the opening file to be played should have the same name like the episode, but replace the episode number with 'op', like Death-Note-op-rip.mp3 instead of Death-Note-7-rip.mp3
    - If not show's exact path, but only directory given in path line, then the opening file to be played should be named op.mp3 or op.avi or any file type, the first found one will be picked.
    - FILETYPE is optional and can be something like 'mp3' or 'avi' to play an opening with a filetype different from the episode files.
    - You can have multiple opening files to simulate different show seasons. If you want to play a different opening from episode 23 on, replace 'op' with 'op23' in the filename. This file will then be played for all episodes from episode 23, as long as there is no higher opening file that is still equal or lower than your current episode.
{format_options_help_specifiers(ENDING_OPTION, "bool", "FILETYPE")}
    Play ending after episode.
    - Same explanations as with opening, but replace 'op' with 'ed'.
{format_options_help_specifiers(STATUS_OPTION, "bool")}
    Display full status on every playback start.
{format_options_help_specifiers(PAUSE_OPTION, "bool")}
    Pause player on every playback start.
{format_options_help_specifiers(VOLUME_OPTION, "int")}
    INT {VOLUME_LIMIT_LOWER}-{VOLUME_LIMIT_UPPER}, volume multiplier in percent.
    - Is multiplied with the show volume option and the session volume option for the total playback volume.
{format_options_help_specifiers(JUMP_OPTION, "int")}
    Jump to INT seconds on every playback start, gets overridden by show jump option.
{format_options_help_specifiers(SPEED_OPTION, "int")}
    INT {SPEED_LIMIT_LOWER}-{SPEED_LIMIT_UPPER}, playback speed for every played file, gets overridden by show speed option.
{format_options_help_specifiers(MUTE_OPTION, "bool")}
    Mute player of first playback when starting {PROGRAM_NAME}, no other playback will be muted by this option.
{format_options_help_specifiers(FULLSCREEN_OPTION, "bool")}
    Fullscreen player, no effect if no video track available or currently chosen.
{format_options_help_specifiers(QUEUE_SHOW_DISPLAY_OPTION, "bool")}
    Display queue on episode playback start.
{format_options_help_specifiers(SUSPEND_OPTION, "bool")}
    If {SUSPEND_OPTION} is activated, then exiting {PROGRAM_NAME} using '{EXIT_COMMAND}' is the same as using '{SUSPEND_COMMAND}'. Global option '{SUSPEND_OPTION}' is compatible with global option'{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}', but using '{SUSPEND_OPTION}' as a show option is not compatible with option '{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}'.
{format_options_help_specifiers(SUSPEND_QUEUE_OPTION, "bool")}
    If {SUSPEND_QUEUE_COMMAND} is activated, then exiting {PROGRAM_NAME} using '{EXIT_COMMAND}' is the same as using '{SUSPEND_QUEUE_COMMAND}'. Overrides option '{SUSPEND_OPTION}'. Global option '{SUSPEND_QUEUE_OPTION}' is compatible with global option'{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}', but using '{SUSPEND_QUEUE_OPTION}' as a show option is not compatible with option '{SHUTDOWN_OPTION_GLOBAL}' and command '{SHUTDOWN_COMMAND}'.
{format_options_help_specifiers(NO_RERUN_OPTION, "bool")}
    Disable a show line in config file if last episode was played by adding a '#' at the beginning. This makes the show line a comment, which is completely ignored by {PROGRAM_NAME} afterwards. Current episode in config will still be set to first episode. To enable a show line again, just delete the '#' at the beginning of the show line in the config file. This has no effect on shows with option '{RANDOM_OPTION_SHOW}'.
{format_options_help_specifiers(MONITOR_OPTION, "int")}
    Print full player status every INT seconds.
{format_options_help_specifiers(MONITOR_PLAYER_JUMPS_OPTION, "bool")}
    Print full player status if the playback length or current playback position changed more than it should with normal playback. This also triggers when you intentionally change the playback position and was mainly used for analysing the debug player jumps functionality.
{format_options_help_specifiers(DEBUG_PLAYER_JUMPS_OPTION, "bool")}
    Experimental auto restarter, triggers on drastic encountered player length or position changes, then restarts the current playback and jumps to 1 second after the change was registered.

{format_options_help_specifiers(PROBABILITY_OPTION_GLOBAL, "int")}
    INT {PROBABILITY_LIMIT_LOWER}-{PROBABILITY_LIMIT_UPPER}, sets default probability for all shows that don't use the probability option themselves.
{format_options_help_specifiers(COMMERCIALS_PLAY_OPTION_GLOBAL, "int")}
    If set, then show option '{COMMERCIAL_WEIGHT_OPTION_SHOW}' is ignored, so commercials will be played like normal shows.
    - Usually no show with commercial option will be randomly picked for a show, only if user intentionally gets it with commands '{PLAY_SHOW_COMMAND}' or '{QUEUE_SHOW_COMMAND}'.
{format_options_help_specifiers(COMMERCIAL_WEIGHT_OPTION_GLOBAL, "int")}
    Sets the global commercial weight to INT.
    - If global commercial weight is higher than 0, then commercials will be played between episodes, if any are set. A simple search and fill algorithm will pick shows that are marked as commercials until the global commercial weight is full and then play them.
{format_options_help_specifiers(SHUTDOWN_OPTION_GLOBAL, "int")}
    Activates automatic shutdown with INT minutes, turning off {PROGRAM_NAME} in INT minutes. Can be manually disabled with '{SHUTDOWN_COMMAND} 0', behaves like the '{SHUTDOWN_COMMAND}' command.
{format_options_help_specifiers(LAST_EPISODE_OPTION_GLOBAL, "int")}
    Turn off {PROGRAM_NAME} after INT finished episodes.
{format_options_help_specifiers(UNIQUE_SHOWS_OPTION_GLOBAL, "bool")}
    Will avoid playing shows more than once during a session. Once every show was finished once, every show can be played again. Gets overridden by user choices like '{PLAY_SHOW_COMMAND}' and '{QUEUE_SHOW_COMMAND}'.
{format_options_help_specifiers(HARD_UNIQUE_SHOWS_OPTION_GLOBAL, "bool")}
    Will prevent playing shows more than once during a session. End {PROGRAM_NAME} if every show finished once. Queue will not be automatically filled anymore by shows that have been finished before. Gets overridden by user choices like '{PLAY_SHOW_COMMAND}' and '{QUEUE_SHOW_COMMAND}'.


    {color.BOLD}{color.GREEN}Show Options{color.END}
{format_options_help_specifiers(OPENING_OPTION, "bool", "FILETYPE")}
    Same as global option, but only for this show.
{format_options_help_specifiers(ENDING_OPTION, "bool", "FILETYPE")}
    Same as global option, but only for this show.
{format_options_help_specifiers(STATUS_OPTION, "bool")}
    Same as global option, but only for this show.
{format_options_help_specifiers(PAUSE_OPTION, "bool")}
    Same as global option, but only for this show's episode.
{format_options_help_specifiers(VOLUME_OPTION, "int")}
    INT {VOLUME_LIMIT_LOWER}-{VOLUME_LIMIT_UPPER}, volume multiplier in percent.
    - Is multiplied with the global volume option and the session volume option for the total playback volume.
{format_options_help_specifiers(JUMP_OPTION, "int")}
    Same as global option, but only for this show.
{format_options_help_specifiers(SPEED_OPTION, "int")}
    Same as global option, but only for this show.
{format_options_help_specifiers(MUTE_OPTION, "bool")}
    Mute player when this show's episode starts.
{format_options_help_specifiers(FULLSCREEN_OPTION, "bool")}
    Same as global option, but only for this show.
{format_options_help_specifiers(QUEUE_SHOW_DISPLAY_OPTION, "bool")}
    Display queue on this show's episode playback start.
{format_options_help_specifiers(SUSPEND_OPTION, "bool")}
    Same as global option, but only for this show.
{format_options_help_specifiers(SUSPEND_QUEUE_OPTION, "bool")}
    Same as global option, but only for this show.
{format_options_help_specifiers(NO_RERUN_OPTION, "bool")}
    Same as global option, but only for this show.
{format_options_help_specifiers(MONITOR_OPTION, "int")}
    Same as global option, but only for this show.
{format_options_help_specifiers(MONITOR_PLAYER_JUMPS_OPTION, "bool")}
    Same as global option, but only for this show.
{format_options_help_specifiers(DEBUG_PLAYER_JUMPS_OPTION, "bool")}
    Same as global option, but only for this show.

{format_options_help_specifiers(RANDOM_OPTION_SHOW, "bool")}
    Randomizes which episode to play.
{format_options_help_specifiers(MULTIPLE_EPISODES_OPTION_SHOW, "int")}
    Play INT episodes of this show in a row, skipping endings, commercials and openings in between.
    - Ignored if INT is 0 or 1.
{format_options_help_specifiers(PROBABILITY_OPTION_SHOW, "int")}
    INT {PROBABILITY_LIMIT_LOWER}-{PROBABILITY_LIMIT_UPPER}, sets probability for this show.
    - When a show is randomly picked for the queue, then the chance for it to be added to the queue is this probability, if set. Else default probability is 100 or set by global probability option.
{format_options_help_specifiers(COMMERCIAL_WEIGHT_OPTION_SHOW, "int")}
    Sets show's commercial weight.
    - This will classify this show as a commercial and exclude it from automatic addition to queue.
    - This commercial can be picked for commercial playback between episodes, if the global commercial weight is at least as this commercial's weight.
{format_options_help_specifiers(COMMERCIAL_MAX_OPTION_SHOW, "int")}
    Maximum amount of times this show is played as a commercial between episodes.
    - No effect if no commercial weight set on this show.
{format_options_help_specifiers(COMMERCIAL_MIN_OPTION_SHOW, "int")}
    Minimum amount of times this show is played as a commercial between episodes by giving it priority at being picked until minimum amount is satisfied.
    - Does not override the global commercial weight.
    - No effect if no commercial weight set on this show.
{format_options_help_specifiers(VIDEO_TRACK_OPTION_SHOW, "int")}
    Sets video track for this show's episode.
{format_options_help_specifiers(AUDIO_TRACK_OPTION_SHOW, "int")}
    Sets audio track for this show's episode.
{format_options_help_specifiers(SUBTITLE_TRACK_OPTION_SHOW, "int")}
    Sets subtitle track for this show's episode.

{color.BOLD}---*** End of Options Help ***---{color.END}"""


MESSAGE_REPORT_BUGS = f'Please report bugs to {color.BOLD}{color.GREEN}alex.mai@posteo.net{color.END} or at {color.BOLD}{color.YELLOW}gitlab.com/CptMaister/ranimetv{color.END}\n'



class OptionsSession:
    """Class for an object carrying options for this session.
    """

    volume_value = None
    speed_value = None
    mute_bool = None
    monitor_value = None

    def __init__(self):
        self.volume_value = None    # multiplied with episode and global volume
        self.speed_value = None # processed by hierarchy: sesh, ep, og
        self.mute_bool = None   # processed by hierarchy: sesh, ep, og
        self.monitor_value = None   # processed by hierarchy: sesh, ep, og
        self.commercial_weight_value_global = None # overrides global value


class OptionsHandler:
    """Class for option handling, handles all global and show specific options.
    """

    # options variables, listed here for clarity
    # available for global and show options:
    opening_bool = None
    opening_filetype_string = None # if any, then supposed to be string like 'mp3', without a dot
    ending_bool = None
    ending_filetype_string = None
    status_bool = None
    pause_bool = None
    volume_value = None
    jump_value = None
    speed_value = None
    mute_bool = None
    fullscreen_bool = None
    queue_display_bool = None
    suspend_bool = None
    suspend_queue_bool = None
    no_rerun_bool = None
    monitor_value = None
    monitor_jump_bool = None
    debug_player_jumps_bool = None

    # only global options:
    probability_value_global = None
    commercials_play_bool_global = None
    commercial_weight_value_global = None
    shutdown_value_global = None
    last_episodes_value_global = None
    unique_shows_bool_global = None
    hard_unique_shows_bool_global = None

    # only show options:
    random_bool_show = None
    multiple_episodes_value_show = None
    probability_value_show = None
    commercial_weight_value_show = None
    commercial_max_value_show = None
    commercial_min_value_show = None
    video_track_int_show = None
    audio_track_int_show = None
    subtitle_track_int_show = None


    def __init__(self, option_arguments: str, show_options: bool):

        if option_arguments == None:
            return

        if show_options == False:
            option_arguments = option_arguments.replace(CONFIG_GLOBAL_OPTIONS_LINE_STARTER, "", 1)
        option_arguments_split = option_arguments.split(CONFIG_FIELD_SEPARATOR)
        self.print_option_explanation = False

        for arg in option_arguments_split:
            if arg.strip() == "":
                continue

            arg_split = arg.split(":")
            if len(arg_split) != 2:
                self.handle_option_invalid(arg)
                continue

            option = arg_split[0]
            option = whitespace_reduce(option)

            value = arg_split[1]
            value = whitespace_reduce(value)

            # check if None for every option, so the option gets ignored if it is a duplicate
            if option.lower() == OPENING_OPTION and self.opening_bool == None:
                return_helper = self.handle_option_extra_param_filetype(arg, option, value)
                self.opening_bool = return_helper[0]
                self.opening_filetype_string = return_helper[1]

            elif option.lower() == ENDING_OPTION and self.ending_bool == None:
                return_helper = self.handle_option_extra_param_filetype(arg, option, value)
                self.ending_bool = return_helper[0]
                self.ending_filetype_string = return_helper[1]

            elif option.lower() == STATUS_OPTION and self.status_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.status_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.status_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == PAUSE_OPTION and self.pause_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.pause_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.pause_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == VOLUME_OPTION and self.volume_value == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(VOLUME_COMMAND, value)
                    continue
                value_int = min(VOLUME_LIMIT_UPPER, max(value_int, VOLUME_LIMIT_LOWER))
                self.volume_value = value_int

            elif option.lower() == JUMP_OPTION and self.jump_value == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(JUMP_OPTION, value)
                    continue
                value_int = max(value_int, 0)
                if value_int > 600:
                    message = f'{color.WARNING}Warning for {JUMP_COMMAND} option:{color.END} Skipping first ({value_int}) seconds, that\'s more than 10 minutes.'
                    print(message)
                self.jump_value = value_int

            elif option.lower() == SPEED_OPTION and self.speed_value == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(SPEED_OPTION, value)
                    continue
                value_int = min(SPEED_LIMIT_UPPER, max(value_int, SPEED_LIMIT_LOWER))
                self.speed_value = value_int

            elif option.lower() == MUTE_OPTION and self.mute_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.mute_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.mute_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == FULLSCREEN_OPTION and self.fullscreen_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.fullscreen_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.fullscreen_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == QUEUE_SHOW_DISPLAY_OPTION and self.queue_display_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.queue_display_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.queue_display_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == SUSPEND_OPTION and self.suspend_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.suspend_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.suspend_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == SUSPEND_QUEUE_OPTION and self.suspend_queue_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.suspend_queue_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.suspend_queue_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == NO_RERUN_OPTION and self.no_rerun_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.no_rerun_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.no_rerun_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == MONITOR_OPTION and self.monitor_value == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(MONITOR_OPTION, value)
                    continue
                value_int = max(value_int, 0)
                self.monitor_value = value_int

            elif option.lower() == MONITOR_PLAYER_JUMPS_OPTION and self.monitor_jump_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.monitor_jump_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.monitor_jump_bool = False
                else:
                    self.handle_option_invalid(arg)

            elif option.lower() == DEBUG_PLAYER_JUMPS_OPTION and self.debug_player_jumps_bool == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.debug_player_jumps_bool = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.debug_player_jumps_bool = False
                else:
                    self.handle_option_invalid(arg)

            # handle global options

            elif show_options == False and option.lower() == PROBABILITY_OPTION_GLOBAL and self.probability_value_global == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(PROBABILITY_OPTION_GLOBAL, value)
                    continue
                value_int = min(100, max(value_int, 1))
                self.probability_value_global = value_int

            elif show_options == False and option.lower() == COMMERCIALS_PLAY_OPTION_GLOBAL and self.commercials_play_bool_global == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.commercials_play_bool_global = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.commercials_play_bool_global = False
                else:
                    self.handle_option_invalid(arg)

            elif show_options == False and option.lower() == COMMERCIAL_WEIGHT_OPTION_GLOBAL and self.commercial_weight_value_global == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(COMMERCIAL_WEIGHT_OPTION_GLOBAL, value)
                    continue
                value_int = max(value_int, 0)
                self.commercial_weight_value_global = value_int

            elif show_options == False and option.lower() == SHUTDOWN_OPTION_GLOBAL and self.shutdown_value_global == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(SHUTDOWN_OPTION_GLOBAL, value)
                    continue
                value_int = max(value_int, 0)
                self.shutdown_value_global = value_int

            elif show_options == False and option.lower() == LAST_EPISODE_OPTION_GLOBAL and self.last_episodes_value_global == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(LAST_EPISODE_OPTION_GLOBAL, value)
                    continue
                value_int = max(value_int, 0)
                self.last_episodes_value_global = value_int

            elif show_options == False and option.lower() == UNIQUE_SHOWS_OPTION_GLOBAL and self.unique_shows_bool_global == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.unique_shows_bool_global = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.unique_shows_bool_global = False
                else:
                    self.handle_option_invalid(arg)

            elif show_options == False and option.lower() == HARD_UNIQUE_SHOWS_OPTION_GLOBAL and self.hard_unique_shows_bool_global == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.hard_unique_shows_bool_global = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.hard_unique_shows_bool_global = False
                else:
                    self.handle_option_invalid(arg)

            # handle show options

            elif show_options == True and option.lower() == RANDOM_OPTION_SHOW and self.random_bool_show == None:
                if value.lower() in CONFIG_OPTIONS_YES_OPTIONS:
                    self.random_bool_show = True
                elif value.lower() in CONFIG_OPTIONS_NO_OPTIONS:
                    self.random_bool_show = False
                else:
                    self.handle_option_invalid(arg)

            elif show_options == True and option.lower() == MULTIPLE_EPISODES_OPTION_SHOW and self.multiple_episodes_value_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(MULTIPLE_EPISODES_OPTION_SHOW, value)
                    continue
                value_int = min(100, max(value_int, 0))
                self.multiple_episodes_value_show = value_int

            elif show_options == True and option.lower() == PROBABILITY_OPTION_SHOW and self.probability_value_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(PROBABILITY_OPTION_SHOW, value)
                    continue
                value_int = min(100, max(value_int, 0))
                self.probability_value_show = value_int

            elif show_options == True and option.lower() == COMMERCIAL_WEIGHT_OPTION_SHOW and self.commercial_weight_value_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(COMMERCIAL_WEIGHT_OPTION_SHOW, value)
                    continue
                value_int = max(value_int, 0)
                self.commercial_weight_value_show = value_int

            elif show_options == True and option.lower() == COMMERCIAL_MAX_OPTION_SHOW and self.commercial_max_value_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(COMMERCIAL_MAX_OPTION_SHOW, value)
                    continue
                value_int = max(value_int, 0)
                self.commercial_max_value_show = value_int

            elif show_options == True and option.lower() == COMMERCIAL_MIN_OPTION_SHOW and self.commercial_min_value_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(COMMERCIAL_MIN_OPTION_SHOW, value)
                    continue
                value_int = max(value_int, 0)
                self.commercial_min_value_show = value_int

            elif show_options == True and option.lower() == VIDEO_TRACK_OPTION_SHOW and self.video_track_int_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(VIDEO_TRACK_OPTION_SHOW, value)
                    continue
                self.video_track_int_show = value_int

            elif show_options == True and option.lower() == AUDIO_TRACK_OPTION_SHOW and self.audio_track_int_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(AUDIO_TRACK_OPTION_SHOW, value)
                    continue
                self.audio_track_int_show = value_int

            elif show_options == True and option.lower() == SUBTITLE_TRACK_OPTION_SHOW and self.subtitle_track_int_show == None:
                try:
                    value_int = int(value)
                except ValueError:
                    self.handle_option_invalid_integer(SUBTITLE_TRACK_OPTION_SHOW, value)
                    continue
                self.subtitle_track_int_show = value_int

            # no match
            else:
                self.handle_option_invalid(arg)

        if self.print_option_explanation:
            message = f"""Options are separated with '{CONFIG_FIELD_SEPARATOR}' and written as OPTION:BOOLEAN or OPTION:INTEGER. Options in a show's line will overwrite global options (type command '{CONFIG_HELP_COMMAND}' for more information).
            For example:
            {color.BOLD}ALIAS; FIRST; LAST; CURRENT; {OPENING_OPTION}:true; {ENDING_OPTION}:true.mp4; {VOLUME_COMMAND}:85; {SPEED_COMMAND}: 120{color.END}
            to play the opening before every episode of this show (it has the same filetype as the episodes and instead of the episode number it has 'op' in its name), play the ending after every episode ('ed' in its name instead of the episode number, if filetype differs from episodes, specify like here for example for '.mp4' file), start every episode with volume at 85%, speed at 120%.
            Options are not case-sensitive and spaces and tabs can be set freely, as long as they don't separate an option specifier or its value."""
            print(message)


    def handle_option_invalid(self, arg: str):
        message = f'{color.WARNING}Warning in config:{color.END} ({arg.strip()}) invalid option, will be ignored.'
        print(message)
        self.print_option_explanation = True


    def handle_option_invalid_integer(self, option: str, value: str):
        message = f'{color.WARNING}Warning in config:{color.END} invalid value for \'{option}\': ({value}) can\'t be converted to integer number.'
        print(message)
        self.print_option_explanation = True


    def handle_option_extra_param_filetype(self, arg: str, option: str, value: str) -> (bool, str):
        return_bool = None
        return_str = None

        value_filetype_split = value.split(".")
        value_bool = value_filetype_split[0].strip()
        if value_bool.lower() in CONFIG_OPTIONS_YES_OPTIONS:
            return_bool = True
        elif value_bool.lower() in CONFIG_OPTIONS_NO_OPTIONS:
            return_bool = False
        else:
            self.handle_option_invalid(arg)

        # get differing filetype if given
        if return_bool and len(value_filetype_split) > 1:
            return_str = ".".join(value_filetype_split[1:]).strip()

        return (return_bool, return_str)

    #previously used for commercial option, now unused. delete?
    def handle_option_extra_param_int(self, arg: str, option: str, value: str):
        return_bool = None
        return_int = None

        value_filetype_split = value.split(".")
        value_bool = value_filetype_split[0].strip()
        if value_bool.lower() in CONFIG_OPTIONS_YES_OPTIONS:
            return_bool = True
        elif value_bool.lower() in CONFIG_OPTIONS_NO_OPTIONS:
            return_bool = False
        else:
            self.handle_option_invalid(arg)

        # get int (seconds or amount) if given
        if return_bool and len(value_filetype_split) == 2:
            try:
                return_int = int(value_filetype_split[1])
            except ValueError:
                self.handle_option_invalid_integer(option + ":" + value, value_filetype_split[1])
                return -1 # failed, call continue where this is used
        return (return_bool, return_int)


class OptionsGlobal:
    """Object carrying all global options from the global config line.
    """

    # lets parent class handle all options, then sets None on those, that are not global options
    def __init__(self, global_options_line: str):
        options = OptionsHandler(global_options_line, False)

        self.opening_bool = options.opening_bool
        self.opening_filetype_string = options.opening_filetype_string
        self.ending_bool = options.ending_bool
        self.ending_filetype_string = options.ending_filetype_string
        self.status_bool = options.status_bool
        self.pause_bool = options.pause_bool
        self.volume_value = options.volume_value
        self.jump_value = options.jump_value
        self.speed_value = options.speed_value
        self.mute_bool = options.mute_bool
        self.fullscreen_bool = options.fullscreen_bool
        self.queue_display_bool = options.queue_display_bool
        self.suspend_bool = options.suspend_bool
        self.suspend_queue_bool = options.suspend_queue_bool
        self.no_rerun_bool = options.no_rerun_bool
        self.monitor_value = options.monitor_value
        self.monitor_jump_bool = options.monitor_jump_bool
        self.debug_player_jumps_bool = options.debug_player_jumps_bool

        self.probability_value_global = options.probability_value_global
        self.commercials_play_bool_global = options.commercials_play_bool_global
        self.commercial_weight_value_global = options.commercial_weight_value_global
        self.shutdown_value_global = options.shutdown_value_global
        self.last_episodes_value_global = options.last_episodes_value_global
        self.unique_shows_bool_global = options.unique_shows_bool_global
        self.hard_unique_shows_bool_global = options.hard_unique_shows_bool_global



def get_files_list_from_directory(path_to_directory) -> [str]:
    files_list = []
    with os.scandir(path_to_directory) as entries:
        for entry in entries:
            if entry.is_file():
                files_list.append(entry.name)
    return files_list


def get_files_full_paths_list_from_directory(path_to_directory) -> [str]:
    files_list = []
    with os.scandir(path_to_directory) as entries:
        for entry in entries:
            if entry.is_file():
                files_list.append(os.path.join(path_to_directory, entry.name))
    return files_list


def get_files_amount_from_directory(path_to_directory) -> int:
    counter = 0
    try:
        with os.scandir(path_to_directory) as entries:
            for entry in entries:
                if entry.is_file():
                    counter += 1
        return counter
    except:
        return -1


class ShowEpisode:
    """Object carrying all config information for one show's episode.

    When initiating give it one line from the config starting with the alias of the show to be played and the line connecting the alias to the os.path.
    """

    def __init__(self, show_line: str, path_line: str, print_messages=True, no_options=False):
        show_line_split = show_line.split(CONFIG_FIELD_SEPARATOR)
        self.config_alias = show_line_split[CONFIG_POSITION_SHOW_ALIAS].strip()

        message_error_hint_config_help = f"Type '{color.BOLD}{CONFIG_HELP_COMMAND}{color.END}' to see instructions on writing a proper config file. Fix problems in config, else {self.config_alias} can't be played."

        if len(show_line_split) < 4:
            message = f"""{color.ERROR}Error in config for ({self.config_alias}):{color.END} not enough arguments.
            Faulty line given:
            ({show_line})
            But a Show Line has to start with ALIAS; FIRST_EPISODE; LAST_EPISODE; CURRENT_EPISODE, for example:
            {color.BOLD}Death Note; 07; 20; 13{color.END}
            if you only want to play Death Note episodes 7 to 20 and the next one to be played is episode 13. The leading zero in '07' is necessary if your path line is using exact paths (instead of a whole directory) and your files' episode numbers have leading zeros, for example when your file (including path) is
            /home/user/Videos/Favorite Anime/DeathNote/Death-Note-07-rip.mp3
            Options can go after these first 4 arguments.
            If you use a directory (instead of exact paths) in the path line, then you can also write 'last' in place of the LAST_EPISODE to automatically get the amount of files in your directory.
            {message_error_hint_config_help}
            """
            if print_messages:
                print(message)
            self.is_valid = False
            sleep(5)
            return

        # get path connected to the alias
        if path_line == None or path_line == "":
            message = f"""{color.ERROR}Error in config for {self.config_alias}:{color.END} no path line found.
            There has to be a line where the show's alias name is connected to its path and name as it is saved on your system. For example if you have a line
            Death Note; 01; 20; 07
            you will also need a line like
            Death Note = /home/user/Videos/Favorite Anime/DeathNote/Death-Note-; -rip.mp3
            with episode 7's file called Death-note-07-rip.mp3 in this case. {message_error_hint_config_help}
            """
            if print_messages:
                print(message)
            self.is_valid = False
            return
        path_line_split = path_line.split("=")

        if len(path_line_split) > 2:
            message = f"""{color.ERROR}Error in config for {self.config_alias}:{color.END} path line contains too many '='. {message_error_hint_config_help}
            """
            if print_messages:
                print(message)
            self.is_valid = False
            return

        path_arguments = path_line_split[1].split(CONFIG_FIELD_SEPARATOR)

        # check if path line has exact path or directory and handle
        if len(path_arguments) == 2:
            self.exact_path = True
            self.show_path_before_number = path_arguments[0].strip().strip("'").strip("\"")
            self.show_path_after_number_with_file_ending = path_arguments[1].strip().strip("'").strip("\"")
        elif len(path_arguments) == 1:
            self.exact_path = False # only directory passed in path line, not exact episodes' path with only the integer missing
            self.dir_files_full_paths_list = None
            self.directory_path = path_arguments[0].strip().strip("'").strip("\"")
        else:
            message = f"""{color.ERROR}Error in config for {self.config_alias}:{color.END} path line contains too many arguments after '='. {message_error_hint_config_help}
            """
            if print_messages:
                print(message)


        # check for necessary arguments first, last, current
        try:
            self.config_first_ep = int(show_line_split[CONFIG_POSITION_SHOW_FIRST_EP].strip())

            if self.exact_path == False and show_line_split[CONFIG_POSITION_SHOW_LAST_EP].strip().lower() == "last":
                self.config_last_ep = get_files_amount_from_directory(self.directory_path)
                if self.config_last_ep == -1:
                    message = f"{color.ERROR}Error in config for {self.config_alias}:{color.END} no files found. Check the path line in your config file again.\nTried looking here: {self.directory_path}\n{message_error_hint_config_help}\n"
                    if print_messages:
                        print(message)
                    self.is_valid = False
                    return
            else:
                self.config_last_ep = int(show_line_split[CONFIG_POSITION_SHOW_LAST_EP].strip())

            if LEFT_OFF_AT_SEPARATOR in show_line_split[CONFIG_POSITION_SHOW_CURRENT_EP]: # this episode was playing last time RAnimeTV was used and finished with 'suspend' option or command
                current_ep_split = show_line_split[CONFIG_POSITION_SHOW_CURRENT_EP].strip().split(LEFT_OFF_AT_SEPARATOR)
                self.config_current_ep = int(current_ep_split[0])
                self.suspend_value = int(current_ep_split[1])
            else:
                self.config_current_ep = int(show_line_split[CONFIG_POSITION_SHOW_CURRENT_EP].strip())
                self.suspend_value = None
        except:
            message = f"{color.ERROR}Error in config for {self.config_alias}:{color.END} ({show_line_split[CONFIG_POSITION_SHOW_FIRST_EP].strip()}), ({show_line_split[CONFIG_POSITION_SHOW_LAST_EP].strip()}) or ({show_line_split[CONFIG_POSITION_SHOW_CURRENT_EP].strip()}) is not a valid integer expression.\n{message_error_hint_config_help}\n"
            if print_messages:
                print(message)
            self.is_valid = False
            return

        # ensure that first_ep <= last_ep
        if self.config_first_ep > self.config_last_ep:
            message = f'{color.ERROR}Error in config for {self.config_alias}:{color.END} first_ep is {self.first_ep} and is not allowed to be bigger than last_ep {self.last_ep}.'
            if print_messages:
                print(message)
            self.is_valid = False

        # handle leading zeros
        if self.exact_path:
            self.config_first_ep_digits = len(show_line_split[CONFIG_POSITION_SHOW_FIRST_EP].strip()) # for handling leading zeros
        else:
            self.config_first_ep_digits = None

        # get options
        if not no_options:
            option_arguments = show_line_split[4:]
            self.set_show_options(f'{CONFIG_FIELD_SEPARATOR} '.join(option_arguments))

        # some last stuff:
        if self.exact_path:
            self.directory_path = os.path.dirname(self.generate_full_episode_current_path())

        self.is_valid = True

        self.dir_files_full_paths_list = None
        self.episodes_full_paths_list = None


    def __str__(self):
        if self.exact_path:
            return f'{self.config_alias} episode {self.config_current_ep}'
        else:
            self.generate_dir_files_full_paths_list_if_empty()
            file_full_path = self.dir_files_full_paths_list[self.config_current_ep - 1]
            file_name = os.path.basename(file_full_path)
            if "." in file_name:
                file_name = file_name[:file_name.rfind(".")]
            return file_name


    def set_show_options(self, option_arguments: str):
        options = OptionsHandler(option_arguments, True)

        self.opening_bool = options.opening_bool
        self.opening_filetype_string = options.opening_filetype_string
        self.ending_bool = options.ending_bool
        self.ending_filetype_string = options.ending_filetype_string
        self.status_bool = options.status_bool
        self.pause_bool = options.pause_bool
        self.volume_value = options.volume_value
        self.jump_value = options.jump_value
        self.speed_value = options.speed_value
        self.mute_bool = options.mute_bool
        self.fullscreen_bool = options.fullscreen_bool
        self.queue_display_bool = options.queue_display_bool
        self.suspend_bool = options.suspend_bool
        self.suspend_queue_bool = options.suspend_queue_bool
        self.no_rerun_bool = options.no_rerun_bool
        self.monitor_value = options.monitor_value
        self.monitor_jump_bool = options.monitor_jump_bool
        self.debug_player_jumps_bool = options.debug_player_jumps_bool

        self.random_bool_show = options.random_bool_show
        self.multiple_episodes_value_show = options.multiple_episodes_value_show
        self.probability_value_show = options.probability_value_show
        self.commercial_weight_value_show = options.commercial_weight_value_show
        self.commercial_max_value_show = options.commercial_max_value_show
        self.commercial_min_value_show = options.commercial_min_value_show
        self.video_track_int_show = options.video_track_int_show
        self.audio_track_int_show = options.audio_track_int_show
        self.subtitle_track_int_show = options.subtitle_track_int_show

        if self.random_bool_show:
            self.randomize_current_ep()


    def randomize_current_ep(self):
        if self.suspend_value == None: #don't randomize if resuming from previous 'suspend' so RAnimeTV will actually play the same episode as last time
            self.config_current_ep = randint(self.config_first_ep, self.config_last_ep)


    def generate_full_episode_current_path(self) -> str:
        if self.exact_path:
            return f'{self.show_path_before_number}{self.generate_episode_number_str(self.config_current_ep)}{self.show_path_after_number_with_file_ending}'
        else:
            self.generate_episode_full_paths_list_if_empty()
            if (self.directory_path in self.episodes_full_paths_list[0]): # 0 or any existing index is fine
                return f'{self.episodes_full_paths_list[self.config_current_ep - 1]}' # -1 as the list is 0-indexed
            else:
                return f'{os.path.join(self.directory_path, self.episodes_full_paths_list[self.config_current_ep - 1])}' # -1 as the list is 0-indexed


    def generate_episode_current_path_base(self) -> str:
        full_path = self.generate_full_episode_current_path()
        return unquote(os.path.basename(os.path.normpath(full_path)))


    def generate_full_episode_x_path(self, episode: int) -> str:
        if self.exact_path:
            return f'{self.show_path_before_number}{self.generate_episode_number_str(episode)}{self.show_path_after_number_with_file_ending}'
        else:
            self.generate_episode_full_paths_list_if_empty()
            return f'{os.path.join(self.directory_path, self.episodes_full_paths_list[episode - 1])}'


    def generate_full_op_ed_path(self, specifier, filetype_global: str=None) -> str:
        if specifier != "opening" and specifier != "ending":
            raise Exception("Error in code, generate_full_op_ed_path's specifier has to be 'opening' or 'ending'.")

        if specifier == "opening":
            spec = "op"
            filetype_show = self.opening_filetype_string
        else:
            spec = "ed"
            filetype_show = self.ending_filetype_string

        full_path = ""

        if self.exact_path:
            # filter to get all files starting with '...op' or '...ed'
            path_starter = f'{self.show_path_before_number}{spec}'
            path_after_int = self.show_path_after_number_with_file_ending

        else: # not exact path in path line, only directory
            # filter to get all files starting with 'op or 'ed'
            path_starter = f'{os.path.join(self.directory_path, spec)}'
            path_after_int = "."

        # now pick most fitting file, realizes functionality of different openings for different episode numbers
        self.generate_dir_files_full_paths_list_if_empty()
        full_path = self.helper_pick_best_op_ed_file(path_starter, path_after_int)

        # handle alternate filetype ending
        if full_path != "" and (filetype_show or filetype_global):
            if filetype_show:
                filetype_string = filetype_show
            else:
                filetype_string = filetype_global
            index_last_dot = full_path.rfind(".")
            full_path = f'{full_path[:index_last_dot]}.{filetype_string}'

        return full_path


    def helper_pick_best_op_ed_file(self, path_starter: str, path_after_int: str) -> str:
        """Returns full file path to be played.

        Examples for parameters:
        path_starter    home/PATH/TO/FILE/death-note-op
        path_after_int  -rip.mp3
        """

        filtered_files = []
        for f in self.dir_files_full_paths_list:
            if f.startswith(path_starter):
                filtered_files.append(f)

        file_tuple = ("", 0) # hold currently best fitting file and its int from which episode on in it supposed to be played, 0 if no int (standard op/ed file), best fitting int is highest but lower than self.config_current_ep
        for f in filtered_files:
            if f.startswith(path_starter + path_after_int) and file_tuple[1] == 0:
                file_tuple = (f, 0)
            else: # check if file is PATH + INT + '.FILETYPE'
                if f[len(path_starter):f.find(".", len(path_starter))] != "": #f[2:f.find(".")] removes the first 2 characters and everything from the first '.'
                    try:
                        episode_start_int = int(f[len(path_starter):f.find(path_after_int, len(path_starter))])
                        if file_tuple[1] < episode_start_int <= self.config_current_ep:
                            file_tuple = (f, episode_start_int)
                    except ValueError:
                        pass

        return file_tuple[0]


    def generate_dir_files_full_paths_list_if_empty(self):
        if self.dir_files_full_paths_list == None:
            self.dir_files_full_paths_list = sorted(get_files_full_paths_list_from_directory(self.directory_path))


    def generate_episode_full_paths_list_if_empty(self):
        self.generate_dir_files_full_paths_list_if_empty()

        filtered_episodes_list = []

        if self.exact_path:
            for f in self.dir_files_full_paths_list:
                if f.startswith(self.show_path_before_number) and not (f.startswith(self.show_path_before_number + "op") or f.startswith(self.show_path_before_number + "ed")):
                    filtered_episodes_list.append(f)
        else:
            for f in self.dir_files_full_paths_list:
                if not "op" in f and not "ed" in f:
                    filtered_episodes_list.append(f)
                else:
                    if "op" in f:
                        check_op_ed = "op"
                    else:
                        check_op_ed = "ed"
                    try:
                        path_starter = os.path.join(self.directory_path, check_op_ed)
                        episode_start_int = int(f[len(path_starter):f.find(".", len(path_starter))])
                        # if no ValueError thrown, then the file is an opening/ending file like 'home/PATH/TO/EPISODES/op123.mp4'
                    except ValueError:
                        filtered_episodes_list.append(f)

        self.episodes_full_paths_list = filtered_episodes_list



    def generate_config_show_line_arguments(self, next_episode: bool=False, left_off_at: int=None) -> str:
        """Generates the part of a config line of a show line containing the necessary arguments, no options.

        If bool next_episode is True, then in place of the current episode parameter will be the next episode. It will be the same as the first episode, if current episode is last episode (i.e. it will start from the first again).
        If left_off_at is given, it will be appended to the current episode param, as seen in below code. This is the seconds at which to start the episode the next time it is played, in seconds, and used to continue where one left off after stopping. Additionally, when starting RAnimeTV, it will search for this in all show lines and play this episode instead of randomly picking one.
        """

        # add necessary arguments to list: alias, first, last, current episode
        show_arguments = []

        show_arguments.append(self.config_alias)

        if self.exact_path:
            first_ep_leading_zeros = self.generate_episode_number_str(self.config_first_ep)
            show_arguments.append(f'{first_ep_leading_zeros}')

            last_ep_leading_zeros = self.generate_episode_number_str(self.config_last_ep)
            show_arguments.append(f'{last_ep_leading_zeros}')

        else:
            show_arguments.append(f'{self.config_first_ep}')
            show_arguments.append(f'{self.config_last_ep}')


        if next_episode:
            #if self.config_current_ep >= self.config_last_ep:
            #    current_ep = self.config_first_ep
            #else:
            #    current_ep = self.config_current_ep + 1
            current_ep = self.get_next_episode_str(self.config_first_ep, self.config_last_ep, self.config_current_ep)
        else:
            current_ep = self.generate_episode_number_str(current_ep)

        if left_off_at != None:
            current_ep = f'{current_ep}{LEFT_OFF_AT_SEPARATOR}{left_off_at}'

        show_arguments.append(f'{current_ep}')

        # create str joined by config separator, add tab for better readability
        return (f'{CONFIG_FIELD_SEPARATOR}\t').join(show_arguments)


    def get_next_episode_str(self, first, last, current, width: int=None) -> str:
        next_ep = self.get_next_episode(first, last, current)

        if self.exact_path:
            if width == None:
                width = self.config_first_ep_digits
            return self.generate_episode_number_str(next_ep, width)

        else:
            return str(next_ep)


    def get_next_episode(self, first, last, current) -> int:
        if current >= last:
            next_ep = first
        else:
            next_ep = current + 1
        return next_ep


    def generate_instance_next_episode(self):
        return_instance = deepcopy(self)
        return_instance.config_current_ep = self.get_next_episode(self.config_first_ep, self.config_last_ep, self.config_current_ep)
        return return_instance


    def generate_config_show_line_options(self) -> str:
        show_options = generate_config_show_global_line_options_list(self)

        return (f'{CONFIG_FIELD_SEPARATOR} ').join(show_options)


    def generate_config_show_line_full(self, next_episode: bool=False, left_off_at: int=None):
        """Returns full config show line, with line break.
        """
        show_arguments = self.generate_config_show_line_arguments(next_episode, left_off_at)
        show_options = self.generate_config_show_line_options()

        # join necessary arguments str with options str
        if show_options != '':
            return (f'{CONFIG_FIELD_SEPARATOR} ').join([show_arguments, show_options]) + "\n"
        else:
            return show_arguments + "\n"


    def generate_episode_number_str(self, number: int, width: int=None) -> str:
        """adds as many leading 0 as necessary to be at least as long as first_ep in config.

        e.g. if first_ep is '01' in config, then it will make '07' out of 7 and '13' out of 13, '123' out of 123 etc
        """
        if width == None:
            width = self.config_first_ep_digits
        return ("{0:0=" + str(width) + "d}").format(number)



def generate_config_show_global_line_options_list(option_object) -> [str]:
    # add optional options to options list
    options_list = []

    if option_object.opening_bool != None:
        if option_object.opening_filetype_string:
            options_list.append(f'{OPENING_OPTION}:{option_object.opening_bool}.{option_object.opening_filetype_string}')
        else:
            options_list.append(f'{OPENING_OPTION}:{option_object.opening_bool}')
    if option_object.ending_bool != None:
        if option_object.ending_filetype_string:
            options_list.append(f'{ENDING_OPTION}:{option_object.ending_bool}.{option_object.ending_filetype_string}')
        else:
            options_list.append(f'{ENDING_OPTION}:{option_object.ending_bool}')
    options_list.append(f'{STATUS_OPTION}:{option_object.status_bool}') if option_object.status_bool != None else None
    options_list.append(f'{PAUSE_OPTION}:{option_object.pause_bool}') if option_object.pause_bool != None else None
    options_list.append(f'{VOLUME_OPTION}:{option_object.volume_value}') if option_object.volume_value != None else None
    options_list.append(f'{JUMP_OPTION}:{option_object.jump_value}') if option_object.jump_value != None else None
    options_list.append(f'{SPEED_OPTION}:{option_object.speed_value}') if option_object.speed_value != None else None
    options_list.append(f'{MUTE_OPTION}:{option_object.mute_bool}') if option_object.mute_bool != None else None
    options_list.append(f'{FULLSCREEN_OPTION}:{option_object.fullscreen_bool}') if option_object.fullscreen_bool != None else None
    options_list.append(f'{QUEUE_SHOW_DISPLAY_OPTION}:{option_object.queue_display_bool}') if option_object.queue_display_bool != None else None
    options_list.append(f'{SUSPEND_OPTION}:{option_object.suspend_bool}') if option_object.suspend_bool != None else None
    options_list.append(f'{SUSPEND_QUEUE_OPTION}:{option_object.suspend_queue_bool}') if option_object.suspend_queue_bool != None else None
    options_list.append(f'{NO_RERUN_OPTION}:{option_object.no_rerun_bool}') if option_object.no_rerun_bool != None else None
    options_list.append(f'{MONITOR_OPTION}:{option_object.monitor_value}') if option_object.monitor_value != None else None
    options_list.append(f'{MONITOR_PLAYER_JUMPS_OPTION}:{option_object.monitor_jump_bool}') if option_object.monitor_jump_bool != None else None
    options_list.append(f'{DEBUG_PLAYER_JUMPS_OPTION}:{option_object.debug_player_jumps_bool}') if option_object.debug_player_jumps_bool != None else None

    if isinstance(option_object, OptionsGlobal):
        options_list.append(f'{PROBABILITY_OPTION_GLOBAL}:{option_object.probability_value_global}') if option_object.probability_value_global != None else None
        options_list.append(f'{COMMERCIALS_PLAY_OPTION_GLOBAL}:{option_object.commercials_play_bool_global}') if option_object.commercials_play_bool_global != None else None
        options_list.append(f'{COMMERCIAL_WEIGHT_OPTION_GLOBAL}:{option_object.commercial_weight_value_global}') if option_object.commercial_weight_value_global != None else None
        options_list.append(f'{SHUTDOWN_OPTION_GLOBAL}:{option_object.shutdown_value_global}') if option_object.shutdown_value_global != None else None
        options_list.append(f'{LAST_EPISODE_OPTION_GLOBAL}:{option_object.last_episodes_value_global}') if option_object.last_episodes_value_global != None else None
        options_list.append(f'{UNIQUE_SHOWS_OPTION_GLOBAL}:{option_object.unique_shows_bool_global}') if option_object.unique_shows_bool_global != None else None
        options_list.append(f'{HARD_UNIQUE_SHOWS_OPTION_GLOBAL}:{option_object.hard_unique_shows_bool_global}') if option_object.hard_unique_shows_bool_global != None else None

    if isinstance(option_object, ShowEpisode):
        options_list.append(f'{RANDOM_OPTION_SHOW}:{option_object.random_bool_show}') if option_object.random_bool_show != None else None
        options_list.append(f'{MULTIPLE_EPISODES_OPTION_SHOW}:{option_object.multiple_episodes_value_show}') if option_object.multiple_episodes_value_show != None else None
        options_list.append(f'{PROBABILITY_OPTION_SHOW}:{option_object.probability_value_show}') if option_object.probability_value_show != None else None
        options_list.append(f'{COMMERCIAL_WEIGHT_OPTION_SHOW}:{option_object.commercial_weight_value_show}') if option_object.commercial_weight_value_show != None else None
        options_list.append(f'{COMMERCIAL_MAX_OPTION_SHOW}:{option_object.commercial_max_value_show}') if option_object.commercial_max_value_show != None else None
        options_list.append(f'{COMMERCIAL_MIN_OPTION_SHOW}:{option_object.commercial_min_value_show}') if option_object.commercial_min_value_show != None else None
        options_list.append(f'{VIDEO_TRACK_OPTION_SHOW}:{option_object.video_track_int_show}') if option_object.video_track_int_show != None else None
        options_list.append(f'{AUDIO_TRACK_OPTION_SHOW}:{option_object.audio_track_int_show}') if option_object.audio_track_int_show != None else None
        options_list.append(f'{SUBTITLE_TRACK_OPTION_SHOW}:{option_object.subtitle_track_int_show}') if option_object.subtitle_track_int_show != None else None

    return options_list



class ShowsQueue:

    def __init__(self, config_path: str):
        self.config_path = config_path
        self.queue = [] # a queue-like list holding the next few shows as ShowEpisode instances to be played and displayed with the 'queue' command, populated in generate_next_show_episode_instance or by using the queue command. Not actually using real python queues
        self.already_played = [] # strs, needed for global options 'unique' and 'uunique'. Every entry consists of the show's alias and its current episode, like 'Death Note 17'.
        self.started_playbacks = [] # path strs, used for 'history' command

    def populate_automatic(self, og: OptionsGlobal, amount: int=None):
        """Populates list shows_queue with episodes to be played.

        If no amount given, it will be filled up until it holds QUEUE_SIZE_STANDARD many episodes. If an epsode can't be created from config, errors are thrown here.
        """

        if amount == None and len(self.queue) >= QUEUE_SIZE_STANDARD:
            return

        # add first element to queue if left off last time with 'suspend' option or command
        alias_temp = get_config_left_off_at_alias()
        if alias_temp != "":
            self.add_entry_from_aliases([alias_temp])

        #reload previous queue if queue was saved in config using command or option 'ssuspend'
        self.handle_suspend_queue_resume()

        # prevents flooding the terminal with error messages if the config file contains too many incorrect lines
        invalid_shows_in_config_counter = 0
        invalid_shows_in_config_counter_limit = 5

        # filter to exclude show lines that contain the option specifiers contained in the filter list
        filter_options = []
        if not og.commercials_play_bool_global:
            filter_options.append(COMMERCIAL_WEIGHT_OPTION_SHOW)

        # aliases to choose from are filtered if 'unique' global option is set
        if og.hard_unique_shows_bool_global or og.unique_shows_bool_global:
            aliases_to_choose_next_episode_from = get_config_unique_filtered_show_aliases(self, filter_options)
        else:
            aliases_to_choose_next_episode_from = get_config_all_show_aliases(filter_options)

        initial_queue_size = len(self.queue)
        while True:
            if amount == None and len(self.queue) >= QUEUE_SIZE_STANDARD:
                return
            elif amount != None and len(self.queue) >= initial_queue_size + amount:
                return
            elif len(aliases_to_choose_next_episode_from) == 0:
                if amount != None and og.hard_unique_shows_bool_global:
                    message = f'QUEUE CAN\'T BE FILLED ANY MORE - global option \'{HARD_UNIQUE_SHOWS_OPTION_GLOBAL}\' doesn\'t allow shows to be played more than once in a session.'
                    print(message)
                    break # no aliases left means the config is messed up or the 'unique' or 'uunique' global option is set and there are no shows left for now
                elif amount != None and og.unique_shows_bool_global:
                    aliases_to_choose_next_episode_from = get_config_all_show_aliases(filter_options)
                else:
                    aliases_to_choose_next_episode_from = get_config_all_show_aliases(filter_options)

            # the return value here might carry an error message
            alias = self.add_entry_from_aliases(aliases_to_choose_next_episode_from, add_to_front=False, probability_dropping=True, probability_global=og.probability_value_global)

            if alias == f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}':
                invalid_shows_in_config_counter += 1
                if invalid_shows_in_config_counter >= invalid_shows_in_config_counter_limit:
                    message = f'{color.ERROR}Failed to load a show\'s information from config {invalid_shows_in_config_counter_limit} times in a row.{color.END} Exiting program.'
                    print(message)
                    exit_program_event.set()
                continue

            if alias == f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}dropped':
                continue

            aliases_to_choose_next_episode_from.remove(alias)


    def handle_suspend_queue_resume(self):
        with open(self.config_path, encoding=ENCODING_CONFIG_FILE) as f:
            content = f.readlines()
        queue_line = ""
        for line in content:
            if line.startswith(CONFIG_SUSPEND_QUEUE_LINE_STARTER):
                queue_line = line
                content.remove(line)
                break

        # overwrite config file, delete queue_line
        with open(CONFIG_FULL_PATH, 'r+', encoding=ENCODING_CONFIG_FILE) as f:
            f.seek(0)
            f.truncate()
            f.write("".join(content))
            f.close()

        if queue_line != "":
            queue_line = queue_line.strip(CONFIG_SUSPEND_QUEUE_LINE_STARTER).strip()
            aliases = queue_line.split(";")
            if len(aliases) > 0:
                for a in aliases:
                    temp_str = self.add_entry_from_aliases([a])
                message = f"{color.WARNING}RELOAD SUSPENDED QUEUE{color.END} - reloaded queue from last time."
                print(message)


    def add_entry_from_aliases(self, aliases: [str], add_to_front: bool=False, probability_dropping: bool=False, probability_global: int=None) -> str:
        if probability_global == None:
            probability_global = 100

        # get random show_line and path_line from available aliases
        show_line, show_alias_path_line, alias = get_config_random_show_and_path_line_from_aliases(aliases)
        if alias == "": #nothing found
            return f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}'

        # try creating a show_episode_instance and handle consecutive loading errors
        show_episode_instance = ShowEpisode(show_line, show_alias_path_line)
        if not show_episode_instance.is_valid:
            message = f"""{color.ERROR}Failed to load show line{color.END} {show_line}. Missing necessary arguments or faulty path line."""
            return f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}'

        invalid_shows_in_config_counter = 0

        if probability_dropping and (show_episode_instance.probability_value_show != None or probability_global < 100):
            if show_episode_instance.probability_value_show == None:
                prob = probability_global # use global if no show probability set
            else:
                prob = show_episode_instance.probability_value_show
            random_int = randint(1, 100)
            if prob < random_int:
                return f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}dropped'

        if add_to_front:
            self.queue.insert(0, show_episode_instance)
        else:
            self.queue.append(show_episode_instance)
        return alias


    def add_entry_at_front_from_alias(self, player: vlc.MediaPlayer, alias_input: str) -> str:
        alias = self.add_entry_from_aliases([alias_input], add_to_front=True, probability_dropping=False)

        return alias



    def clear_queue(self):
        """Removes all but the first element of the queue. Also removes last one if playing commercial.

        This is because the first entry is supposed to be the show currently playing."""
        while len(self.queue) > 1:
            self.queue.pop(1)
        if playing_commercials_event.is_set():
            self.queue.pop(0)

    def pop_queue(self):
        """Removes last element of the queue. Can't be used on the last one. except if playing commercial.

        This is because the first entry is supposed to be the show currently playing."""
        if not playing_commercials_event.is_set():
            if len(self.queue) > 1:
                self.queue.pop(-1)
        else:
            if len(self.queue) > 0:
                self.queue.pop(-1)

    def get_queue_table(self):
        if len(self.queue) == 0:
            message = 'Currently the queue is empty.'
        else:
            max_width = 0 # for pretty formatting
            for e in self.queue:
                max_width = max (max_width, len(e.config_alias))

            queue_strings = []
            queue_strings.append(f'{color.BOLD}Current queue:{color.END}')

            for entry in self.queue:
                entry_string = f'{entry.config_alias.ljust(max_width)}\t{entry.config_current_ep}'

                if entry.opening_bool:
                    entry_string = entry_string + "\top"
                    if entry.opening_filetype_string:
                        entry_string = entry_string + f'.{entry.opening_filetype_string}'
                else:
                    entry_string = entry_string + "\t"

                if entry.ending_bool:
                    entry_string = entry_string + "\ted"
                    if entry.ending_filetype_string:
                        entry_string = entry_string + f'.{entry.ending_filetype_string}'

                queue_strings.append(entry_string)

            message = "\n\t".join(queue_strings)
        return message




##### Other Setup #####

# some threading.Events
event_player_Stopped = Event()
playing_opening_event = Event()
playing_episode_event = Event()
playing_ending_event = Event()
playing_commercials_event = Event()

episode_finished_event = Event()
episode_skipped_event = Event()
episode_stop_all_playback_event = Event()
episode_skip_to_commercial_event = Event()
episode_error_event = Event()
player_restart_event = Event()
episode_full_restart_event = Event()

exit_program_event = Event()
input_registered_event = Event()
input_handled_event = Event()

shutdown_running_event = Event() # used for 'shutdown' option and command
monitoring_running_event = Event() # used for 'monitor' option and command
monitoring_player_jumps_running_event = Event() # used for 'monitor-jump' option and command

# used for 'debug' command and option
event_player_Playing = Event()
media_changed_counter = 0
event_player_LengthChanged_event = Event()
debug_player_jumps_running_event = Event()
debug_player_last_time = 0
DEBUG_PLAYER_JUMPS_INTERVALL = 0.1

# used for suspend command and option
suspend_event = Event()
suspend_queue_event = Event()
left_off_at_seconds = 0

# used for 'episodes' show option
skip_straight_to_next_episode_event = Event()
skip_straight_to_next_episode_counter = 0

# used for 'last' command and global option
last_episodes_for_today_counter = 0

# used for 'shutdown' command and option
shutdown_minutes = 1

# used for 'analysis' and 'analysis-full' command
analysis_running_event = Event()




### Input Handling ###
# see https://stackoverflow.com/questions/5404068/how-to-read-keyboard-input/53344690#53344690
inputQueue = Queue()

def read_keyboard_input():
    global inputQueue
    event_player_Playing.wait() # wait for player to start playing for the first time
    message_input_startup = f'Ready for keyboard input. Type \'{HELP_COMMAND}\' to see all commands :-)'
    print(message_input_startup)

    input_program_dead_counter = 0
    input_handled_event.set()
    while True:
        input_str = input()
        inputQueue.queue.clear() # in case someone is typing really fast
        inputQueue.put(input_str)

        input_registered_event.set() # this will activate handle_input() in the playThread

        # make sure program is handling inputs, else print warning to terminal
        if not input_handled_event.is_set():
            input_program_dead_counter = input_program_dead_counter + 1
        else:
            input_handled_event.clear()
            input_program_dead_counter = 0

        if input_program_dead_counter == 1:
            message = f'\n{color.ERROR}Input can\'t be handled.{color.END} The program probably crashed somewhere.\nPress Ctrl+C to force quit.'
            print(message)
            print(MESSAGE_REPORT_BUGS)

        if input_program_dead_counter >= 5:
            message = f'\n{color.ERROR}Input still can\'t be handled.{color.END} Exiting'
            print(message)
            exit_program_event.set()


def player_wait_input(player: vlc.MediaPlayer, options_session: OptionsSession, ep_instance: ShowEpisode, options_global: OptionsGlobal, shows_queue: ShowsQueue):
    # input handling, looping as long as the player hasn't stopped
    event_player_Playing.wait()
    while not event_player_Stopped.is_set():
        input_registered_event.wait() # this is set in the inputThread when input is registered

        input_handled_event.set() # tells the input that program is alive

        if not event_player_Stopped.is_set():
            try:
                handle_input(player, options_session, ep_instance, options_global, shows_queue)
            except Exception as e:
                print(e)
                message = f'{color.RED}Something went wrong with the input.{color.END} If it doesn\'t respond, close the terminal or press Ctrl+C to force quit.'
                print(message)
                print(MESSAGE_REPORT_BUGS)

        input_registered_event.clear()


def handle_input(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue):
    global inputQueue

    if (inputQueue.qsize() > 0):
        input_str = inputQueue.get().strip()

        if (input_str.lower() == HELP_COMMAND or input_str == HELP_C) and input_str != HELP_FULL_COMMAND:
            input_handle_help()

        elif input_str == HELP_FULL_COMMAND or input_str == HELP_FULL_C:
            input_handle_help_full()

        elif input_str.lower() == STATUS_COMMAND or input_str == STATUS_C:
            input_handle_status(player, ep)

        elif input_str.lower() == PAUSE_COMMAND or input_str == PAUSE_C:
            input_handle_pause(player)

        elif input_str.lower() == VOLUME_COMMAND or input_str == VOLUME_C:
            input_handle_volume_display(player, sesh, ep, og)

        elif input_str.lower().startswith(VOLUME_COMMAND + " ") or input_str.startswith(VOLUME_C + " "):
            input_handle_volume_x(player, sesh, ep, og, input_str)

        elif input_str.lower().startswith(JUMP_COMMAND + " ") or input_str.startswith(JUMP_C + " "):
            input_handle_jump_x(player, input_str)

        elif input_str.lower() == BACK_COMMAND or input_str == BACK_C:
            input_handle_back(player)

        elif input_str.lower().startswith(BACK_COMMAND + " ") or input_str.startswith(BACK_C + " "):
            input_handle_back_x(player, input_str)

        elif input_str.lower() == FORWARD_COMMAND or input_str == FORWARD_C:
            input_handle_forward(player)

        elif input_str.lower().startswith(FORWARD_COMMAND + " ") or input_str.startswith(FORWARD_C + " "):
            input_handle_forward_x(player, input_str)

        elif input_str.lower() == RESTART_COMMAND or input_str == RESTART_C:
            input_handle_restart(player)

        elif input_str.lower() == SKIP_EPISODE_COMMAND:
            input_handle_player_next_no_save(player, sesh, ep, og)

        elif input_str.lower() == FINISH_EPISODE_COMMAND:
            input_handle_player_next_with_save(player, ep, og)

        elif input_str.lower() == SHOW_CONFIG_COMMAND or input_str == SHOW_CONFIG_C:
            input_handle_show_config(ep)

        elif input_str.lower() == CONFIG_HELP_COMMAND:
            input_handle_config_help()

        elif input_str.lower() == EXIT_COMMAND or input_str == EXIT_C or input_str == EXIT_C_2:
            input_handle_exit(player, option_handle_bools(ep.suspend_bool, og.suspend_bool), option_handle_bools(ep.suspend_queue_bool, og.suspend_queue_bool))


        ## extra commands
        elif input_str.lower() == OPEN_PLAYBACK_DIRECTORY_COMMAND:
            input_handle_open_playback_directory(player)

        elif input_str.lower() == OPEN_CONFIG_COMMAND:
            input_handle_open_config()

        elif input_str.lower() == OPTIONS_HELP_COMMAND:
            input_handle_options_help()

        elif input_str.lower() == DISPLAY_CURRENT_OPTIONS_COMMAND:
            input_handle_display_current_options(ep, og)

        elif input_str.lower() == SPEED_COMMAND:
            input_handle_speed_display(player)

        elif input_str.lower().startswith(SPEED_COMMAND + " "):
            input_handle_speed_x(player, sesh, input_str)

        elif input_str.lower().startswith(HARD_VOLUME_COMMAND + " "):
            input_handle_hard_volume_x(player, sesh, ep, og, input_str)

        elif input_str.lower() == MUTE_COMMAND or input_str == MUTE_C:
            input_handle_mute(player, sesh)

        elif input_str.lower() == FULLSCREEN_COMMAND:
            input_handle_fullscreen_toggle(player)

        elif input_str.lower() == DISPLAY_SHOWS_COMMAND:
            input_handle_display_shows()

        elif input_str.lower() == QUEUE_SHOW_COMMAND or input_str == QUEUE_SHOW_C:
            input_handle_queue_show_display(s_que)

        elif input_str.lower().startswith(QUEUE_SHOW_COMMAND + " ") or input_str.startswith(QUEUE_SHOW_C + " "):
            input_handle_queue_show_x(s_que, input_str)

        elif input_str.lower() == QUEUE_SHOW_LOAD_COMMAND or input_str == QUEUE_SHOW_LOAD_C:
            input_handle_queue_show_load(og, s_que)

        elif input_str.lower().startswith(QUEUE_SHOW_LOAD_COMMAND + " ") or input_str.startswith(QUEUE_SHOW_LOAD_C + " "):
            input_handle_queue_show_load_x(og, s_que, input_str)

        elif input_str.lower() == QUEUE_SHOW_CLEAR_COMMAND or input_str == QUEUE_SHOW_CLEAR_C:
            input_handle_queue_show_clear(s_que)

        elif input_str.lower() == QUEUE_SHOW_POP_COMMAND or input_str == QUEUE_SHOW_POP_C:
            input_handle_queue_show_pop(s_que)

        elif input_str.lower().startswith(PLAY_SHOW_COMMAND + " "):
            input_handle_play_show_x(player, s_que, input_str)

        #elif input_str.lower() == PLAY_EPISODE_COMMAND:
        #    input_handle_play_episode_current(player)
        #elif input_str.lower().startswith(PLAY_EPISODE_COMMAND + " "):
        #    input_handle_play_episode_x(player, input_str)
        #elif input_str.lower() == PREVIOUS_EPISODE_COMMAND or input_str.lower() == PREVIOUS_EPISODE_C:
        #    input_handle_previous_episode()
        #elif input_str.lower() == NEXT_EPISODE_COMMAND:
        #    input_handle_next_episode()
        #elif input_str.lower() == LOOP_EPISODE_COMMAND:
        #    input_handle_loop_episode() #reset debug_player_last_timereseten
        #elif input_str.lower().startswith(LOOP_EPISODE_COMMAND + " "):
        #    input_handle_loop_x_episode(input_str) #reset debug_player_last_time

        elif input_str.lower() == END_COMMAND:
            input_handle_end_of_episode(player)

        elif input_str.lower().startswith(END_COMMAND + " "):
            input_handle_end_x_of_episode(player, input_str)

        elif input_str.lower() == SUSPEND_COMMAND:
            input_handle_suspend(player)

        elif input_str.lower() == SUSPEND_QUEUE_COMMAND:
            input_handle_suspend_queue(player)

        elif input_str.lower() == SHUTDOWN_COMMAND:
            input_handle_shutdown()

        elif input_str.lower().startswith(SHUTDOWN_COMMAND + " "):
            input_handle_shutdown_x(player, og, input_str)

        elif input_str.lower() == (LAST_EPISODE_COMMAND):
            input_handle_last_episode()

        elif input_str.lower().startswith(LAST_EPISODE_COMMAND + " "):
            input_handle_last_episode_x(input_str)

        elif input_str.lower() == SKIP_FULL_EPISODE_COMMAND:
            input_handle_skip_full_episode(player)

        elif input_str.lower() == FINISH_FULL_EPISODE_COMMAND:
            input_handle_finish_full_episode(player)

        elif input_str.lower() == SKIP_TO_COMMERCIALS_COMMAND or input_str == SKIP_TO_COMMERCIALS_C:
            input_handle_skip_to_commercials(player)

        elif input_str.lower().startswith(COMMERCIALS_GLOBAL_OVERRIDE_COMMAND + " ") or input_str.startswith(COMMERCIALS_GLOBAL_OVERRIDE_C + " "):
            input_handle_commercials_global_override_x(sesh, og, input_str)

        elif input_str.lower() == HARD_PLAY_COMMAND:
            input_handle_resume(player)

        elif input_str.lower() == HARD_PAUSE_COMMAND:
            input_handle_hard_pause(player)

        elif input_str.lower().startswith(VIDEO_TRACK_COMMAND + " "):
            input_handle_video_track_x(player, ep, input_str)

        elif input_str.lower() == VIDEO_TRACK_COMMAND:
            input_handle_display_video_track(player)

        elif input_str.lower().startswith(AUDIO_TRACK_COMMAND + " "):
            input_handle_audio_track_x(player, ep, input_str)

        elif input_str.lower() == AUDIO_TRACK_COMMAND:
            input_handle_display_audio_track(player)

        elif input_str.lower().startswith(SUBTITLE_TRACK_COMMAND + " "):
            input_handle_subtitle_track_x(player, ep, input_str)

        elif input_str.lower() == SUBTITLE_TRACK_COMMAND :
            input_handle_display_subtitle_track(player)

        elif input_str.lower() == HISTORY_COMMAND:
            input_handle_history(s_que)

        elif input_str.lower().startswith(MONITOR_COMMAND + " "):
            input_handle_monitor_x(player, sesh, ep, input_str)

        elif input_str.lower() == MONITOR_PLAYER_JUMPS_COMMAND:
            input_handle_monitor_jumps(player, ep)

        elif input_str.lower() == MONITOR_OFF_COMMAND:
            input_handle_monitors_off()

        elif input_str.lower() == DEBUG_PLAYER_JUMPS_COMMAND:
            input_handle_debug_player_jumps(player)

        elif input_str.lower() == MEDIA_LENGTHS_ANALYSIS_QUICK_COMMAND:
            input_handle_media_lengths_analysis_quick()

        elif input_str.lower() == MEDIA_LENGTHS_ANALYSIS_FULL_COMMAND:
            input_handle_media_lengths_analysis_full()

        elif input_str.lower() == ABOUT_COMMAND:
            input_handle_about()

        else:
            if input_str.strip() != "":
                message = f'Unknown command: {input_str}'
                print(message)

        # print empty line for better readability
        print(" ")




##### METHODS FROM INPUT #####

def input_handle_help():
    print("Help")
    print(MESSAGE_HELP_SHORT)


def input_handle_help_full():
    print("HELP")
    print(MESSAGE_HELP_FULL)


def input_handle_status(player: vlc.MediaPlayer, ep_instance: ShowEpisode):
    message = f'{color.INPUT_ANSWER}STATUS{color.END} - show status.'
    print(message)
    print_full_player_status(player, ep_instance, full_status=False)


def print_full_player_status(player: vlc.MediaPlayer, ep_instance: ShowEpisode, full_status: bool):
    milli = player.get_time()
    sec = str(floor(milli / 1000))

    milli_length = player.get_length()
    sec_length = str(floor(milli_length / 1000))

    if playing_opening_event.is_set():
        status_what_is_playing = f'{ep_instance.config_alias} opening'
    elif playing_ending_event.is_set():
        status_what_is_playing = f'{ep_instance.config_alias} ending'
    elif playing_commercials_event.is_set():
        commercial_path = player.get_media().get_mrl()
        commercial_name = os.path.basename(os.path.normpath(commercial_path)) # see https://stackoverflow.com/questions/3925096/how-to-get-only-the-last-part-of-a-path-in-python
        status_what_is_playing = f'commercial {unquote(commercial_name)}'
    else:
        status_what_is_playing = f'{str(ep_instance)}'

    status_playing = f'Playing {status_what_is_playing} at {get_h_m_s_from_milliseconds(milli)} / {get_h_m_s_from_milliseconds(milli_length)}'
    status_seconds = f'(second {sec}/{sec_length}).'
    status_state = f'State: {str(player.get_state())[6:]}.'
    status_output = f'Audio output device:{str(player.audio_output_device_get())}.'
    status_muted = f'Muted: {("Yes" if player.audio_get_mute() else "No")}.'
    status_volume = f'Volume: {player.audio_get_volume()}%.'
    status_speed = f'Speed: {int(player.get_rate() * 100)}%'
    status_rest = f'Track: {player.audio_get_track()}. Channel: {str(player.audio_get_channel())}. Delay: {str(player.audio_get_delay())}. Role: {str(player.get_role())}.'

    if full_status:
        print("   ", status_playing, status_seconds, status_state, status_muted, status_volume, status_speed, status_output, status_rest)
    else:
        print(status_playing, status_seconds, status_state, status_muted, status_volume, status_speed, status_output)


def get_h_m_s_from_milliseconds(milli: int):
    """Get milliseconds converted to hh:mm:ss. If hh == 0, then mm:ss"""
    sec = floor(milli / 1000)
    s = sec % 60
    m = floor(sec / 60) % 60
    h = floor(sec / (60 * 60))
    if h > 0:
        return f'{"{0:0=2d}".format(h)}:{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'
    else:
        return f'{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'


def input_handle_pause(player: vlc.MediaPlayer):
    if player.is_playing() == 1:
        message = f'{color.INPUT_ANSWER}PAUSING{color.END} - pause playback. Enter \'{PAUSE_COMMAND}\' again to resume playback.'
        print(message)
        player.set_pause(1)
    else:
        message = f'{color.INPUT_ANSWER}RESUMING{color.END} - resume playback.'
        print(message)
        player.set_pause(0)

    milli = player.get_time()
    milli_length = player.get_length()
    message = f'Currently at {get_h_m_s_from_milliseconds(milli)} ({floor(milli / 1000)}s) / {get_h_m_s_from_milliseconds(milli_length)} ({floor(milli_length / 1000)}s).'
    print(message)


def input_handle_volume_display(player, sesh, ep, og):
    message = f"""{color.BOLD}Player volume {player.audio_get_volume()}%{color.END}
     Session: {sesh.volume_value if sesh.volume_value else 100}%
      Global: {og.volume_value if og.volume_value else 100}%
        Show: {ep.volume_value if ep.volume_value else 100}%"""
    print(message)


def input_handle_volume_x(player: vlc.MediaPlayer, sesh, ep, og, input_str: str):
    volume_to = input_handle_number_input(input_str, VOLUME_COMMAND, VOLUME_C, VOLUME_LIMIT_LOWER, VOLUME_LIMIT_UPPER)

    if volume_to != None:

        message = f'{color.INPUT_ANSWER}VOLUME {volume_to}{color.END} - Set volume to {volume_to}%.'
        print(message)
        sesh.volume_value = volume_to
        player.audio_set_volume(option_handle_volume(sesh, ep, og))


def input_handle_jump_x(player: vlc.MediaPlayer, input_str: str):
    jump_to = input_handle_number_timestamp_input(input_str, JUMP_COMMAND, JUMP_C)

    if jump_to != -1:

        current_time = player.get_time()
        message = f'{color.INPUT_ANSWER}JUMP {jump_to}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) to {get_h_m_s_from_milliseconds(jump_to * 1000)} ({jump_to}s).'
        print(message)

        # handle jumping over episode end
        goal_time = jump_to * 1000

        handle_jump_forward_helper(player, goal_time)

        print_now_at_time(player.get_time())


def handle_jump_forward_helper(player: vlc.MediaPlayer, goal_milli: int):
    global debug_player_last_time

    ep_len = player.get_length() # audio length in milliseconds
    current_time = player.get_time()
    if ep_len < (goal_milli):
        if ep_len - (JUMP_PLAYBACK_BUMPER_SIZE_STANDARD* 1000) > current_time:
            message = f'File is only {get_h_m_s_from_milliseconds(ep_len)} long, but trying to go to {get_h_m_s_from_milliseconds(goal_milli)}. Jumping {JUMP_PLAYBACK_BUMPER_SIZE_STANDARD} seconds close to end instead.'
            print(message)
            goal_milli = ep_len - (JUMP_PLAYBACK_BUMPER_SIZE_STANDARD * 1000) # jump 20 seconds close to end
        else:
            message = f'File is only {get_h_m_s_from_milliseconds(ep_len)} long, but trying to go to {get_h_m_s_from_milliseconds(goal_milli)}. Nothing happened.'
            print(message)
            return
    debug_player_last_time = goal_milli
    player.set_time(goal_milli)


def print_now_at_time(milli: int):
    message = f'Now at {get_h_m_s_from_milliseconds(milli)} ({floor(milli / 1000)} seconds).'
    print(message)


def input_handle_number_timestamp_input(input_str: str, command: str, comm="") -> int:
    """Get int from input, can be timestamp.

    Tries to fetch an int or timestamp like mm:ss or h:m:s from an input_str and returns as seconds. Returns -1 if not successful. Pass the input_str, the long command and the short command, if existing.

    For example for the command 'jump' you have to pass an integer or timestamp afterwards. Anything like 'jump 123', 'jump 4:20', 'jump 00:12:03' is possible, as well as typing 'j' instead of 'jump'. The result of these examples will be the intergers 123, 260 and 723 respectively.
    Negative numbers anywhere are turned to 0.
    If an error occurs parsing the command, like when typing 'jump 4m20s', then the method will return -1.
    """
    try:
        if input_str.lower().startswith(command + " "):
            number_str = input_str[(len(command) + 1):]
        elif comm:
            number_str = input_str[(len(comm) + 1):]
        if ":" in number_str:
            split = number_str.split(":")
            h = 0
            if len(split) == 3:
                h = max(int(split[0]), 0)
                m = max(int(split[1]), 0)
                s = max(int(split[2]), 0)
            else:
                m = max(int(split[0]), 0)
                s = max(int(split[1]), 0)
            number = h * 3600 + m * 60 + s
        else:
            number = int(number_str)
            number = max(number, 0)
        return number
    except ValueError:
        message = f'Bad input! Enter a positive number after \'{command}\', like: \'{command} 83\', or a timestamp, like: \'{command} 12:34\''
        print(message)
        return -1 # arbitrary error integer, only positive integers are correct as output

def input_handle_number_input(input_str: str, command: str, comm: str="", lower_limit: int=None, upper_limit: int=None) -> int:
    """Get int from input.

    Tries to fetch an int from an input_str and returns as integer bounded by lower_limit and upper_limit. Returns None if not successful. Pass the input_str, the long command and the short command, if existing.

    For example for the command 'volume' you have to pass an integer afterwards. Anything like 'volume 123' is possible, as well as typing 'v' instead of 'volume'.
    Negative numbers are turned to lower_limit.
    If an error occurs parsing the command, like when typing 'volume a420s', then the method will return None.
    """
    try:
        if input_str.lower().startswith(command + " "):
            number_str = input_str[(len(command) + 1):]
        elif comm:
            number_str = input_str[(len(comm) + 1):]
        number = int(number_str)
        if lower_limit != None:
            number = max(number, lower_limit)
        if upper_limit != None:
            number = min(number, upper_limit)
        return number
    except ValueError:
        message = f'Bad input! Enter a positive number after \'{command}\', like: \'{command} 83\''
        print(message)
        return None


def input_handle_back(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}BACK{color.END} - go back {BACK_PLAYBACK_SIZE_STANDARD} seconds.'
    print(message)

    input_handle_back_by_helper(player, BACK_PLAYBACK_SIZE_STANDARD)


def input_handle_back_x(player: vlc.MediaPlayer, input_str: str):
    back_by = input_handle_number_timestamp_input(input_str, BACK_COMMAND, BACK_C)

    if back_by != -1:
        current_time = player.get_time()
        message = f'{color.INPUT_ANSWER}BACK {back_by}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) back by {back_by}s.'
        print(message)

        input_handle_back_by_helper(player, back_by)


def input_handle_back_by_helper(player: vlc.MediaPlayer, seconds: int):
    global debug_player_last_time

    current_time = player.get_time()
    if (current_time - seconds * 1000) < 100:
        goal_milli = 0
    else:
        goal_milli = current_time - seconds * 1000

    debug_player_last_time = goal_milli
    player.set_time(goal_milli)
    print_now_at_time(player.get_time())


def input_handle_forward(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}FORWARD{color.END} - go forward {FORWARD_PLAYBACK_SIZE_STANDARD} seconds.'
    print(message)
    player.set_time(player.get_time() + (FORWARD_PLAYBACK_SIZE_STANDARD * 1000))
    print_now_at_time(player.get_time())


def input_handle_forward_x(player: vlc.MediaPlayer, input_str: str):
    forward_by = input_handle_number_timestamp_input(input_str, FORWARD_COMMAND, FORWARD_C)

    if forward_by != -1:

        current_time = player.get_time()
        message = f'{color.INPUT_ANSWER}FORWARD {forward_by}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) forward by {forward_by}s.'
        print(message)

        # handle jumping over episode end
        goal_time = current_time + forward_by * 1000

        handle_jump_forward_helper(player, goal_time)

        print_now_at_time(player.get_time())


def input_handle_restart(player: vlc.MediaPlayer):
    player_was_playing = (player.is_playing() == 1)
    if not player_was_playing:
        message_extra = f' Playback is still paused, enter \'{PAUSE_COMMAND}\' to resume playback.'
    else:
        message_extra = ""

    if playing_opening_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this opening.{message_extra}'
    elif playing_episode_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this episode.{message_extra}'
    elif playing_ending_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this ending.{message_extra}'
    elif playing_commercials_event.is_set():
        message = f'{color.INPUT_ANSWER}RESTARTING{color.END} - restart this commercial.{message_extra}'

    print(message)

    player_restart_event.set()
    player.stop()
    player.play()
    event_player_Playing.wait()
    if not player_was_playing:
        player.set_pause(1) # pause again if player was paused before restart
    player_restart_event.clear()


def input_handle_player_next_no_save(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal):
    if playing_opening_event.is_set():
        message = f'{color.INPUT_ANSWER}SKIPPING OPENING{color.END} - go to episode.\n'
    elif playing_episode_event.is_set():
        if option_handle_bools(ep.ending_bool, og.ending_bool):
            message = f'{color.INPUT_ANSWER}SKIPPING EPISODE{color.END} - stop this episode without marking it as watched, play ending.'
        else:
            if og.commercial_weight_value_global == None and sesh.commercial_weight_value_global == None:
                message = f'{color.INPUT_ANSWER}SKIPPING EPISODE{color.END} - stop this episode without marking it as watched, play next show.'
            else:
                message = f'{color.INPUT_ANSWER}SKIPPING EPISODE{color.END} - stop this episode without marking it as watched, play commercials.'
        episode_skipped_event.set()
    elif playing_ending_event.is_set():
        if og.commercial_weight_value_global == None and sesh.commercial_weight_value_global == None:
            message = f'{color.INPUT_ANSWER}SKIPPING ENDING{color.END} - play next show.\n'
        else:
            message = f'{color.INPUT_ANSWER}SKIPPING ENDING{color.END} - play commercials.\n'
    elif playing_commercials_event.is_set():
        message = f'{color.INPUT_ANSWER}SKIPPING THIS COMMERCIAL{color.END} - play next thing. If there are more cms and you want to skip all of them at once, use \'{SKIP_FULL_EPISODE_COMMAND}\'.\n'

    print(message, end="")
    player.stop()


def input_handle_player_next_with_save(player: vlc.MediaPlayer, ep: ShowEpisode, og: OptionsGlobal):
    if playing_episode_event.is_set():
        message = f'{color.INPUT_ANSWER}FINISHING EPISODE{color.END} - mark this episode as watched, play next show.'
        episode_finished_event.set()
        print(message, end="")
        player.stop()
    else:
        input_handle_player_next_no_save(player, sesh, ep, og)


def input_handle_show_config(ep_instance: ShowEpisode):
    message = f'{color.INPUT_ANSWER}SHOW CONFIG{color.END} - show path and contents of current config file.'
    print(message)
    with open(CONFIG_FULL_PATH, encoding=ENCODING_CONFIG_FILE) as f:
        content = f.readlines()
    print(CONFIG_FULL_PATH)
    for line in content:
        if ep_instance.config_alias in line:
            line = color.BOLD + color.CYAN + line + color.END
        print(line, end="")


def input_handle_config_help():
    print(MESSAGE_CONFIG_HELP)


def input_handle_exit(player, suspend_bool, suspend_queue_bool):
    if suspend_queue_bool:
        input_handle_suspend_queue(player)
    elif suspend_bool:
        input_handle_suspend(player)
    else:
        helper_exit_program(player)


def helper_exit_program(player):
    # stop player
    player_restart_event.set() # spaghetti way of avoiding player.stop() to trigger next playback
    player.stop()

    # print and end
    message = f'{color.INPUT_ANSWER}EXITING{color.END} - end program.'
    print(message)
    exit_program_event.set() # this will make the main() end and with it the whole program



##### Extra Commands Handle Methods

def input_handle_open_playback_directory(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}OPEN PLAYBACK DIRECTORY{color.END} - a new directory explorer window with the current playback file should be opened.'
    print(message)
    playback_path = os.path.dirname(player.get_media().get_mrl())
    open_file(playback_path)


def input_handle_open_config():
    message = f'{color.INPUT_ANSWER}OPEN CONFIG{color.END} - a new window with the config file should be opened.'
    print(message)
    open_file(CONFIG_FULL_PATH)


# see https://stackoverflow.com/questions/17317219/is-there-an-platform-independent-equivalent-of-os-startfile
def open_file(filename: str):
    """Opens a file or directory with system's standard program.
    """

    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        call([opener, filename])


def input_handle_options_help():
    print(MESSAGE_OPTIONS_HELP)


def input_handle_display_current_options(ep: ShowEpisode, og: OptionsGlobal):
    show_options = generate_config_show_global_line_options_list(ep)
    if len(show_options) == 0:
        show_options = ["None"]
    show_message = f'{color.BOLD}Show Options:{color.END}'
    show_options.insert(0, show_message)

    global_options = generate_config_show_global_line_options_list(og)
    if len(global_options) == 0:
        global_options = ["None"]
    global_message = f'{color.BOLD}Global Options:{color.END}'
    global_options.insert(0, global_message)

    print("\n\t".join(show_options))
    print("\n\t".join(global_options))


def input_handle_speed_display(player: vlc.MediaPlayer):
    message = f'{color.BOLD}Player speed {int(player.get_rate() * 100)}%{color.END}'
    print(message)


def input_handle_speed_x(player: vlc.MediaPlayer, sesh: OptionsSession, input_str: str):
    speed_to = input_handle_number_input(input_str, SPEED_COMMAND, None, SPEED_LIMIT_LOWER, SPEED_LIMIT_UPPER)

    if speed_to != None:

        message = f'{color.INPUT_ANSWER}SPEED {speed_to}{color.END} - Set playback speed to {speed_to}%.'
        print(message)

        sesh.speed_value = speed_to
        player.set_rate(speed_to / 100)


def input_handle_hard_volume_x(player: vlc.MediaPlayer, sesh, ep, og, input_str: str):
    volume_to = input_handle_number_input(input_str, HARD_VOLUME_COMMAND, None, 0, 1000)

    if volume_to != None:

        message = f"{color.INPUT_ANSWER}VOLUME {volume_to}{color.END} - Set player's volume to {volume_to}%, ignoring settings."
        print(message)

        ep_volume_mult = (ep.volume_value / 100) if ep.volume_value else 1
        og_volume_mult = (og.volume_value / 100) if og.volume_value else 1

        if ep_volume_mult != 0 and og_volume_mult != 0:
            sesh_volume_value = volume_to * (1 / ep_volume_mult) * (1 / og_volume_mult)
            sesh.volume_value = int(sesh_volume_value)

        player.audio_set_volume(volume_to)


def input_handle_mute(player: vlc.MediaPlayer, sesh: OptionsSession):
    mute_state = player.audio_get_mute()
    if mute_state == 0: # 0 if not muted
        message = f'{color.INPUT_ANSWER}MUTE{color.END}'
        mute_bool = True
    else:
        message = f'{color.INPUT_ANSWER}UNMUTE{color.END}'
        mute_bool = False
    print(message)
    sesh.mute_bool = mute_bool
    player.audio_set_mute(mute_bool)


def input_handle_fullscreen_toggle(player: vlc.MediaPlayer):
    media_has_fullscreen = player.has_vout() # 1 if has video track, 0 if none
    if media_has_fullscreen == 0:
        message = f'{color.INPUT_ANSWER}FULLSCREEN{color.END} - but this media has no video track.'
    else:
        media_is_fullscreen = player.get_fullscreen() # 1 if is fullscreen, else 0
        if media_is_fullscreen == 0:
            message = f'{color.INPUT_ANSWER}FULLSCREEN{color.END} - enable.'
            player.set_fullscreen(1)
        else:
            message = f'{color.INPUT_ANSWER}FULLSCREEN{color.END} - disable.'
            player.set_fullscreen(0)
    print(message)


def input_handle_display_shows():
    show_aliases = get_config_all_show_aliases([COMMERCIAL_WEIGHT_OPTION_SHOW])
    if len(show_aliases) > 0:
        show_aliases.insert(0, "\nShows:")
        print("\n\t".join(show_aliases))
    else:
        print("No shows found in config.")

    commercial_aliases = get_config_all_show_aliases([COMMERCIAL_WEIGHT_OPTION_SHOW], True)
    if len(commercial_aliases) > 0:
        commercial_aliases.insert(0, "\nCommercials:")
        print("\n\t".join(commercial_aliases))
    else:
        print("No commercials found in config.")


def input_handle_queue_show_display(s_que: ShowsQueue):
    message = s_que.get_queue_table()
    print (message)


def input_handle_queue_show_x(s_que: ShowsQueue, input_str: str):
    if input_str.lower().startswith(QUEUE_SHOW_COMMAND + " "):
        arg = input_str[(len(QUEUE_SHOW_COMMAND) + 1):]
    else:
        arg = input_str[(len(QUEUE_SHOW_C) + 1):]
    alias = s_que.add_entry_from_aliases([arg])
    if alias == f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}':
        message = f"{color.INPUT_ANSWER}QUEUE {arg}{color.END} - failed to find a show, maybe no show found with a name similar to '{arg}', maybe its config line is faulty."
        print(message)
    else:
        message = f'{color.INPUT_ANSWER}QUEUE {arg}{color.END} - added {alias} to end of queue.'
        print(message)


def input_handle_queue_show_load(og: OptionsGlobal, s_que: ShowsQueue):
    s_que.populate_automatic(og, QUEUE_SIZE_STANDARD)
    message = f'{color.INPUT_ANSWER}QUEUE LOAD{color.END} - loaded {QUEUE_SIZE_STANDARD} new entries into queue.'
    print(message)


def input_handle_queue_show_load_x(og: OptionsGlobal, s_que: ShowsQueue, input_str: str):
    load_size = input_handle_number_input(input_str, QUEUE_SHOW_LOAD_COMMAND, QUEUE_SHOW_LOAD_C, 0, QUEUE_LOAD_LIMIT_UPPER)

    if load_size != None:

        s_que.populate_automatic(og, load_size)
        message = f'{color.INPUT_ANSWER}QUEUE LOAD {load_size}{color.END} - loaded {load_size} new entries into queue.'
        print(message)


def input_handle_queue_show_clear(s_que: ShowsQueue):
    s_que.clear_queue()
    if playing_commercials_event.is_set():
        message = f'{color.INPUT_ANSWER}QUEUE CLEARED{color.END} - deleted all entries in queue.'
    else:
        message = f'{color.INPUT_ANSWER}QUEUE CLEARED{color.END} - deleted all entries in queue except for current episode.'
    print(message)


def input_handle_queue_show_pop(s_que: ShowsQueue):
    queue_size_before = len(s_que.queue)
    s_que.pop_queue()
    if len(s_que.queue) == queue_size_before: # nothing was removed
        message = f'{color.INPUT_ANSWER}QUEUE POP{color.END} - nothing removed, queue already too small.'
    else:
        message = f'{color.INPUT_ANSWER}QUEUE POP{color.END} - deleted last entry in queue.'
    print(message)


def input_handle_play_show_x(player: vlc.MediaPlayer, s_que: ShowsQueue, input_str: str):
    arg = input_str[(len(PLAY_SHOW_COMMAND) + 1):]
    alias = s_que.add_entry_at_front_from_alias(player, arg)
    if alias == f'{CONFIG_GLOBAL_OPTIONS_LINE_STARTER}': # this is returned if there was an error
        message = f"{color.INPUT_ANSWER}PLAY {arg}{color.END} - failed to find a show, maybe no show found with a name similar to '{arg}', maybe its config line is faulty."
        print(message)
    else:
        message = f'{color.INPUT_ANSWER}PLAY {arg}{color.END} - playing {alias} now.'
        print(message)
        episode_full_restart_event.set() # with this event, nothing is printed and nothing happens when the episode finishes. Also it prevents deleting the first entry of the queue
        skip_straight_to_next_episode_event.clear() # avoid option 'episodes' causing bugs
        player.stop()


#def input_handle_play_episode_current(player: vlc.MediaPlayer):
#    pass
#def input_handle_play_episode_x(player: vlc.MediaPlayer, input_str: str):
#    pass
#def input_handle_previous_episode():
#    pass
#def input_handle_next_episode():
#    pass
#def input_handle_loop_episode():
#    pass
#def input_handle_loop_x_episode(input_str: str):
#    pass


def input_handle_end_of_episode(player: vlc.MediaPlayer):
    global debug_player_last_time

    current_time = player.get_time()
    message = f'{color.INPUT_ANSWER}END{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) to 5s close to end of episode.'
    print(message)

    goal_milli = player.get_length() - 5000
    debug_player_last_time = goal_milli
    player.set_time(goal_milli)
    print_now_at_time(player.get_time())


def input_handle_end_x_of_episode(player: vlc.MediaPlayer, input_str: str):
    global debug_player_last_time

    end_to = input_handle_number_input(input_str, END_COMMAND, 0)

    if end_to != None:

        current_time = player.get_time()
        message = f'{color.INPUT_ANSWER}END {end_to}{color.END} - Jump from current position {get_h_m_s_from_milliseconds(current_time)} ({floor(current_time / 1000)}s) to {end_to}s close to end of episode.'
        print(message)

        goal_milli = player.get_length() - (end_to * 1000)
        debug_player_last_time = goal_milli
        player.set_time(goal_milli)
        sleep(0.3)
        print_now_at_time(player.get_time())


def input_handle_suspend(player):
    handle_suspend(player)


def handle_suspend(player):
    global left_off_at_seconds

    suspend_event.set()
    if playing_episode_event.is_set():
        left_off_at_seconds = int(player.get_time() / 1000)
    else:
        left_off_at_seconds = 0

    player.stop()


def input_handle_suspend_queue(player):
    handle_suspend_queue(player)


def handle_suspend_queue(player):
    global left_off_at_seconds

    suspend_queue_event.set()
    if playing_episode_event.is_set():
        left_off_at_seconds = int(player.get_time() / 1000)
    else:
        left_off_at_seconds = 0

    player.stop()



def input_handle_shutdown():
    if shutdown_running_event.is_set():
        message = f"{color.INPUT_ANSWER}SHUTDOWN{color.END} - {shutdown_minutes} minutes left before automatic end of {PROGRAM_NAME}."
    else:
        message = f"{color.INPUT_ANSWER}SHUTDOWN{color.END} - missing argument, type '{SHUTDOWN_COMMAND} 7' to activate shutdown timer with 7 minutes, turning off {PROGRAM_NAME} in 7 minutes."
    print(message)


def input_handle_shutdown_x(player, og, input_str: str):
    global shutdown_minutes

    shutdown_min = input_handle_number_timestamp_input(input_str, SHUTDOWN_COMMAND)

    if shutdown_min != -1:

        if shutdown_min == 0:
            if shutdown_running_event.is_set():
                shutdown_running_event.clear()
                message = f'{color.INPUT_ANSWER}SHUTDOWN CLEAR{color.END} - turned off shutdown, was at {shutdown_minutes} minutes left.'
                shutdown_running_event.clear()
            else:
                message = f"{color.INPUT_ANSWER}SHUTDOWN CLEAR{color.END} - but the shutdown wasn't even turned on."

        # typed 'shutdown X', activate shutdown with X minutes, if X is int
        else:
            if shutdown_running_event.is_set():
                shutdown_minutes = shutdown_min
                message = f"{color.INPUT_ANSWER}SHUTDOWN {shutdown_minutes}{color.END} - updated shutdown timer to {shutdown_minutes} minutes. Type '{SHUTDOWN_COMMAND} 0' to cancel."
            else:
                handle_shutdown_x(player, shutdown_min, og.suspend_bool, og.suspend_queue_bool)
                message = ""

    if message:
        print(message)


def handle_shutdown_x(player, shutdown_mins, suspend_bool, suspend_queue_bool):
    global shutdown_minutes
    shutdown_minutes = shutdown_mins
    shutdown_thread = Thread(target=shutdown_start, args=(player, suspend_bool, suspend_queue_bool), daemon=True)
    shutdown_thread.start()
    shutdown_running_event.set()


def shutdown_start(player: vlc.MediaPlayer, global_suspend_bool: bool, global_suspend_queue_bool: bool):
    global shutdown_minutes
    shutdown_running_event.wait()

    if shutdown_minutes > 1:
        message = f"{color.WARNING}SHUTDOWN IN {shutdown_minutes} MINUTES{color.END} - {PROGRAM_NAME} automatically ends in {shutdown_minutes} minutes. Type '{SHUTDOWN_COMMAND} 0' to cancel. Type '{SHUTDOWN_COMMAND}' to display remaining time.\n"
        print(message)
        while shutdown_minutes > 1:

            for i in range(60): #sleep for a minute
                sleep(1)
                if not shutdown_running_event.is_set():
                    break
            shutdown_minutes -= 1

            if not shutdown_running_event.is_set():
                break

    if not shutdown_running_event.is_set():
        return

    message = f"{color.WARNING}SHUTDOWN IN 1 MINUTE{color.END} - {PROGRAM_NAME} automatically ends in 1 minute. Type '{SHUTDOWN_COMMAND} 0' to cancel.\n"
    print(message)

    for i in range(60): #sleep for a minute
        sleep(1)
        if not shutdown_running_event.is_set():
            break
    shutdown_minutes -= 1

    if not shutdown_running_event.is_set():
        return

    message = f"{color.WARNING}SHUTDOWN{color.END} - timer finished. Have a great day :-)\n"
    print(message)

    if global_suspend_queue_bool:
        handle_suspend_queue(player)
    elif global_suspend_bool:
        handle_suspend(player)
    else:
        exit_program_event.set()


def input_handle_last_episode():
    global last_episodes_for_today_counter

    if last_episodes_for_today_counter == 0: # was not turned on
        message = f"LAST - canceled, but was not turned on. Type '{LAST_EPISODE_COMMAND} 3' to automatically end {PROGRAM_NAME} after 3 episodes."
    else:
        message = f'LAST - canceled, was at {last_episodes_for_today_counter}.'
        last_episodes_for_today_counter = 0
    print(message)


def input_handle_last_episode_x(input_str):
    global last_episodes_for_today_counter

    last_int = input_handle_number_input(input_str, LAST_EPISODE_COMMAND, "", 0)

    if last_int != None:
        if last_int > 0:
            if last_int == 1:
                if playing_commercials_event.is_set():
                    message = f"{color.INPUT_ANSWER}LAST 1{color.END} - turn off {PROGRAM_NAME} after next episode."
                else:
                    message = f"{color.INPUT_ANSWER}LAST 1{color.END} - turn off {PROGRAM_NAME} after current episode."
            else:
                if playing_commercials_event.is_set():
                    message = f"{color.INPUT_ANSWER}LAST {last_int}{color.END} - turn off {PROGRAM_NAME} after {last_int} episodes."
                else:
                    message = f"{color.INPUT_ANSWER}LAST {last_int}{color.END} - turn off {PROGRAM_NAME} after {last_int} episodes, including currently running."
            last_episodes_for_today_counter = last_int
        else:
            message = f'LAST 0 - is ignored, use at least 1.'
        print(message)


def input_handle_skip_full_episode(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}SKIP FULL EPISODE{color.END} - skipping everything up to the next show\'s episode playback.'
    print(message)
    episode_stop_all_playback_event.set()
    episode_skipped_event.set()

    skip_straight_to_next_episode_event.clear() # cancel 'episodes' option
    skip_straight_to_next_episode_counter = 0 # cancel 'episodes' option

    player.stop()


def input_handle_finish_full_episode(player: vlc.MediaPlayer):
    if playing_opening_event.is_set() or playing_episode_event.is_set():
        message = f'{color.INPUT_ANSWER}FINISH FULL EPISODE{color.END} - skipping everything up to the next show\'s episode playback and updating config.'
    else:
        message = f'{color.INPUT_ANSWER}SKIP FULL EPISODE{color.END} - skipping everything up to the next show\'s episode playback.'
    print(message)
    episode_stop_all_playback_event.set()
    episode_finished_event.set()

    skip_straight_to_next_episode_event.clear() # cancel 'episodes' option
    skip_straight_to_next_episode_counter = 0 # cancel 'episodes' option

    player.stop()


def input_handle_skip_to_commercials(player: vlc.MediaPlayer):
    if playing_commercials_event.is_set():
        message = f'SKIP TO COMMERCIALS - already playing commercials, nothing happened.'
        print(message)
    else:
        message = f'SKIP TO COMMERCIALS - skipping current episode playback'
        print(message)
        episode_skipped_event.set() # this avoids changing current episode in the config
        episode_skip_to_commercial_event.set() # skips opening, episode and ending

        skip_straight_to_next_episode_event.clear() # cancel 'episodes' option
        skip_straight_to_next_episode_counter = 0 # cancel 'episodes' option

        player.stop()


def input_handle_commercials_global_override_x(sesh: OptionsSession, og: OptionsGlobal, input_str: str):
    cms_to = input_handle_number_input(input_str, COMMERCIALS_GLOBAL_OVERRIDE_COMMAND, COMMERCIALS_GLOBAL_OVERRIDE_C, 0)

    if cms_to != None:

        message = f"{color.INPUT_ANSWER}COMMERCIALS {cms_to}{color.END} - Set global commercials weight to {cms_to} for this session, overriding config's global option commercials value."
        print(message)

        sesh.commercial_weight_value_global = cms_to


def input_handle_resume(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}RESUME PLAYBACK{color.END} - resume playback. No effect if playback is already running.'
    print(message)
    player.set_pause(0)


def input_handle_hard_pause(player: vlc.MediaPlayer):
    message = f'{color.INPUT_ANSWER}HARD PAUSE{color.END} - pause playback. No effect if playback is already paused.'
    print(message)
    player.set_pause(1)


def input_handle_video_track_x(player: vlc.MediaPlayer, ep: ShowEpisode, input_str: str):
    track_int = input_handle_number_input(input_str, VIDEO_TRACK_COMMAND)

    if track_int != None:
        message = f'{color.INPUT_ANSWER}VIDEO {track_int}{color.END} - change video track of current playback to {track_int}.'
        print(message)

        if (player.video_set_track(track_int) != 0): # returns 0 if success, else -1
            message = f'{ep.config_alias} episode {ep.config_current_ep} has no video track number {track_int}, command has no effect.'
            print(message)


def input_handle_display_video_track(player: vlc.MediaPlayer):
    track_int = player.video_get_track()
    track_amount = player.video_get_track_count()
    if track_amount > 2:
        message = f'{color.INPUT_ANSWER}VIDEO{color.END} - player uses video track {track_int}, media has {track_amount - 1} video tracks.' # subtract one because the 'turned-off-track' -1 is also counted if at least one other track exists
    elif track_amount == 2:
        message = f'{color.INPUT_ANSWER}VIDEO{color.END} - player uses video track {track_int}, media has 1 video track.'
    else:
        message = f'{color.INPUT_ANSWER}VIDEO{color.END} - player uses video track {track_int}, media has {track_amount} video tracks.'

    print(message)


def input_handle_audio_track_x(player: vlc.MediaPlayer, ep: ShowEpisode, input_str: str):
    track_int = input_handle_number_input(input_str, AUDIO_TRACK_COMMAND)

    if track_int != None:
        message = f'{color.INPUT_ANSWER}AUDIO {track_int}{color.END} - change audio track of current playback to {track_int}.'
        print(message)

        if (player.audio_set_track(track_int) != 0): # returns 0 if success, else -1
            message = f'{ep.config_alias} episode {ep.config_current_ep} has no audio track number {track_int}, command has no effect.'
            print(message)


def input_handle_display_audio_track(player: vlc.MediaPlayer):
    track_int = player.audio_get_track()
    track_amount = player.audio_get_track_count()
    if track_amount > 2:
        message = f'{color.INPUT_ANSWER}AUDIO{color.END} - player uses audio track {track_int}, media has {track_amount - 1} audio tracks.' # subtract one because the 'turned-off-track' -1 is also counted if at least one other track exists
    elif track_amount == 2:
        message = f'{color.INPUT_ANSWER}AUDIO{color.END} - player uses audio track {track_int}, media has 1 audio track.'
    else:
        message = f'{color.INPUT_ANSWER}AUDIO{color.END} - player uses audio track {track_int}, media has {track_amount} audio tracks.'
    print(message)


def input_handle_subtitle_track_x(player: vlc.MediaPlayer, ep: ShowEpisode, input_str: str):
    track_int = input_handle_number_input(input_str, SUBTITLE_TRACK_COMMAND)

    if track_int != None:
        message = f'{color.INPUT_ANSWER}SUBTITLE {track_int}{color.END} - change subtitle track of current playback to {track_int}.'
        print(message)

        if (player.video_set_spu(track_int) != 0): # returns 0 if success, else -1
            message = f'{ep.config_alias} episode {ep.config_current_ep} has no subtitle track number {track_int}, command has no effect.'
            print(message)


def input_handle_display_subtitle_track(player: vlc.MediaPlayer):
    track_int = player.video_get_spu()
    track_amount = player.video_get_spu_count()
    if track_amount > 2:
        message = f'{color.INPUT_ANSWER}SUBTITLE{color.END} - player uses subtitle track {track_int}, media has {track_amount - 1} subtitle tracks.' # subtract one because the 'turned-off-track' -1 is also counted if at least one other track exists
    elif track_amount == 2:
        message = f'{color.INPUT_ANSWER}SUBTITLE{color.END} - player uses subtitle track {track_int}, media has 1 subtitle track.'
    else:
        message = f'{color.INPUT_ANSWER}SUBTITLE{color.END} - player uses subtitle track {track_int}, media has {track_amount} subtitle tracks.'
    print(message)


def input_handle_history(s_que: ShowsQueue):
    message_playbacks = deepcopy(s_que.started_playbacks)
    message_playbacks.insert(0, f"{color.BOLD}Files played so far:{color.END}")
    print("\n\t".join(message_playbacks))

    message_finished = [f"{x[0]} ep {x[1]}" for x in s_que.already_played]
    message_finished.insert(0, f"{color.BOLD}Episodes finished so far:{color.END}")
    print("\n\t".join(message_finished))


def input_handle_monitor_x(player: vlc.MediaPlayer, sesh: OptionsSession, ep_instance: ShowEpisode, input_str: str):
    if not monitoring_running_event.is_set():

        monitor_interval = input_handle_number_input(input_str, MONITOR_COMMAND, MONITOR_LIMIT_LOWER)

        if monitor_interval != None:

            message = f'{color.INPUT_ANSWER}TOGGLE MONITOR: ON{color.END} - prints modified status every {monitor_interval} seconds.'
            print(message)
            sesh.monitor_value = monitor_interval
            monitoring_player_on(player, ep_instance, monitor_interval)

    else:
        monitoring_player_off()
        message = f'{color.INPUT_ANSWER}TOGGLE MONITOR: OFF{color.END}'
        print(message)


def monitoring_player_on(player: vlc.MediaPlayer, ep_instance: ShowEpisode, interval: int):
    message = f'ACTIVATE MONITORING'
    print(message)
    monitoring_running_event.set()
    monitorThread = Thread(target=monitoring_player, args=(player, ep_instance, interval), daemon=True)
    monitorThread.start()


def monitoring_player_off():
    monitoring_running_event.clear()


def monitoring_player(player: vlc.MediaPlayer, ep_instance: ShowEpisode, interval: int):
    current_media_changed_counter = media_changed_counter
    while monitoring_running_event.is_set():
        if current_media_changed_counter == media_changed_counter:
            print_full_player_status(player, ep_instance, True)
            sleep(interval)
        else:
            break


def input_handle_monitor_jumps(player: vlc.MediaPlayer, ep_instance: ShowEpisode):
    if not monitoring_player_jumps_running_event.is_set():
        message = f'{color.INPUT_ANSWER}TOGGLE JUMP MONITOR: ON{color.END} - checks every 0.1 seconds if the player did an unusual jump in playback time. Prints results if anything notable happens.'
        print(message)
        monitoring_player_jumps_on(player, ep_instance)
    else:
        monitoring_player_jumps_off()
        message = f'{color.INPUT_ANSWER}TOGGLE JUMP MONITOR: OFF{color.END}'
        print(message)


def monitoring_player_jumps_on(player: vlc.MediaPlayer, ep_instance: ShowEpisode):
    message = f'ACTIVATE JUMP MONITORING'
    print(message)
    monitoring_player_jumps_running_event.set()
    monitorJumpsThread = Thread(target=monitoring_player_jumps, args=(player, ep_instance), daemon=True)
    monitorJumpsThread.start()


def monitoring_player_jumps_off():
    monitoring_player_jumps_running_event.clear()


def monitoring_player_jumps(player: vlc.MediaPlayer, ep_instance: ShowEpisode):
    # this monitor also reports "weird" playback time behaviour when there was an input, could have been one of the intentional playback time change commands like forward, back, end or jump
    last_length = player.get_length()
    last_time = player.get_time()
    current_media_changed_counter = media_changed_counter
    while monitoring_player_jumps_running_event.is_set():
        if current_media_changed_counter == media_changed_counter:
            sleep(0.1)
            current_time = player.get_time()
            current_length = player.get_length()
            print_full = False
            if current_time - last_time > 1000 or current_time < last_time:
                # something weird happened with current playback position
                message = f'Weird playback position: 0.1 seconds ago was {get_h_m_s_from_milliseconds(last_time)} / {last_time}ms, now is {get_h_m_s_from_milliseconds(current_time)} / {current_time}ms.'
                print(message)
                print_full = True
            if current_length < last_length - 1000 or current_length > last_length + 1000:
                # something weird happened with the mp3 length
                message = f'Weird player length: 0.1 seconds ago was {get_h_m_s_from_milliseconds(last_length)} / {last_length}ms, now is {get_h_m_s_from_milliseconds(current_length)} / {current_length}ms.'
                print(message)
                print_full = True

            if print_full:
                print_full_player_status(player, ep_instance, True)

            last_length = current_length
            last_time = current_time
        else:
            break


def input_handle_monitors_off():
    monitoring_running_event.clear()
    monitoring_player_jumps_running_event.clear()
    message = f'{color.INPUT_ANSWER}MONITORS OFF{color.END} - turned off all monitors'
    print(message)


def input_handle_debug_player_jumps(player: vlc.MediaPlayer):
    if not debug_player_jumps_running_event.is_set():
        message = f'{color.BOLD}ACTIVATE DEBUG PLAYER JUMPS{color.END}'
        print(message)
        debug_player_jumps_on(player)
    else:
        message = f'{color.BOLD}DEACTIVATE DEBUG PLAYER JUMPS{color.END}'
        print(message)
        debug_player_jumps_off()


def debug_player_jumps_on(player: vlc.MediaPlayer):
    debug_player_jumps_running_event.set()
    debugPlayerJumpsThread = Thread(target=debug_player_jumps, args=(player,), daemon=True)
    debugPlayerJumpsThread.start()


def debug_player_jumps_off():
    debug_player_jumps_running_event.clear()


def debug_player_jumps(player: vlc.MediaPlayer):
    global debug_player_last_time
    if player.get_state() == vlc.State.Playing:
        event_player_Stopped.clear()
        event_player_Playing.set()

    event_player_Playing.wait()
    message = f'{color.GREEN_BG}DEBUG PLAYER JUMPS READY{color.END}'
    print(message)

    while debug_player_jumps_running_event.is_set():
        event_player_Playing.wait()
        # setup
        event_player_LengthChanged_event.clear()
        debug_player_last_time = player.get_time()
        media_counter = media_changed_counter
        debugPlayerJumpsFulfillThread = Thread(target=debug_player_jumps_fulfill, args=(player,), daemon=True)
        debugPlayerJumpsFulfillThread.start()
        #message = f'{color.GREEN_BG}DEBUG PLAYER JUMPS READY{color.END}'
        #print(message)

        # update debug_player_last_time as long as the same file is still playing
        while debug_player_jumps_running_event.is_set():
            # this while loop breaks if the media was changed or debugging is turned off
            event_player_Playing.wait()
            if media_counter != media_changed_counter:
                break
            debug_player_last_time = max(debug_player_last_time, player.get_time())
            sleep(DEBUG_PLAYER_JUMPS_INTERVALL)


def debug_player_jumps_fulfill(player):
    media_counter = media_changed_counter
    length_normal = player.get_length()
    while debug_player_jumps_running_event.is_set() and media_counter == media_changed_counter:
        event_player_LengthChanged_event.wait()
        #ensure that media hasn't changed during wait. First condition is for media that has mildy fluctuating length all the time
        if (abs(length_normal - player.get_length()) > (DEBUG_PLAYER_JUMPS_ANOMALY_SENSITIVITY_SIZE_STANDARD * 1000)) and player.get_state() == vlc.State.Playing and media_counter == media_changed_counter:
            player_restart_event.set() # used to not change config, program handles player.stop() as episode finished and tries to set next episode in config
            player.stop()
            player.play()
            event_player_Playing.wait()
            player_restart_event.clear()
            player.set_time(debug_player_last_time + (DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD * 1000))
            print(f'{color.GREEN_BG}ENCOUNTERED PLAYER JUMP, RESTARTED AT {get_h_m_s_from_milliseconds(debug_player_last_time + 1000)}{color.END}')
        event_player_LengthChanged_event.clear()


def input_handle_media_lengths_analysis_quick():
    if analysis_running_event.is_set():
        message= f"{color.INPUT_ANSWER}STOP RUNNING ANALYSIS{color.END}"
        print(message)
        analysis_running_event.clear()
    else:
        message = f"{color.INPUT_ANSWER}QUICK LENGTHS ANALYSIS{color.END} - this might take a while, depending on the amount of files and your computer's CPU performance. If this slows down your laptop, (force) quit {PROGRAM_NAME}. The results will be displayed in your console once they are ready."
        print(message)

        analysisThread = Thread(target=handle_media_lengths_analysis, args=(MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD,), daemon=True)
        analysisThread.start()
        analysis_running_event.set()


def input_handle_media_lengths_analysis_full():
    if analysis_running_event.is_set():
        message= f"{color.INPUT_ANSWER}STOP RUNNING ANALYSIS{color.END}"
        print(message)
        analysis_running_event.clear()
    else:
        message = f"{color.INPUT_ANSWER}FULL LENGTHS ANALYSIS{color.END} - this might take a while, depending on the amount of files and your computer's CPU performance. If this slows down your laptop, (force) quit {PROGRAM_NAME}. The results will be displayed in your console once they are ready (~10 seconds per 50h of media)."
        print(message)

        analysisThread = Thread(target=handle_media_lengths_analysis, daemon=True)
        analysisThread.start()
        analysis_running_event.set()


def handle_media_lengths_analysis(quick_max_episodes: int=0):
    # get show and commercial lines from config
    config_show_lines = get_config_show_lines([COMMERCIAL_WEIGHT_OPTION_SHOW])
    config_commercial_lines = get_config_show_lines([COMMERCIAL_WEIGHT_OPTION_SHOW], True)

    analysis_counter = 0
    lines_amount = len(config_show_lines + config_commercial_lines)

    # get path_lines once and pass to the method below so it doesn't do it for every show_line
    path_lines = get_config_path_lines()

    total_shows_milli = 0
    total_commercials_milli = 0
    show_tuples = []
    commercial_tuples = []

    # loop through show lines, call media_lengths_analysis_show on the ShowEpisode instance to get the necessary info
    for line in config_show_lines:
        path_line = get_config_alias_path_line(line, path_lines)
        show = ShowEpisode(line, path_line, False, True)

        if show.is_valid:
            show_tuples.append(media_lengths_analysis_show(show, quick_max_episodes))
        else:
            print(f"Analysis: Failed to load config line, is not set up properly:{line}\n  or no correct path line: {path_line}")

        if not analysis_running_event.is_set():
            return

        analysis_counter += 1
        if analysis_counter % 5 == 0 or analysis_counter == lines_amount:
            printlog(f"Analysis progress: {analysis_counter}/{lines_amount}")

    # same for commercial lines
    for line in config_commercial_lines:
        path_line = get_config_alias_path_line(line, path_lines)
        commercial = ShowEpisode(line, path_line, False, True)

        if show.is_valid:
            commercial_tuples.append(media_lengths_analysis_show(commercial, quick_max_episodes))
        else:
            print(f"Analysis: Failed to load config line, is not set up properly:{line}\n  or no correct path line: {path_line}")

        if not analysis_running_event.is_set():
            return

        analysis_counter += 1
        if analysis_counter % 5 == 0 or analysis_counter == lines_amount:
            printlog(f"Analysis progress: {analysis_counter}/{lines_amount}")

    # analysis finished, build messages to print afterwards:

    message_shows = ["Shows:", "ALIAS                TOTAL     AVERAGE  EPISODES"]
    for s_t in show_tuples:
        alias = s_t[0]
        total_time = get_h_m_s_from_milliseconds(s_t[1])
        average_time = get_h_m_s_from_milliseconds(int(s_t[1] / s_t[2]))
        files_amount = s_t[2]
        message_shows.append(f"{alias: <20} {total_time: <9} {average_time: <8} {files_amount}")
        total_shows_milli += s_t[1]

    message_commercials = ["Commercials:"]
    for c_t in commercial_tuples:
        alias = c_t[0]
        total_time = get_h_m_s_from_milliseconds(c_t[1])
        average_time = get_h_m_s_from_milliseconds(int(c_t[1] / c_t[2]))
        files_amount = c_t[2]
        message_commercials.append(f"{alias: <20} {total_time: <9} {average_time: <8} {files_amount}")
        total_commercials_milli += c_t[1]

    # print messages
    print("\n  ".join(message_shows))
    print("\n  ".join(message_commercials))
    print(f"\nAll shows:       {get_h_m_s_from_milliseconds(total_shows_milli)}")
    print(f"All commercials: {get_h_m_s_from_milliseconds(total_commercials_milli)}")
    print(f"All files:       {get_h_m_s_from_milliseconds(total_shows_milli + total_commercials_milli)}")
    analysis_running_event.clear()


def media_lengths_analysis_show(show: ShowEpisode, quick_max_episodes: int=0) -> (str, int, int, int):
    """This method is useless if ffmpeg / ffprobe is not installed.
    """

    episodes_amount = show.config_last_ep + 1 - show.config_first_ep
    length_milli_total = 0

    if quick_max_episodes == 0:
        analyze_episodes = range(show.config_first_ep, show.config_last_ep + 1)
    else:
        # get quick_max_episodes many random ints from the possible episodes to analyze
        analyze_episodes = []
        while len(analyze_episodes) < quick_max_episodes:
            if len(analyze_episodes) == episodes_amount:
                break
            next_int = randint(show.config_first_ep, show.config_last_ep)
            if not next_int in analyze_episodes:
                analyze_episodes.append(next_int)

    for i in analyze_episodes:
        file_full_path = show.generate_full_episode_x_path(i)
        if file_full_path != "":
            length_milli = get_file_length_milli(file_full_path)
            if length_milli == -1:
                break
            elif length_milli == 0:
                episodes_amount -= 1
            else:
                length_milli_total += length_milli
        else:
            episodes_amount -= 1

    # extrapolate from lower analysis amount:
    if quick_max_episodes > 0 and len(analyze_episodes) < episodes_amount:
        length_milli_total = length_milli_total * (episodes_amount / len(analyze_episodes))

    return (show.config_alias, length_milli_total, episodes_amount)


def input_handle_about():
    print(MESSAGE_ABOUT)






##### PLAYER METHODS #####

### Main Player Methods ###

def play_start():

    # vlc stuff creating a player and setting media
    vlc_instance = vlc.Instance()
    player = vlc_instance.media_player_new()

    # attach player events
    player_attach_events(player)

    # load global options into object, create empty session options
    options_global_instance = OptionsGlobal(get_config_global_options_line())
    options_session = OptionsSession()

    #process some global startup options
    global_options_startup_handle(player, options_global_instance)

    # create show queue object
    shows_queue_instance = ShowsQueue(CONFIG_FULL_PATH)

    while True: # RAnimeTV is supposed to always be playing something, only interrupted by errors or pausing

        # get next episode and show queue handling
        show_episode_instance = generate_next_show_episode_instance(shows_queue_instance, options_global_instance)

        if show_episode_instance.is_valid:
            # here everything from the show_episode_instance will be handled and played, including all show options
            play_episode_full(player, options_session, show_episode_instance, options_global_instance, shows_queue_instance)


def generate_next_show_episode_instance(s_que: ShowsQueue, og: OptionsGlobal) -> ShowEpisode:
    if len(s_que.queue) > 0 and not episode_full_restart_event.is_set() and not skip_straight_to_next_episode_event.is_set():
        s_que.queue.pop(0) #first entry in the queue is the episode that is playing at the moment, so the one in the first place was just played in this case
    s_que.populate_automatic(og)

    ep_instance_from_queue = s_que.queue[0] # this ep_instance's current_episode might be wrong or its options or episode arguments might have been changed in the config by the user
    alias = ep_instance_from_queue.config_alias

    show_line, show_alias_path_line, alias_temp = get_config_random_show_and_path_line_from_aliases([alias])

    show_episode_instance = ShowEpisode(show_line, show_alias_path_line)

    return show_episode_instance


def play_episode_full(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue):

    # clear some threading events in case they are still set
    clear_threading_events_before_play_start()

    # announce episode
    message = f'\n{color.BOLD}{color.CYAN}Playing episode {ep.config_current_ep} of {ep.config_alias}.{color.END} Current Run: {ep.config_first_ep}-{ep.config_last_ep}.'
    print(message)

    # check if episode file exists
    if not check_episode_file_exists(ep.generate_full_episode_current_path()):
        return

    # playback flow: opening, episode, ending, commercials. with config handling after episode and ending
    play_opening_episode_ending(player, sesh, ep, og, s_que, "opening")
    play_opening_episode_ending(player, sesh, ep, og, s_que, "episode")

    handle_episode_finish(ep, s_que, no_rerun_bool_global=og.no_rerun_bool)

    play_opening_episode_ending(player, sesh, ep, og, s_que, "ending")

    handle_episode_finish_post_ending(og, s_que)

    play_commercials(player, sesh, ep, og, s_que)

    handle_episode_finish_post_commercials(ep, s_que)


def check_episode_file_exists(episode_file_full_path):
    if not os.path.isfile(episode_file_full_path):
        wait_seconds = 5
        message = f'{color.WARNING}Warning: no file found at {episode_file_full_path}{color.END}, skipping this episode, trying next show in {wait_seconds} seconds. Terminate program with Ctrl+C.\n'
        print(message)

        episode_error_event.set()
        sleep(wait_seconds)
        return False
    else:
        return True


def play_opening_episode_ending(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue, specifier: str):
    if not specifier in ["episode", "opening", "ending"]:
        raise Exception("Error in program code, play_opening_episode_ending's specifier has to be 'episode', 'opening' or 'ending'.")

    if play_skip_bool_handle(specifier, ep, og):
        return

    set_playing_events(specifier)
    if specifier == "episode":
        is_episode = True
        player_option_handle_episodes(ep) # handle 'episodes' show option
    else:
        is_episode = False

    # get file path
    file_full_path = player_get_full_file_path(specifier, ep, og)
    if file_full_path == "":
        return

    message = f'{color.PATH_PLAYBACK}{specifier} path: {file_full_path}{color.END}'
    print(message)
    # save playback path for 'history' command
    s_que.started_playbacks.append(file_full_path)

    # tell player to play file from path
    media = vlc.Instance().media_new_path(file_full_path)
    player.set_media(media)

    # play the player and apply options. some can be applied before starting, some need the player to be running
    player_handle_options_pre_play(player, sesh, ep, og, s_que, is_episode)
    player.play()

    if is_episode and ep.suspend_value != None:
        player_handle_suspend_resume(player, ep)

    player_handle_options_post_play(player, sesh, ep, og, s_que, is_episode)


    # input handling and waiting while opening is playing
    player_wait_input(player, sesh, ep, og, s_que)

    # clear events
    if suspend_event.is_set() or suspend_queue_event.is_set():
        return # do this to keep the playing_xyz_event set
    else:
        playing_opening_event.clear()
        playing_episode_event.clear()
        playing_ending_event.clear()
        if is_episode and not episode_skipped_event.is_set():
            episode_finished_event.set()


def player_get_full_file_path(specifier: str, ep: ShowEpisode, og: OptionsGlobal):
    # get file path
    if specifier == "episode":
        file_full_path = ep.generate_full_episode_current_path()
    elif specifier == "opening":
        file_full_path = ep.generate_full_op_ed_path(specifier, og.opening_filetype_string)
    else:
        file_full_path = ep.generate_full_op_ed_path(specifier, og.ending_filetype_string)

    # check if returned path is empty
    if specifier != "episode":
        if file_full_path == "":
            message = f'{color.WARNING}Warning: no {specifier} file found for {ep.config_alias} ep {ep.config_current_ep}, skipping {specifier}.\n'
            print(message)
            return ""

        # check if file exists locally
        if not os.path.isfile(file_full_path):
            message = f'{color.WARNING}Warning: no file found at {file_full_path}{color.END}, skipping {specifier}.\n'
            print(message)
            return ""

    return file_full_path


def player_handle_suspend_resume(player: vlc.MediaPlayer, ep: ShowEpisode):
    message = f"{color.WARNING}RESUME SUSPEND{color.END} - resuming playback of {ep.config_alias} ep {ep.config_current_ep} at {ep.suspend_value - SUSPEND_BACK_SIZE_STANDARD if ep.suspend_value >= 10 else 0} seconds."
    print(message)
    update_config_show_line(ep, update_print=False, left_off_at=None, delete_left_off_at=True)
    player.set_time((ep.suspend_value - SUSPEND_BACK_SIZE_STANDARD) * 1000)


def play_commercials(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue):
    """Disclaimer: this is the most spaghetti method in this whole program. This whole functionality was not intended at first and sloppily added at the end of the initial development while the only developer working on it had to do his taxes, change his bank account and insurances, get a new phone, change his official place of residence, write a bachelor's thesis and try not to die from the novel coronavirus during a worsening climate crisis. If you read this chuck me an e-mail if you want and tell me about your troubles.
    """

    if play_skip_bool_handle("commercial", ep, og):
        return

    set_playing_events("commercial")

    # sesh.commercial_weight_value_global overrides og.commercial_weight_value_global if existing
    if sesh.commercial_weight_value_global != None:
        cm_global_weight = sesh.commercial_weight_value_global
    else:
        cm_global_weight = og.commercial_weight_value_global

    #cm_global_weight will be used later, do not change. cm_weight_globla will be decremented along the way
    cm_weight_global = cm_global_weight

    # get cm lines and error handling
    cm_lines = get_config_show_lines([COMMERCIAL_WEIGHT_OPTION_SHOW], True)
    if len(cm_lines) == 0:
        message = f"NO COMMERCIALS FOUND - global {COMMERCIAL_WEIGHT_OPTION_GLOBAL} weight is set but there are currently no show lines in your config that are marked as commercials. Can't play any commercials until this is fixed."
        print(message)
        return

    # generate ShowEpisode objects from cm_lines, store in list which is then sorted and chosen from. I feel like all of this method we are in right now is complete spaghetti and was not thought of when the whole ShowEpisode concept was designed
    cms_list_all = []
    for line in cm_lines:
        path_line = get_config_alias_path_line(line)
        cm_instance = ShowEpisode(line, path_line)
        if not cm_instance.is_valid or cm_instance.commercial_weight_value_show == None:
            message = f"""{color.ERROR}Failed to load{color.END} commercial line {line}. Config line not valid or no valid commercial option and value set."""
            continue
        if cm_instance.commercial_weight_value_show <= cm_weight_global:
            cm_tuple = (cm_instance.commercial_weight_value_show, cm_instance)
            cms_list_all.append(cm_tuple)

    if len(cms_list_all) == 0:
        message = f"{color.ERROR}NO COMMERCIALS FOUND{color.END} - global {COMMERCIAL_WEIGHT_OPTION_GLOBAL} weight is set but there are currently no properly set up show lines in your config that are marked as commercials with commercial weight lower than global commercial weight. Can't play any commercials until this is fixed."
        print(message)
        return

    sorted_cms_all = sorted(cms_list_all, key=lambda tup: tup[0])

    # check if all commercials have weight 0, if so, then commercial global weight will never be exhausted, resulting in commercials forever. This doesn't happen if all cms are limited by commercial-max
    if sorted_cms_all[-1][0] == 0: # weight of last one in list is 0
        all_cms_limited = True

        for cm in sorted_cms_all: # print warning if a cm is included without commercial-max limit
            if cm[1].commercial_max_value_show == None:
                message = f"{color.WARNING}ONLY COMMERCIALS WITH 0 WEIGHT{color.END} - this is ok, but you should know that this means that commercials will be played until you type '{SKIP_FULL_EPISODE_COMMAND}'."
                print(message)
                all_cms_limited = False
                break

        if all_cms_limited: # all cms have a commercial-max value
            weights_only_zero = False
            # now fill cms_chosen_list with commercials until limits commercial-max reached
            cms_chosen_list = []
            while len(sorted_cms_all) > 0:
                cms_chosen_list.append(sorted_cms_all[0])
                if sorted_cms_all[0][1].commercial_max_value_show > 1:
                    sorted_cms_all[0][1].commercial_max_value_show -= 1
                else:
                    sorted_cms_all.pop(0)

            shuffle(cms_chosen_list)
            helper_print_upcoming_commercials(cms_chosen_list, cm_global_weight)

        else:
            weights_only_zero = True

            random_cm_int = randint(0, len(sorted_cms_all) - 1)
            cm_chosen = sorted_cms_all[random_cm_int]
            cms_chosen_list = [cm_chosen] # just add one, add one more later after every playback

    # not only commercials with weight 0:
    else:
        weights_only_zero = False

        # choose commercials for later playback
        cms_chosen_list = []
        drop_limit = 20
        sorted_cms_possible = deepcopy(sorted_cms_all)
        while cm_weight_global > 0 and drop_limit > 0:

            # only go through ones that have small enough weight, listed in sorted_cms_possible
            highest_possible_index = -1
            for i in range(0, len(sorted_cms_possible)):
                if sorted_cms_possible[i][0] <= cm_weight_global * 2:
                    highest_possible_index = i
                else:
                    break
            if highest_possible_index == -1:
                break
            while len(sorted_cms_possible) > highest_possible_index + 1:
                sorted_cms_possible.pop() #deletes last item from list

            # also drop all commercials that have their commercial-max value exhausted. this value is decremented every time the commercial is picked down below
            # REMINDER: cm lists here consist of cm_tuple = (cm_instance.commercial_weight_value_show, cm_instance)
            cms_to_remove = []
            for cm in sorted_cms_possible:
                if cm[1].commercial_max_value_show != None:
                    if cm[1].commercial_max_value_show < 1:
                        cms_to_remove.append(cm)
            for cm in cms_to_remove:
                sorted_cms_possible.remove(cm)
            if len(sorted_cms_possible) == 0:
                break

            # now choose commercials, but first prioritze commercials with a commercial_min_value_show, decrement this value whenever the commercial is picked, shuffle cms to not always pick first in sorted list first
            sorted_cms_possible_with_min_value = []
            cm_chosen = None
            for cm in sorted_cms_possible:
                if cm[1].commercial_min_value_show != None:
                    if cm[1].commercial_min_value_show > 0 and cm[1].commercial_weight_value_show < cm_weight_global:
                        sorted_cms_possible_with_min_value.append(cm)

            if len(sorted_cms_possible_with_min_value) > 0:
                shuffle(sorted_cms_possible_with_min_value)
                cm_chosen = sorted_cms_possible_with_min_value[0]
                irremovable_bool = True

            if cm_chosen == None:
                # choose one randomly, if there was none chosen because of commercial_min_value_show
                random_cm_int = randint(0, len(sorted_cms_possible) - 1)

                cm_chosen = sorted_cms_possible[random_cm_int]

                # probability drop
                if og.probability_value_global == None:
                    probability_global = 100
                else:
                    probability_global = og.probability_value_global
                if cm_chosen[1].probability_value_show != None or probability_global < 100:
                    if cm_chosen[1].probability_value_show == None:
                        prob = og.probability_value_global # use global if no show probability set
                    else:
                        prob = cm_chosen[1].probability_value_show
                    random_int = randint(1, 100)
                    if prob < random_int:
                        drop_limit -= 1
                        continue

                irremovable_bool = False

            # decrement cm_chosen's commercial_min_value_show and commercial_max_value_show in sorted_cms_possible
            cm_index = sorted_cms_possible.index(cm_chosen)
            if sorted_cms_possible[cm_index][1].commercial_min_value_show != None:
                if sorted_cms_possible[cm_index][1].commercial_min_value_show > 0:
                    sorted_cms_possible[cm_index][1].commercial_min_value_show -= 1
            if sorted_cms_possible[cm_index][1].commercial_max_value_show != None:
                if sorted_cms_possible[cm_index][1].commercial_max_value_show > 0:
                    sorted_cms_possible[cm_index][1].commercial_max_value_show -= 1

            cms_chosen_list.append((cm_chosen[0], cm_chosen[1], irremovable_bool)) # irremovable_bool == True means it can't be removed in the next phase
            cm_weight_global -= cm_chosen[0]

            drop_limit = 20


        # this happens if the last chosen commercial in the previous loop had a weight exceeding global weight limit. In this case remove the smallest one that fixes it at once if possible, but not the last one, else multiple
        if cm_weight_global < 0:

            sorted_cms_chosen = sorted(cms_chosen_list, key=lambda tup: tup[0])
            for i in range(1, len(sorted_cms_chosen)):
                if sorted_cms_chosen[i][0] > abs(cm_weight_global):
                    if sorted_cms_chosen[i-1][0] == 0:
                        cm_to_remove = sorted_cms_chosen[i]
                        if cm_to_remove[2] == True:
                            continue
                    else:
                        cm_to_remove = sorted_cms_chosen[i-1]
                        if cm_to_remove[2] == True:
                            continue
                    if cm_to_remove in cms_chosen_list:
                        cms_chosen_list.remove(cm_to_remove)
                        cm_weight_global += cm_to_remove[0]
            # none found to remove, so remove the last one after all
            if cm_weight_global < 0:
                cm_weight_global += cms_chosen_list[-1][0]
                cms_chosen_list.pop()

            if cm_weight_global > 0:
                #get all cms with weight above 0 and below cm_weight_global and add them if weight left
                cms_left_to_pick = []
                for cm in sorted_cms_all:
                    if cm[0] > 0 and cm[0] <= cm_weight_global:
                        cms_left_to_pick.append(cm)

                #now add them to the cms_chosen_list starting from reversed order
                while True and cm_weight_global > 0:
                    for i in reversed(range(0, len(cms_left_to_pick))):
                        if cms_left_to_pick[i][0] <= cm_weight_global:
                            cms_chosen_list.append(cms_left_to_pick[i])
                            cm_weight_global -= cms_left_to_pick[i][0]
                            break # start from top again to add higher_weight ones first
                    else: # if none left to add, exit while loop
                        break


        ## fix commercials that would play the same thing
        # first locate duplicates, for now remove from chosen commercials, add again later after they are changed
        cms_chosen_copy = deepcopy(cms_chosen_list)
        cms_chosen_duplicates = []
        duplicate_indexes = []
        for i in range(0, len(cms_chosen_list)):
            cm_tuple = cms_chosen_list[i]
            # first remove element from cms_chosen_copy
            for cm in cms_chosen_copy:
                if cm_tuple[1].config_alias == cm[1].config_alias:
                    cms_chosen_copy.remove(cm)
                    break
            # then if there is still an element same as cm_tuple in cms_chosen_copy, then add it to the duplicates list
            for cm in cms_chosen_copy:
                if cm_tuple[1].config_alias == cm[1].config_alias:
                    cms_chosen_duplicates.append(cm_tuple)
                    duplicate_indexes.append(i)
                    break

        # remove duplicates from cms_chosen_list for now, add later once the current_ep of cm_tuple has been changed
        for dupl_iter in reversed(duplicate_indexes):
            cms_chosen_list.pop(dupl_iter)

        # now apply spaghetti fix for duplicates, increasing their current episode until they are not longer duplicates (with loop limit to avoid infinite loop), then adding them back to cms_chosen_list
        prev_alias = ""
        prev_ep = 0
        for i in range(0, len(cms_chosen_duplicates)):
            cm_dupl = deepcopy(cms_chosen_duplicates[i])

            loop_limit = 0
            comp_list = helper_get_commercial_tuple_comparison_list(cms_chosen_list)

            while (cm_dupl[1].config_alias, cm_dupl[1].config_current_ep) in comp_list and loop_limit < 6:
                if cm_dupl[1].random_bool_show and loop_limit < 4:
                        cm_dupl[1].randomize_current_ep()
                else:
                    if prev_alias == cm_dupl[1].config_alias:
                        # create new tuple consisting of ShowEpisode with current_ep from last one, then create one with next episode with the ep.method
                        cm_new_instance_temp = deepcopy(cm_dupl[1])
                        cm_new_instance_temp.config_current_ep = prev_ep
                        cm_dupl = (cm_dupl[0], cm_new_instance_temp.generate_instance_next_episode(), cm_dupl[2])
                        prev_ep = cm_dupl[1].config_current_ep
                    else:
                        cm_dupl = (cm_dupl[0], cm_dupl[1].generate_instance_next_episode(), cm_dupl[2])
                        prev_alias = cm_dupl[1].config_alias
                        prev_ep = cm_dupl[1].config_current_ep
                loop_limit += 1

            cms_chosen_list.append(cm_dupl)


        # shuffle the list, see https://stackoverflow.com/questions/976882/shuffling-a-list-of-objects
        shuffle(cms_chosen_list)

        # information on the upcoming commercials printing:
        helper_print_upcoming_commercials(cms_chosen_list, cm_global_weight)


    ## play the commercials
    while len(cms_chosen_list) > 0:
        if episode_stop_all_playback_event.is_set() or episode_full_restart_event.is_set():
            break

        cm_instance = cms_chosen_list[0][1]
        cms_chosen_list.pop(0)

        # get path and check if commercial file exists
        cm_file_full_path = cm_instance.generate_full_episode_current_path()
        if not os.path.isfile(cm_file_full_path):
            message = f'{color.WARNING}Warning: no file found at {cm_file_full_path}{color.END}, skipping commercial.\n'
            print(message)
            return
        else:
            # save playback path for 'history' command
            s_que.started_playbacks.append(cm_file_full_path)

        message = f'{color.PATH_PLAYBACK}commercial path: {cm_file_full_path}{color.END}'
        print(message)

        media = vlc.Instance().media_new_path(cm_file_full_path)
        player.set_media(media)

        player_handle_options_pre_play(player, sesh, cm_instance, og, s_que, True)
        player.play()
        player_handle_options_post_play(player, sesh, cm_instance, og, s_que, True)

        update_config_show_line(cm_instance, False)

        if weights_only_zero:
            random_cm_int = randint(0, len(sorted_cms_all) - 1)
            cm_chosen = sorted_cms_all[random_cm_int]
            cm_instance = cm_chosen[1].generate_instance_next_episode()
            if cm_instance.random_bool_show:
                cm_instance.randomize_current_ep()
            cms_chosen_list = [(cm_instance.commercial_weight_value_show, cm_instance)]

        # input handling and waiting while commercial is playing
        player_wait_input(player, sesh, cm_instance, og, s_que)

        if suspend_event.is_set() or suspend_queue_event.is_set():
            break

    playing_commercials_event.clear()


def helper_get_commercial_tuple_comparison_list(cm_tuple_list: [(int, ShowEpisode, bool)]) -> [(int, str)]:
    return_list = []
    for cm in cm_tuple_list:
        new_entry = (cm[1].config_alias, cm[1].config_current_ep)
        return_list.append(new_entry)
    return return_list


def helper_print_upcoming_commercials(cms_chosen_list, cm_global_weight):
    message = []
    cm_total_length_milli = 0
    cm_total_weight = 0
    for entry in cms_chosen_list:
        cm_total_weight += entry[1].commercial_weight_value_show
        try:
            file_length_milli = get_file_length_milli(entry[1].generate_full_episode_current_path())
            cm_total_length_milli += file_length_milli
            file_length = get_h_m_s_from_milliseconds(file_length_milli)

            commercial_name = entry[1].generate_episode_current_path_base()
            message.append(f'{entry[0]} {file_length} {commercial_name}')

        except Exception:
            commercial_name = entry[1].generate_episode_current_path_base()
            message.append(f'{entry[0]} {commercial_name}')

    if cm_total_length_milli != 0:
        start_message = f'{color.BOLD}Upcoming commercials {cm_total_weight}/{cm_global_weight} ({get_h_m_s_from_milliseconds(cm_total_length_milli)}):{color.END}'
    else:
        start_message = f'{color.BOLD}Upcoming commercials {cm_total_weight}/{cm_global_weight}:{color.END}'
    message.insert(0, start_message)

    print("\n\t".join(message))


def get_file_length_milli(filename):
    try:
        result = run(["ffprobe", "-v", "error", "-show_entries",
                                "format=duration", "-of",
                                "default=noprint_wrappers=1:nokey=1", filename],
            stdout=PIPE,
            stderr=STDOUT)
        return int(float(result.stdout) * 1000)
    except Exception:
        if os.path.isfile(filename):
            message = "Need to install ffmpeg to get the length of a file."
            print(message)
            return -1
        else:
            return 0




### Config Update / Episode Done Methods ###

def handle_episode_finish(ep: ShowEpisode, s_que: ShowsQueue, no_rerun_bool_global: bool=False):
    if episode_full_restart_event.is_set() or suspend_queue_event.is_set():
        pass

    elif suspend_event.is_set():
        if playing_episode_event.is_set():
            message = f'{color.WARNING}SUSPEND{color.END} - saving where you left off.'
            print(message)
            update_config_show_line(ep, False, left_off_at_seconds)
        else:
            message = f'SUSPEND - no effect outside of an episode.'
            print(message)
        exit_program_event.set()

    elif episode_skipped_event.is_set(): #this should be checked before episode_finished_event as both might be set
        message = f'EPISODE SKIPPED - config file was not changed, current episode same as before.\n'
        print(message)

    elif episode_error_event.is_set():
        message = f'PLAYER ERROR - an error caused the episode to stop. Trying to play next show in 5 seconds. Press {color.BOLD}Ctrl+C{color.END} or press ENTER a few times to terminate early.'
        print(message)
        sleep(5)
        episode_error_event.clear()

    elif episode_finished_event.is_set():
        message = f'EPISODE FINISHED - stop of player was signaled.'
        print(message)

        s_que.already_played.append((ep.config_alias, ep.config_current_ep)) # used for global options 'uunique', 'unique' and 'history'

        # create boolean that decides if a show line is disabled in config file (by adding a # at the beginning) if the last episode just finished playing
        if option_handle_bools(ep.no_rerun_bool, no_rerun_bool_global):
            if ep.random_bool_show:
                disable_show_bool = False
            else:
                disable_show_bool = True
        else:
            disable_show_bool = False
        
        update_config_show_line(ep, disable_show_bool=disable_show_bool)

    else:
        message = f'UNKNOWN - episode is finished, but what happened? Press {color.BOLD}Ctrl+C{color.END} or press ENTER a few times to terminate early. Playing next show in 3 seconds, I hope...'
        print(message)
        sleep(3)


def handle_episode_finish_post_ending(og: OptionsGlobal, s_que: ShowsQueue):
    global last_episodes_for_today_counter

    if skip_straight_to_next_episode_event.is_set():
        return # used for 'episodes' option

    # handle 'last' command and option
    if episode_finished_event.is_set(): # only count down if episode not skipped
        if last_episodes_for_today_counter > 0:
            last_episodes_for_today_counter -= 1
            if last_episodes_for_today_counter == 0:
                message = "Last episode for now. Have a great day :-)"
                print(message)
                exit_program_event.set()
            else:
                message = f"{last_episodes_for_today_counter} shows left to watch. Type '{LAST_EPISODE_COMMAND}' to cancel."
                print(message)

    # handle 'unique' and 'uunique' option
    if og.unique_shows_bool_global or og.hard_unique_shows_bool_global:

        # get all show_aliases
        filter_options = []
        if not og.commercials_play_bool_global:
            filter_options.append(COMMERCIAL_WEIGHT_OPTION_SHOW)

        show_aliases = get_config_all_show_aliases(filter_options)

        # check if all aliases have already been played
        all_shows_played = False
        for alias in s_que.already_played:
            if alias[0] in show_aliases:
                show_aliases.remove(alias[0])
        if show_aliases == 0:
            all_shows_played = True

        if all_shows_played:
            if og.hard_unique_shows_bool_global:
                message = f"""ALL SHOWS PLAYED ONCE - option '{HARD_UNIQUE_SHOWS_OPTION_GLOBAL}' was set, ending {PROGRAM_NAME} now.
                Have a great day :-)"""
                print(message)
                exit_program_event.set()
            else: # unique_shows_bool_global True
                s_que.already_played.clear()


def handle_episode_finish_post_commercials(ep, s_que):
    if suspend_queue_event.is_set():
        if playing_episode_event.is_set():
            message = f'{color.WARNING}SUSPEND QUEUE{color.END} - saving where you left off and current queue.'
            print(message)
            update_config_show_line(ep, False, left_off_at_seconds)
        else:
            message = f'{color.WARNING}SUSPEND QUEUE{color.WARNING} - saving current queue.'
            print(message)
        handle_save_config_queue_line(ep, s_que)
        exit_program_event.set()

    elif suspend_event.is_set():
        if playing_episode_event.is_set():
            message = f'{color.WARNING}SUSPEND{color.END} - saving where you left off.'
            print(message)
            update_config_show_line(ep, False, left_off_at_seconds)
        else:
            message = f'SUSPEND - no effect outside of an episode.'
            print(message)
        exit_program_event.set()


def update_config_show_line(ep: ShowEpisode, update_print: bool=True, left_off_at: int=None, delete_left_off_at: bool=False, disable_show_bool: bool=False):
    """Updates config file with next episode of current show or handles suspend.

    If left_off_int == None, then this method will try to find the config show line that fits the passed ShowEpisode and update that line's current episode to be the next one.
    If left_off_at is an integer != None, then the current episode will not be changed, but the seconds where the player left_off_at will be saved.
    """

    ### get contents of config_file
    with open(CONFIG_FULL_PATH, encoding=ENCODING_CONFIG_FILE) as f:
        content = f.readlines()

    # create new config file content
    new_content = []
    update_message = ""
    line_updated_boolean = False
    for line in content:
        if ep.config_alias in line and not "=" in line and not line_updated_boolean:

            # only overwrite current episode in config, this allows changing the rest of the config line during playback without it being overwritten. overwrite whole line if parsing to integers doesn't work
            line_split = line.split(CONFIG_FIELD_SEPARATOR)

            disable_show_line_bool = False

            try:
                if left_off_at == None and delete_left_off_at == False:
                    config_first = int(line_split[CONFIG_POSITION_SHOW_FIRST_EP])
                    if line_split[CONFIG_POSITION_SHOW_LAST_EP].strip().lower() == "last":
                        config_last = ep.config_last_ep
                    else:
                        config_last = int(line_split[CONFIG_POSITION_SHOW_LAST_EP])

                    # handle option no-reruns, set bool here, handle below after new_line is created
                    if ep.config_current_ep == config_last and option_handle_bools(ep.no_rerun_bool, disable_show_bool):
                        disable_show_line_bool = True

                    new_current_entry = "\t" + ep.get_next_episode_str(config_first, config_last, ep.config_current_ep, len(line_split[CONFIG_POSITION_SHOW_FIRST_EP].strip()))

                else:
                    if delete_left_off_at:
                        new_current_entry = f"\t{ep.config_current_ep}"
                    else:
                        new_current_entry = f"\t{ep.config_current_ep}{LEFT_OFF_AT_SEPARATOR}{left_off_at}"

                new_line = replace_entry_in_line(line, CONFIG_POSITION_SHOW_CURRENT_EP, new_current_entry, CONFIG_FIELD_SEPARATOR) + "\n"
            except:
                new_line = ep.generate_config_show_line_full(True, left_off_at)

            # handle option no-reruns, bool was set in the try-statement before
            if disable_show_line_bool:
                new_line = '#' + new_line

            new_content.append(new_line)

            if update_print:
                try:
                    next_ep = int(new_line[:new_line.index("#")].split(CONFIG_FIELD_SEPARATOR)[CONFIG_POSITION_SHOW_CURRENT_EP])
                except:
                    next_ep = int(new_line.split(CONFIG_FIELD_SEPARATOR)[CONFIG_POSITION_SHOW_CURRENT_EP])
                update_message = f'Updated {ep.config_alias} in config, next episode will be {next_ep}.\n'

            line_updated_boolean = True
        else:
            new_content.append(line)

    # if line can't be found in config anymore
    if not line_updated_boolean:
        if left_off_at == None:
            new_content.append(ep.generate_config_show_line_full(True))
        else:
            new_content.append(ep.generate_config_show_line_full(False, left_off_at))

    # overwrite config file
    with open(CONFIG_FULL_PATH, 'r+', encoding=ENCODING_CONFIG_FILE) as f:
        f.seek(0)
        f.truncate()
        f.write("".join(new_content))
        f.close()

    if update_print:
        print(update_message)


def replace_entry_in_line(line: str, position: int, new_entry: str, separator: str) -> str:
    comment = ""
    try:
        comment = " " + line[line.index("#"):].strip("\n")
        line = line[:line.index("#")] # cut off everything from '#' on
    except:
        pass
    line_split = line.strip("\n").split(separator)
    new_line_left = separator.join(line_split[:position])
    new_line_right = separator.join(line_split[position + 1:])
    if new_line_left == "":
        new_line = separator.join([new_entry, new_line_right])
    elif new_line_right == "":
        new_line = separator.join([new_line_left, new_entry])
    else:
        new_line = separator.join([new_line_left, new_entry, new_line_right])
    return new_line + comment


def handle_save_config_queue_line(ep: ShowEpisode, s_que: ShowsQueue): #TODO
    # get contents of config_file
    with open(CONFIG_FULL_PATH, encoding=ENCODING_CONFIG_FILE) as f:
        content = f.readlines()

    # delete queue line in config if still existing (shouldn't happen, queue line is deleted when starting RAnimeTV)
    for line in content:
        if line.startswith(CONFIG_SUSPEND_QUEUE_LINE_STARTER):
            content.remove(line)

    # create queue line and add to beginning of config file
    queue_aliases = []
    if len(s_que.queue) > 0:
        for s in s_que.queue:
            queue_aliases.append(s.config_alias)

    # remove first entry from list. Don't if opening was running. Remove if episode was running, because the suspend command/option already saves it in the corresponding show line. Remove if ending or commercials were playing bc episode was finished.
    if len(queue_aliases) > 0:
        if not playing_opening_event.is_set():
            if ep.config_alias == queue_aliases[0]:
                queue_aliases.pop(0)

    if len(queue_aliases) > 0:
        queue_line = f"{CONFIG_SUSPEND_QUEUE_LINE_STARTER} {'; '.join(queue_aliases)}\n"

        content.insert(0, queue_line)

        # overwrite config file
        with open(CONFIG_FULL_PATH, 'r+', encoding=ENCODING_CONFIG_FILE) as f:
            f.seek(0)
            f.truncate()
            f.write("".join(content))
            f.close()


### Player Options handler methods ###
## all other options are handled somewhere else in the code

def global_options_startup_handle(player: vlc.MediaPlayer, og: OptionsGlobal):
    global shutdown_minutes
    global last_episodes_for_today_counter

    if og.debug_player_jumps_bool:
        debug_player_jumps_on(player)

    if og.shutdown_value_global:
        handle_shutdown_x(player, og.shutdown_value_global, og.suspend_bool, og.suspend_queue_bool)

    if og.last_episodes_value_global:
        last_episodes_for_today_counter = og.last_episodes_value_global


def player_option_handle_episodes(ep: ShowEpisode):
    # handles 'episodes' show option
    global skip_straight_to_next_episode_counter

    if ep.multiple_episodes_value_show != None:
        if skip_straight_to_next_episode_counter > 0:
            skip_straight_to_next_episode_counter -= 1
            if skip_straight_to_next_episode_counter == 1: # done with this show, this will be the last time
                skip_straight_to_next_episode_counter = 0
                skip_straight_to_next_episode_event.clear()
        elif ep.multiple_episodes_value_show > 1:
            skip_straight_to_next_episode_counter = ep.multiple_episodes_value_show
            skip_straight_to_next_episode_event.set()
    else:
        skip_straight_to_next_episode_event.clear()


def player_handle_options_pre_play(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue, is_episode: bool):
    player_option_handle_speed(player, sesh, ep, og)
    player_option_handle_mute(player, sesh, ep, og, s_que)
    player_option_handle_fullscreen(player, ep, og)
    player_option_handle_queue_display(ep, og, s_que, is_episode)


def player_handle_options_post_play(player: vlc.MediaPlayer, sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue, is_episode: bool):
    event_player_Playing.wait()

    player_option_handle_volume(player, sesh, ep, og)
    player_option_handle_pause(player, ep, og)
    player_option_handle_jump(player, ep, og, is_episode)
    player_option_handle_video_track(player, ep, og, is_episode)
    player_option_handle_audio_track(player, ep, og, is_episode)
    player_option_handle_subtitle_track(player, ep, og, is_episode)
    player_option_handle_monitor(player, sesh, ep, og)
    player_option_handle_monitor_jump(player, ep, og)
    player_option_handle_status(player, ep, og)


def player_option_handle_speed(player, sesh, ep, og):
    player.set_rate(option_handle_speed(sesh, ep, og))


def player_option_handle_mute(player, sesh, ep, og, s_que):
    player.audio_set_mute(option_handle_mute(sesh, ep, og, s_que))


def player_option_handle_fullscreen(player, ep, og):
    player.set_fullscreen(option_handle_fullscreen(ep, og))


def player_option_handle_queue_display(ep, og, s_que, is_episode):
    if is_episode and ep.commercial_weight_value_show == None:
        if option_handle_bools(ep.queue_display_bool, og.queue_display_bool):
            message = s_que.get_queue_table()
            print(message)


def player_option_handle_volume(player, sesh, ep, og):
    set_volume = option_handle_volume(sesh, ep, og)
    sleep(0.05) # setting player volume immediately after player.play() will fail if the program doesn't wait here for at least 0.01-0.02 seconds. 0.02 will usually always be fine, but turned up to 0.05 just to be sure
    player.audio_set_volume(set_volume)


def player_option_handle_status(player, ep, og):
    if option_handle_bools(ep.status_bool, og.status_bool):
        sleep(0.1) #give status jump and suspend enough time to change playback position first
        print_full_player_status(player, ep, True)


def player_option_handle_pause(player, ep, og):
    if option_handle_bools(ep.pause_bool, og.pause_bool):
        player.set_pause(1)
    else:
        player.set_pause(0)


def player_option_handle_jump(player, ep, og, is_episode):
    if is_episode and ep.suspend_value == None:
        if ep.jump_value:
            handle_jump_forward_helper(player, ep.jump_value * 1000)
        elif og.jump_value:
            handle_jump_forward_helper(player, og.jump_value * 1000)


def player_option_handle_video_track(player, ep, og, is_episode):
    if ep.video_track_int_show != None and is_episode:
        if (player.video_set_track(ep.video_track_int_show) != 0): # returns 0 if success, else -1
            message = f'{ep.config_alias} episode {ep.config_current_ep} has no video track number {ep.video_track_int_show}, option has no effect.'


def player_option_handle_audio_track(player, ep, og, is_episode):
    if ep.audio_track_int_show != None and is_episode:
        if (player.audio_set_track(ep.audio_track_int_show) != 0): # returns 0 if success, else -1
            message = f'{ep.config_alias} episode {ep.config_current_ep} has no audio track number {ep.audio_track_int_show}, option has no effect.'


def player_option_handle_subtitle_track(player, ep, og, is_episode):
    if ep.subtitle_track_int_show != None and is_episode:
        if (player.video_set_spu(ep.subtitle_track_int_show) != 0): # returns 0 if success, else -1
            message = f'{ep.config_alias} episode {ep.config_current_ep} has no subtitle track number {ep.subtitle_track_int_show}, option has no effect.'


def player_option_handle_monitor(player, sesh, ep, og):
    if monitoring_running_event.is_set():
        monitoring_player_off()
        sleep(0.5)

    if sesh.monitor_value:
        sleep(0.5) #give status jump and suspend enough time to change playback position first
        monitoring_player_on(player, ep, sesh.monitor_value)
    elif ep.monitor_value:
        sleep(0.5)
        monitoring_player_on(player, ep, ep.monitor_value)
    elif og.monitor_value:
        sleep(0.5)
        monitoring_player_on(player, ep, og.monitor_value)


def player_option_handle_monitor_jump(player, ep, og):
    if monitoring_player_jumps_running_event.is_set():
        monitoring_player_jumps_off()
        sleep(0.5)

    if option_handle_bools(ep.monitor_jump_bool, og.monitor_jump_bool):
        monitoring_player_jumps_on(player, ep)
    else:
        monitoring_player_jumps_off()


def option_handle_bools(show_bool: bool, global_bool: bool):
    if show_bool != None:
        return show_bool
    elif global_bool != None:
        return global_bool
    else:
        return False

def option_handle_volume(sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal) -> int:
    if (sesh.volume_value != None) or (ep.volume_value != None) or (og.volume_value != None):
        sesh_volume_mult = (sesh.volume_value / 100) if sesh.volume_value else 1
        ep_volume_mult = (ep.volume_value / 100) if ep.volume_value else 1
        og_volume_mult = (og.volume_value / 100) if og.volume_value else 1
        result_volume = int(100 * sesh_volume_mult * ep_volume_mult * og_volume_mult)
        return result_volume
    else:
        return 100 # 100 is 100%

def option_handle_speed(sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal) -> float:
    if sesh.speed_value:
        return (sesh.speed_value / 100)
    elif ep.speed_value:
        return (ep.speed_value / 100)
    elif og.speed_value:
        return (og.speed_value / 100)
    else:
        return 1 # 1 is 100%

def option_handle_mute(sesh: OptionsSession, ep: ShowEpisode, og: OptionsGlobal, s_que: ShowsQueue):
    if sesh.mute_bool or option_handle_bools(ep.mute_bool, (og.mute_bool and len(s_que.started_playbacks) == 1)): # global mute option only affects first playback of the session
        return 1 # 1 activates mute, 0 turns off
    else:
        return 0

def option_handle_fullscreen(ep: ShowEpisode, og: OptionsGlobal):
    fullscreen_boolean = option_handle_bools(ep.fullscreen_bool, og.fullscreen_bool)
    if fullscreen_boolean:
        return 1
    else:
        return 0




### Player Event Handling and Setup ###

def player_attach_events(player: vlc.MediaPlayer):
    events = player.event_manager()
    events.event_attach(vlc.EventType.MediaPlayerEncounteredError, player_event_EncounteredError)
    events.event_attach(vlc.EventType.MediaPlayerStopped, player_event_Stopped)
    # player events introduced and mainly used for debug player jumps mode:
    events.event_attach(vlc.EventType.MediaPlayerLengthChanged, player_event_LengthChanged)
    events.event_attach(vlc.EventType.MediaPlayerMediaChanged, player_event_MediaChanged)
    events.event_attach(vlc.EventType.MediaPlayerPaused, player_event_Paused)
    events.event_attach(vlc.EventType.MediaPlayerPlaying, player_event_Playing)


def clear_threading_events_before_play_start():
    playing_opening_event.clear()
    playing_episode_event.clear()
    playing_ending_event.clear()
    playing_commercials_event.clear()

    episode_finished_event.clear()
    episode_skipped_event.clear()
    episode_stop_all_playback_event.clear()
    episode_skip_to_commercial_event.clear()
    episode_error_event.clear()
    player_restart_event.clear()
    episode_full_restart_event.clear()

    exit_program_event.clear()


def play_skip_bool_handle(specifier: str, ep: ShowEpisode, og: OptionsGlobal):
    if specifier == "opening":
        return (not option_handle_bools(ep.opening_bool, og.opening_bool)) or episode_stop_all_playback_event.is_set() or episode_full_restart_event.is_set() or skip_straight_to_next_episode_event.is_set() or suspend_event.is_set() or suspend_queue_event.is_set() or ep.suspend_value != None
    elif specifier == "episode":
        return episode_stop_all_playback_event.is_set() or episode_full_restart_event.is_set() or episode_skip_to_commercial_event.is_set() or suspend_event.is_set() or suspend_queue_event.is_set()
    elif specifier == "ending":
        return (not option_handle_bools(ep.ending_bool, og.ending_bool)) or episode_stop_all_playback_event.is_set() or episode_error_event.is_set() or episode_full_restart_event.is_set() or episode_skip_to_commercial_event.is_set() or skip_straight_to_next_episode_event.is_set() or suspend_event.is_set() or suspend_queue_event.is_set()
    elif specifier == "commercial":
        return og.commercial_weight_value_global == None or og.commercial_weight_value_global == 0 or episode_stop_all_playback_event.is_set() or episode_error_event.is_set() or episode_full_restart_event.is_set() or skip_straight_to_next_episode_event.is_set() or suspend_event.is_set() or suspend_queue_event.is_set()


def set_playing_events(specifier: str):
    playing_opening_event.clear()
    playing_episode_event.clear()
    playing_ending_event.clear()
    playing_commercials_event.clear()

    if specifier == "opening":
        playing_opening_event.set()
    elif specifier == "episode":
        playing_episode_event.set()
    elif specifier == "ending":
        playing_ending_event.set()
    elif specifier == "commercial":
        playing_commercials_event.set()


def player_event_Stopped(event):
    event_player_Playing.clear() # used for debug player jumps
    if not player_restart_event.is_set(): # this event is set when player.stop(), player.play() is called to restart the episode
        event_player_Stopped.set()
        input_registered_event.set() # this is necessary to stop the loop with handle_input() from waiting


def player_event_EncounteredError(event):
    if playing_opening_event.is_set():
        pass
    elif playing_episode_event.is_set():
        episode_error_event.set()
    elif playing_ending_event.is_set():
        pass
    elif playing_commercials_event.is_set():
        pass
    print(f'    {color.GREEN} ER{color.END}{color.BOLD}{color.BLUE}RO{color.END}{color.BOLD}{color.RED}R P{color.END}{color.BOLD}{color.PURPLE}LA{color.END}{color.BOLD}{color.CYAN}YE{color.END}{color.BOLD}{color.YELLOW}R{color.END}') # this should never happen, i.e. if this happens, there is a bug or very corrupt media file


def player_event_LengthChanged(event):
    if event_player_Playing.is_set(): # only set event if state is playing, length changes before playing (loading file?)
        event_player_LengthChanged_event.set()


def player_event_MediaChanged(event):
    global media_changed_counter
    event_player_Playing.clear()
    media_changed_counter += 1


def player_event_Paused(event):
    event_player_Playing.clear()


def player_event_Playing(event):
    event_player_Stopped.clear()
    event_player_Playing.set()




##### Config Reading and Filtering #####

def get_config_filtered_lines(config_path: str=None) -> [str]:
    """Get all usable lines from config file, trimmed comments and whitespace.

    All lines' tabs and multiple spaces are reduced to one space. Leading and trailing spaces are deleted. It also cuts off everything in a line after a '#'. Empty lines are not returned.
    """
    if config_path == None:
        config_full_path = CONFIG_FULL_PATH
    else:
        config_full_path = config_path

    with open(config_full_path, encoding=ENCODING_CONFIG_FILE) as f:
        content = f.readlines()
    # remove leading and trailing whitespace characters including newlines
    content = [x.strip() for x in content]

    ### filter comments and empty lines, reduce whitespace
    filtered_lines = []
    for line in content:
        try:
            line = line[:line.index("#")] # cut off everything from '#' on
        except:
            pass
        # replace tabs and multiple spaces with one space
        line = whitespace_reduce(line)
        if line != '':
            filtered_lines.append(line)

    return filtered_lines


def get_config_global_options_line(config_path: str=None) -> str:
    if config_path == None:
        config_full_path = CONFIG_FULL_PATH
    else:
        config_full_path = config_path

    config_filtered_lines = get_config_filtered_lines(config_full_path)

    for line in config_filtered_lines:
        if line.startswith(CONFIG_GLOBAL_OPTIONS_LINE_STARTER):
            return line
    else:
        return None


def get_config_show_lines(option_filters_exclude: [str]=None, filters_include_only: bool=False) -> [str]:
    """Get all lines that contain a show's information and options.

    Lines specifying a show's alias and path connection are not returned.
    If str-list option_filters_exclude is passed, then all lines that have one of their options equal to one of the list's elements will be excluded.
    If bool filters_only is passed as True, then the opposite will happen, returning only the lines with at least one of the options of the str-list. filters_only has no effect if option_filters_exclude isn't passed.
    """
    config_filtered_lines = get_config_filtered_lines()
    show_lines = []
    for line in config_filtered_lines:
        if not '=' in line and not CONFIG_GLOBAL_OPTIONS_LINE_STARTER in line and not CONFIG_SUSPEND_QUEUE_LINE_STARTER in line: # '=' only appears in lines setting aliases
            show_lines.append(line)

    # open config and exit program if config is too empty
    if len(show_lines) == 0:
        handle_config_useless()

    # filter show_lines if any filters passed
    if option_filters_exclude != None:
        if len(option_filters_exclude) > 0:

            filtered_show_lines = []

            for line in show_lines:
                filter_option_found = False
                line_split = line.split(CONFIG_FIELD_SEPARATOR)

                for i in range(4, len(line_split)):
                    line_component = line_split[i]
                    comp_split = line_component.split(":")

                    if len(comp_split) == 2 and whitespace_reduce(comp_split[0]) in option_filters_exclude:
                        filter_option_found = True
                        break

                if filters_include_only:
                    if filter_option_found == True:
                        filtered_show_lines.append(line)
                else:
                    if filter_option_found == False:
                        filtered_show_lines.append(line)

            return filtered_show_lines

    #else
    return show_lines


def get_config_left_off_at_alias() -> str:
    alias = ""
    show_lines = get_config_show_lines()
    for line in show_lines:
        line_split = line.split(CONFIG_FIELD_SEPARATOR)
        if len(line_split) >= 4:
            if LEFT_OFF_AT_SEPARATOR in line_split[CONFIG_POSITION_SHOW_CURRENT_EP]:
                alias = whitespace_reduce(line_split[CONFIG_POSITION_SHOW_ALIAS])
                break
    return alias


def get_config_random_show_and_path_line_from_aliases(aliases: [str]) -> (str, str, str):

    config_show_lines = get_config_show_lines()

    alias_number = randint(0, len(aliases) - 1)
    alias = aliases[alias_number]

    show_lines = search_alg_match_show_lines(alias, config_show_lines)
    show_lines_len = len(show_lines)

    if show_lines_len == 0:
        show_line = ""
        alias_return = ""
    else:
        if show_lines_len == 1:
            show_line = show_lines[0]
        else:
            show_number = randint(0, show_lines_len - 1) # ignoring probability option of shows here
            show_line = show_lines[show_number]
        alias_return = whitespace_reduce(show_line.split(CONFIG_FIELD_SEPARATOR)[0])

    show_alias_path_line = get_config_alias_path_line(show_line)
    return show_line, show_alias_path_line, alias_return


def get_config_all_show_aliases(option_filters_exclude: [str]=None, filters_include_only: bool=False) -> [str]:
    """Returns all show aliases from given config file as strings.
    """

    config_show_lines = get_config_show_lines(option_filters_exclude, filters_include_only)

    show_aliases = []
    for line in config_show_lines:
        alias = line.split(CONFIG_FIELD_SEPARATOR)[CONFIG_POSITION_SHOW_ALIAS]
        show_aliases.append(alias)

    return show_aliases


def get_config_unique_filtered_show_aliases(shows_queue: [ShowsQueue], option_filters_exclude: [str]=None, filters_include_only: bool=False) -> [str]:
    """Returns all show aliases that are not present in the current ShowsQueue.queue and not in the ShowsQueue.already_played
    """

    aliases = get_config_all_show_aliases(option_filters_exclude, filters_include_only)

    # remove already played
    for show in shows_queue.already_played:
        show_alias = show[0]
        if show_alias in aliases:
            aliases.remove(show_alias)

    # remove if in current queue
    for show_episode in shows_queue.queue:
        show_alias = show_episode.config_alias
        if show_alias in aliases:
            aliases.remove(show_alias)

    return aliases


def get_config_path_lines() -> [str]:
    """Get all lines that contain a show's alias and path connection.

    Only lines specifying a show's alias and path connection are returned.
    """
    config_filtered_lines = get_config_filtered_lines()
    path_lines = []
    for line in config_filtered_lines:
        if '=' in line: # '=' only appears in lines setting aliases
            path_lines.append(line)

    return path_lines


def get_config_alias_path_line(show_line: str, path_lines=None) -> str:
    if path_lines == None:
        path_lines = get_config_path_lines()

    alias_path_line = ""
    show_alias = show_line.split(CONFIG_FIELD_SEPARATOR)[CONFIG_POSITION_SHOW_ALIAS].strip()
    for line in path_lines:
        if show_alias.lower() in line.lower():
            alias_path_line = line
            break

    return alias_path_line


def search_alg_match_show_lines(input_alias: str, config_lines: [str]):
    possible_show_lines = [] # input_alias doesn't have to be the full alias. If input_alias doesn't match with any show_line's alias, then it will try to be matched to the words in the show_lines' aliases
    found_something_bool = False

    search_phase = 1
    # phases become looser with growing integer value, only increasing if nothing matched yet
    ### This was the first concept for the search phases, might be changed since then by now, but this should give a basic idea of the search algorithms intentions:
    # phase 1: try exact match, this is done for example by the automatic queue building, where the program had saved ShowEpisode instances created out of config show lines earlier. Can result in multiple matches if config contains duplicate show_aliases.
    # phase 2: same as 1, but ignore case (small/capital letters).
    # phase 3: split show lines' alias into words consisting only of latin letters (a-z, A-Z) and see if one of the words starts with input_alias (if itself only consists of one such word). Ignores case.
    # phase 4: same as phase 3, but now not with startswith, but only checks if contained in.
    # phase 5: only if input_alias string is more than one latin alphabet word. Same as phase 3, but iterating over all words of input_alias string.
    # phase 6: same as phase 5, but replace the "3" with a "4" in its description

    if input_alias.strip() == "":
        return []

    # these are the numbers of the search_phases where the strings are split and no non-alphabet chars are replaced. This is used to reduce redundancy and improve extendability
    phases1 = [3, 4, 5, 6]
    sizee = len(phases1)
    phases2 = [x + sizee for x in phases1] # list comprehension (<- for reference)
    phases_pairs = list(map(lambda a, b: [a, b], phases1, phases2))

    # with every while-loop the possible_show_lines will only keep the ones with the best matches from the loop (beginning from search_phase 3), once a match was found
    check_lines = config_lines

    while True:
        matched_index_tuple_list = [[x] for x in check_lines]
        for line in check_lines:

            show_alias = line.split(CONFIG_FIELD_SEPARATOR)[CONFIG_POSITION_SHOW_ALIAS]

            # exact match 1: 1
            if search_phase == 1 and input_alias == show_alias:
                possible_show_lines.append(line)

            # exact match 1: 1 ignore case
            elif search_phase == 2 and input_alias.lower() == show_alias.lower():
                possible_show_lines.append(line)

            elif search_phase in phases1 + phases2:
                if search_phase in phases1:
                    show_words = show_alias.split()
                    input_words = input_alias.split()
                else:
                    # replace all characters, that are not standard latin letters a-z or A-Z, with spaces, using regex
                    show_words = sub("[^(a-zA-z]", " ",  show_alias).split()
                    input_words = sub("[^(a-zA-z]", " ",  input_alias).split()


                for input_word in input_words:
                    for show_word in show_words:

                        # exact match n : n
                        if search_phase in phases_pairs[0]:
                            if show_word == input_word:

                                matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_lines, line, search_phase)

                        # exact match n : n ignore case
                        elif search_phase in phases_pairs[1]:
                            if show_word.lower() == input_word.lower():

                                matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_lines, line, search_phase)

                        # starts with n : n ignore case
                        elif search_phase in phases_pairs[2]:
                            if show_word.lower().startswith(input_word.lower()):

                                matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_lines, line, search_phase)

                        # is in n : n ignore case
                        elif search_phase in phases_pairs[3]:
                            if input_word.lower() in show_word.lower():

                                matched_index_tuple_list = helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_lines, line, search_phase)

        # stop if there were exact matches during first two search phases
        if search_phase < phases1[0] and len(possible_show_lines) > 0:
            break

        # remove elements without tuples
        matched_index_tuple_list = [x for x in matched_index_tuple_list if len(x) > 1]

        if len(matched_index_tuple_list) > 0:
            found_something_bool = True
            # evaluate weights, lower is better, then more tuples is better. Throw out worse matches here. This is kinda spaghetti
            while True and len(matched_index_tuple_list) > 1:
                a = matched_index_tuple_list[0]
                len_a = len(a)

                for b in matched_index_tuple_list[1:]:
                    temp_len = min(len_a, len(b))
                    temp_ind = 1
                    keep_str = ""

                    while temp_ind < temp_len:
                        if a[temp_ind][0] < b[temp_ind][0]:
                            keep_str = "a"
                            break
                        if a[temp_ind][0] > b[temp_ind][0]:
                            keep_str = "b"
                            break
                        if a[temp_ind][1] < b[temp_ind][1]:
                            keep_str = "a"
                            break
                        if a[temp_ind][1] > b[temp_ind][1]:
                            keep_str = "b"
                            break
                        temp_ind += 1
                    else:
                        if len_a > len(b):
                            keep_str = "a"
                        if len_a < len(b):
                            keep_str = "b"

                    if keep_str == "a":
                        matched_index_tuple_list.remove(b)
                        break
                    if keep_str == "b":
                        matched_index_tuple_list.remove(a)
                        break
                else:
                    break

            # remove weight tuples
            possible_show_lines = [x[0] for x in matched_index_tuple_list]

        # last search_phase is done
        if search_phase == phases2[-1]:
            break

        if found_something_bool == True:
            if len(possible_show_lines) == 1:
                break
            if len(possible_show_lines) == 0:
                possible_show_lines = check_lines
                break
            else:
                check_lines = possible_show_lines

        search_phase += 1

    return possible_show_lines


def helper_handle_matched_list(matched_index_tuple_list, input_words, input_word, show_words, show_word, check_lines, line, search_phase):

    index_input_word = input_words.index(input_word)
    index_show_word = show_words.index(show_word)
    index_tuple = (index_input_word, index_show_word)

    index_check = check_lines.index(line)

    matched_index_tuple_list.insert(index_check, matched_index_tuple_list[index_check] + [index_tuple])
    matched_index_tuple_list.pop(index_check + 1)

    return matched_index_tuple_list




### Some Useful Small Methods ###

def whitespace_reduce(string: str) -> str:
    string = sub('\t', ' ', string)
    string = sub(' +', ' ', string).strip()
    return string


def create_config_if_not_existing():
    if not os.path.isfile(CONFIG_FULL_PATH):
        with open(CONFIG_FULL_PATH, 'w', encoding=ENCODING_CONFIG_FILE) as configfile:
            sample_config_content = """# this is some exemplary config content. Everything in a line after a '#' will be ignored, similar to commenting in programming. See the config help in your terminal to learn more about how to set up a config file.
# Death Note;\t01; 37;\t13
# JoJo's 5;\t22; 39;\t22

# Death Note = /home/user/Videos/Favorite Anime/DeathNote/Death-Note- ; -rip.mp3
# JoJo's 5 = /home/user/Desktop/JoJo/Part 5/JJBA_GW_ ; .mp4"""
            configfile.write(sample_config_content)
            configfile.close()
            handle_config_useless()


def handle_config_useless():
    message = f'Config file not set up. Opening config file and displaying how to set it up in 5 seconds.'
    print(message)
    sleep(5)
    open_file(CONFIG_FULL_PATH)
    print(MESSAGE_CONFIG_HELP)
    exit_program_event.set()




##### Script Start #####
def main():

    create_config_if_not_existing()

    # input stuff
    inputThread = Thread(target=read_keyboard_input, daemon=True)
    inputThread.start()

    # main thread doing all the stuff
    playThread = Thread(target=play_start, daemon=True)
    playThread.start()

    # this threading Event is set when the string EXIT_COMMAND for ending the program is passed during playback input
    exit_program_event.wait()

    end_message = f'{color.HIGHLIGHT}Turn Off {PROGRAM_NAME}{color.END}'
    print(end_message)


if __name__ == "__main__":
    main()