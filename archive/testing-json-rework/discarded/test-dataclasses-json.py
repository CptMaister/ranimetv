
from re import sub  # substitute string parts with regex search, used to reduce whitespace
import os, sys # file path processing, handle args, detect current operating system, open dir in explorer

import PySimpleGUI as sg
from json import (load as jsonload, dump as jsondump)

from dataclasses import dataclass
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class GlobalConfig:
    current_user_id: int
    next_user_id: int
    users: list
    






def load_json_to_dict(json_file_path) -> dict:
    try:
        with open(json_file_path, 'r') as f:
            dict_from_json = jsonload(f)
    except Exception as e:
        # TODO: handle unexpected missing files
        sg.popup_quick_message(f'exception {e}', f'No json file found at {json_file_path}... please do not change the json-files manually from outside the GUI. If this happened unexpectedly, please contact me: {CONTACT_EMAIL} ...or try to fix the code yourself, see {LINK_TO_GITLAB_REPO}', keep_on_top=True, background_color='red', text_color='white')
    return dict_from_json

def save_json_complete(json_file_path, dict_to_json):
    with open(json_file_path, 'w') as f:
        jsondump(dict_to_json, f)
    print("save_json_complete erledigt")


test_dict = load_json_to_dict("../user-config-1.json")


# from https://goodcode.io/articles/python-dict-object/
# class DictObj(object):
#     def __init__(self, d):
#         self.__dict__ = d

# from https://joelmccune.com/python-dictionary-as-object/
class DictToObj:
    def __init__(self, in_dict:dict):
        assert isinstance(in_dict, dict)
        for key, val in in_dict.items():
            if isinstance(val, (list, tuple)):
                setattr(self, key, [DictToObj(x) if isinstance(x, dict) else x for x in val])
            else:
                setattr(self, key, DictToObj(val) if isinstance(val, dict) else val)

# self written, derived from DictToObj
class ObjToDict:
    def __init__(self, in_obj: object):
        assert isinstance(in_obj, object)
        for key, val in in_obj.__dict__.items():
            if isinstance(val, (list, tuple)):
                self.__dict__[key] = [ObjToDict(x).__dict__ if isinstance(x, DictToObj) else x for x in val]
            else:
                self.__dict__[key] = ObjToDict(val).__dict__ if isinstance(val, DictToObj) else val


tobj = DictToObj(test_dict)
tdict = ObjToDict(tobj).__dict__

save_json_complete("test-dict-ob-dict-save.json", tdict)


