
from re import sub  # substitute string parts with regex search, used to reduce whitespace
import os, sys # file path processing, handle args, detect current operating system, open dir in explorer

import PySimpleGUI as sg
from json import (load as jsonload, dump as jsondump)



def load_json_to_dict(json_file_path) -> dict:
    try:
        with open(json_file_path, 'r') as f:
            dict_from_json = jsonload(f)
    except Exception as e:
        # TODO: handle unexpected missing files
        sg.popup_quick_message(f'exception {e}', f'No json file found at {json_file_path}... please do not change the json-files manually from outside the GUI. If this happened unexpectedly, please contact me: {CONTACT_EMAIL} ...or try to fix the code yourself, see {LINK_TO_GITLAB_REPO}', keep_on_top=True, background_color='red', text_color='white')
    return dict_from_json

def save_json_complete(json_file_path, dict_to_json):
    with open(json_file_path, 'w') as f:
        jsondump(dict_to_json, f)
    print("save_json_complete erledigt")


test_dict = load_json_to_dict("../global-config.json")






class JsonKeys:
    GCurrentUserId = 'currentUserId'
    GNextUserId = 'nextUserId'
    GUsers_Dicts = 'users'
    GUsersId = 'id'
    GUsersName_String = 'name'
    GCurrentBasePath_Index = 'currentBasePath'
    GBasePaths_StrArray = 'basePaths'
    GUseBasePath_Bool = 'useBasePaths'              #default False
    GWindowSettings_Dict = 'windowSettings'
    GWindowSettingsGuiTheme_String = 'guiTheme'     #default TODO
    GWindowSettingsLanguage_String = 'language'     #default "en"
    GWindowSettingsFontSize_Int = 'fontSize'        #default TODO 
    UCurrentProfile_Id = 'currentProfile'
    UIdManagement_Dict = 'idManagement'
    UIdManagementNextProfileId = 'nextProfileId'
    UIdManagementNextShowId = 'nextShowId'
    UExcludeFilesEndingWith = 'excludeFilesEndingWith'  #TODO: implement
    UProfiles_Dicts = 'profiles'
    UProfilesId = 'profileId'
    UProfilesName_String = 'profileName'
    UProfilesSettings_Dict = 'profileSettings'
    UProfilesStartWithShows = 'startWithShows'       #default [], kinda overrides suspendedQueue, ignores playAllShows, profileInclude, profileExclude
    UProfilesSuspendedQueue_IdArray = 'suspendedQueue'
    UProfilesPlayAllShows_Bool = 'playAllShows'      #default False, overrides include and exclude
    UProfilesInclude_IdArray = 'profilesInclude'             #default []
    UProfilesExclude_IdArray = 'profilesExclude'             #default [], overpowers include
    # former user (global) player settings, now part of profile:
    UProfilesCommercialsPlay_Bool = 'commercialsPlay'           #default True
    UProfilesCommercialsStartWeight_Value = 'commercialsStartWeight'    #default 0  #TODO: implement
    UProfilesCommercialsBreakWeight_Value = 'commercialsBreakWeight'    #default 0
    UProfilesProbability_Value = 'probability'           #default 100
    UProfilesShutdown_Value = 'shutdown'                 #default None
    UProfilesShowsLimit_Value = 'showsLimit'             #default None   # formerly called 'lastEpisodes'
    UProfilesUniqueShows_Bool = 'uniqueShows'            #default False
    UProfilesHardUniqueShows_Bool = 'hardUniqueShows'    #default False
    UShows_Dicts = 'shows'
    UShowsId = 'showId'
    UShowsName_String = 'showName'
    UShowsDoNotUseBasePath_Bool = 'doNotUseBasePath'    #default False
    UShowsDirectoryPath_String = 'directoryPath'
    UShowsActiveProfiles_IdArray = 'activeProfiles'
    UShowsFirstEpisode_Int = 'firstEpisode'
    UShowsLastEpisode_Int = 'lastEpisode'
    UShowsCurrentEpisode_Int = 'currentEpisode'
    UShowsSuspendedTimeSeconds_Int = 'suspendedTimeSeconds'
    UShowsSettings_Dict = 'showSettings'
    USPSRandom_Bool = 'random'                          #default False
    USPSMultipleEpisodes_Value = 'multipleEpisodes'     #default 1
    USPSProbability_Value = 'probability'               #default None
    USPSCommercialWeight_Value = 'commercialWeight'     #default None
    USPSCommercialMax_Value = 'commercialMax'           #default None
    USPSCommercialMin_Value = 'commercialMin'           #default 0
    USPSVideoTrack_Int = 'videoTrack'
    USPSAudioTrack_Int = 'audioTrack'
    USPSSubtitleTrack_Int = 'subtitleTrack'
    UCPSOpening_Bool = 'opening'            #default False
    UCPSEnding_Bool = 'ending'              #default False
    UCPSStatus_Bool = 'status'              #default False  #TODO GUI not needed
    UCPSPause_Bool = 'pause'                #default False
    UCPSVolume_Value = 'volume'             #default 100
    UCPSJump_Value = 'jump'                 #default 0
    UCPSSpeed_Value = 'speed'               #default 100
    UCPSMute_Bool = 'mute'                  #default False
    UCPSFullscreen_Bool = 'fullscreen'      #default False
    UCPSQueueDisplay_Bool = 'queueDisplay'  #default False  #TODO: GUI not needed
    UCPSSuspend_Bool = 'suspend'            #default False  #TODO: make default True
    UCPSSuspendQueue_Bool = 'suspendQueue'  #default False  #TODO: make default True
    UCPSNoRerun_Bool = 'noRerun'            #default False
    UCPSMonitor_Value = 'monitor'           #default False  #TODO: GUI not needed
    UCPSMonitorJump_Bool = 'monitorJump'    #default False  #TODO: GUI put into planned status bar??
    UCPSDebugPlayerJumps_Bool = 'debugPlayerJumps'  #default False  #TODO: make default True
    SVolume_Value = 'volumeValue'
    SSpeed_Value = 'speedValue'
    SMute_Bool = 'muteBool'
    SMonitor_Seconds = 'monitorValue'
    SCommercialWeight_Value = 'commercialWeightValueGlobal'
    QQueue_IdArray = 'queue'
    QAlreadyPlayed_IdArray = 'alreadyPlayedIds'
    QStartedPlaybacksPaths_StrArray = 'startedPlaybacksPaths'



class ConfigGlobal:
    def __init__(self, g_json: dict):
        self.current_user_id = g_json.get(JsonKeys.GCurrentUserId, None)
        self.next_user_id = g_json.get(JsonKeys.GNextUserId, None)
        self.current_base_path = g_json.get(JsonKeys.GCurrentBasePath_Index, None)
        self.use_base_paths = g_json.get(JsonKeys.GUseBasePath_Bool, None)
        self.users = list(map(ConfigGlobalUser, g_json.get(JsonKeys.GUsers_Dicts, None)))
        self.base_paths = g_json.get(JsonKeys.GBasePaths_StrArray, None)
        self.window_settings = ConfigGlobalWindowsettings(g_json.get(JsonKeys.GWindowSettings_Dict, None))


class ConfigGlobalUser:
    def __init__(self, gu_json: dict):
        self.userId = gu_json.get(JsonKeys.GUsersId, None)
        self.userName = gu_json.get(JsonKeys.GUsersName_String, None)


class ConfigGlobalWindowsettings:
    def __init__(self, gw_json: dict):
        self.gui_theme = gw_json.get(JsonKeys.GWindowSettingsGuiTheme_String, None)
        self.language = gw_json.get(JsonKeys.GWindowSettingsLanguage_String, None)
        self.font_size = gw_json.get(JsonKeys.GWindowSettingsFontSize_Int, None)


class ObjToDict: #to get the dict, take the resulting object 'thing' like this to really get it as dict: thing.__dict__
    def __init__(self, in_obj: object):
        assert isinstance(in_obj, object)
        for key, val in in_obj.__dict__.items():
            if isinstance(val, (list, tuple)):
                self.__dict__[key] = [ObjToDict(x).__dict__ if isinstance(x, (ConfigGlobalUser, ConfigGlobalWindowsettings)) else x for x in val]
            else:
                self.__dict__[key] = ObjToDict(val).__dict__ if isinstance(val, (ConfigGlobalUser, ConfigGlobalWindowsettings)) else val





tobj = ConfigGlobal(test_dict)

tdict = ObjToDict(tobj).__dict__

save_json_complete("test-dict-obj-man-save.json", tdict)


