#! /usr/bin/python

# here is all the stuff that carries no own methods / logic, just gui elements and constant values that are loaded into the main program

import math
from math import floor
import sys
import os.path
import random
from enum import Enum, unique, auto as auto_enum

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from equalizer_bar import EqualizerBar

import vlc



PROGRAM_NAME = "RAnimeTV GUI (working title)"


TIMER_PLAYBACK_UPDATE_MS = 250
TIMER_EQUALIZER_UPDATE_MS = 100
BUTTON_PLAYBACK_SQUARE_SIZE = 35
BUTTON_PLAYBACK_ICON_SIZE = 25
BUTTON_PLAYBACK_NARROW_WIDTH = 20
BUTTON_PLAYBACK_SQUARE_FONT_SIZE = 20
BUTTON_QUEUE_SHOWS_WIDTH = 23
BUTTON_QUEUE_SHOWS_HEIGHT = 23
BUTTON_QUEUE_SHOWS_ICON_SIZE = 20
BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM = 16

TEXT_CONTROL_PLAY = "▶" # TODO delete unused
STATUSTIP_CONTROL_PLAY = "Play"
ICON_PLAY = "icons/play.svg"
ICON_PLAY_START = "icons/play_start.svg"
TEXT_CONTROL_PAUSE = "⏸" # TODO delete unused
STATUSTIP_CONTROL_PAUSE = "Pause"
# TEXT_CONTROL_ # TODO, also translation here?
# TEXT_CONTROL_
# TEXT_CONTROL_
# TEXT_CONTROL_
# TEXT_CONTROL_
# TEXT_CONTROL_



ICON_PAUSE = "icons/pause.svg"
ICON_STOP = "icons/stop.svg"
ICON_SKIP = "icons/skip.svg"
ICON_BACKWARD = "icons/backward.svg"
ICON_FORWARD = "icons/forward.svg"
ICON_LOOP = "icons/loop.svg"
ICON_LOOP = "icons/loop.svg"
ICON_LOOPING = "icons/looping.svg"
ICON_TIMER = "icons/timer.svg"
ICON_TIMED = "icons/timed.svg"
ICON_SPEAKER_UNMUTED = "icons/speaker_unmuted.svg"
ICON_SPEAKER_MUTED = "icons/speaker_muted.svg"
ICON_FOLDER = "icons/folder.svg"
ICON_FULLSCREEN = "icons/fullscreen.svg"
ICON_EDIT = "icons/edit.svg"
ICON_ADD = "icons/add_plus.svg"
ICON_REMOVE = "icons/remove_x.svg"
ICON_SORT = "icons/sort.svg"
ICON_MOVE_UP = "icons/move_up.svg"
ICON_MOVE_DOWN = "icons/move_down.svg"
ICON_MOVE_LEFT = "icons/move_left.svg"



### dummy lists, to be replaced by configs later ###
DUMMY_USER_LIST = ["Alex", "Extra", "User3"]
DUMMY_PROFILE_LIST = ["Normal", "Fokus", "Chill"]
DUMMY_BASEPATH_LIST = ["/media/alex/678001755456034B/Sprachen", "D:\\Sprachen"]

DUMMY_SHOW_LIST = ["Death Note", "Samurai Champloo", "Dr. Stone", "Psycho Pass S2", "Mob Psycho 100"]
DUMMY_QUEUE_LIST = ["Mob Psycho 100", "Death Note", "Psycho Pass S2"]
DUMMY_QUEUE_LIST_L = ["Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2"]
DUMMY_QUEUE_LIST_T = (
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "❌"),
)
DUMMY_QUEUE_LIST_T_L = (
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "❌"),
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "❌"),
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "❌"),
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "❌"),
)
DUMMY_HISTORY_LIST = ["path/to/Death Note 2", "long/path/very/long/to/Samurai Champloo 1", "home/Dr. Stone", "Psycho Pass S2 13", "Mob Psycho 100"]
DUMMY_CMS_LIST = ["Ajinomoto", "Nintendo cms 1min", "Sony"]
DUMMY_VERSION_TEXT = "V.0.0.0"



##### code directly copied from ranimetv code, maybe import later instead
def get_h_m_s_from_milliseconds(milli: int):
    """Get milliseconds converted to hh:mm:ss. If hh == 0, then mm:ss"""
    sec = floor(milli / 1000)
    s = sec % 60
    m = floor(sec / 60) % 60
    h = floor(sec / (60 * 60))
    if h > 0:
        return f'{"{0:0=2d}".format(h)}:{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'
    else:
        return f'{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'


@unique
class DockedState(Enum):
    PLAYER_WINDOW = auto_enum()
    EXTRA_WINDOW = auto_enum()
    NOWHERE = auto_enum()


@unique
class MediaAreaState(Enum):
    VIDEO_EQUALIZER = auto_enum()
    VIDEO_NOEQUALIZER = auto_enum()
    EQUALIZERONLY = auto_enum()
    MANAGEMENT_ONLY = auto_enum()
    NOTHING = auto_enum()

    @staticmethod
    def next_value(state):
        if state == MediaAreaState.VIDEO_EQUALIZER:
            return MediaAreaState.VIDEO_NOEQUALIZER
        elif state == MediaAreaState.VIDEO_NOEQUALIZER:
            return MediaAreaState.EQUALIZERONLY
        elif state == MediaAreaState.EQUALIZERONLY:
            return MediaAreaState.MANAGEMENT_ONLY
        elif state == MediaAreaState.MANAGEMENT_ONLY:
            return MediaAreaState.NOTHING
        elif state == MediaAreaState.NOTHING:
            return MediaAreaState.VIDEO_EQUALIZER




class UiManagementTopRow(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.user_profiles_paths_edit_button = UiManagementToolButton(ICON_EDIT, "Edit Users, Profiles and Base Paths.")

        self.user_select_label = QLabel("User:")
        self.user_select_label.setStatusTip("Current User.")

        self.user_select_combo = QComboBox()
        self.user_select_combo.setMaximumWidth(110)
        self.user_select_combo.addItems(DUMMY_USER_LIST)
        self.user_select_combo.setStatusTip("Select the current User. Only if no running session.")

        self.sep_1 = QLabel("||")

        self.profile_select_label = QLabel("Profile:")
        self.profile_select_label.setStatusTip("Current Profile.")

        self.profile_select_combo = QComboBox()
        self.profile_select_combo.setMaximumWidth(110)
        self.profile_select_combo.addItems(DUMMY_PROFILE_LIST)
        self.profile_select_combo.setStatusTip("Select the current Profile. Only if no running session.")
        # self.profile_select_combo.setDisabled(True) # TODO: disable when playing

        self.sep_2 = QLabel("||")

        self.basepath_checkbox = QCheckBox("Base Path:")
        # self.basepath_checkbox.setCheckState(Qt.PartiallyChecked) # tristate null disabled
        # self.basepath_checkbox.setTristate(True) # None, True, False
        self.basepath_checkbox.setStatusTip("Toggle if base path should be used. Only if no running session.")

        self.basepath_select_combo = QComboBox()
        self.basepath_select_combo.setMaximumWidth(110)
        self.basepath_select_combo.addItems(DUMMY_BASEPATH_LIST)
        self.basepath_select_combo.setStatusTip("Select the current base path. Only if no running session.")


        self.addStretch()
        # self.addLayout(self.user_select_box)
        # self.addLayout(self.profile_select_box)
        # self.addLayout(self.basepath_select_box)
        self.addWidget(self.user_profiles_paths_edit_button)
        self.addWidget(self.user_select_label)
        self.addWidget(self.user_select_combo)
        self.addWidget(self.sep_1)
        self.addWidget(self.profile_select_label)
        self.addWidget(self.profile_select_combo)
        self.addWidget(self.sep_2)
        # self.addWidget(self.basepath_select_label)
        self.addWidget(self.basepath_checkbox)
        self.addWidget(self.basepath_select_combo)
        self.setContentsMargins(5, 5, 5, 5)





class UiManagementQueueHistoryTabs(QTabWidget):

    def __init__(self):
        super().__init__()

        # queue list and buttons first
        # self.queue_list = QListWidget() # TODO CONTINUE
        # self.queue_list.addItems(DUMMY_QUEUE_LIST_L)
        # self.queue_list.currentTextChanged.connect(self.handleQueueListSelected)
        # self.queue_list.setAlternatingRowColors(True)
        # self.queue_list.setSelectionMode(QAbstractItemView.ExtendedSelection) # see https://doc.qt.io/qt-5/qabstractitemview.html#SelectionMode-enum
        # # self.queue_list.setAutoScroll(True) # TODO for drag&drop later, see https://youtu.be/Huonlw6kRQA?t=279
        # # self.queue_list.setTextElideMode(Qt.ElideRight) # TODO: apparently right side ellipsis, see https://doc.qt.io/qt-5/qabstractitemview.html#textElideMode-prop
        # self.queue_list.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel) # TODO or ScrollPerItem? (nah), see https://doc.qt.io/qt-5/qabstractitemview.html#ScrollMode-enum

        # self.queue_list.clearSelection() # TODO call when a button does some action on the list, maybe if removes an item? But maybe keeping the selection on the next item then might be good (needs experimenting) -- "Deselects all selected items. The current index will not be changed."


        self.queue_table = UiQueueTable()
        self.queue_table.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # buttons for queue list interaction
        self.queue_buttons = UiManagementQueueButtons()


        self.v_box_queue = QVBoxLayout()
        self.v_box_queue.addWidget(self.queue_table)
        self.v_box_queue.addLayout(self.queue_buttons)
        self.v_box_queue.setContentsMargins(0, 0, 0, 0)

        self.queue_widget = QWidget()
        self.queue_widget.setLayout(self.v_box_queue)


        # history, showing all played files so far
        self.history_list = QListWidget()
        self.history_list.addItems(DUMMY_HISTORY_LIST)
        self.history_list.setAlternatingRowColors(True)
        self.history_list.setSelectionMode(QAbstractItemView.NoSelection)


        # put into tabs
        self.addTab(self.queue_widget, "Queue")
        self.addTab(self.history_list, "History")







class UiQueueTable(QTableView):

    def __init__(self):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.model = QStandardItemModel(0, 4, self)
        self.model.setHorizontalHeaderLabels(["Name", "Ep", "op", "ed"])

        tmp_it = QStandardItem("tmp")
        font = tmp_it.font()
        font.setBold(True)

        for name, episode_number, opening_mark, ending_mark in DUMMY_QUEUE_LIST_T_L:
            it_name = QStandardItem(name)
            it_ep = QStandardItem(str(episode_number))
            it_op = QStandardItem(opening_mark)
            it_ed = QStandardItem(ending_mark)

            it_name.setFont(font)
            it_name.setEditable(False)
            it_ep.setEditable(False)
            it_op.setEditable(False)
            it_ed.setEditable(False)
            self.model.appendRow([it_name, it_ep, it_op, it_ed])

        self.setModel(self.model)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(2, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(3, QHeaderView.ResizeToContents)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()



    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())




class UiManagementShowsCmsTabs(QTabWidget):

    def __init__(self):
        super().__init__()

        self.shows_list = QListWidget()
        self.shows_list.addItems(DUMMY_SHOW_LIST)
        self.shows_list.setAlternatingRowColors(True)
        self.shows_list.setSelectionMode(QAbstractItemView.SingleSelection)

        # self.shows_list.doubleClicked.connect() # TODO use double click? maybe to open show settings. Try itemDoubleClicked to pass the item, else index passed (apparently, not tested yet)
        # self.shows_list.activated.connect() # TODO should happen if ENTER is pressed, but maybe also on other signals like Double Click (TODO: test!!!)
        # self.shows_list.reset() # TODO: maybe call this if a show is added or deleted or just edited. Might be useful in queue as well, if there something changes
        # self.shows_list.scrollToBottom() # TODO maybe use if new entry is added (maybe not needed), maybe also call setCurrentIndex so it is selected. Also use scrollToItem to scroll to just edited item etc, maybe should call update(index) after an edit of item at index. And scrollToTop.

        # buttons for show list interaction
        self.shows_buttons = UiManagementShowsCmsButtons()


        self.v_box_shows = QVBoxLayout()
        # self.v_box_shows.addWidget(self.shows_list_label)
        self.v_box_shows.addWidget(self.shows_list)
        self.v_box_shows.addLayout(self.shows_buttons)
        self.v_box_shows.setContentsMargins(0, 0, 0, 0)

        # self.v_box_shows_frame = QFrame()
        # self.v_box_shows_frame.setLayout(self.v_box_shows)

        self.shows_widget = QWidget()
        self.shows_widget.setLayout(self.v_box_shows)

        # add widget for commercials
        self.commercials_list = QListWidget()
        self.commercials_list.addItems(DUMMY_CMS_LIST)
        self.commercials_list.setAlternatingRowColors(True)
        self.commercials_list.setSelectionMode(QAbstractItemView.NoSelection)

        # combine shows and commercials in a tab widget and add to frame
        self.addTab(self.shows_widget, "Shows")
        self.addTab(self.commercials_list, "Commercials")





class UiManagementShowsCmsButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()
        self.add_to_queue = UiManagementToolButton(ICON_MOVE_LEFT, "Add show to end of queue.")
        self.sort = UiManagementToolButton(ICON_SORT, "Select shows sort mode.")
        self.add_new = UiManagementToolButton(ICON_ADD, "Add a new show.")
        self.edit = UiManagementToolButton(ICON_EDIT, "Edit show.")

        self.addWidget(self.add_to_queue)
        self.addWidget(self.sort)
        self.addStretch()
        self.addWidget(self.add_new)
        self.addWidget(self.edit)
        self.setContentsMargins(5, 5, 5, 5)





class UiManagementQueueButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD, "Add one show to queue.")
        self.remove = UiManagementToolButton(ICON_REMOVE, "Remove selection from queue.")
        self.move_up = UiManagementToolButton(ICON_MOVE_UP, "Move selection up in queue.")
        self.move_down = UiManagementToolButton(ICON_MOVE_DOWN, "Move selection down in queue.")

        self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addStretch()
        self.addWidget(self.move_up)
        self.addWidget(self.move_down)
        self.setContentsMargins(5, 0, 5, 5)




class UiManagementToolButton(QToolButton):

    def __init__(self, icon_path: str, status_tip: str = ""):
        super().__init__()
        self.setIcon(QIcon(icon_path))
        self.setIconSize(
            QSize(BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM, BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM))
        self.setFixedSize(BUTTON_QUEUE_SHOWS_WIDTH, BUTTON_QUEUE_SHOWS_HEIGHT)
        self.setStatusTip(status_tip)




class UiShowsNewDialog(QDialog):
    # TODO CONTINUE: block MainWindow, add StatusTips (and the bar for it), finish up
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Add a new show")
        self.setWindowModality(Qt.ApplicationModal)


        # form layout for the main settings
        self.form_main_settings = UiShowFormMainSettings()


        # cancel and save buttons
        self.su_button_cancel = QToolButton()
        self.su_button_cancel.setText("Cancel")
        self.su_button_cancel.clicked.connect(self.hide)

        self.su_button_save = QToolButton()
        self.su_button_save.setText("Save")
        self.su_button_save.clicked.connect(self.handleSaveNewShow)

        self.su_buttons_h_box = QHBoxLayout()
        self.su_buttons_h_box.addWidget(self.su_button_cancel)
        self.su_buttons_h_box.addWidget(self.su_button_save)


        # combine all (form and buttons)
        self.layout_all = QVBoxLayout()
        self.layout_all.addLayout(self.form_main_settings)
        self.layout_all.addLayout(self.su_buttons_h_box)

        self.setLayout(self.layout_all)


    def handleSaveNewShow(self):
        """Create new show with the given attributes. Check validity before saving
        """
        # TODO: check if directory actually exists (combined with Base Path functionality), check if directory already used in another show (or the same name) (only give a warning, don't block)
        # state, string, pos = self.form_main_settings.name_validator.validate(self.form_main_settings.su_name_input.text(), 0)
        # print("Name input regex: " + str(state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable")
        #
        # state, string, pos = self.form_main_settings.path_validator.validate(self.form_main_settings.su_path_input.text(), 0)
        # print("Path input regex: " + str(state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable")



class UiShowFormMainSettings(QFormLayout):

    def __init__(self):
        super().__init__()

        self.su_name_label = QLabel("Name")
        self.su_name_label.setToolTip(
            "Give the show a name. Can be different from the actual file and directory names.")
        self.su_name_input = QLineEdit()
        self.su_name_input.setMinimumWidth(350)
        self.su_name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.su_name_input.setValidator(self.name_validator)
        self.su_name_input.setPlaceholderText("Enter show name")


        self.su_active_check = QCheckBox("Active")
        self.su_active_check.setChecked(True)
        self.su_active_check.setToolTip("Whether this show should be queued in any profile.")

        self.su_no_basepath = QCheckBox("No Base Path")
        self.su_no_basepath.setToolTip(
            "If checked, the base path will not used for this show, even if it is activated.")

        self.h_box_active_basepath = QHBoxLayout()
        self.h_box_active_basepath.addWidget(self.su_active_check)
        self.h_box_active_basepath.addWidget(self.su_no_basepath)


        self.su_path_label = QLabel("Path")
        self.su_path_input = QLineEdit()
        self.su_path_input.setPlaceholderText("Directory of the files.")

        self.path_regex = QRegExp('^.+$')
        self.path_validator = QRegExpValidator(self.path_regex)
        self.su_path_input.setValidator(self.path_validator)

        self.su_path_file_button = QToolButton()
        self.su_path_file_button.setText("Search")
        self.su_path_file_button.clicked.connect(self.openDirectoryDialog)
        self.su_path_h_box = QHBoxLayout()
        self.su_path_h_box.addWidget(self.su_path_input)
        self.su_path_h_box.addWidget(self.su_path_file_button)


        self.su_episodes_label = QLabel("Episode")

        self.su_episode_current_label = QLabel("Current")
        self.su_episode_current_input = QSpinBox()
        self.su_episode_current_input.setMinimum(1)

        self.su_episode_first_labelcheck = QCheckBox("First")
        self.su_episode_first_input = QSpinBox()
        self.su_episode_first_input.setMinimum(1)
        self.su_episode_first_input.setDisabled(True) # TODO: enable when checked box, save only when checked box

        self.su_episode_last_labelcheck = QCheckBox("Last")
        self.su_episode_last_input = QSpinBox()
        self.su_episode_last_input.setMinimum(1) # TODO set Max as maximum files/episodes found in the directory, also set as default if value is null in config
        self.su_episode_last_input.setDisabled(True) # TODO: enable when checked box, save only when checked box

        self.su_episodes_h_box = QHBoxLayout()
        self.su_episodes_h_box.addWidget(self.su_episode_current_label)
        self.su_episodes_h_box.addWidget(self.su_episode_current_input)
        self.su_episodes_h_box.addStretch()
        self.su_episodes_h_box.addWidget(self.su_episode_first_labelcheck)
        self.su_episodes_h_box.addWidget(self.su_episode_first_input)
        self.su_episodes_h_box.addStretch()
        self.su_episodes_h_box.addWidget(self.su_episode_last_labelcheck)
        self.su_episodes_h_box.addWidget(self.su_episode_last_input)


        self.su_profiles_label = QLabel("Active Profiles")
        # self.su_profiles_label.setWordWrap(True)
        self.su_profiles_checks_v_box = QVBoxLayout()
        self.su_profiles_check_all = QCheckBox("All Profiles")
        self.su_profiles_check_all.setChecked(True)
        self.su_profiles_check_all.setToolTip(
            "If True, then played in all profiles. Ignores Checkboxes below and settings like 'Exclude Profile' (in Profile Settings)")

        self.su_profiles_checks_v_box.addWidget(self.su_profiles_check_all)
        self.su_profiles_checks_single = list()
        for i in range(13):
            self.su_profiles_checks_single.append(QCheckBox("profile" + str(i)))
        self.su_profiles_checks_grid_box = QGridLayout()
        for i in range(13):
            self.su_profiles_checks_grid_box.addWidget(self.su_profiles_checks_single[i], int(i / 3) + 1, i % 3)

        self.su_profiles_checks_v_box.addLayout(self.su_profiles_checks_grid_box)

        # self.su_suspended_label = QLabel("Suspended Time")
        # self.su_suspended_time = QLabel("xyz TODO")

        self.addRow(self.su_name_label, self.su_name_input)
        self.addRow(QLabel(""), self.h_box_active_basepath)
        self.addRow(self.su_path_label, self.su_path_h_box)
        self.addRow(self.su_episodes_label, self.su_episodes_h_box)
        self.addRow(self.su_profiles_label, self.su_profiles_checks_v_box)
        # self.addRow(self.su_suspended_label, self.su_suspended_time)


    def openDirectoryDialog(self):
        select_dialog = QFileDialog()
        if self.su_path_input.text() == "":
            pass
            # if current_base_path: # TODO
            #     select_dialog.setDirectory(current_base_path) # TODO
        else:
            select_dialog.setDirectory(os.path.dirname(self.su_path_input.text()))

        # select_dialog.setFileMode(QFileDialog.DirectoryOnly)
        # select_dialog.setAcceptMode(QFileDialog.AcceptOpen)
        dir_name = select_dialog.getExistingDirectory(self.su_path_file_button, "Select directory containing the show's episodes")
        # QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
        if not dir_name:
            return
        self.su_path_input.setDisabled(True)
        self.su_path_input.setText(dir_name)



class UiShowsEditDialog(QDialog):
    # TODO CONTINUE: block MainWindow, add StatusTips (and the bar for it), finish up
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Edit shows")
        self.setWindowModality(Qt.ApplicationModal)

        # create stack layout
        self.shows_list = QListWidget()
        self.shows_list.addItems(DUMMY_SHOW_LIST)

        # test coloring a single item
        tmp_item = QListWidgetItem("TODO shows bold, cms normal")
        tmp_font = tmp_item.font()
        tmp_font.setBold(True)
        tmp_item.setFont(tmp_font)
        self.shows_list.addItem(tmp_item)
        self.shows_list.currentTextChanged.connect(self.handleShowsEditListSelected)
        # self.shows_list.setAlternatingRowColors(True)
        self.shows_list.setSelectionMode(QAbstractItemView.SingleSelection)

        # TODO: preselect the show that is to be edited (or none selected, then first) and fill forms

        # settings forms will be put here
        self.v_box_settings_all = QVBoxLayout()

        self.se_stack_h_box = QHBoxLayout()
        self.se_stack_h_box.addWidget(self.shows_list)
        self.se_stack_h_box.addLayout(self.v_box_settings_all)




        # form layout for the main settings
        self.form_main_settings = UiShowFormMainSettings()
        # TODO CONTINUE: all the other settings

        self.v_box_settings_all.addLayout(self.form_main_settings)


        # cancel and save buttons
        self.se_button_cancel = QToolButton()
        self.se_button_cancel.setText("Cancel")
        self.se_button_cancel.clicked.connect(self.hide)

        self.se_button_save = QToolButton()
        self.se_button_save.setText("Save")

        self.se_buttons_h_box = QHBoxLayout()
        self.se_buttons_h_box.addWidget(self.se_button_cancel)
        self.se_buttons_h_box.addWidget(self.se_button_save)


        # combine all (form and buttons)
        self.layout_all = QVBoxLayout()
        self.layout_all.addLayout(self.se_stack_h_box)
        self.layout_all.addLayout(self.se_buttons_h_box)

        self.setLayout(self.layout_all)


    def handleShowsEditListSelected(self):
        """Switch the settings to the selected show. Warn if not saved.
        """
        # TODO
        pass





class UiPlaybackControlHBox:

    def __init__(
            self,
            window_style: QStyle,
            mediaplayer: vlc.MediaPlayer,
            open_file_dialog,
            timer_equalizer_start,
            timer_equalizer_stop,
            equalizer_isVisible,
            toggle_media_area_state,
            set_media_area_state,
            get_media_area_state
    ):
        self.mediaplayer = mediaplayer
        self.open_file_dialog = open_file_dialog
        self.timer_equalizer_start = timer_equalizer_start
        self.timer_equalizer_stop = timer_equalizer_stop
        self.equalizer_isVisible = equalizer_isVisible
        self.toggle_media_area_state = toggle_media_area_state
        self.set_media_area_state = set_media_area_state
        self.get_media_area_state = get_media_area_state


        # start timer
        self.timer_playback = QTimer()
        self.timer_playback.setInterval(TIMER_PLAYBACK_UPDATE_MS)
        self.timer_playback.timeout.connect(self.updatePlaybackUI)


        # time label and slider layout
        self.time_progress_label = QLabel("00:00")
        self.time_progress_label.setStatusTip("Playback time.")

        self.time_progress_slider = QSlider(Qt.Horizontal)
        self.time_progress_slider.setStatusTip("Playback position.")
        self.time_progress_slider.setMaximum(10000)
        self.time_progress_slider.sliderPressed.connect(self.handleTimesliderPressed)
        self.time_progress_slider.sliderMoved.connect(self.handleTimesliderMoved)
        self.time_progress_slider.sliderReleased.connect(self.handleTimesliderReleased)

        self.time_progress_slider_moving = False
        self.time_progress_slider_new_position = False

        self.h_box_time = QHBoxLayout()
        self.h_box_time.addWidget(self.time_progress_label)
        self.h_box_time.setAlignment(self.time_progress_label, Qt.AlignCenter)
        self.h_box_time.addWidget(self.time_progress_slider)
        self.h_box_time.setContentsMargins(5, 0, 0, 0)


        self.show_extra_lines = False # TODO read from global-config

        # standard (first) line
        self.play_pause_button = QToolButton()
        self.play_pause_button.setIcon(QIcon(ICON_PLAY_START))
        self.play_pause_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.play_pause_button.setStatusTip("Start the current session.")
        self.play_pause_button.clicked.connect(self.handlePlayPausePlayback)
        self.play_pause_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.stop_button = QToolButton()
        self.stop_button.setPopupMode(QToolButton.DelayedPopup)
        self.stop_button.setIcon(QIcon(ICON_STOP))
        self.stop_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.stop_button.setStatusTip("Stop session. Hold Left Click to choose from other options.")
        self.stop_button.clicked.connect(self.handleStopPlayback)
        self.stop_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE + 8, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.menu_stop_options = UiMenuStopOptions()
        self.stop_button.setMenu(self.menu_stop_options.menu)


        self.skip_button = QToolButton()
        self.skip_button.setPopupMode(QToolButton.DelayedPopup)
        self.skip_button.setIcon(QIcon(ICON_SKIP))
        self.skip_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.skip_button.setStatusTip("Skip current media. Hold Left Click to choose from other options.")
        self.skip_button.clicked.connect(self.handleSkipPlayback)
        self.skip_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE + 8, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.menu_skip_options = UiMenuSkipOptions()
        self.skip_button.setMenu(self.menu_skip_options.menu)


        self.jump_back_button = QToolButton()
        self.jump_back_button.setIcon(QIcon(ICON_BACKWARD))
        self.jump_back_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.jump_back_button.setStatusTip("Jump back 5 seconds.") #TODO: adjust seconds amount
        self.jump_back_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.jump_forward_button = QToolButton()
        self.jump_forward_button.setIcon(QIcon(ICON_FORWARD))
        self.jump_forward_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.jump_forward_button.setStatusTip("Jump forward 5 seconds.") #TODO: adjust seconds amount
        self.jump_forward_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)


        self.mute_button = QToolButton()
        self.mute_button.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.mute_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.mute_button.setStatusTip("Toggle mute.")
        self.mute_button.setCheckable(True)
        self.mute_button.clicked.connect(self.handleMutePlayback)
        self.mute_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.volume_label = QLabel("100")
        self.volume_label.setContentsMargins(0, 0, 0, 0)
        # self.volume_label.setFont(QFont("Arial", 5))

        self.volume_slider = QSlider(Qt.Horizontal)
        self.volume_slider.setMaximum(200)  # TODO: use ranimetv MAX VOLUME?
        self.volume_slider.setValue(100)
        self.volume_slider.setStatusTip("Volume")
        self.volume_slider.setContentsMargins(0, 0, 0, 0)
        self.volume_slider.valueChanged.connect(self.handleSetVolume)
        self.volume_slider.setMaximumWidth(230)

        self.v_box_volume = QVBoxLayout()
        self.v_box_volume.addWidget(self.volume_label)
        self.v_box_volume.setAlignment(self.volume_label, Qt.AlignCenter)
        self.v_box_volume.addWidget(self.volume_slider)

        self.fullscreen_button = QPushButton()
        self.fullscreen_button.setIcon(QIcon(ICON_FULLSCREEN))
        self.fullscreen_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.fullscreen_button.setStatusTip("Toggle full screen.")
        self.fullscreen_button.setCheckable(True)
        # self.fullscreen_button.clicked.connect(self.handle_toggle_fullscreen)
        self.fullscreen_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.toggle_media_area_button = QToolButton()
        self.toggle_media_area_button.setArrowType(Qt.UpArrow)
        self.toggle_media_area_button.setPopupMode(QToolButton.DelayedPopup)
        self.menu_media_area_states = UiMenuMediaAreaStates(self.toggle_media_area_button, self.set_media_area_state, self.get_media_area_state)
        self.toggle_media_area_button.setMenu(self.menu_media_area_states.menu)
        self.toggle_media_area_button.setStatusTip("Toggle media area. Hold Left Click to choose state.")
        # self.toggle_media_area_button.clicked.connect(self.toggle_media_area_state())
        self.toggle_media_area_button.clicked.connect(lambda: self.menu_media_area_states.selected_option(MediaAreaState.next_value(self.get_media_area_state()))) # takes care of checking the right one in the popup, else self.toggle_media_area_state() would be enough
        self.toggle_media_area_button.setFixedSize(int(BUTTON_PLAYBACK_SQUARE_SIZE / 2), int(BUTTON_PLAYBACK_SQUARE_SIZE / 2) - 3)

        self.toggle_extras_button = QToolButton()
        self.toggle_extras_button.setArrowType(Qt.DownArrow)
        self.toggle_extras_button.setStatusTip("Toggle extra buttons.")
        self.toggle_extras_button.setCheckable(True)
        self.toggle_extras_button.clicked.connect(self.handleToggleExtraButtons)
        self.toggle_extras_button.setFixedSize(int(BUTTON_PLAYBACK_SQUARE_SIZE / 2), int(BUTTON_PLAYBACK_SQUARE_SIZE / 2) - 3)

        self.v_box_arrow_buttons = QVBoxLayout()
        self.v_box_arrow_buttons.addWidget(self.toggle_media_area_button)
        self.v_box_arrow_buttons.addWidget(self.toggle_extras_button)


        # extra (second, usually hidden) line

        self.open_dir_button = QToolButton()
        self.open_dir_button.setIcon(QIcon(ICON_FOLDER))
        self.open_dir_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.open_dir_button.setStatusTip("Open folder containing the currently played media.")
        self.open_dir_button.clicked.connect(self.handleOpenDirCurrentMedia)
        self.open_dir_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.loop_button = QToolButton()
        self.loop_button.setIcon(QIcon(ICON_LOOP))
        self.loop_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.loop_button.setStatusTip("Loop current media.")
        self.loop_button.setCheckable(True)
        self.loop_button.clicked.connect(self.handleLoopPlayback)
        self.loop_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.timer_button = QToolButton()
        self.timer_button.setIcon(QIcon(ICON_TIMER))
        self.timer_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.timer_button.setStatusTip("Set turn off countdown.")
        self.timer_button.clicked.connect(self.handleShutdownEpisodesLimitDialog)
        self.timer_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.slower_button = QToolButton()
        self.slower_button.setText("🐢")
        self.slower_button.setStatusTip("Slow down playback.")
        self.slower_button.clicked.connect(self.handleSlowerPlayback)
        self.slower_button.setFont(QFont("Arial", BUTTON_PLAYBACK_SQUARE_FONT_SIZE))
        self.slower_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.faster_button = QToolButton()
        self.faster_button.setText("🐇")
        self.faster_button.setStatusTip("Speed up playback.")
        self.faster_button.clicked.connect(self.handleFasterPlayback)
        self.faster_button.setFont(QFont("Arial", BUTTON_PLAYBACK_SQUARE_FONT_SIZE))
        self.faster_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        # combine all extra buttons
        self.h_box_playback_buttons_extra = QHBoxLayout()
        self.h_box_playback_buttons_extra.addWidget(self.open_dir_button)
        self.h_box_playback_buttons_extra.addWidget(self.loop_button)
        self.h_box_playback_buttons_extra.addWidget(self.timer_button)
        self.h_box_playback_buttons_extra.addWidget(self.slower_button)
        self.h_box_playback_buttons_extra.addWidget(self.faster_button)
        self.h_box_playback_buttons_extra.addStretch()
        self.h_box_playback_buttons_extra.setContentsMargins(0, 0, 0, 0)


        # put extra buttons in a frame that can be easily hidden
        self.h_box_playback_buttons_extra_frame_side = QFrame()
        self.h_box_playback_buttons_extra_frame_side.setLayout(self.h_box_playback_buttons_extra)
        # self.h_box_playback_buttons_extra_frame_side.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum) #FIXME: i have no idea why this works (not necessary anymore after alignment was set to bottom
        self.h_box_playback_buttons_extra_frame_side.setHidden(True)

        # disable some buttons until media playback starts
        self.handlePlaybackControlsEnabled(False)


        # combine all standard buttons
        self.h_box_playback_buttons_main = QHBoxLayout()
        self.h_box_playback_buttons_main.addWidget(self.play_pause_button)
        self.h_box_playback_buttons_main.addWidget(self.stop_button)
        self.h_box_playback_buttons_main.addWidget(self.skip_button)
        self.h_box_playback_buttons_main.addWidget(self.jump_back_button)
        self.h_box_playback_buttons_main.addWidget(self.jump_forward_button)
        self.h_box_playback_buttons_main.addStretch()
        self.h_box_playback_buttons_main.addWidget(self.h_box_playback_buttons_extra_frame_side)
        self.h_box_playback_buttons_main.addStretch()
        self.h_box_playback_buttons_main.addWidget(self.mute_button)
        self.h_box_playback_buttons_main.addLayout(self.v_box_volume)
        self.h_box_playback_buttons_main.addWidget(self.fullscreen_button)
        self.h_box_playback_buttons_main.addLayout(self.v_box_arrow_buttons)


        # combine both lines
        self.v_box_playback_all = QVBoxLayout()
        self.v_box_playback_all.addLayout(self.h_box_time)
        self.v_box_playback_all.addLayout(self.h_box_playback_buttons_main)
        # self.v_box_playback_all.addWidget(self.h_box_playback_buttons_extra_frame_bottom)
        self.v_box_playback_all.setContentsMargins(10, 0, 10, 0)



    def handlePlaybackControlsEnabled(self, enabled: bool): # TODO make sure all buttons are here
        self.time_progress_slider.setEnabled(enabled)

        self.stop_button.setEnabled(enabled)
        self.skip_button.setEnabled(enabled)
        self.jump_back_button.setEnabled(enabled)
        self.jump_forward_button.setEnabled(enabled)
        self.loop_button.setEnabled(enabled)
        self.mute_button.setEnabled(enabled) # TODO: should always be enabled, needs to be handled properly later
        self.volume_slider.setEnabled(enabled) # TODO: should always be enabled, needs to be handled properly later
        self.open_dir_button.setEnabled(enabled)
        self.loop_button.setEnabled(enabled)
        self.timer_button.setEnabled(enabled)
        self.slower_button.setEnabled(enabled)
        self.faster_button.setEnabled(enabled)
        self.fullscreen_button.setEnabled(enabled)


    def updatePlaybackUI(self):
        """updates the user interface"""
        # setting the slider or the playback to the desired position
        if self.time_progress_slider_moving:
            if self.time_progress_slider_new_position:
                self.mediaplayer.set_position(self.time_progress_slider.value() / 10000.0)
                self.time_progress_slider_new_position = False
        else:
            self.media_length_string = get_h_m_s_from_milliseconds(
                self.mediaplayer.get_length())  # TODO: move into player handle options post play
            self.time_progress_slider.setValue(floor(self.mediaplayer.get_position() * 10000))
            self.time_progress_label.setText(
                get_h_m_s_from_milliseconds(self.mediaplayer.get_time()) + " / " + self.media_length_string)


            # TODO maybe delete this later
            if not self.mediaplayer.is_playing():
                # no need to call this function if nothing is played
                self.timer_playback.stop()
                if not self.isPaused:
                    # after the video finished, the play button stills shows
                    # "Pause", not the desired behavior of a media player
                    # this will fix it
                    self.handleStopPlayback()


    # handle button presses

    def handlePlayPausePlayback(self):
        """Toggle play/pause status
        """
        if self.mediaplayer.is_playing():
            self.mediaplayer.pause()
            self.play_pause_button.setIcon(QIcon(ICON_PLAY))
            self.play_pause_button.setStatusTip(STATUSTIP_CONTROL_PLAY)
            self.isPaused = True
            self.timer_equalizer_stop()
        else:
            if self.mediaplayer.play() == -1:
                self.open_file_dialog()
                return
            self.mediaplayer.play()
            self.handlePlaybackControlsEnabled(True)
            self.play_pause_button.setIcon(QIcon(ICON_PAUSE))
            self.play_pause_button.setStatusTip(STATUSTIP_CONTROL_PAUSE)
            self.timer_playback.start()
            if not self.equalizer_isVisible:
                self.timer_equalizer_start()
            self.isPaused = False


    def handleStopPlayback(self): # TODO stop session, enable choosing other profile or user
        """Stop player
        """
        self.mediaplayer.stop()
        self.timer_playback.stop()
        self.timer_equalizer_stop()
        self.time_progress_slider.setValue(0)
        self.time_progress_label.setText("00:00")
        self.play_pause_button.setIcon(QIcon(ICON_PLAY_START))
        self.handlePlaybackControlsEnabled(False)


    def handleStopPlaybackMenu(self, point): # TODO: normal, suspend, ssuspend
        # show context menu
        self.stop_button_menu.exec_(self.stop_button.mapToGlobal(point))


    def handleSkipPlayback(self): # TODO
        """Skip this media
        """


    def handleSkipHardPlayback(self): # TODO
        """Skip to the next episode (or its opening)
        """


    def handleSkipToCommercials(self): # TODO
        """Skip to commercials
        """


    def handleSlowerPlayback(self): # TODO
        """Speed down playback
        """


    def handleFasterPlayback(self): # TODO
        """Speed up playback
        """


    def handleShutdownEpisodesLimitDialog(self): # TODO
        """Implement old 'shutdown' and 'last' command as gui
        """


    def handleOpenDirCurrentMedia(self): # TODO
        """Open directory of currently playing media
        """


    def handleLoopPlayback(self): # TODO
        """Loop current media forever (toggle)
        """
        if self.loop_button.isChecked():
            self.loop_button.setIcon(QIcon(ICON_LOOPING))
        else:
            self.loop_button.setIcon(QIcon(ICON_LOOP))

        # TODO


    def handleSetVolume(self, volume):
        """Set the volume
        """
        self.mediaplayer.audio_set_volume(volume) # TODO replace by proper volume handling
        self.volume_label.setText(str(volume))


    def handleMutePlayback(self):
        """Mute the playback (toggle)
        """
        # TODO replace by proper mute handling
        # TODO correct possible button desync in update_ui ??
        mute_state = self.mediaplayer.audio_get_mute()
        if mute_state == 0:  # 0 if not muted
            mute_bool = True
            self.mute_button.setIcon(QIcon(ICON_SPEAKER_MUTED))
        else:
            mute_bool = False
            self.mute_button.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.mediaplayer.audio_set_mute(mute_bool)


    def handleTimesliderPressed(self):
        """Deactivate updating of slider, else dragged Slider bugs around
        """
        # self.timer_playback.stop()
        self.time_progress_slider_moving = True


    def handleTimesliderReleased(self):
        """Reactivate updating of slider, else dragged Slider bugs around
        """
        if self.time_progress_slider_new_position:
            self.mediaplayer.set_position(self.time_progress_slider.value() / 10000.0)
        self.time_progress_slider_new_position = False
        # self.timer_playback.start()
        self.time_progress_slider_moving = False


    def handleTimesliderMoved(self, position):
        """Set the playback time with the slider
        """
        # setting the position to where the slider was dragged
        # self.mediaplayer.set_position(position / 10000.0)
        self.time_progress_slider_new_position = True
        self.time_progress_label.setText(get_h_m_s_from_milliseconds(math.floor(self.mediaplayer.get_length() * (position / 10000.0))) + " / " + self.media_length_string) # TODO: cache get_length somewhere
        # the vlc MediaPlayer needs a float value between 0 and 1, Qt
        # uses integer variables, so you need a factor; the higher the
        # factor, the more precise are the results
        # (10000 should be enough)


    def handleToggleExtraButtons(self):
        if self.show_extra_lines:
            # self.h_box_playback_buttons_extra_frame_bottom.setHidden(True)
            self.h_box_playback_buttons_extra_frame_side.setHidden(True)
        else:
            # self.h_box_playback_buttons_extra_frame_bottom.setHidden(False)
            self.h_box_playback_buttons_extra_frame_side.setHidden(False)
        self.show_extra_lines = not self.show_extra_lines


    def handleToggleMediaArea(self):
        pass # TODO




class UiMenubarPlayer:

    def __init__(
            self,
            open_file_dialog
    ):
        self.menubar = QMenuBar()

        self.menubar_file = self.menubar.addMenu("&File")
        self.menubar_extra = self.menubar.addMenu("&Extra")


        # File
        self.menu_open = QAction("&Open")
        self.menu_open.setShortcut("Ctrl+O")  # shortcut example!
        self.menu_open.setStatusTip("Open a new File")
        self.menu_open.triggered.connect(open_file_dialog)

        self.menu_exit = QAction("&Exit")
        self.menu_exit.triggered.connect(sys.exit)

        self.menubar_file.addAction(self.menu_open)
        self.menubar_file.addSeparator()
        self.menubar_file.addAction(self.menu_exit)


        # Extra
        self.menu_about = QAction("&About")
        self.menu_about.setStatusTip("About this program")
        self.menu_about.triggered.connect(self.showDialogAbout)

        self.menubar_extra.addAction(self.menu_about)



    @staticmethod # TODO: move somewhere else
    def showDialogAbout(self):
        about = QMessageBox()
        about.setWindowTitle("About")
        about.setText("""Python script with GUI for customizable ordered playback of a randomly picked
series of predefined media files from your local computer.""")
        about.setInformativeText("""TODO finish this, put into own variable.

python-vlc is used to play the media files, see https://www.olivieraubert.net/vlc/python-ctypes/

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.

Source Code: gitlab.com/CptMaister/ranimetv
Contact: alex.mai@posteo.net""")
        about.setIcon(QMessageBox.Information)

        # about.setStandardButtons(QMessageBox.Close|QMessageBox.Ok)
        about.setStandardButtons(QMessageBox.Close)
        about.exec_()



# TODO: add functionality
class UiMenuStopOptions:

    def __init__(
            self
    ):

        self.menu = QMenu()

        self.menu_queue_time = self.menu.addAction("&Suspend Session")
        self.menu_queue_time.setStatusTip("Stop, continue here next time with current queue.")
        self.menu_queue_time.setCheckable(True)
        self.menu_queue_time.setChecked(True) # TODO: read checked from settings
        # self.menu_queue_time.triggered.connect()

        self.menu_time = self.menu.addAction("&Suspend Show")
        self.menu_time.setStatusTip("Stop, continue here next time, don't save current queue.")
        self.menu_time.setCheckable(True)
        # self.menu_time.triggered.connect()

        self.menu_no_vid = self.menu.addAction("&Suspend Queue")
        self.menu_no_vid.setStatusTip("Stop, save current queue, but not current playback position.")
        self.menu_no_vid.setCheckable(True)
        # self.menu_no_vid.triggered.connect()

        self.menu_simple_stop = self.menu.addAction("&Stop")
        self.menu_simple_stop.setStatusTip("Stop, discard current position and queue.")
        self.menu_simple_stop.setCheckable(True)
        # self.menu_simple_stop.triggered.connect()


# TODO: add functionality
class UiMenuSkipOptions:

    def __init__(
            self
    ):

        self.menu = QMenu()

        self.menu_queue_time = self.menu.addAction("&Skip")
        self.menu_queue_time.setStatusTip("Skip current media.")
        # self.menu_queue_time.triggered.connect()

        self.menu_no_vid = self.menu.addAction("&Skip Show")
        self.menu_no_vid.setStatusTip("Skip to commercials (if you have any).")
        # self.menu_no_vid.triggered.connect()

        self.menu_time = self.menu.addAction("&Skip All")
        self.menu_time.setStatusTip("Skip to next show's episode.")
        # self.menu_time.triggered.connect()



class UiMenuMediaAreaStates:

    def __init__(
            self,
            toggle_media_area_button,
            set_media_area_state,
            get_media_area_state
    ):
        self.toggle_media_area_button = toggle_media_area_button
        self.set_media_area_state = set_media_area_state
        self.get_media_area_state = get_media_area_state

        self.menu = QMenu()

        self.menu_vid_equ = self.menu.addAction("&Normal")
        self.menu_vid_equ.setStatusTip("Show video, equalizer if audio, management if nothing playing.")
        self.menu_vid_equ.setCheckable(True)
        self.menu_vid_equ.setChecked(True) # TODO: read checked from settings
        self.menu_vid_equ.triggered.connect(lambda: self.selectedOption(MediaAreaState.VIDEO_EQUALIZER))

        self.menu_no_eq = self.menu.addAction("&No Equalizer")
        self.menu_no_eq.setStatusTip("Show Management during audio playback.")
        self.menu_no_eq.setCheckable(True)
        self.menu_no_eq.triggered.connect(lambda: self.selectedOption(MediaAreaState.VIDEO_NOEQUALIZER))

        self.menu_no_vid = self.menu.addAction("&No Video")
        self.menu_no_vid.setStatusTip("Show audio equalizer for video and audio playback.")
        self.menu_no_vid.setCheckable(True)
        self.menu_no_vid.triggered.connect(lambda: self.selectedOption(MediaAreaState.EQUALIZERONLY))

        self.menu_manage_only = self.menu.addAction("&Management only")
        self.menu_manage_only.setStatusTip("Show Management even during media playback.")
        self.menu_manage_only.setCheckable(True)
        self.menu_manage_only.triggered.connect(lambda: self.selectedOption(MediaAreaState.MANAGEMENT_ONLY))

        self.menu_nothing = self.menu.addAction("&Nothing")
        self.menu_nothing.setStatusTip("Show nothing in media area.")
        self.menu_nothing.setCheckable(True)
        self.menu_nothing.triggered.connect(lambda: self.selectedOption(MediaAreaState.NOTHING))


    def selectedOption(self, state: MediaAreaState):
        self.set_media_area_state(state)

        menu_vid_equ_checked = False
        menu_no_eq_checked = False
        menu_no_vid_checked = False
        menu_manage_only_checked = False
        menu_nothing_checked = False

        if state == MediaAreaState.VIDEO_EQUALIZER:
            menu_vid_equ_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Normal. Hold Left Click to choose state.")
        elif state == MediaAreaState.VIDEO_NOEQUALIZER:
            menu_no_eq_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: No Equalizer. Hold Left Click to choose state.")
        elif state == MediaAreaState.EQUALIZERONLY:
            menu_no_vid_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: No Video. Hold Left Click to choose state.")
        elif state == MediaAreaState.MANAGEMENT_ONLY:
            menu_manage_only_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Management only. Hold Left Click to choose state.")
        elif state == MediaAreaState.NOTHING:
            menu_nothing_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Nothing. Hold Left Click to choose state.")

        self.menu_vid_equ.setChecked(menu_vid_equ_checked)
        self.menu_no_eq.setChecked(menu_no_eq_checked)
        self.menu_no_vid.setChecked(menu_no_vid_checked)
        self.menu_manage_only.setChecked(menu_manage_only_checked)
        self.menu_nothing.setChecked(menu_nothing_checked)



def handlePrintTest():
    print("Hello! :-)")



if __name__ == "__main__":
    # app = QApplication(sys.argv)
    app = QApplication([])
    # app.setStyle(QStyleFactory.create("Fusion"))
    # print(QStyleFactory.keys())
    player = PlayerWindow()
    player.show()
    player.resize(640, 480) # TODO: better sizing
    # if sys.argv[1:]:
    #     player.open_file_dialog(sys.argv[1])
    sys.exit(app.exec_())
