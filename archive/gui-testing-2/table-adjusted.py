import sys

from PyQt5 import QtCore, QtGui, QtWidgets

# copied from https://stackoverflow.com/questions/61500139/what-is-the-best-widget-in-pyqt5-to-show-a-checked-list-with-columns
# then removed the stuff I don't need for my project here

class Dialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()

        self.model = QtGui.QStandardItemModel(0, 3, self)

        self.model.setHorizontalHeaderLabels(["Show", "Ep", "op"])

        for firstname, lastname, company in (
            ("Larry", "Ellison", "Henlo"),
            ("Steve", "Jobs", "Apple"),
            ("Steve", "Ballmer", "Microsoft"),
            ("Bill", "Gates", "Microsoft"),
        ):
            it_firstname = QtGui.QStandardItem(firstname)
            it_lastname = QtGui.QStandardItem(lastname)
            it_company = QtGui.QStandardItem(company)
            self.model.appendRow([it_firstname, it_lastname, it_company])

        self.view = QtWidgets.QTableView(showGrid=False, selectionBehavior=QtWidgets.QAbstractItemView.SelectRows)

        self.view.setModel(self.model)
        self.view.setHorizontalHeader(QtWidgets.QHeaderView(QtCore.Qt.Horizontal))

        self.view.verticalHeader().hide()

        # self.view.horizontalHeader().setMinimumSectionSize(0)
        self.view.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.view.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.view.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
        # self.view.horizontalHeader().(True)

        self.box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok
            | QtWidgets.QDialogButtonBox.Cancel
            | QtWidgets.QDialogButtonBox.NoButton
            | QtWidgets.QDialogButtonBox.Help,
            QtCore.Qt.Vertical,
        )

        hlay = QtWidgets.QHBoxLayout(self)
        hlay.addWidget(self.view)
        hlay.addWidget(self.box)

        self.box.accepted.connect(self.accept)
        self.box.rejected.connect(self.reject)
        self.box.helpRequested.connect(self.on_helpRequested)

        self.resize(500, 240)

    # @QtCore.pyqtSlot()
    def on_helpRequested(self):
        QtWidgets.QMessageBox.aboutQt(self)



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    w = Dialog()
    w.show()

    sys.exit(app.exec_())