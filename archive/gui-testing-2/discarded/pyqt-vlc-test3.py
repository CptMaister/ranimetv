import sys
import vlc
from PyQt5 import QtCore, QtGui
from video_player_main_window import Ui_MainWindow

class StartQT4(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.vlc_instance = vlc.Instance()
        self.mediaplayer = self.vlc_instance.media_player_new()
        self.mediaplayer.set_hwnd(int(self.frame.winId()))
        self.media_path = "test_video.mp4"
        self.media = self.vlc_instance.media_new(self.media_path)
        self.media.get_mrl()
        self.mediaplayer.set_media(self.media)
        self.mediaplayer.play()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = StartQT4()
    myapp.show()
    sys.exit(app.exec_())