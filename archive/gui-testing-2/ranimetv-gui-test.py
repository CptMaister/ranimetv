#! /usr/bin/python
import copy
import math
import threading
import time
from math import floor
import sys
import os.path
import random
from enum import Enum, unique, auto as auto_enum

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from equalizer_bar import EqualizerBar
import hashlib, colorsys

import vlc


TIMER_PLAYBACK_UPDATE_MS = 250
TIMER_EQUALIZER_UPDATE_MS = 100
BUTTON_PLAYBACK_SQUARE_SIZE = 35
BUTTON_PLAYBACK_ICON_SIZE = 25
BUTTON_PLAYBACK_NARROW_WIDTH = 20
BUTTON_PLAYBACK_SQUARE_FONT_SIZE = 20
BUTTON_QUEUE_SHOWS_WIDTH = 23
BUTTON_QUEUE_SHOWS_HEIGHT = 23
BUTTON_QUEUE_SHOWS_ICON_SIZE = 20
BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM = 16

STATUSTIP_CONTROL_PLAY = "Play"
ICON_PLAY = "icons/play.svg"
ICON_PLAY_START = "icons/play_start.svg"
STATUSTIP_CONTROL_PAUSE = "Pause"
ICON_PAUSE = "icons/pause.svg"
ICON_STOP = "icons/stop.svg"
ICON_SKIP = "icons/skip.svg"
ICON_BACKWARD = "icons/backward.svg"
ICON_FORWARD = "icons/forward.svg"
ICON_LOOP = "icons/loop.svg"
ICON_LOOP = "icons/loop.svg"
ICON_LOOPING = "icons/looping.svg"
ICON_TIMER = "icons/timer.svg"
ICON_TIMED = "icons/timed.svg"
ICON_FASTER = "icons/faster.svg"
ICON_SLOWER = "icons/slower.svg"
ICON_SPEAKER_UNMUTED = "icons/speaker_unmuted.svg"
ICON_SPEAKER_MUTED = "icons/speaker_muted.svg"
ICON_FOLDER = "icons/folder.svg"
ICON_FOLDER_BLACK = "icons/folder_black.svg"
ICON_FULLSCREEN = "icons/fullscreen.svg"
ICON_EDIT = "icons/edit.svg"
ICON_EDIT_THIN = "icons/edit_thin.svg"
ICON_ADD = "icons/add_plus.svg"
ICON_ADD_THIN = "icons/add_plus_thin.svg"
ICON_REMOVE = "icons/remove_x.svg"
ICON_REMOVE_THIN = "icons/remove_x_thin.svg"
ICON_SORT = "icons/sort.svg"
ICON_MOVE_UP = "icons/move_up.svg"
ICON_MOVE_DOWN = "icons/move_down.svg"
ICON_MOVE_LEFT = "icons/move_left.svg"
ICON_DUPLICATE = "icons/duplicate.svg"
# TEXT_CONTROL_ # TODO
# TEXT_CONTROL_
# TEXT_CONTROL_
# TEXT_CONTROL_
# TEXT_CONTROL_
# TEXT_CONTROL_


TEXT_RADIO_BINARY_TRUE = "✔"
TEXT_RADIO_BINARY_FALSE = "✘"

TEXT_RADIO_TERNARY_HARD = "✔✔"
TEXT_RADIO_TERNARY_TRUE = "✔"
TEXT_RADIO_TERNARY_FALSE = "✘"
TEXT_RADIO_TERNARY_NONE = "~"


PROGRAM_NAME = "RAnimeTV GUI (working title)"

### dummy lists, to be replaced by configs later ###
DUMMY_USER_LIST = ["Alex", "Extra", "User3"]
DUMMY_PROFILE_LIST = ["Normal", "Fokus", "Chill"]
DUMMY_BASEPATH_LIST = ["/media/alex/678001755456034B/Sprachen", "D:\\Sprachen"]

DUMMY_SORT_LIST = ["Creation Date", "Name", "Tags", "Current Episode"]

DUMMY_SHOW_LIST = ["Death Note", "Samurai Champloo", "Dr. Stone", "Psycho Pass S2", "Mob Psycho 100"]
DUMMY_SHOW_LIST_T = [
    ("Death Note", 2, ["tag1", "tag2"]),
    ("Samurai Champloo", 3, ["tag1", "tag2"]),
    ("Dr. Stone", 4, ["tag1", "tag2"]),
    ("Psycho Pass S2", 6, ["tag1", "tag2"]),
    ("Mob Psycho 100", 5, ["tag1", "tag2"])
]

DUMMY_SUSPENDED_QUEUE_LIST = ["Samurai Champloo", "Dr. Stone", "Mob Psycho 100", "Samurai Champloo", "Psycho Pass S2"]
DUMMY_STARTWITHSHOWS_LIST = ["Ajinomoto", "Dr. Stone"]

DUMMY_CMS_LIST = ["Ajinomoto", "Nintendo cms 1min", "Sony"]
DUMMY_CMS_LIST_T = [
    ("Ajinomoto", 7, None, 1),
    ("Nintendo cms 1min", 2, 1, None),
    ("Sony", 3, None, None),
    ("Test langer Name Werbung ho cms 15-30sec", 2, 1, None)
]

DUMMY_QUEUE_LIST = ["Mob Psycho 100", "Death Note", "Psycho Pass S2"]
DUMMY_QUEUE_LIST_L = ["Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2", "Mob Psycho 100", "Death Note", "Psycho Pass S2"]
DUMMY_QUEUE_LIST_T = [
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "✘"),
]
DUMMY_QUEUE_LIST_T_L = [
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "✘"),
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "✘"),
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "✘"),
    ("Mob Psycho 100", 3, "✔", "✔"),
    ("Death Note", 17, "✔", "✘"),
    ("Psycho Pass S2", 5, "✔", "✘"),
    ("Death Note", 18, "✔", "✘"),
]
DUMMY_HISTORY_LIST = ["path/to/Death Note 2", "long/path/very/long/to/Samurai Champloo 1", "home/Dr. Stone", "Psycho Pass S2 13", "Mob Psycho 100"]
DUMMY_HISTORY_LIST_T = [
    ("path/to/Death Note 2", "0:23"),
    ("long/path/very/long/to/Samurai Champloo 1", "7:23"),
    ("home/Dr. Stone", "7:56"),
    ("Psycho Pass S2 13", "9:23"),
    ("Mob Psycho 100", "2:43:01")
]

DUMMY_LIST_PROFILES = ["Profile1", "Fokus", "Late Night Funkin", "Background", "News stuff", "Commercials Only wtf", "P7", "P8", "P9"]

DUMMY_LIST_EXCLUDE_PROFILES = ["Profile1", "P9"]
DUMMY_LIST_INCLUDE_PROFILES = ["Fokus", "Late Night Funkin", "Background", "News stuff", "P8"]

DUMMY_LIST_TAGS = ["youtube", "anime", "series", "teach", "commercial", "let's play"]




DUMMY_VERSION_TEXT = "V.0.0.0"




##### Set Limits, Standard values etc #####

VOLUME_LIMIT_LOWER = 0
VOLUME_LIMIT_UPPER = 200
SPEED_LIMIT_LOWER = 40
SPEED_LIMIT_UPPER = 300
MONITOR_LIMIT_LOWER = 1
PROBABILITY_LIMIT_LOWER = 5
PROBABILITY_LIMIT_UPPER = 100
QUEUE_LOAD_LIMIT_UPPER = 25


QUEUE_SIZE_STANDARD = 3
BACK_PLAYBACK_SIZE_STANDARD = 5
FORWARD_PLAYBACK_SIZE_STANDARD = 5
JUMP_PLAYBACK_BUMPER_SIZE_STANDARD = 20
SUSPEND_BACK_SIZE_STANDARD = 10
SUSPENDED_OPENING_TIME = 60
END_PLAYBACK_SIZE_STANDARD = 5
DEBUG_PLAYER_JUMPS_ANOMALY_SENSITIVITY_SIZE_STANDARD = 2
DEBUG_PLAYER_JUMPS_SKIP_SIZE_STANDARD = 1
MEDIA_LENGTH_ANALYSIS_QUICK_SIZE_STANDARD = 3





##### code directly copied from ranimetv code, maybe import later instead
def get_h_m_s_from_milliseconds(milli: int):
    """Get milliseconds converted to hh:mm:ss. If hh == 0, then mm:ss"""
    sec = floor(milli / 1000)
    s = sec % 60
    m = floor(sec / 60) % 60
    h = floor(sec / (60 * 60))
    if h > 0:
        return f'{"{0:0=2d}".format(h)}:{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'
    else:
        return f'{"{0:0=2d}".format(m)}:{"{0:0=2d}".format(s)}'


@unique
class DockedState(Enum):
    PLAYER_WINDOW = auto_enum()
    EXTRA_WINDOW = auto_enum()
    NOWHERE = auto_enum()


@unique
class MediaAreaState(Enum):
    VIDEO_EQUALIZER = auto_enum()
    VIDEO_NOEQUALIZER = auto_enum()
    EQUALIZERONLY = auto_enum()
    MANAGEMENT_ONLY = auto_enum()
    NOTHING = auto_enum()

    @staticmethod
    def next_value(state):
        if state == MediaAreaState.VIDEO_EQUALIZER:
            return MediaAreaState.VIDEO_NOEQUALIZER
        elif state == MediaAreaState.VIDEO_NOEQUALIZER:
            return MediaAreaState.EQUALIZERONLY
        elif state == MediaAreaState.EQUALIZERONLY:
            return MediaAreaState.MANAGEMENT_ONLY
        elif state == MediaAreaState.MANAGEMENT_ONLY:
            return MediaAreaState.NOTHING
        elif state == MediaAreaState.NOTHING:
            return MediaAreaState.VIDEO_EQUALIZER





# noinspection PyUnresolvedReferences
class PlayerWindow(QMainWindow):
    """A simple Media Player using VLC and Qt
    """
    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.setWindowTitle(PROGRAM_NAME)

        # creating a basic vlc instance
        self.instance = vlc.Instance()
        # creating an empty vlc media player
        self.mediaplayer = self.instance.media_player_new()

        self.createUI()
        self.isPaused = False


    def createUI(self):
        """Set up the user interface, signals & slots
        """
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)

        self.ui_media_area = UiMediaArea(
            self.mediaplayer,
            self.style()
        )

        # playback control stuff (time slider, volume, all playback controlling buttons)
        self.ui_playback_control = UiPlaybackControlVBox(
            self.style(),
            self.mediaplayer,
            self.ui_media_area.equalizer,
            self.open_file_dialog,
            self.ui_media_area.timer_equalizer.start,
            self.ui_media_area.timer_equalizer.stop,
            self.ui_media_area.toggle_media_area_state,
            self.ui_media_area.set_media_area_state,
            self.ui_media_area.get_media_area_state,
            self.ui_media_area.showTextOnMedia
        )

        # combine all layouts
        self.vboxlayout = QVBoxLayout()
        self.vboxlayout.addLayout(self.ui_media_area)
        self.vboxlayout.addLayout(self.ui_playback_control)
        self.vboxlayout.setContentsMargins(0, 0, 0, 0)

        self.vboxlayout.setAlignment(self.ui_media_area, Qt.AlignCenter)
        self.vboxlayout.setAlignment(self.ui_playback_control, Qt.AlignBottom)

        self.widget.setLayout(self.vboxlayout)


        # self.widget.setAutoFillBackground(True) # TODO delete
        # tmp_palette_yellow = QPalette() # TODO delete
        # tmp_palette_yellow.setColor(QPalette.Window, Qt.darkYellow) # TODO delete
        # self.widget.setPalette(tmp_palette_yellow) # TODO delete


        # create menu bar
        self.ui_menubar_player = UiMenubarPlayer(
            self.open_file_dialog
        )

        self.setMenuBar(self.ui_menubar_player)
        # self.statusBar() # activate status tip bar
        self.status_bar = self.statusBar()
        self.status_bar.addPermanentWidget(QLabel(DUMMY_VERSION_TEXT))
        self.setStatusBar(self.status_bar)

        self.setWindowIcon(QIcon("icons/logo_1.svg"))



    # some methods

    # def open_file_dialog(self, filename=None):
    def open_file_dialog(self):
        """Open a media file in a MediaPlayer
        """
        # if filename is None:
        #     filename = QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
        filename = QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
        if not filename:
            return

        # create the media
        if sys.version < '3':
            filename = unicode(filename)
        self.media = self.instance.media_new(filename)
        # put the media in the media player
        self.mediaplayer.set_media(self.media)

        # parse the metadata of the file
        self.media.parse()
        # set the title of the track as window title
        self.setWindowTitle(self.media.get_meta(0))

        self.ui_playback_control.handlePlayPausePlayback()
        self.ui_media_area.timer_equalizer.start()



class UiMediaArea(QVBoxLayout):

    def __init__(
            self,
            mediaplayer: vlc.MediaPlayer,
            window_style: QStyle
    ):
        super().__init__()

        self.mediaplayer = mediaplayer

        # In this widget, the video will be drawn
        if sys.platform == "darwin":  # for MacOS
            from PyQt5.QtWidgets import QMacCocoaViewContainer
            self.mediaplayer_frame = QMacCocoaViewContainer(0)
        else:
            self.mediaplayer_frame = QFrame()
        self.palette = self.mediaplayer_frame.palette()
        self.palette.setColor(QPalette.Window,
                              QColor(0, 0, 0))
        self.mediaplayer_frame.setPalette(self.palette)
        self.mediaplayer_frame.setAutoFillBackground(True)
        self.mediaplayer_frame.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding) #FIXME: i have no idea why this works

        self.h_box_mediaplayer = QHBoxLayout()
        self.h_box_mediaplayer.addWidget(self.mediaplayer_frame)
        self.h_box_mediaplayer.setContentsMargins(0, 0, 0, 0)

        self.h_box_mediaplayer_frame = QFrame()
        self.h_box_mediaplayer_frame.setLayout(self.h_box_mediaplayer)
        # self.h_box_mediaplayer_frame.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.h_box_mediaplayer_frame.setHidden(True) # hide all stuff first, show desired ones in self.handle_media_area_state()


        # the media player has to be 'connected' to the QFrame
        # (otherwise a video would be displayed in it's own window)
        # this is platform specific!
        # you have to give the id of the QFrame (or similar object) to
        # vlc, different platforms have different functions for this
        if sys.platform.startswith('linux'): # for Linux using the X Server
            self.mediaplayer.set_xwindow(self.mediaplayer_frame.winId())
        elif sys.platform == "win32": # for Windows
            self.mediaplayer.set_hwnd(self.mediaplayer_frame.winId())
        elif sys.platform == "darwin": # for MacOS
            self.mediaplayer.set_nsobject(int(self.mediaplayer_frame.winId()))


        # Equalizer that visualizes media without a video track
        self.equalizer = EqualizerBar(8, 3)  # number of columns, height (defined by colors amount, else red)
        self.equalizer.setColors(['#0C0786', '#40039C', '#6A00A7', '#8F0DA3', '#B02A8F', '#CA4678', '#E06461','#F1824C', '#FCA635', '#FCCC25', '#EFF821'])
        # self.equalizer.setVisible(True)  # make visible when no video track available
        self.equalizer.setDecayFrequencyMs(TIMER_EQUALIZER_UPDATE_MS)
        # self.equalizer.setColors(["#810f7c", "#8856a7", "#8c96c6", "#b3cde3", "#edf8fb"]) # adjust color of bars (and height automatically)
        # self.equalizer.setBarPadding(90) # set padding outside of equalizer, in pixel
        self.equalizer.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding) #FIXME: i have no idea why this works

        self.equalizer_layout = QHBoxLayout()
        self.equalizer_layout.addWidget(self.equalizer)
        self.equalizer_layout.setContentsMargins(0, 0, 0, 0)
        self.setupEqualizerText()

        self.equalizer_frame = QFrame()
        self.equalizer_frame.setLayout(self.equalizer_layout)
        # self.equalizer_frame.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.equalizer_frame.setHidden(True) # hide all stuff first, show desired ones in self.handle_media_area_state()


        # create timer updating the equalizer
        self.timer_equalizer = QTimer()
        self.timer_equalizer.setInterval(TIMER_EQUALIZER_UPDATE_MS)
        self.timer_equalizer.timeout.connect(self.update_equalizer)

        # put equalizer in a frame that can be easily hidden
        # self.equalizer_frame = QFrame()
        # self.equalizer_frame.setLayout(self.equalizer_layout)
        # self.equalizer_frame.setHidden(True)


        self.management_ui = UiManagementMain(window_style)

        self.management_ui_frame = QFrame()
        self.management_ui_frame.setLayout(self.management_ui)
        self.management_ui_frame.setContentsMargins(0, 0, 0, 0)
        # this is some hard cheese to get this thing to work (so that .setHidden(False) won't pop it out into a new window) FIXME
        self.management_ui_h_box_tmp = QHBoxLayout()
        self.management_ui_h_box_tmp.addWidget(self.management_ui_frame)
        # self.management_ui_h_box_tmp.setContentsMargins(5, 5, 5, 0)

        self.management_ui_frame_tmp = QFrame()
        self.management_ui_frame_tmp.setLayout(self.management_ui_h_box_tmp)

        # self.management_ui_frame_tmp.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding) #FIXME: i have no idea why this works
        # self.management_ui_frame.setHidden(True) # hide all stuff first, show desired ones in self.handle_media_area_state()



        # combine all media area things (video player, equalizer, show management)
        self.addWidget(self.h_box_mediaplayer_frame)
        self.addWidget(self.equalizer_frame)
        self.addWidget(self.management_ui_frame_tmp)
        # self.addWidget(self.management_frame)

        # self.management_ui_frame.setAutoFillBackground(True) # TODO delete
        # tmp_palette_yellow = QPalette() # TODO delete
        # tmp_palette_yellow.setColor(QPalette.Window, Qt.darkYellow) # TODO delete
        # self.management_ui_frame.setPalette(tmp_palette_yellow) # TODO delete


        self.visibility_state: MediaAreaState = MediaAreaState.VIDEO_EQUALIZER
        self.handle_media_area_state()


    def toggle_media_area_state(self):
        # rotate through the enum
        self.visibility_state = MediaAreaState.next_value(self.visibility_state)

        print(self.visibility_state)

        self.handle_media_area_state()


    def set_media_area_state(self, state: MediaAreaState):
        self.visibility_state = state
        self.handle_media_area_state()


    def get_media_area_state(self):
        return self.visibility_state


    def handle_media_area_state(self):
        self.handle_show_hide_equalizer()

        if (not self.mediaplayer.get_media() is None) and (
                self.visibility_state == MediaAreaState.VIDEO_EQUALIZER
                or self.visibility_state == MediaAreaState.VIDEO_NOEQUALIZER
        ) and self.mediaplayer.has_vout() == 1:
            print("add video")
            self.h_box_mediaplayer_frame.setHidden(False)
        else:
            print("remove video")
            self.h_box_mediaplayer_frame.setHidden(True)

        if (
                self.visibility_state == MediaAreaState.MANAGEMENT_ONLY
                or (
                    (not self.visibility_state == MediaAreaState.NOTHING)
                    and self.mediaplayer.get_media() is None # TODO: do this when session is stopped, better detection than check if media is null maybe later
                )
        ):
            self.management_ui_frame.setHidden(False)
            pass
        else:
            self.management_ui_frame.setHidden(True)
            pass



    def update_equalizer(self):
        self.equalizer.setValues([
            min(100, v + random.randint(0, 50) if random.randint(0, 5) > 2 else v)
            for v in self.equalizer.values()
        ])


    def handle_show_hide_equalizer(self):
        # handle if visualizer should be shown instead of media if no video track present # TODO: move into player handle options post play
        if self.mediaplayer.get_media() is None or (
                self.visibility_state is MediaAreaState.VIDEO_NOEQUALIZER
                or self.visibility_state is MediaAreaState.MANAGEMENT_ONLY
                or self.visibility_state is MediaAreaState.NOTHING
                or (
                    self.visibility_state is MediaAreaState.VIDEO_EQUALIZER
                    and self.mediaplayer.has_vout() == 1  # 1 if has video track, 0 if none
                )
        ):
            self.hide_equalizer()
        else:
            self.show_equalizer()


    def show_equalizer(self):
        if self.mediaplayer.is_playing():
            self.timer_equalizer.start()
        self.equalizer_frame.setHidden(False)


    def hide_equalizer(self):
        self.timer_equalizer.stop()
        self.equalizer_frame.setHidden(True)


    def showTextOnMedia(self, text: str, duration: int = 2000):
        vlcPlayerTextActivate(self.mediaplayer, text, VlcPlayerTextPosition.TOP_LEFT, duration)
        self.eqTextActivate(text, duration)


    def setupEqualizerText(self):
        self.eq_texts = []

        self.eq_text_shade1 = QLabel(self.equalizer)
        self.eq_text_shade1.setText("Henlo")
        self.tmp_font = self.eq_text_shade1.font()
        self.tmp_font.setPointSize(int(self.equalizer.height() / 22))
        self.eq_text_shade1.setFont(self.tmp_font)
        self.eq_text_shade1.move(22, 22)
        self.eq_text_shade1.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade1)

        self.eq_text_shade2 = QLabel(self.equalizer)
        self.eq_text_shade2.setText("Henlo")
        self.eq_text_shade2.setFont(self.tmp_font)
        self.eq_text_shade2.move(18, 18)
        self.eq_text_shade2.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade2)

        self.eq_text_shade3 = QLabel(self.equalizer)
        self.eq_text_shade3.setText("Henlo")
        self.eq_text_shade3.setFont(self.tmp_font)
        self.eq_text_shade3.move(22, 18)
        self.eq_text_shade3.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade3)

        self.eq_text_shade4 = QLabel(self.equalizer)
        self.eq_text_shade4.setText("Henlo")
        self.eq_text_shade4.setFont(self.tmp_font)
        self.eq_text_shade4.move(18, 22)
        self.eq_text_shade4.setStyleSheet("color: black")
        self.eq_texts.append(self.eq_text_shade4)

        self.eq_text = QLabel(self.equalizer)
        self.eq_text.setText("Henlo")
        self.eq_text.setFont(self.tmp_font)
        self.eq_text.move(20, 20)
        self.eq_text.setStyleSheet("color: white")
        self.eq_texts.append(self.eq_text)


    def eqTextActivate(self, text: str, duration: int = 2000):
        self.eq_text_thread = threading.Thread(target=self.eqTextActivateThread, args=(text, duration), daemon=True)
        self.eq_text_thread.start()


    def eqTextActivateThread(self, text: str, duration: int = 2000):
        self.tmp_font.setPointSize(int(self.equalizer.height() / 22))
        for t in self.eq_texts:
            t.setText(text)
            t.setFont(self.tmp_font)
            t.adjustSize()

        time.sleep(duration / 1000)

        if text != self.eq_text.text():
            return

        for t in self.eq_texts:
            t.setText("")





class UiManagementMain(QVBoxLayout):

    def __init__(
            self,
            window_style: QStyle
    ):
        super().__init__()

        # some important selection stuff
        self.control_top_row = UiManagementTopRow()

        self.control_top_row.user_select_combo.currentIndexChanged.connect(self.handleUserChanged)
        self.control_top_row.profile_select_combo.currentIndexChanged.connect(self.handleProfileChanged)
        self.control_top_row.basepath_checkbox.clicked.connect(self.handleBasepathChanged)
        self.control_top_row.basepath_select_combo.currentIndexChanged.connect(self.handleBasepathChanged)


        # queue and show management
        self.qh_tabs = UiManagementQueueHistoryTabs()

        # self.qh_tabs.queue_buttons.add_one.clicked.connect() # TODO
        # self.qh_tabs.queue_buttons.remove.clicked.connect() # TODO
        # self.qh_tabs.queue_buttons.move_up.clicked.connect() # TODO
        # self.qh_tabs.queue_buttons.move_down.clicked.connect() # TODO



        # self.shows_list = QListView()
        self.sc_tabs = UiManagementShowsCmsTabs()

        # self.sc_tabs.shows_buttons.add_to_queue.clicked.connect() # TODO
        self.sc_tabs.shows_buttons.sort.clicked.connect(self.handleShowSort)
        self.sc_tabs.shows_buttons.add_new.clicked.connect(self.handleShowAdd)
        self.sc_tabs.shows_buttons.edit.clicked.connect(self.handleShowEdit)

        self.sc_tabs.commercials_buttons.sort.clicked.connect(self.handleCommercialSort)
        self.sc_tabs.commercials_buttons.add_new.clicked.connect(self.handleShowAdd)
        self.sc_tabs.commercials_buttons.edit.clicked.connect(self.handleShowEdit)


        # put into splitter, makes the width of the queues and shows tables adjustable
        self.splitter_queue_shows = QSplitter(Qt.Horizontal)
        self.splitter_queue_shows.addWidget(self.qh_tabs)
        self.splitter_queue_shows.addWidget(self.sc_tabs)
        self.splitter_queue_shows.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)


        # combine all
        self.addLayout(self.control_top_row)
        self.addWidget(self.splitter_queue_shows)
        self.setContentsMargins(0, 0, 0, 0)



    def handleUserChanged(self):
        """Changes the current user, opens popup dialog if session is running (user has to stop first).
        """
        selected_index = self.control_top_row.user_select_combo.currentIndex()
        print("User changed: " + DUMMY_USER_LIST[selected_index])
        # TODO


    def handleProfileChanged(self):
        """Changes the current profile, opens popup dialog if session is running (user has to stop first).
        """
        selected_index = self.control_top_row.profile_select_combo.currentIndex()
        print("Profile changed: " + DUMMY_PROFILE_LIST[selected_index])
        # TODO


    def handleBasepathChanged(self):
        """Changes the current basepath, opens popup dialog if session is running and checkbox is checked (user has to stop first).
        """
        selected_index = self.control_top_row.basepath_select_combo.currentIndex()
        print("Basepath changed: " + DUMMY_BASEPATH_LIST[selected_index])
        print("Basepath checked: " + str(self.control_top_row.basepath_checkbox.isChecked()))
        # TODO

    def handleShowSort(self):
        item, okPressed = QInputDialog.getItem(self.sc_tabs.shows_buttons.sort, "Sort Shows", "Sort by:", DUMMY_SORT_LIST, 0, False)
        if okPressed and item:
            print(item)

    def handleCommercialSort(self):
        item, okPressed = QInputDialog.getItem(self.sc_tabs.shows_buttons.sort, "Sort Commercials", "Sort by:", DUMMY_SORT_LIST, 0, False)
        if okPressed and item:
            print(item)


    def handleShowAdd(self):
        """Open widget in new window to create new show
        """
        self.dialog_new_show = UiShowsNewDialog()
        self.dialog_new_show.show()


    def handleShowEdit(self):
        """Open widget in new window to edit selected show
        """
        show_index = self.sc_tabs.shows_table.currentIndex().row()
        print("Edit show with index: " + str(show_index)) # TODO
        self.dialog_edit_shows = UiShowsEditDialog(
            self.sc_tabs.shows_table.currentIndex().row()
        )
        self.dialog_edit_shows.show()




class UiManagementTopRow(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.user_profiles_paths_edit_button = UiManagementToolButton(ICON_EDIT, "Edit Users, Profiles and Base Paths.")
        self.user_profiles_paths_edit_button.clicked.connect(self.editUserProfilePath)

        self.user_select_label = QLabel("User:")
        self.user_select_label.setStatusTip("Current User.")

        self.user_select_combo = QComboBox()
        self.user_select_combo.setMaximumWidth(110)
        self.user_select_combo.addItems(DUMMY_USER_LIST)
        self.user_select_combo.setStatusTip("Select the current User. Only if no running session.")

        self.sep_1 = QLabel("||")

        self.profile_select_label = QLabel("Profile:")
        self.profile_select_label.setStatusTip("Current Profile.")

        self.profile_select_combo = QComboBox()
        self.profile_select_combo.setMaximumWidth(110)
        self.profile_select_combo.addItems(DUMMY_PROFILE_LIST)
        self.profile_select_combo.setStatusTip("Select the current Profile. Only if no running session.")
        # self.profile_select_combo.setDisabled(True) # TODO: disable when playing

        self.sep_2 = QLabel("||")

        self.basepath_checkbox = QCheckBox("Base Path:")
        # self.basepath_checkbox.setCheckState(Qt.PartiallyChecked) # tristate null disabled
        # self.basepath_checkbox.setTristate(True) # None, True, False
        self.basepath_checkbox.setStatusTip("Toggle if base path should be used. Only if no running session.")

        self.basepath_select_combo = QComboBox()
        self.basepath_select_combo.setMaximumWidth(110)
        self.basepath_select_combo.addItems(DUMMY_BASEPATH_LIST)
        self.basepath_select_combo.setStatusTip("Select the current base path. Only if no running session.")


        self.addStretch()
        # self.addLayout(self.user_select_box)
        # self.addLayout(self.profile_select_box)
        # self.addLayout(self.basepath_select_box)
        self.addWidget(self.user_profiles_paths_edit_button)
        self.addWidget(self.user_select_label)
        self.addWidget(self.user_select_combo)
        self.addWidget(self.sep_1)
        self.addWidget(self.profile_select_label)
        self.addWidget(self.profile_select_combo)
        self.addWidget(self.sep_2)
        # self.addWidget(self.basepath_select_label)
        self.addWidget(self.basepath_checkbox)
        self.addWidget(self.basepath_select_combo)
        self.setContentsMargins(5, 5, 5, 5)


    def editUserProfilePath(self):
        """Open widget in new window to edit users, profiles, base paths
        """
        self.dialog_edit = UiEditUserProfilePath(
            self.user_select_combo.currentIndex(),
            self.profile_select_combo.currentIndex(),
            self.basepath_select_combo.currentIndex()
        )
        self.dialog_edit.show()




class UiEditUserProfilePath(QDialog):

    def __init__(
            self,
            index_user: int = 0,
            index_profile: int = 0,
            index_path: int = 0
    ):
        super().__init__()

        self.setWindowTitle("Edit users, profiles, base paths")
        self.setWindowModality(Qt.ApplicationModal)

        self.edit_generalSettings_widget = UiEditGeneralSettingsWidget(index_path)
        self.edit_users_widget = UiEditUsersWidget(index_user)
        self.edit_profiles_widget = UiEditProfilesWidget(index_profile)

        # put into tabs
        self.tabs = QTabWidget()
        self.tabs.addTab(self.edit_generalSettings_widget, "General Settings")
        self.tabs.addTab(self.edit_users_widget, "Users")
        self.tabs.addTab(self.edit_profiles_widget, "Profiles")

        self.tabs.setCurrentIndex(2)

        self.layout_all = QVBoxLayout()
        self.layout_all.addWidget(self.tabs)

        self.setLayout(self.layout_all)



class UiEditUsersWidget(QFrame):

    def __init__(
            self,
            index_current: int = 0
    ):
        super().__init__()

        self.users_list = QListWidget()
        self.users_list.addItems(DUMMY_USER_LIST)
        self.users_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.users_list.setCurrentRow(index_current)
        self.users_list.setFixedWidth(150)
        self.users_list.setAlternatingRowColors(True)

        self.users_buttons = UiEditUsersButtons()
        self.users_buttons.add_one.clicked.connect(self.addUserDialog)
        self.users_buttons.remove.clicked.connect(self.removeUserDialog)
        self.users_buttons.duplicate.clicked.connect(self.duplicateUserDialog)

        self.v_box_users = QVBoxLayout()
        self.v_box_users.addWidget(self.users_list)
        self.v_box_users.addLayout(self.users_buttons)
        self.v_box_users.setContentsMargins(0, 0, 0, 0)

        # form layouts for the settings
        self.main_settings = UiUserMainSettings()

        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save,
            Qt.Horizontal
        )
        self.button_box.accepted.connect(lambda: print("save user")) # TODO handle save button

        # combine settings and buttons
        self.v_box_settings_all = QVBoxLayout()
        self.v_box_settings_all.addLayout(self.main_settings)
        self.v_box_settings_all.addWidget(self.button_box)

        # combine all in stack-like layout
        self.layout_all = QHBoxLayout()
        self.layout_all.addLayout(self.v_box_users)
        self.layout_all.addLayout(self.v_box_settings_all)
        self.layout_all.addStretch()

        self.setLayout(self.layout_all)


    def addUserDialog(self):
        i, okPressed = QInputDialog.getText(self, "New User", "Name of new user:", QLineEdit.Normal, "")
        if okPressed:
            print(i)


    def removeUserDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete user '" + self.users_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText("This action cannot be undone. You will lose all settings for all the profiles and shows of this user.")
        msg.setWindowTitle("Delete User")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeUserConfirmed)
        msg.exec_()

    def removeUserConfirmed(self):
        print("Delete " + self.users_list.selectedItems()[0].text()) # TODO


    def duplicateUserDialog(self):
        i, okPressed = QInputDialog.getText(self, "Duplicate User", "Name of copy of '" + self.users_list.selectedItems()[0].text() + "':", QLineEdit.Normal, "")
        if okPressed:
            print(i)



class UiEditUsersButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new User.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected User.", True, True)
        self.duplicate = UiManagementToolButton(ICON_DUPLICATE, "Duplicate the current User.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.duplicate)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)




class UiUserMainSettings(QFormLayout):

    def __init__(self):
        super().__init__()

        self.name_label = UiLabelTooltip("Name", "Give the user a name.")
        self.name_input = QLineEdit()
        self.name_input.setMinimumWidth(350)
        self.name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.name_input.setValidator(self.name_validator)
        self.name_input.setPlaceholderText("Enter user name")

        self.addRow(self.name_label, self.name_input)





class UiEditProfilesWidget(QFrame):

    def __init__(
            self,
            index_current: int = 0
    ):
        super().__init__()

        self.profiles_list = QListWidget()
        self.profiles_list.addItems(DUMMY_PROFILE_LIST)
        self.profiles_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.profiles_list.setCurrentRow(index_current)
        self.profiles_list.setFixedWidth(150)
        self.profiles_list.setAlternatingRowColors(True)

        self.profiles_buttons = UiEditProfilesButtons()
        self.profiles_buttons.add_one.clicked.connect(self.addProfileDialog)
        self.profiles_buttons.remove.clicked.connect(self.removeProfileDialog)
        self.profiles_buttons.duplicate.clicked.connect(self.duplicateProfileDialog)

        self.v_box_profiles = QVBoxLayout()
        self.v_box_profiles.addWidget(self.profiles_list)
        self.v_box_profiles.addLayout(self.profiles_buttons)
        self.v_box_profiles.setContentsMargins(0, 0, 0, 0)

        # form layouts for the settings
        self.main_settings = UiProfileMainSettings()
        self.extra_settings = UiProfileExtraSettings()
        self.common_settings = UiShowProfileCommonSettings(False)

        # add extra and common settings into an HBox
        self.settings_h_box = QHBoxLayout()
        self.settings_h_box.addLayout(self.extra_settings)
        self.settings_h_box.addLayout(self.common_settings)

        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save,
            Qt.Horizontal
        )
        self.button_box.accepted.connect(lambda: print("save profile")) # TODO handle save button

        # combine settings and buttons
        self.v_box_settings_all = QVBoxLayout()
        self.v_box_settings_all.addLayout(self.main_settings)
        self.v_box_settings_all.addLayout(self.settings_h_box)
        self.v_box_settings_all.addWidget(self.button_box)

        # combine all in stack-like layout
        self.layout_all = QHBoxLayout()
        self.layout_all.addLayout(self.v_box_profiles)
        self.layout_all.addLayout(self.v_box_settings_all)

        self.setLayout(self.layout_all)


    def addProfileDialog(self):
        i, okPressed = QInputDialog.getText(self, "New Profile", "Name of new profile:", QLineEdit.Normal, "")
        if okPressed:
            print(i)


    def removeProfileDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete profile '" + self.profiles_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText("This action cannot be undone.")
        msg.setWindowTitle("Delete Profile")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeProfileConfirmed)
        msg.exec_()

    def removeProfileConfirmed(self):
        print("Delete " + self.profiles_list.selectedItems()[0].text()) # TODO


    def duplicateProfileDialog(self):
        i, okPressed = QInputDialog.getText(self, "Duplicate Profile", "Name of copy of '" + self.profiles_list.selectedItems()[0].text() + "':", QLineEdit.Normal, "")
        if okPressed:
            print(i)



class UiEditGeneralSettingsWidget(QFrame):

    def __init__(
            self,
            index_current_base_path: int = 0
    ):
        super().__init__()

        self.general_settings = UiEditGeneralSettingsLayout()

        self.base_paths = UiEditBasePathsLayout(index_current_base_path)

        self.layout_all = QVBoxLayout()
        self.layout_all.addLayout(self.general_settings)
        self.layout_all.addLayout(self.base_paths)

        self.setLayout(self.layout_all)



class UiEditGeneralSettingsLayout(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.form = UiEditGeneralSettingsForm()

        self.title_label = UiLabelTooltip("General Settings and GUI", "Some settings influencing all users and profiles. Also some graphical user interface settings.", True)

        self.addWidget(self.title_label)
        self.addLayout(self.form)




class UiEditGeneralSettingsForm(QFormLayout):

    def __init__(self):
        super().__init__()

        self.noAutosave_label = UiLabelTooltip("No Save", "When enabled, nothing will be saved until this is turned off again.\nThis setting will be disabled when you close the program, since this setting can't be saved.")
        self.noAutosave_box = UiFormRadioBinary(default=False)

        self.dynamicSettings_label = UiLabelTooltip("Dynamic Settings", "When enabled, changing one of the following values in the playback control, it will be automatically updated in the show's settings:\nvolume, speed, video track, audio track, subtitle track.\n\nA second volume slider will be added to the playback controls. The normal slider will be on top, the show volume slider below.")
        self.dynamicSettings_box = UiFormRadioBinary(default=False)

        self.excludeFilesEndingWith_label = UiLabelTooltip("Exclude Files", "Ignore all files that end with a specified string.\nSeparate multiple with comma.")
        self.excludeFilesEndingWith_input = QLineEdit()
        self.excludeFilesEndingWith_input.setMinimumWidth(250)
        self.excludeFilesEndingWith_input.setMaxLength(500)
        self.excludeFilesEndingWith_input.setPlaceholderText("wav, .mp4, full.mp3, .txt, ...")


        self.addRow(self.noAutosave_label, self.noAutosave_box)
        self.addRow(self.dynamicSettings_label, self.dynamicSettings_box)
        self.addRow(self.excludeFilesEndingWith_label, self.excludeFilesEndingWith_input)




class UiEditBasePathsLayout(QVBoxLayout):

    def __init__(
            self,
            index_current: int = 0
    ):
        super().__init__()

        self.title_label = UiLabelTooltip("Base Paths", "Add, edit and delete base paths.", True)

        self.basePaths_list = QListWidget()
        self.basePaths_list.addItems(DUMMY_BASEPATH_LIST)
        self.basePaths_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.basePaths_list.setCurrentRow(index_current)
        self.basePaths_list.setAlternatingRowColors(True)

        self.basePaths_buttons = UiEditBasePathsButtons()
        self.basePaths_buttons.add_one.clicked.connect(self.addBasePathDialog)
        self.basePaths_buttons.remove.clicked.connect(self.removeBasePathDialog)
        self.basePaths_buttons.edit.clicked.connect(self.editBasePathDialog)

        self.h_box = QHBoxLayout()
        self.h_box.addLayout(self.basePaths_buttons)
        self.h_box.addWidget(self.basePaths_list)

        self.addWidget(self.title_label)
        self.addLayout(self.h_box)


    def addBasePathDialog(self): # TODO
        self.abp_dialog = QDialog()
        self.abp_dialog.setWindowTitle("Create new Base Path")
        self.abp_dialog.setWindowModality(Qt.ApplicationModal)
        self.abp_dialog.setMinimumWidth(500)

        path_box = UiPathBox()

        button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal
        )
        button_box.accepted.connect(lambda: print("Create new base path: " + path_box.path_input.text())) # TODO handle save button
        button_box.rejected.connect(self.abp_dialog.hide)

        # combine path box and buttons
        layout_all = QVBoxLayout()
        layout_all.addLayout(path_box)
        layout_all.addWidget(button_box)

        self.abp_dialog.setLayout(layout_all)
        self.abp_dialog.show()


    def removeBasePathDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete base path '" + self.basePaths_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText(
            "This action cannot be undone.")
        msg.setWindowTitle("Delete Base Path")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeBasePathConfirmed)
        msg.exec_()

    def removeBasePathConfirmed(self):
        print("Delete " + self.basePaths_list.selectedItems()[0].text())  # TODO


    def editBasePathDialog(self): # TODO
        self.ebp_dialog = QDialog()
        self.ebp_dialog.setWindowTitle("Edit Base Path")
        self.ebp_dialog.setWindowModality(Qt.ApplicationModal)
        self.ebp_dialog.setMinimumWidth(500)

        path_box = UiPathBox()
        path_box.path_input.setText(self.basePaths_list.selectedItems()[0].text())

        button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal
        )
        button_box.accepted.connect(lambda: print("set new base path: " + path_box.path_input.text())) # TODO handle save button
        button_box.rejected.connect(self.ebp_dialog.hide)

        # combine path box and buttons
        layout_all = QVBoxLayout()
        layout_all.addLayout(path_box)
        layout_all.addWidget(button_box)

        self.ebp_dialog.setLayout(layout_all)
        self.ebp_dialog.show()





class UiEditBasePathsButtons(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new Base Path.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected Base Path.", True, True)
        self.edit = UiManagementToolButton(ICON_EDIT_THIN, "Edit the current Base Path.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.edit)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)




class UiManagementQueueHistoryTabs(QTabWidget):

    def __init__(self):
        super().__init__()

        # queue list and buttons first
        # self.queue_list = QListWidget() # TODO CONTINUE
        # self.queue_list.addItems(DUMMY_QUEUE_LIST_L)
        # self.queue_list.currentTextChanged.connect(self.handleQueueListSelected)
        # self.queue_list.setAlternatingRowColors(True)
        # self.queue_list.setSelectionMode(QAbstractItemView.ExtendedSelection) # see https://doc.qt.io/qt-5/qabstractitemview.html#SelectionMode-enum
        # # self.queue_list.setAutoScroll(True) # TODO for drag&drop later, see https://youtu.be/Huonlw6kRQA?t=279
        # # self.queue_list.setTextElideMode(Qt.ElideRight) # TODO: apparently right side ellipsis, see https://doc.qt.io/qt-5/qabstractitemview.html#textElideMode-prop
        # self.queue_list.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel) # TODO or ScrollPerItem? (nah), see https://doc.qt.io/qt-5/qabstractitemview.html#ScrollMode-enum

        # self.queue_list.clearSelection() # TODO call when a button does some action on the list, maybe if removes an item? But maybe keeping the selection on the next item then might be good (needs experimenting) -- "Deselects all selected items. The current index will not be changed."


        self.queue_table = UiQueueTable()
        self.queue_table.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # buttons for queue list interaction
        self.queue_buttons = UiManagementQueueButtons()

        self.v_box_queue = QVBoxLayout()
        self.v_box_queue.addWidget(self.queue_table)
        self.v_box_queue.addLayout(self.queue_buttons)
        self.v_box_queue.setContentsMargins(0, 0, 0, 0)

        self.queue_widget = QWidget()
        self.queue_widget.setLayout(self.v_box_queue)


        # history, showing all played files so far
        self.history_table = UiHistoryTable()
        self.history_table.setSelectionMode(QAbstractItemView.SingleSelection)

        # buttons for history list interaction
        self.history_buttons = UiManagementHistoryButtons()

        self.v_box_history = QVBoxLayout()
        self.v_box_history.addWidget(self.history_table)
        self.v_box_history.addLayout(self.history_buttons)
        self.v_box_history.setContentsMargins(0, 0, 0, 0)

        self.history_widget = QWidget()
        self.history_widget.setLayout(self.v_box_history)


        # put into tabs
        self.addTab(self.queue_widget, "Queue")
        self.addTab(self.history_widget, "History")







class UiQueueTable(QTableView):

    def __init__(self):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.model = QStandardItemModel(0, 4, self)
        self.model.setHorizontalHeaderLabels(["Name", "Ep", "op", "ed"])

        tmp_it = QStandardItem("tmp")
        tmp_font_bold = tmp_it.font()
        tmp_font_bold.setBold(True)

        for name, episode_number, opening_mark, ending_mark in DUMMY_QUEUE_LIST_T_L:
            it_name = QStandardItem(name)
            it_ep = QStandardItem(str(episode_number))
            it_op = QStandardItem(opening_mark)
            it_ed = QStandardItem(ending_mark)

            it_ep.setTextAlignment(Qt.AlignCenter)
            it_op.setTextAlignment(Qt.AlignCenter)
            it_ed.setTextAlignment(Qt.AlignCenter)

            it_name.setFont(tmp_font_bold)
            it_name.setEditable(False)
            it_ep.setEditable(False)
            it_op.setEditable(False)
            it_ed.setEditable(False)
            self.model.appendRow([it_name, it_ep, it_op, it_ed])

        self.setModel(self.model)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(2, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(3, QHeaderView.ResizeToContents)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()



    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())





class UiHistoryTable(QTableView):

    def __init__(self):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.model = QStandardItemModel(0, 2, self)
        self.model.setHorizontalHeaderLabels(["File Path", "Length"])
        self.setToolTip("Base Path: TODO")


        for name, length in DUMMY_HISTORY_LIST_T:
            it_name = QStandardItem(name)
            it_length = QStandardItem(length)

            it_length.setTextAlignment(Qt.AlignRight)

            it_name.setEditable(False)
            it_length.setEditable(False)
            self.model.appendRow([it_name, it_length])

        self.setModel(self.model)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()



    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())





class UiManagementShowsCmsTabs(QTabWidget):

    def __init__(self):
        super().__init__()

        # self.shows_list = QListWidget()
        # self.shows_list.addItems(DUMMY_SHOW_LIST)
        # self.shows_list.setAlternatingRowColors(True)
        # self.shows_list.setSelectionMode(QAbstractItemView.SingleSelection)

        # self.shows_list.doubleClicked.connect() # TODO use double click? maybe to open show settings. Try itemDoubleClicked to pass the item, else index passed (apparently, not tested yet)
        # self.shows_list.activated.connect() # TODO should happen if ENTER is pressed, but maybe also on other signals like Double Click (TODO: test!!!)
        # self.shows_list.reset() # TODO: maybe call this if a show is added or deleted or just edited. Might be useful in queue as well, if there something changes
        # self.shows_list.scrollToBottom() # TODO maybe use if new entry is added (maybe not needed), maybe also call setCurrentIndex so it is selected. Also use scrollToItem to scroll to just edited item etc, maybe should call update(index) after an edit of item at index. And scrollToTop.

        self.shows_table = UiShowsTable()
        self.shows_table.setSelectionMode(QAbstractItemView.ExtendedSelection)


        # buttons for show list interaction
        self.shows_buttons = UiManagementShowsCmsButtons()


        self.v_box_shows = QVBoxLayout()
        # self.v_box_shows.addWidget(self.shows_list_label)
        self.v_box_shows.addWidget(self.shows_table)
        self.v_box_shows.addLayout(self.shows_buttons)
        self.v_box_shows.setContentsMargins(0, 0, 0, 0)

        # self.v_box_shows_frame = QFrame()
        # self.v_box_shows_frame.setLayout(self.v_box_shows)

        self.shows_widget = QWidget()
        self.shows_widget.setLayout(self.v_box_shows)



        # add widget for commercials

        self.commercials_table = UiCommercialsTable()
        self.commercials_table.setSelectionMode(QAbstractItemView.SingleSelection)


        # buttons for show list interaction
        self.commercials_buttons = UiManagementShowsCmsButtons(isShows=False)


        self.v_box_commercials = QVBoxLayout()
        # self.v_box_commercials.addWidget(self.commercials_list_label)
        self.v_box_commercials.addWidget(self.commercials_table)
        self.v_box_commercials.addLayout(self.commercials_buttons)
        self.v_box_commercials.setContentsMargins(0, 0, 0, 0)

        # self.v_box_commercials_frame = QFrame()
        # self.v_box_commercials_frame.setLayout(self.v_box_commercials)

        self.commercials_widget = QWidget()
        self.commercials_widget.setLayout(self.v_box_commercials)


        # self.commercials_list = QListWidget()
        # self.commercials_list.addItems(DUMMY_CMS_LIST)
        # self.commercials_list.setAlternatingRowColors(True)
        # self.commercials_list.setSelectionMode(QAbstractItemView.NoSelection)

        # combine shows and commercials in a tab widget and add to frame
        self.addTab(self.shows_widget, "Shows")
        self.addTab(self.commercials_widget, "Commercials")





class UiShowsTable(QTableView):

    def __init__(self):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.model = QStandardItemModel(0, 3, self)
        self.model.setHorizontalHeaderLabels(["Name", "Ep", "Tags"])

        # tmp_it = QStandardItem("tmp")
        # tmp_font_bold = tmp_it.font()
        # tmp_font_bold.setBold(True)

        for name, ep, tags in DUMMY_SHOW_LIST_T:
            it_name = QStandardItem(name)
            it_ep = QStandardItem(str(ep))
            it_tags = QStandardItem(", ".join(tags))

            it_ep.setTextAlignment(Qt.AlignCenter)

            it_name.setEditable(False)
            it_ep.setEditable(False)
            it_tags.setEditable(False)
            self.model.appendRow([it_name, it_ep, it_tags])

        self.setModel(self.model)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()



    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())








class UiCommercialsTable(QTableView):

    def __init__(self):
        super().__init__(showGrid=False, selectionBehavior=QAbstractItemView.SelectRows)

        self.model = QStandardItemModel(0, 4, self)
        self.model.setHorizontalHeaderLabels(["Name", "Val", "Min", "Max"])

        # tmp_it = QStandardItem("tmp")
        # tmp_font_bold = tmp_it.font()
        # tmp_font_bold.setBold(True)

        for name, cm_value, cm_min, cm_max in DUMMY_CMS_LIST_T:
            it_name = QStandardItem(name)
            it_cm_value = QStandardItem(str(cm_value) if cm_value is not None else "")
            it_cm_min = QStandardItem(str(cm_min) if cm_min is not None else "")
            it_cm_max = QStandardItem(str(cm_max) if cm_max is not None else "")

            it_cm_value.setTextAlignment(Qt.AlignCenter)
            it_cm_min.setTextAlignment(Qt.AlignCenter)
            it_cm_max.setTextAlignment(Qt.AlignCenter)

            it_name.setEditable(False)
            it_cm_value.setEditable(False)
            it_cm_min.setEditable(False)
            it_cm_max.setEditable(False)
            self.model.appendRow([it_name, it_cm_value, it_cm_min, it_cm_max])

        self.setModel(self.model)

        self.clicked.connect(self.handleRowSelected)

        # some styling
        self.setHorizontalHeader(QHeaderView(Qt.Horizontal))
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.resizeColumnToContents(1)
        self.resizeColumnToContents(2)
        self.resizeColumnToContents(3)

        self.resizeRowsToContents()
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()



    def handleRowSelected(self, index: QModelIndex): # index here is the same as self.currentIndex()
        print(self.currentIndex().row())





class UiManagementShowsCmsButtons(QHBoxLayout):

    def __init__(self, isShows: bool = True):
        super().__init__()

        entity_str = "show" if isShows else "commercial"

        self.add_to_queue = UiManagementToolButton(ICON_MOVE_LEFT, ("Add show to end of queue." if isShows else ""))
        self.sort = UiManagementToolButton(ICON_SORT, f"Select {entity_str} sort mode.")
        self.add_new = UiManagementToolButton(ICON_ADD, f"Add a new show{' or commercial' if not isShows else ''}.")
        self.edit = UiManagementToolButton(ICON_EDIT, f"Edit {entity_str}.")

        if not isShows:
            self.add_to_queue.setDisabled(True)

        self.addWidget(self.add_to_queue)
        self.addWidget(self.sort)
        self.addStretch()
        self.addWidget(self.add_new)
        self.addWidget(self.edit)
        self.setContentsMargins(5, 0, 5, 5)





class UiManagementQueueButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD, "Add one show to queue.")
        self.remove = UiManagementToolButton(ICON_REMOVE, "Remove selection from queue.")
        self.move_up = UiManagementToolButton(ICON_MOVE_UP, "Move selection up in queue.")
        self.move_down = UiManagementToolButton(ICON_MOVE_DOWN, "Move selection down in queue.")

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addStretch()
        self.addWidget(self.move_up)
        self.addWidget(self.move_down)
        self.setContentsMargins(5, 0, 5, 5)





class UiManagementHistoryButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.open_folder = UiManagementToolButton(ICON_FOLDER_BLACK, "Open file folder.")

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.open_folder)
        self.addStretch()
        self.setContentsMargins(5, 0, 5, 5)




class UiManagementToolButton(QToolButton):

    def __init__(
            self,
            icon_path: str,
            status_tip: str = "",
            tooltip_instead_of_statustip: bool = False,
            bigger: bool = False
    ):
        super().__init__()
        self.setIcon(QIcon(icon_path))
        self.setIconSize(
            QSize(int(BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM * (1.4 if bigger else 1)), int(BUTTON_QUEUE_SHOWS_ICON_SIZE_CUSTOM * (1.4 if bigger else 1))))
        self.setFixedSize(int(BUTTON_QUEUE_SHOWS_WIDTH * (1.4 if bigger else 1)), int(BUTTON_QUEUE_SHOWS_HEIGHT * (1.4 if bigger else 1)))
        if tooltip_instead_of_statustip:
            self.setToolTip(status_tip)
        else:
            self.setStatusTip(status_tip)




class UiShowsNewDialog(QDialog):
    # TODO CONTINUE: block MainWindow, add StatusTips (and the bar for it), finish up
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Add a new show")
        self.setWindowModality(Qt.ApplicationModal)


        # form layout for the main settings
        self.main_settings = UiShowMainSettings()


        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal,
        )

        self.button_box.accepted.connect(self.handleSaveNewShow) # TODO handle save button
        self.button_box.rejected.connect(self.hide)


        # combine all (form and buttons)
        self.layout_all = QVBoxLayout()
        self.layout_all.addLayout(self.main_settings)
        self.layout_all.addStretch()
        self.layout_all.addWidget(self.button_box)

        self.setLayout(self.layout_all)


    def handleSaveNewShow(self):
        """Create new show with the given attributes. Check validity before saving
        """
        # TODO: check if directory actually exists (combined with Base Path functionality), check if directory already used in another show (or the same name) (only give a warning, don't block)
        # state, string, pos = self.form_main_settings.name_validator.validate(self.form_main_settings.su_name_input.text(), 0)
        # print("Name input regex: " + str(state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable")
        #
        # state, string, pos = self.form_main_settings.path_validator.validate(self.form_main_settings.su_path_input.text(), 0)
        # print("Path input regex: " + str(state) + "; 0 = Invalid, 1 = Intermediate, 2 = Acceptable")
        print("save new show")
        self.hide()


class UiShowMainSettings(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.form = UiShowFormMainSettings()

        self.title_label = UiLabelTooltip("Main Show Settings", "Essential properties of the show.", True)

        self.addWidget(self.title_label)
        self.addLayout(self.form)



class UiShowFormMainSettings(QFormLayout):

    def __init__(self):
        super().__init__()

        self.name_label = UiLabelTooltip("Name", "Give the show a name. Can be different from the actual file and directory names.")
        self.name_input = QLineEdit()
        self.name_input.setMinimumWidth(350)
        self.name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.name_input.setValidator(self.name_validator)
        self.name_input.setPlaceholderText("Enter show name")


        self.active_check = QCheckBox("Active")
        self.active_check.setChecked(True)
        self.active_check.setToolTip("Whether this show should be queued in any profile.")

        self.no_basepath = QCheckBox("No Base Path")
        self.no_basepath.setToolTip("If checked, the base path will not used for this show, even if it is activated.")

        self.h_box_active_basepath = QHBoxLayout()
        self.h_box_active_basepath.addWidget(self.active_check)
        self.h_box_active_basepath.addWidget(self.no_basepath)
        self.h_box_active_basepath.addStretch()


        self.path_label = QLabel("Path")
        self.path_box = UiPathBox()


        self.episodes_label = QLabel("Episode")

        self.episode_current_label = QLabel("Current")
        self.episode_current_input = QSpinBox()
        self.episode_current_input.setMinimum(1)

        self.episode_first_labelcheck = QCheckBox("First")
        self.episode_first_input = QSpinBox()
        self.episode_first_input.setMinimum(1)
        self.episode_first_input.setDisabled(True) # TODO: enable when checked box, save only when checked box

        self.episode_last_labelcheck = QCheckBox("Last")
        self.episode_last_input = QSpinBox()
        self.episode_last_input.setMinimum(1) # TODO set Max as maximum files/episodes found in the directory, also set as default if value is null in config
        self.episode_last_input.setDisabled(True) # TODO: enable when checked box, save only when checked box

        self.episodes_h_box = QHBoxLayout()
        self.episodes_h_box.addWidget(self.episode_current_label)
        self.episodes_h_box.addWidget(self.episode_current_input)
        self.episodes_h_box.addStretch()
        self.episodes_h_box.addWidget(self.episode_first_labelcheck)
        self.episodes_h_box.addWidget(self.episode_first_input)
        self.episodes_h_box.addStretch()
        self.episodes_h_box.addWidget(self.episode_last_labelcheck)
        self.episodes_h_box.addWidget(self.episode_last_input)
        self.episodes_h_box.addStretch()


        self.profiles_label = QLabel("Active Profiles")
        # self.profiles_label.setWordWrap(True)
        self.profiles_checks_v_box = QVBoxLayout()
        self.profiles_check_all = QCheckBox("All Profiles")
        self.profiles_check_all.setChecked(True)
        self.profiles_check_all.setToolTip(
            "If True, then played in all profiles. Ignores Checkboxes below and settings like 'Exclude Profile' (in Profile Settings)")

        self.profiles_checks_v_box.addWidget(self.profiles_check_all)

        self.profiles_checks_grid_box = QGridLayout()
        grid_cols = 3
        for i in range(len(DUMMY_LIST_PROFILES)):
            self.profiles_checks_grid_box.addWidget(QCheckBox(DUMMY_LIST_PROFILES[i]), int(i / grid_cols) + 1, i % grid_cols)

        self.profiles_checks_grid_h_box = QHBoxLayout()
        self.profiles_checks_grid_h_box.addLayout(self.profiles_checks_grid_box)
        self.profiles_checks_grid_h_box.addStretch()

        self.profiles_checks_v_box.addLayout(self.profiles_checks_grid_h_box)

        if True: #TODO: if suspended time
            self.suspended_label = UiLabelTooltip("Suspended at", "This show will continue playing at the specified time. This usually happens when a show is interrupted during playback by skipping the show or stopping the session.")
            self.suspended_time = QLineEdit()
            self.suspended_time.setText("123 TODO and convert to timestamp")
            self.suspended_time.setDisabled(True)


        self.tags_label = UiLabelTooltip("Tags", "Tags are only used for sorting and filtering in the user interface. They do not influence the playback in any way.")

        self.tags_add_input = QLineEdit()
        self.tags_add_input.setPlaceholderText("Add a tag. Unused tags are deleted on restart.")
        self.tags_add_input.setMaxLength(50)

        self.tags_add_button = UiManagementToolButton(ICON_ADD, "Add tag from input.")
        self.tags_add_button.setToolTip("Add tag from input.")

        self.tags_add_h_box = QHBoxLayout()
        self.tags_add_h_box.addWidget(self.tags_add_input)
        self.tags_add_h_box.addWidget(self.tags_add_button)


        self.tags_checks_grid_box = QGridLayout()
        grid_cols = 3
        for i in range(len(DUMMY_LIST_TAGS)):
            self.tags_checks_grid_box.addWidget(QCheckBox(DUMMY_LIST_TAGS[i]), int(i / grid_cols) + 1, i % grid_cols)

        self.tags_checks_grid_h_box = QHBoxLayout()
        self.tags_checks_grid_h_box.addLayout(self.tags_checks_grid_box)
        self.tags_checks_grid_h_box.addStretch()

        self.tags_v_box = QVBoxLayout()
        self.tags_v_box.addLayout(self.tags_add_h_box)
        self.tags_v_box.addLayout(self.tags_checks_grid_h_box)


        self.addRow(self.name_label, self.name_input)
        self.addRow(QLabel(""), self.h_box_active_basepath)
        self.addRow(self.path_label, self.path_box)
        self.addRow(self.episodes_label, self.episodes_h_box)
        self.addRow(self.profiles_label, self.profiles_checks_v_box)
        self.addRow(self.tags_label, self.tags_v_box)
        if True: # TODO
            self.addRow(self.suspended_label, self.suspended_time)




class UiPathBox(QHBoxLayout):

    def __init__(
            self,
            search_file: bool = False
    ):
        super().__init__()

        self.search_file = search_file

        self.path_input = QLineEdit()
        self.path_input.setPlaceholderText("Select a file" if search_file else "Directory of the files.")

        self.path_regex = QRegExp('^.+$')
        self.path_validator = QRegExpValidator(self.path_regex)
        self.path_input.setValidator(self.path_validator)

        self.path_file_button = QToolButton()
        self.path_file_button.setText("Search")
        self.path_file_button.clicked.connect(self.openDirectoryDialog)

        self.path_open_button = UiManagementToolButton(ICON_FOLDER_BLACK, "Open the folder.")
        self.path_open_button.clicked.connect(self.openPathDirectory)

        self.addWidget(self.path_input)
        self.addWidget(self.path_file_button)
        self.addWidget(self.path_open_button)


    def openDirectoryDialog(self):
        select_dialog = QFileDialog()
        if self.path_input.text() == "":
            pass
            # if current_base_path: # TODO, only if "No Basepath" Checkbox is not checked
            #     select_dialog.setDirectory(current_base_path) # TODO
        else:
            select_dialog.setDirectory(os.path.dirname(self.path_input.text()))

        # select_dialog.setFileMode(QFileDialog.DirectoryOnly)
        # select_dialog.setAcceptMode(QFileDialog.AcceptOpen)
        if self.search_file:
            dir_name = select_dialog.getOpenFileName(self.path_file_button, "Select a file") # TODO only used for Mp3Merge, make only .apkg files selectable
        else:
            dir_name = select_dialog.getExistingDirectory(self.path_file_button, "Select directory of the files")
        # QFileDialog.getOpenFileName(self, "Open File", os.path.expanduser('~'))[0]
        if not dir_name:
            return
        self.path_input.setDisabled(True)
        self.path_input.setText(dir_name)

    def openPathDirectory(self):
        print("open directory: " + self.path_input.text()) # TODO copy code





class UiShowExtraSettings(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.form = UiShowFormExtraSettings()

        self.title_label = UiLabelTooltip("Extra Show Settings", "Some settings that are mainly separate from the 'Common Settings' by not being available in Profile Settings.", True)

        self.addWidget(self.title_label)
        self.addLayout(self.form)
        self.addStretch()


class UiShowFormExtraSettings(QFormLayout):

    def __init__(self):
        super().__init__()

        self.random_label = UiLabelTooltip("Random", "Randomize episode playback")
        self.random_box = UiFormRadioBinary(default=False)

        self.multipleEpisodes_label = UiLabelTooltip("Multiple Episodes", "Play this many episodes at once when this show is on. Default is 1.")
        self.multipleEpisodes_box = UiFormEnableSpin(1)

        self.probability_label = UiLabelTooltip("Probability", "Probability in % that this show will be played when it is randomly picked. Default is the value in the profile settings.")
        self.probability_box = UiFormEnableSpin(PROBABILITY_LIMIT_LOWER, PROBABILITY_LIMIT_UPPER, 100, fixed_width=60, add_string="%")

        self.noCommercialsAfter_label = UiLabelTooltip("No Cms After", "Disables commercials after this show.")
        self.noCommercialsAfter_box = UiFormRadioBinary(default=False)

        self.noCommercialsBefore_label = UiLabelTooltip("No Cms Before", "Disables commercials after this show.")
        self.noCommercialsBefore_box = UiFormRadioBinary(default=False)

        self.commercialWeight_label = UiLabelTooltip("Cm Weight", "Makes this show a commercial if it is enabled. Set the commercial weight of this show.")
        self.commercialWeight_box = UiFormEnableSpin(0, spin_default=2)

        self.commercialMax_label = UiLabelTooltip("Cm Max", "Set the upper limit of this commercial. Default is no Limit.")
        self.commercialMax_box = UiFormEnableSpin(1)

        self.commercialMin_label = UiLabelTooltip("Cm Min", "Set the lower limit of this commercial. Default is 0.")
        self.commercialMin_box = UiFormEnableSpin()

        self.videoTrack_label = UiLabelTooltip("Video Track", "Set the video track for this show. Default is usually 0.")
        self.videoTrack_box = UiFormEnableSpin()

        self.audioTrack_label = UiLabelTooltip("Audio Track", "Set the audio track for this show. Default is usually 0.")
        self.audioTrack_box = UiFormEnableSpin()

        self.subtitleTrack_label = UiLabelTooltip("Subtitle Track", "Set the subtitle track for this show. Default is usually 0.")
        self.subtitleTrack_box = UiFormEnableSpin()

        self.recursionLevel_label = UiLabelTooltip("Recursion Level", "Set how many levels of folders to look into in the folder that contains the media for this show. Default is 0.")
        self.recursionLevel_box = UiFormEnableSpin()


        self.addRow(self.random_label, self.random_box)
        self.addRow(self.multipleEpisodes_label, self.multipleEpisodes_box)
        self.addRow(self.probability_label, self.probability_box)
        self.addRow(self.noCommercialsAfter_label, self.noCommercialsAfter_box)
        self.addRow(self.noCommercialsBefore_label, self.noCommercialsBefore_box)
        self.addRow(self.commercialWeight_label, self.commercialWeight_box)
        self.addRow(self.commercialMax_label, self.commercialMax_box)
        self.addRow(self.commercialMin_label, self.commercialMin_box)
        self.addRow(self.videoTrack_label, self.videoTrack_box)
        self.addRow(self.audioTrack_label, self.audioTrack_box)
        self.addRow(self.subtitleTrack_label, self.subtitleTrack_box)
        self.addRow(self.recursionLevel_label, self.recursionLevel_box)





class UiShowProfileCommonSettings(QVBoxLayout):

    def __init__(self, is_show_settings: bool, split_layout: bool = False):
        super().__init__()

        self.title_label = UiLabelTooltip("Common Settings", "Settings that are available for profile settings and show settings. Usually show settings override profile settings, when both are set. Depends on the property. Check more information by hovering the properties with your mouse.", True)
        self.addWidget(self.title_label)


        # create the form here
        self.opening_label = UiLabelTooltip("Opening", "Play an opening before every episode of tashow.\n\nThe opening file should be placed into the same directory as a show's episodes, named 'op' and then the file ending, like 'op.mp3'.\nYou can add a number, like 'op17.mp3', to set this as the opening for every episode from 17 and up. Use this to add different openings for different seasons or whatever.")
        self.opening_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.ending_label = UiLabelTooltip("Ending", "Similar to the 'Opening' setting. Play an ending after every episode of a show.\n\nThe ending file should be placed into the same directory as a show's episodes, named 'ed' and then the file ending, like 'ed.mkv'.\nYou can add a number, like 'ed17.mkv', to set this as the ending for every episode from 17 and up. Use this to add different endings for different seasons or whatever.")
        self.ending_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(default=None)

        self.volume_label = UiLabelTooltip("Volume", ("Set the volume for all episodes of this show." if is_show_settings else "Set the volume for all shows in this profile.") + "\nDoes not affect the openings and endings of a show.\nThe shows' volume (in percent) gets multiplied by the volume in the profile settings and the volume of the volume slider in the playback control.")
        self.volume_box = UiFormEnableSpin(VOLUME_LIMIT_LOWER, VOLUME_LIMIT_UPPER, 100, 60, "%")

        self.speed_label = UiLabelTooltip("Speed", ("Set the speed for all episodes of this show." if is_show_settings else "Set the speed for all shows in this profile.") + "\nDoes not affect the openings and endings of a show.\nThe shows' speed (in percent) gets multiplied by the speed in the profile settings.")
        self.speed_box = UiFormEnableSpin(SPEED_LIMIT_LOWER, SPEED_LIMIT_UPPER, 100, 60, "%")

        self.jump_label = UiLabelTooltip("Jump", "Jump to this many seconds of any episode at the start of the episode.\nNot if continuing a suspended episode.")
        self.jump_box = UiFormEnableSpin(fixed_width=60, add_string="sec")

        self.end_label = UiLabelTooltip("End", "End all episodes this many seconds before they are actually at their end.")
        self.end_box = UiFormEnableSpin(fixed_width=60, add_string="sec")

        self.pause_label = UiLabelTooltip("Pause", "Pause every episode immediately when playback starts.")
        self.pause_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(default=None)

        self.mute_label = UiLabelTooltip("Mute", "Mute every episode immediately when playback starts.")
        self.mute_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(default=None)

        self.fullscreen_label = UiLabelTooltip("Fullscreen", "Activate Fullscreen for every episode immediately when playback starts, if there is a video, not only audio.")
        self.fullscreen_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.suspendShow_label = UiLabelTooltip("Save Time", "Save where playback of an episode was interrupted and continue there the next time the show is played.")
        self.suspendShow_box = UiFormRadioBinary(default=True) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.suspendQueue_label = UiLabelTooltip("Save Queue", "Save the current queue when the session is stopped.")
        self.suspendQueue_box = UiFormRadioBinary(default=True) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.noRerun_label = UiLabelTooltip("No Reruns", "Deactivate a show when its last episode was played. Only works if show's playback is not random.")
        self.noRerun_box = UiFormRadioBinary(default=False) if not is_show_settings else UiFormRadioTernary(
            default=None)

        self.debugPlayerJumps_label = UiLabelTooltip("Debug Jumps", "Experimental debugger that recognizes if the playback is corrupted.\nIf the player length changes, it will restart the playback and jump to a bit second after the vlc mediaplayer's player_length was changed")
        self.debugPlayerJumps_box = UiFormRadioBinary(default=True) if not is_show_settings else UiFormRadioTernary(
            default=None)

        # add form elements together
        self.form_all = QHBoxLayout()

        if split_layout:
            self.split_h_box = QHBoxLayout()

            self.form_left = QFormLayout()
            self.form_left.addRow(self.opening_label, self.opening_box)
            self.form_left.addRow(self.ending_label, self.ending_box)
            self.form_left.addRow(self.volume_label, self.volume_box)
            self.form_left.addRow(self.speed_label, self.speed_box)
            self.form_left.addRow(self.jump_label, self.jump_box)
            self.form_left.addRow(self.end_label, self.end_box)

            self.form_right = QFormLayout()
            self.form_right.addRow(self.pause_label, self.pause_box)
            self.form_right.addRow(self.mute_label, self.mute_box)
            self.form_right.addRow(self.fullscreen_label, self.fullscreen_box)
            self.form_right.addRow(self.suspendShow_label, self.suspendShow_box)
            self.form_right.addRow(self.suspendQueue_label, self.suspendQueue_box)
            self.form_right.addRow(self.noRerun_label, self.noRerun_box)
            self.form_right.addRow(self.debugPlayerJumps_label, self.debugPlayerJumps_box)

            self.split_h_box.addLayout(self.form_left)
            self.split_h_box.addLayout(self.form_right)

            self.form_all.addLayout(self.split_h_box)

        else:
            self.form_solo = QFormLayout()

            self.form_solo.addRow(self.opening_label, self.opening_box)
            self.form_solo.addRow(self.ending_label, self.ending_box)
            self.form_solo.addRow(self.volume_label, self.volume_box)
            self.form_solo.addRow(self.speed_label, self.speed_box)
            self.form_solo.addRow(self.jump_label, self.jump_box)
            self.form_solo.addRow(self.end_label, self.end_box)
            self.form_solo.addRow(self.pause_label, self.pause_box)
            self.form_solo.addRow(self.mute_label, self.mute_box)
            self.form_solo.addRow(self.fullscreen_label, self.fullscreen_box)
            self.form_solo.addRow(self.suspendShow_label, self.suspendShow_box)
            self.form_solo.addRow(self.suspendQueue_label, self.suspendQueue_box)
            self.form_solo.addRow(self.noRerun_label, self.noRerun_box)
            self.form_solo.addRow(self.debugPlayerJumps_label, self.debugPlayerJumps_box)

            self.form_all.addLayout(self.form_solo)


        # add all together
        self.addLayout(self.form_all)
        self.addStretch()



class UiEditProfilesButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new Profile.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected Profile.", True, True)
        self.duplicate = UiManagementToolButton(ICON_DUPLICATE, "Duplicate the current Profile.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.duplicate)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)




class UiProfileMainSettings(QFormLayout):

    def __init__(self):
        super().__init__()

        self.name_label = UiLabelTooltip("Name", "Give the profile a name.")
        self.name_input = QLineEdit()
        self.name_input.setMinimumWidth(350)
        self.name_input.setMaxLength(50)

        self.name_regex = QRegExp('^.+$')
        self.name_validator = QRegExpValidator(self.name_regex)
        self.name_input.setValidator(self.name_validator)
        self.name_input.setPlaceholderText("Enter profile name")

        self.addRow(self.name_label, self.name_input)




class UiProfileExtraSettings(QVBoxLayout):

    def __init__(self):
        super().__init__()

        self.form = UiProfileFormExtraSettings()

        self.title_label = UiLabelTooltip("Profile Settings", "Some optional profile settings.", True)

        # add to checks here
        self.playAllShows_check = QCheckBox("Play All Shows")
        self.playAllShows_check.setToolTip("Play all available shows in this profile.\n\nIgnores settings like 'Exclude Profiles' and ignores which profiles are toggled in a show.")

        self.commercialsPlay_check = QCheckBox("Play Commercials")
        self.commercialsPlay_check.setToolTip("Play commercials if any are available.\n\nDeactivating this is the same as disabling the settings 'Cms Break Weight' and 'Cms Start Weight'.")
        self.commercialsPlay_check.setChecked(True)

        self.checks_h_box = QHBoxLayout()
        self.checks_h_box.addWidget(self.playAllShows_check)
        self.checks_h_box.addWidget(self.commercialsPlay_check)

        # combine all
        self.addWidget(self.title_label)
        self.addLayout(self.checks_h_box)
        self.addLayout(self.form)
        self.addStretch()


class UiProfileFormExtraSettings(QFormLayout):

    def __init__(self):
        super().__init__()

        self.probability_label = UiLabelTooltip("Shows Probability", "Probability in % that any show will be played when it is randomly picked.\nUse this to set the baseline probability for every show to something else than 100 and override the value in single shows' settings.")
        self.probability_box = UiFormEnableSpin(PROBABILITY_LIMIT_LOWER, PROBABILITY_LIMIT_UPPER, 100, fixed_width=60, add_string="%")

        self.commercialsBreakWeight_label = UiLabelTooltip("Cms Break Weight", "Set the value for commercials that should be played between shows.\nIf enabled, then between shows commercials will be picked until the sum of their values is as close as possible to the weight set here.\n\nNo effect if there are no shows that have been set as commercials (do this by enabling a shows setting 'Cm Weight' and setting the commercials' weight).")
        self.commercialsBreakWeight_box = UiFormEnableSpin()

        self.commercialsStartWeight_label = UiLabelTooltip("Cms Start Weight", "Same as 'Cms Break Weight', but for commercials before the first show of a session is played.")
        self.commercialsStartWeight_box = UiFormEnableSpin()

        self.suspendedQueue_label = UiLabelTooltip("Suspended Queue", "Shows that were in the queue when the last session was stopped.\nThese will be loaded into the queue next time this profile is played.") # TODO only show if suspended queue exists
        self.suspendedQueue_list = QListWidget()
        self.suspendedQueue_list.addItems(DUMMY_SUSPENDED_QUEUE_LIST)
        self.suspendedQueue_list.setSelectionMode(QAbstractItemView.NoSelection)
        self.suspendedQueue_list.setAlternatingRowColors(True)
        self.suspendedQueue_list.setMaximumHeight(60)

        self.uniqueShows_label = UiLabelTooltip("Unique Shows", "Avoid playing the same show twice in a session.\nThis does not apply to commercials, only to queued shows.\nSession will end automatically if the strong version is picked once all shows are played once.")
        self.uniqueShows_radio = UiFormRadioNoYesHard()

        self.shutdown_label = UiLabelTooltip("Shutdown Timer", "Automatically stop a session after this many minutes.")
        self.shutdown_box = UiFormEnableSpin(fixed_width=60, add_string="min")

        self.showsLimit_label = UiLabelTooltip("Shows Limit", "Automatically stop a session after this many shows are played.\n\nCommercials are ignored for this counter. Shows that are played as part of the 'Start With Shows' setting are counted.")
        self.showsLimit_box = UiFormEnableSpin()


        self.profilesInclude_label = UiLabelTooltip("Include Profiles", "Include shows that are played in these profiles into the current profile.")
        self.profilesInclude_list = QListWidget()
        self.profilesInclude_list.addItems(DUMMY_LIST_INCLUDE_PROFILES)
        self.profilesInclude_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.profilesInclude_list.setAlternatingRowColors(True)
        self.profilesInclude_list.setMaximumHeight(60)

        self.profilesInclude_add_one = UiManagementToolButton(ICON_ADD_THIN, "Add one profile.", True)
        self.profilesInclude_remove = UiManagementToolButton(ICON_REMOVE_THIN, "Remove selected profile.", True)
        self.profilesInclude_buttons = QVBoxLayout()
        self.profilesInclude_buttons.setContentsMargins(0, 0, 0, 0)
        self.profilesInclude_buttons.addWidget(self.profilesInclude_add_one)
        self.profilesInclude_buttons.addWidget(self.profilesInclude_remove)

        self.profilesInclude_box = QHBoxLayout()
        self.profilesInclude_box.addLayout(self.profilesInclude_buttons)
        self.profilesInclude_box.addWidget(self.profilesInclude_list)


        self.profilesExclude_label = UiLabelTooltip("Exclude Profiles", "Exclude shows that are played in these profiles into the current profile.\nThis is applied after 'Include Profiles'.\nShows that are toggled to be active an all profiles are ignored by this.\n\nNo effect if 'Play All Shows' is toggled.")
        self.profilesExclude_list = QListWidget()
        self.profilesExclude_list.addItems(DUMMY_LIST_EXCLUDE_PROFILES)
        self.profilesExclude_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.profilesExclude_list.setAlternatingRowColors(True)
        self.profilesExclude_list.setMaximumHeight(60)

        self.profilesExclude_add_one = UiManagementToolButton(ICON_ADD_THIN, "Add one profile.", True)
        self.profilesExclude_remove = UiManagementToolButton(ICON_REMOVE_THIN, "Remove selected profile.", True)
        self.profilesExclude_buttons = QVBoxLayout()
        self.profilesExclude_buttons.setContentsMargins(0, 0, 0, 0)
        self.profilesExclude_buttons.addWidget(self.profilesExclude_add_one)
        self.profilesExclude_buttons.addWidget(self.profilesExclude_remove)

        self.profilesExclude_box = QHBoxLayout()
        self.profilesExclude_box.addLayout(self.profilesExclude_buttons)
        self.profilesExclude_box.addWidget(self.profilesExclude_list)


        self.startWithShows_label = UiLabelTooltip("Start With Shows", "Set Shows that will always play at the beginning of a session.\nIf there is a suspended queue, it will play after these shows here.\nWhen the currently playing queue is suspended and there are shows left from this option, they will not be saved into the suspended queue.") # TODO implement
        self.startWithShows_list = QListWidget()
        self.startWithShows_list.addItems(DUMMY_STARTWITHSHOWS_LIST)
        self.startWithShows_list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.startWithShows_list.setAlternatingRowColors(True)
        self.startWithShows_list.setMaximumHeight(60)

        self.startWithShows_add_one = UiManagementToolButton(ICON_ADD_THIN, "Add one show.", True)
        self.startWithShows_remove = UiManagementToolButton(ICON_REMOVE_THIN, "Remove selected shows.", True)
        self.startWithShows_buttons = QVBoxLayout()
        self.startWithShows_buttons.setContentsMargins(0, 0, 0, 0)
        self.startWithShows_buttons.addWidget(self.startWithShows_add_one)
        self.startWithShows_buttons.addWidget(self.startWithShows_remove)

        self.startWithShows_box = QHBoxLayout()
        self.startWithShows_box.addLayout(self.startWithShows_buttons)
        self.startWithShows_box.addWidget(self.startWithShows_list)


        # combine all into form layout
        self.addRow(self.probability_label, self.probability_box)
        self.addRow(self.commercialsBreakWeight_label, self.commercialsBreakWeight_box)
        self.addRow(self.commercialsStartWeight_label, self.commercialsStartWeight_box)
        self.addRow(self.suspendedQueue_label, self.suspendedQueue_list)
        self.addRow(self.uniqueShows_label, self.uniqueShows_radio)
        self.addRow(self.shutdown_label, self.shutdown_box)
        self.addRow(self.showsLimit_label, self.showsLimit_box)
        self.addRow(self.profilesInclude_label, self.profilesInclude_box)
        self.addRow(self.profilesExclude_label, self.profilesExclude_box)
        self.addRow(self.startWithShows_label, self.startWithShows_box)





class UiLabelTooltip(QLabel):

    def __init__(self, name: str, tooltip: str = "", bold: bool = False):
        super().__init__(name)
        self.setToolTip(tooltip)

        if bold:
            tmp_font = self.font()
            tmp_font.setBold(True)
            self.setFont(tmp_font)



class UiFormRadioBinary(QHBoxLayout):

    def __init__(
            self,
            tooltip_true: str = "",
            tooltip_false: str = "",
            default: bool = False,
    ):
        super().__init__()

        self.r_true = QRadioButton(TEXT_RADIO_BINARY_TRUE)
        self.r_true.setToolTip(tooltip_true)
        self.r_false = QRadioButton(TEXT_RADIO_BINARY_FALSE)
        self.r_false.setToolTip(tooltip_false)

        self.group = QButtonGroup()
        self.group.addButton(self.r_true)
        self.group.addButton(self.r_false)

        if default is True:
            self.r_true.setChecked(True)
        else:
            self.r_false.setChecked(True)

        self.addWidget(self.r_true)
        self.addWidget(self.r_false)

        self.setAlignment(Qt.AlignLeft)



class UiFormRadioTernary(QHBoxLayout):

    def __init__(
            self,
            tooltip_true: str = "",
            tooltip_false: str = "",
            tooltip_none: str = "Use this setting from profile settings.",
            default: bool = None
    ):
        super().__init__()

        self.r_true = QRadioButton(TEXT_RADIO_TERNARY_TRUE)
        self.r_true.setToolTip(tooltip_true)
        self.r_false = QRadioButton(TEXT_RADIO_TERNARY_FALSE)
        self.r_false.setToolTip(tooltip_false)
        self.r_none = QRadioButton(TEXT_RADIO_TERNARY_NONE)
        self.r_none.setToolTip(tooltip_none)

        tmp_font = self.r_none.font()
        tmp_font.setBold(True)
        self.r_none.setFont(tmp_font)

        self.group = QButtonGroup()
        self.group.addButton(self.r_true)
        self.group.addButton(self.r_false)
        self.group.addButton(self.r_none)

        if default is True:
            self.r_true.setChecked(True)
        elif default is False:
            self.r_false.setChecked(True)
        else:
            self.r_none.setChecked(True)

        self.addWidget(self.r_true)
        self.addWidget(self.r_false)
        self.addWidget(self.r_none)

        self.setAlignment(Qt.AlignLeft)



class UiFormRadioNoYesHard(QHBoxLayout):

    def __init__(
            self,
            tooltip_hard: str = "",
            tooltip_true: str = "",
            tooltip_false: str = "",
            default: bool = False
    ):
        super().__init__()

        self.r_hard = QRadioButton(TEXT_RADIO_TERNARY_HARD)
        self.r_hard.setToolTip(tooltip_hard)
        self.r_true = QRadioButton(TEXT_RADIO_TERNARY_TRUE)
        self.r_true.setToolTip(tooltip_true)
        self.r_false = QRadioButton(TEXT_RADIO_TERNARY_FALSE)
        self.r_false.setToolTip(tooltip_false)

        self.group = QButtonGroup()
        self.group.addButton(self.r_hard)
        self.group.addButton(self.r_true)
        self.group.addButton(self.r_false)

        if default is True:
            self.r_true.setChecked(True)
        elif default is False:
            self.r_false.setChecked(True)
        else:
            self.r_hard.setChecked(True)

        self.addWidget(self.r_hard)
        self.addWidget(self.r_true)
        self.addWidget(self.r_false)

        self.setAlignment(Qt.AlignLeft)



class UiFormEnableSpin(QHBoxLayout):

    def __init__(
            self,
            spin_minimum: int = 0,
            spin_maximum: int = 9999,
            spin_default: int = 0,
            fixed_width: int = 70,
            add_string: str = None,
    ):
        super().__init__()

        self.c_enable = QCheckBox()
        self.c_enable.clicked.connect(self.toggleEnabled)

        self.s_value = QSpinBox()
        self.s_value.setFixedWidth(fixed_width)
        self.s_value.setMinimum(spin_minimum)
        self.s_value.setMaximum(spin_maximum)
        self.s_value.setValue(spin_default)
        self.s_value.setDisabled(True)

        self.addWidget(self.c_enable)
        self.addWidget(self.s_value)
        if add_string is not None:
            self.addWidget(QLabel(add_string))

        self.setAlignment(Qt.AlignLeft)


    def toggleEnabled(self):
        self.s_value.setDisabled(not self.c_enable.isChecked())





class UiShowsEditDialog(QDialog):

    def __init__(
            self,
            index_current: int = 0
    ):
        super().__init__()

        if index_current == -1: # if nothing was selected
            index_current += 1

        self.setWindowTitle("Edit shows")
        self.setWindowModality(Qt.ApplicationModal)

        # create stack layout
        self.shows_list = QListWidget()
        self.shows_list.addItems(DUMMY_SHOW_LIST)
        self.shows_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.shows_list.setCurrentRow(index_current)
        self.shows_list.setFixedWidth(250)
        self.shows_list.currentTextChanged.connect(self.handleShowsEditListSelected)
        self.shows_list.setAlternatingRowColors(True)

        self.shows_buttons = UiShowsEditButtons()
        self.shows_buttons.add_one.clicked.connect(self.addShowDialog)
        self.shows_buttons.remove.clicked.connect(self.removeShowDialog)
        self.shows_buttons.duplicate.clicked.connect(self.duplicateShowDialog)

        self.v_box_shows = QVBoxLayout()
        self.v_box_shows.addWidget(self.shows_list)
        self.v_box_shows.addLayout(self.shows_buttons)
        self.v_box_shows.setContentsMargins(0, 0, 0, 0)


        # TODO: preselect the show that is to be edited (or none selected, then first) and fill forms


        # form layout for the main settings
        self.main_settings = UiShowMainSettings()
        self.extra_settings = UiShowExtraSettings()
        self.common_settings = UiShowProfileCommonSettings(True)

        # add extra and common settings into an HBox
        self.settings_h_box = QHBoxLayout()
        self.settings_h_box.addLayout(self.extra_settings)
        self.settings_h_box.addLayout(self.common_settings)


        # buttons Save Cancel
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save,
            Qt.Horizontal
        )
        self.button_box.accepted.connect(self.handleShowSave) # TODO handle save button


        # combine settings and buttons
        self.v_box_settings_all = QVBoxLayout()
        self.v_box_settings_all.addLayout(self.main_settings)
        self.v_box_settings_all.addLayout(self.settings_h_box)
        self.v_box_settings_all.addWidget(self.button_box)


        # combine all in stack-like layout
        self.layout_all = QHBoxLayout()
        self.layout_all.addLayout(self.v_box_shows)
        self.layout_all.addLayout(self.v_box_settings_all)

        self.setLayout(self.layout_all)


    def handleShowSave(self):
        print("save show!") # TODO example change Standard Buttons. TODO: add Cancel durign editing, remove when saved. Cancel button restores settings (not defaults)
        self.button_box.hide()
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Save
            | QDialogButtonBox.Cancel,
            Qt.Horizontal
        )
        self.v_box_settings_all.addWidget(self.button_box)


    def handleShowsEditListSelected(self):
        """Switch the settings to the selected show. Warn if not saved.
        """
        # TODO
        pass


    def addShowDialog(self):
        self.dialog_new_show = UiShowsNewDialog()
        self.dialog_new_show.show()


    def removeShowDialog(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Delete show '" + self.shows_list.selectedItems()[0].text() + "'?")
        msg.setInformativeText("This action cannot be undone. This show will be deleted from all profiles.")
        msg.setWindowTitle("Delete Show")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msg.accepted.connect(self.removeShowConfirmed)
        msg.exec_()

    def removeShowConfirmed(self):
        print("Delete " + self.shows_list.selectedItems()[0].text()) # TODO


    def duplicateShowDialog(self):
        i, okPressed = QInputDialog.getText(self, "Duplicate Show", "Name of copy of '" + self.shows_list.selectedItems()[0].text() + "':", QLineEdit.Normal, "")
        if okPressed:
            print(i)




class UiShowsEditButtons(QHBoxLayout):

    def __init__(self):
        super().__init__()

        self.add_one = UiManagementToolButton(ICON_ADD_THIN, "Create a new User.", True, True)
        self.remove = UiManagementToolButton(ICON_REMOVE_THIN, "Delete currently selected User.", True, True)
        self.duplicate = UiManagementToolButton(ICON_DUPLICATE, "Duplicate the current User.", True, True)

        # self.setAlignment(Qt.AlignLeft)
        self.addWidget(self.add_one)
        self.addWidget(self.remove)
        self.addWidget(self.duplicate)
        self.addStretch()
        self.setContentsMargins(0, 0, 0, 0)





class UiPlaybackControlVBox(QVBoxLayout):

    def __init__(
            self,
            window_style: QStyle,
            mediaplayer: vlc.MediaPlayer,
            equalizer: EqualizerBar,
            open_file_dialog,
            timer_equalizer_start,
            timer_equalizer_stop,
            toggle_media_area_state,
            set_media_area_state,
            get_media_area_state,
            showTextOnMedia
    ):
        super().__init__()

        self.mediaplayer = mediaplayer
        self.equalizer = equalizer
        self.open_file_dialog = open_file_dialog
        self.timer_equalizer_start = timer_equalizer_start
        self.timer_equalizer_stop = timer_equalizer_stop
        self.toggle_media_area_state = toggle_media_area_state
        self.set_media_area_state = set_media_area_state
        self.get_media_area_state = get_media_area_state
        self.showTextOnMedia = showTextOnMedia


        # start timer
        self.timer_playback = QTimer()
        self.timer_playback.setInterval(TIMER_PLAYBACK_UPDATE_MS)
        self.timer_playback.timeout.connect(self.updatePlaybackUI)


        # time label and slider layout
        self.time_progress_label = QLabel("00:00")
        self.time_progress_label.setStatusTip("Playback time.")

        self.time_progress_slider = QSlider(Qt.Horizontal)
        self.time_progress_slider.setStatusTip("Playback position.")
        self.time_progress_slider.setMaximum(10000)
        self.time_progress_slider.setFixedHeight(30)
        self.time_progress_slider.sliderPressed.connect(self.handleTimesliderPressed)
        self.time_progress_slider.sliderMoved.connect(self.handleTimesliderMoved)
        self.time_progress_slider.sliderReleased.connect(self.handleTimesliderReleased)

        self.time_progress_slider_moving = False
        self.time_progress_slider_new_position = False

        self.h_box_time = QHBoxLayout()
        self.h_box_time.addWidget(self.time_progress_label)
        self.h_box_time.setAlignment(self.time_progress_label, Qt.AlignCenter)
        self.h_box_time.addWidget(self.time_progress_slider)
        self.h_box_time.setContentsMargins(5, 0, 0, 0)


        self.show_extra_lines = False # TODO read from global-config

        # standard (first) line
        self.play_pause_button = QToolButton()
        self.play_pause_button.setIcon(QIcon(ICON_PLAY_START))
        self.play_pause_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.play_pause_button.setStatusTip("Start the current session.")
        self.play_pause_button.clicked.connect(self.handlePlayPausePlayback)
        self.play_pause_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.stop_button = QToolButton()
        self.stop_button.setPopupMode(QToolButton.DelayedPopup)
        self.stop_button.setIcon(QIcon(ICON_STOP))
        self.stop_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.stop_button.setStatusTip("Stop session. Hold Left Click to choose from other options.")
        self.stop_button.clicked.connect(self.handleStopPlayback)
        self.stop_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE + 8, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.menu_stop_options = UiMenuStopOptions()
        self.stop_button.setMenu(self.menu_stop_options.menu)


        self.skip_button = QToolButton()
        self.skip_button.setPopupMode(QToolButton.DelayedPopup)
        self.skip_button.setIcon(QIcon(ICON_SKIP))
        self.skip_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.skip_button.setStatusTip("Skip current media. Hold Left Click to choose from other options.")
        self.skip_button.clicked.connect(self.handleSkipPlayback)
        self.skip_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE + 8, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.menu_skip_options = UiMenuSkipOptions()
        self.skip_button.setMenu(self.menu_skip_options.menu)


        self.jump_back_button = QToolButton()
        self.jump_back_button.setIcon(QIcon(ICON_BACKWARD))
        self.jump_back_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.jump_back_button.setStatusTip("Jump back 5 seconds.") #TODO: adjust seconds amount
        self.jump_back_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.jump_forward_button = QToolButton()
        self.jump_forward_button.setIcon(QIcon(ICON_FORWARD))
        self.jump_forward_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.jump_forward_button.setStatusTip("Jump forward 5 seconds.") #TODO: adjust seconds amount
        self.jump_forward_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)


        self.mute_button = QToolButton()
        self.mute_button.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.mute_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.mute_button.setStatusTip("Toggle mute.")
        self.mute_button.setCheckable(True)
        self.mute_button.clicked.connect(self.handleMutePlayback)
        self.mute_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.volume_label = QLabel("100")
        self.volume_label.setContentsMargins(0, 0, 0, 0)
        # self.volume_label.setFont(QFont("Arial", 5))

        self.volume_slider = QSlider(Qt.Horizontal)
        self.volume_slider.setMaximum(VOLUME_LIMIT_UPPER)
        self.volume_slider.setValue(100)
        self.volume_slider.setStatusTip("Volume")
        self.volume_slider.setContentsMargins(0, 0, 0, 0)
        self.volume_slider.valueChanged.connect(self.handleSetVolume)
        self.volume_slider.setMinimumWidth(75)
        self.volume_slider.setMaximumWidth(230)

        self.volume_slider_show_dynamic = QSlider(Qt.Horizontal)
        self.volume_slider_show_dynamic.setMaximum(VOLUME_LIMIT_UPPER)
        self.volume_slider_show_dynamic.setValue(100)
        self.volume_slider_show_dynamic.setStatusTip("Dynamic Show Volume")
        self.volume_slider_show_dynamic.setContentsMargins(0, 0, 0, 0)
        self.volume_slider_show_dynamic.valueChanged.connect(self.handleSetVolume)
        self.volume_slider_show_dynamic.setMinimumWidth(75)
        self.volume_slider_show_dynamic.setMaximumWidth(230)

        self.v_box_volume = QVBoxLayout()
        self.v_box_volume.addWidget(self.volume_label)
        self.v_box_volume.setAlignment(self.volume_label, Qt.AlignCenter)
        self.v_box_volume.addWidget(self.volume_slider)
        # self.v_box_volume.addWidget(self.volume_slider_show_dynamic) # TODO CONTINUE add if statement for dynamic settings

        self.fullscreen_button = QPushButton()
        self.fullscreen_button.setIcon(QIcon(ICON_FULLSCREEN))
        self.fullscreen_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.fullscreen_button.setStatusTip("Toggle full screen.")
        self.fullscreen_button.setCheckable(True)
        self.fullscreen_button.clicked.connect(self.handle_toggle_fullscreen)
        self.fullscreen_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.toggle_media_area_button = QToolButton()
        self.toggle_media_area_button.setArrowType(Qt.UpArrow)
        self.toggle_media_area_button.setPopupMode(QToolButton.DelayedPopup)
        self.menu_media_area_states = UiMenuMediaAreaStates(self.toggle_media_area_button, self.set_media_area_state, self.get_media_area_state)
        self.toggle_media_area_button.setMenu(self.menu_media_area_states.menu)
        self.toggle_media_area_button.setStatusTip("Toggle media area. Hold Left Click to choose state.")
        # self.toggle_media_area_button.clicked.connect(self.toggle_media_area_state())
        self.toggle_media_area_button.clicked.connect(lambda: self.menu_media_area_states.selectedOption(MediaAreaState.next_value(self.get_media_area_state()))) # takes care of checking the right one in the popup, else self.toggle_media_area_state() would be enough
        self.toggle_media_area_button.setFixedSize(int(BUTTON_PLAYBACK_SQUARE_SIZE / 2), int(BUTTON_PLAYBACK_SQUARE_SIZE / 2) - 3)

        self.toggle_extras_button = QToolButton()
        self.toggle_extras_button.setArrowType(Qt.DownArrow)
        self.toggle_extras_button.setStatusTip("Toggle extra buttons.")
        self.toggle_extras_button.setCheckable(True)
        self.toggle_extras_button.clicked.connect(self.handleToggleExtraButtons)
        self.toggle_extras_button.setFixedSize(int(BUTTON_PLAYBACK_SQUARE_SIZE / 2), int(BUTTON_PLAYBACK_SQUARE_SIZE / 2) - 3)

        self.v_box_arrow_buttons = QVBoxLayout()
        self.v_box_arrow_buttons.addWidget(self.toggle_media_area_button)
        self.v_box_arrow_buttons.addWidget(self.toggle_extras_button)


        # extra (second, usually hidden) line

        self.open_dir_button = QToolButton()
        self.open_dir_button.setIcon(QIcon(ICON_FOLDER))
        self.open_dir_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.open_dir_button.setStatusTip("Open folder containing the currently played media.")
        self.open_dir_button.clicked.connect(self.handleOpenDirCurrentMedia)
        self.open_dir_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.loop_button = QToolButton()
        self.loop_button.setIcon(QIcon(ICON_LOOP))
        self.loop_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.loop_button.setStatusTip("Loop current media.")
        self.loop_button.setCheckable(True)
        self.loop_button.clicked.connect(self.handleLoopPlayback)
        self.loop_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.timer_button = QToolButton()
        self.timer_button.setIcon(QIcon(ICON_TIMER))
        self.timer_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.timer_button.setStatusTip("Set turn off countdown.")
        self.timer_button.clicked.connect(self.handleShutdownEpisodesLimitDialog)
        self.timer_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.faster_button = QToolButton()
        self.faster_button.setIcon(QIcon(ICON_FASTER))
        self.faster_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.faster_button.setStatusTip("Speed up playback.")
        self.faster_button.clicked.connect(self.handleFasterPlayback)
        self.faster_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        self.slower_button = QToolButton()
        self.slower_button.setIcon(QIcon(ICON_SLOWER))
        self.slower_button.setIconSize(QSize(BUTTON_PLAYBACK_ICON_SIZE, BUTTON_PLAYBACK_ICON_SIZE))
        self.slower_button.setStatusTip("Slow down playback.")
        self.slower_button.clicked.connect(self.handleSlowerPlayback)
        self.slower_button.setFixedSize(BUTTON_PLAYBACK_SQUARE_SIZE, BUTTON_PLAYBACK_SQUARE_SIZE)

        # combine all extra buttons
        self.h_box_playback_buttons_extra = QHBoxLayout()
        self.h_box_playback_buttons_extra.addWidget(self.open_dir_button)
        self.h_box_playback_buttons_extra.addWidget(self.loop_button)
        self.h_box_playback_buttons_extra.addWidget(self.timer_button)
        self.h_box_playback_buttons_extra.addWidget(self.faster_button)
        self.h_box_playback_buttons_extra.addWidget(self.slower_button)
        self.h_box_playback_buttons_extra.addStretch()
        self.h_box_playback_buttons_extra.setContentsMargins(0, 0, 0, 0)


        # put extra buttons in a frame that can be easily hidden
        self.h_box_playback_buttons_extra_frame_side = QFrame()
        self.h_box_playback_buttons_extra_frame_side.setLayout(self.h_box_playback_buttons_extra)
        # self.h_box_playback_buttons_extra_frame_side.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum) #FIXME: i have no idea why this works (not necessary anymore after alignment was set to bottom
        self.h_box_playback_buttons_extra_frame_side.setHidden(True)

        # disable some buttons until media playback starts
        self.handlePlaybackControlsEnabled(False)


        # combine all standard buttons
        self.h_box_playback_buttons_main = QHBoxLayout()
        self.h_box_playback_buttons_main.addWidget(self.play_pause_button)
        self.h_box_playback_buttons_main.addWidget(self.stop_button)
        self.h_box_playback_buttons_main.addWidget(self.skip_button)
        self.h_box_playback_buttons_main.addWidget(self.jump_back_button)
        self.h_box_playback_buttons_main.addWidget(self.jump_forward_button)
        self.h_box_playback_buttons_main.addStretch()
        self.h_box_playback_buttons_main.addWidget(self.h_box_playback_buttons_extra_frame_side)
        self.h_box_playback_buttons_main.addStretch()
        self.h_box_playback_buttons_main.addWidget(self.mute_button)
        self.h_box_playback_buttons_main.addLayout(self.v_box_volume)
        self.h_box_playback_buttons_main.addWidget(self.fullscreen_button)
        self.h_box_playback_buttons_main.addLayout(self.v_box_arrow_buttons)


        # combine both lines
        self.addLayout(self.h_box_time)
        self.addLayout(self.h_box_playback_buttons_main)
        # self.addWidget(self.h_box_playback_buttons_extra_frame_bottom)
        self.setContentsMargins(10, 0, 10, 0)



    def handlePlaybackControlsEnabled(self, enabled: bool): # TODO make sure all buttons are here
        self.time_progress_slider.setEnabled(enabled)

        self.stop_button.setEnabled(enabled)
        self.skip_button.setEnabled(enabled)
        self.jump_back_button.setEnabled(enabled)
        self.jump_forward_button.setEnabled(enabled)
        self.loop_button.setEnabled(enabled)
        self.mute_button.setEnabled(enabled) # TODO: should always be enabled, needs to be handled properly later
        self.volume_slider.setEnabled(enabled) # TODO: should always be enabled, needs to be handled properly later
        self.open_dir_button.setEnabled(enabled)
        self.loop_button.setEnabled(enabled)
        self.timer_button.setEnabled(enabled)
        self.slower_button.setEnabled(enabled)
        self.faster_button.setEnabled(enabled)
        self.fullscreen_button.setEnabled(enabled)


    def updatePlaybackUI(self):
        """updates the user interface"""
        # setting the slider or the playback to the desired position
        if self.time_progress_slider_moving:
            if self.time_progress_slider_new_position:
                self.mediaplayer.set_position(self.time_progress_slider.value() / 10000.0)
                self.time_progress_slider_new_position = False
        else:
            self.media_length_string = get_h_m_s_from_milliseconds(
                self.mediaplayer.get_length())  # TODO: move into player handle options post play
            self.time_progress_slider.setValue(floor(self.mediaplayer.get_position() * 10000))
            self.time_progress_label.setText(
                get_h_m_s_from_milliseconds(self.mediaplayer.get_time()) + " / " + self.media_length_string)


            # TODO maybe delete this later
            if not self.mediaplayer.is_playing():
                # no need to call this function if nothing is played
                self.timer_playback.stop()
                if not self.isPaused:
                    # after the video finished, the play button stills shows
                    # "Pause", not the desired behavior of a media player
                    # this will fix it
                    self.handleStopPlayback()


    # handle button presses

    def handlePlayPausePlayback(self):
        """Toggle play/pause status
        """
        if self.mediaplayer.is_playing():
            self.mediaplayer.pause()
            self.play_pause_button.setIcon(QIcon(ICON_PLAY))
            self.play_pause_button.setStatusTip(STATUSTIP_CONTROL_PLAY)
            self.isPaused = True
            self.timer_equalizer_stop()
        else:
            if self.mediaplayer.play() == -1:
                self.open_file_dialog()
                return
            self.mediaplayer.play()
            self.handlePlaybackControlsEnabled(True)
            self.play_pause_button.setIcon(QIcon(ICON_PAUSE))
            self.play_pause_button.setStatusTip(STATUSTIP_CONTROL_PAUSE)
            self.timer_playback.start()
            if not self.equalizer.isVisible():
                self.timer_equalizer_start()
            self.isPaused = False


    def handleStopPlayback(self): # TODO stop session, enable choosing other profile or user
        """Stop player
        """
        self.mediaplayer.stop()
        self.timer_playback.stop()
        self.timer_equalizer_stop()
        self.time_progress_slider.setValue(0)
        self.time_progress_label.setText("00:00")
        self.play_pause_button.setIcon(QIcon(ICON_PLAY_START))
        self.handlePlaybackControlsEnabled(False)


    def handleStopPlaybackMenu(self, point): # TODO: normal, suspend, ssuspend
        # show context menu
        self.stop_button_menu.exec_(self.stop_button.mapToGlobal(point))


    def handleSkipPlayback(self): # TODO
        """Skip this media
        """


    def handleSkipHardPlayback(self): # TODO
        """Skip to the next episode (or its opening)
        """


    def handleSkipToCommercials(self): # TODO
        """Skip to commercials
        """


    def handleSlowerPlayback(self): # TODO
        """Speed down playback
        """


    def handleFasterPlayback(self): # TODO
        """Speed up playback
        """


    def handleShutdownEpisodesLimitDialog(self): # TODO
        """Implement old 'shutdown' and 'last' command as gui
        """


    def handleOpenDirCurrentMedia(self): # TODO
        """Open directory of currently playing media
        """


    def handleLoopPlayback(self): # TODO
        """Loop current media forever (toggle)
        """
        if self.loop_button.isChecked():
            self.loop_button.setIcon(QIcon(ICON_LOOPING))
        else:
            self.loop_button.setIcon(QIcon(ICON_LOOP))

        # TODO


    def handleSetVolume(self, volume):
        """Set the volume
        """
        self.mediaplayer.audio_set_volume(volume) # TODO replace by proper volume handling
        self.volume_label.setText(str(volume))


    def handleMutePlayback(self):
        """Mute the playback (toggle)
        """
        # TODO replace by proper mute handling
        # TODO correct possible button desync in update_ui ??
        mute_state = self.mediaplayer.audio_get_mute()
        if mute_state == 0:  # 0 if not muted
            mute_bool = True
            self.mute_button.setIcon(QIcon(ICON_SPEAKER_MUTED))
        else:
            mute_bool = False
            self.mute_button.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.mediaplayer.audio_set_mute(mute_bool)


    def handle_toggle_fullscreen(self):
        self.showTextOnMedia("Test hey sped up" + str(random.randint(1, 17)), 2500) # TODO use at appropriate places
        equalizerAdjustByMedia(self.equalizer, "test1" + str(random.randint(1, 2000)), random.randint(3, 30), False) #TODO move somewhere else, use proper name and episode amount

        newBool = self.fullscreen_button.isChecked()
        self.mediaplayer.set_fullscreen(newBool)
        # print("fullscreen: " + str(newBool) + "! " + str(self.mediaplayer.get_fullscreen()))
        # print("TODO not working yet")


    def handleTimesliderPressed(self):
        """Deactivate updating of slider, else dragged Slider bugs around
        """
        # self.timer_playback.stop()
        self.time_progress_slider_moving = True


    def handleTimesliderReleased(self):
        """Reactivate updating of slider, else dragged Slider bugs around
        """
        if self.time_progress_slider_new_position:
            self.mediaplayer.set_position(self.time_progress_slider.value() / 10000.0)
        self.time_progress_slider_new_position = False
        # self.timer_playback.start()
        self.time_progress_slider_moving = False


    def handleTimesliderMoved(self, position):
        """Set the playback time with the slider
        """
        # setting the position to where the slider was dragged
        # self.mediaplayer.set_position(position / 10000.0)
        self.time_progress_slider_new_position = True
        self.time_progress_label.setText(get_h_m_s_from_milliseconds(math.floor(self.mediaplayer.get_length() * (position / 10000.0))) + " / " + self.media_length_string) # TODO: cache get_length somewhere
        # the vlc MediaPlayer needs a float value between 0 and 1, Qt
        # uses integer variables, so you need a factor; the higher the
        # factor, the more precise are the results
        # (10000 should be enough)


    def handleToggleExtraButtons(self):
        if self.show_extra_lines:
            # self.h_box_playback_buttons_extra_frame_bottom.setHidden(True)
            self.h_box_playback_buttons_extra_frame_side.setHidden(True)
        else:
            # self.h_box_playback_buttons_extra_frame_bottom.setHidden(False)
            self.h_box_playback_buttons_extra_frame_side.setHidden(False)
        self.show_extra_lines = not self.show_extra_lines


    def handleToggleMediaArea(self):
        pass # TODO




class UiMenubarPlayer(QMenuBar): # TODO: add logic behind all menu actions

    def __init__(
            self,
            open_file_dialog
    ):
        super().__init__()


        #
        # # File TODO: delete
        # self.menubar_file = self.addMenu("&File") # TODO delete
        # 
        # self.menu_open = QAction("&Open")
        # # self.menu_open.setShortcut("Ctrl+O")
        # self.menu_open.setStatusTip("Open a new File")
        # self.menu_open.triggered.connect(open_file_dialog)
        # self.menubar_file.addAction(self.menu_open)
        # 
        # 
        # self.menubar_file.addSeparator()
        # 
        # 
        # self.menu_exit = QAction("&Exit")
        # self.menu_exit.setShortcut("Ctrl+E")
        # self.menu_exit.setStatusTip("Close the program")
        # self.menu_exit.triggered.connect(sys.exit) # TODO: if session is playing, stop it and show warning: please stop the session first yourself to avoid problems when saving the session. Maybe add checkbox "Ignore the risk and don't show this again".
        # # TODO ^ move this into clicking the X of the window. This "File"-menu will be deleted
        # self.menubar_file.addAction(self.menu_exit)



        # Management
        self.menubar_management = self.addMenu("&Manage")

        self.menu_shows = QAction("&Shows")
        self.menu_shows.setStatusTip("Manage shows.")
        # self.menu_shows.triggered.connect(self.showDialogShowsEdit) # TODO
        self.menubar_management.addAction(self.menu_shows)

        self.menu_profiles = QAction("&Profiles")
        self.menu_profiles.setStatusTip("Manage profiles, users and base paths.")
        # self.menu_profiles.triggered.connect(self.showDialogShowsEdit) # TODO
        self.menubar_management.addAction(self.menu_profiles)



        # Playback
        self.menubar_playback = self.addMenu("&Playback")

        self.menu_play_pause = QAction("&Play")
        self.menu_play_pause.setIcon(QIcon(ICON_PLAY_START)) # TODO use blue and pause icons later
        self.menu_play_pause.setShortcut("Ctrl+P")
        self.menu_play_pause.setStatusTip("TODO: use variables for all status tips, maybe put into new module. Also good for possible translation later.")
        # self.menu_play_pause.triggered.connect()
        self.menubar_playback.addAction(self.menu_play_pause)

        self.menu_stop = QMenu("&Stop")
        self.menu_stop.setIcon(QIcon(ICON_STOP))
        # self.menu_stop.setStatusTip("")
        # self.menu_stop.triggered.connect()
        self.menubar_playback.addMenu(self.menu_stop)

        self.menu_stop_queue_time = QAction("&Suspend Session")
        self.menu_stop_queue_time.setShortcut("Ctrl+Q") # TODO: set shortcut on the stop variant that is selected in the settings
        # self.menu_stop_queue_time.setStatusTip("")
        # self.menu_stop_queue_time.triggered.connect()
        self.menu_stop.addAction(self.menu_stop_queue_time)

        self.menu_stop_time = QAction("&Suspend Show")
        # self.menu_stop_time.setShortcut("Ctrl+Q")
        # self.menu_stop_time.setStatusTip("")
        # self.menu_stop_time.triggered.connect()
        self.menu_stop.addAction(self.menu_stop_time)

        self.menu_stop_queue = QAction("&Suspend Queue")
        # self.menu_stop_queue.setShortcut("Ctrl+Q")
        # self.menu_stop_queue.setStatusTip("")
        # self.menu_stop_queue.triggered.connect()
        self.menu_stop.addAction(self.menu_stop_queue)

        self.menu_stop_simple = QAction("&Stop")
        # self.menu_stop_simple.setShortcut("Ctrl+Q")
        # self.menu_stop_simple.setStatusTip("")
        # self.menu_stop_simple.triggered.connect()
        self.menu_stop.addAction(self.menu_stop_simple)


        self.menu_skip = QMenu("&Skip")
        self.menu_skip.setIcon(QIcon(ICON_SKIP))
        # self.menu_skip.setStatusTip("")
        # self.menu_skip.triggered.connect()
        self.menubar_playback.addMenu(self.menu_skip)

        self.menu_skip_simple = QAction("&Skip")
        self.menu_skip_simple.setShortcut("Ctrl+S")
        # self.menu_skip_simple.setStatusTip("")
        # self.menu_skip_simple.triggered.connect()
        self.menu_skip.addAction(self.menu_skip_simple)

        self.menu_skip_show = QAction("&Skip Show")
        # self.menu_skip_show.setStatusTip("")
        # self.menu_skip_show.triggered.connect()
        self.menu_skip.addAction(self.menu_skip_show)

        self.menu_skip_all = QAction("&Skip All")
        # self.menu_skip_all.setStatusTip("")
        # self.menu_skip_all.triggered.connect()
        self.menu_skip.addAction(self.menu_skip_all)

        self.menu_jump_back = QAction("&Jump Back")
        self.menu_jump_back.setIcon(QIcon(ICON_BACKWARD))
        self.menu_jump_back.setShortcut("Ctrl+B")
        # self.menu_jump_back.setStatusTip("")
        # self.menu_jump_back.triggered.connect()
        self.menubar_playback.addAction(self.menu_jump_back)

        self.menu_jump_forward = QAction("&Jump Forward")
        self.menu_jump_forward.setIcon(QIcon(ICON_FORWARD))
        self.menu_jump_forward.setShortcut("Ctrl+F")
        # self.menu_jump_forward.setStatusTip("")
        # self.menu_jump_forward.triggered.connect()
        self.menubar_playback.addAction(self.menu_jump_forward)


        self.menubar_playback.addSeparator()


        self.menu_mute = QAction("&Mute") # TODO toggleable
        self.menu_mute.setIcon(QIcon(ICON_SPEAKER_UNMUTED))
        self.menu_mute.setShortcut("Ctrl+M")
        self.menu_mute.setCheckable(True)
        # self.menu_mute.setStatusTip("")
        # self.menu_mute.triggered.connect()
        self.menubar_playback.addAction(self.menu_mute)


        self.menu_fullscreen = QAction("&Fullscreen") # TODO toggleable??
        self.menu_fullscreen.setIcon(QIcon(ICON_FULLSCREEN))
        self.menu_fullscreen.setDisabled(True) # TODO set all disabled just like the buttons and enable them at the same times each
        self.menu_fullscreen.setShortcut("Ctrl+F")
        # self.menu_fullscreen.setStatusTip("")
        # self.menu_fullscreen.triggered.connect()
        self.menubar_playback.addAction(self.menu_fullscreen)


        self.menubar_playback.addSeparator()


        self.menu_open_dir = QAction("&Open media directory")
        self.menu_open_dir.setIcon(QIcon(ICON_FOLDER))
        self.menu_open_dir.setShortcut("Ctrl+O")
        # self.menu_open_dir.setStatusTip("")
        # self.menu_open_dir.triggered.connect()
        self.menubar_playback.addAction(self.menu_open_dir)

        self.menu_loop = QAction("&Loop On") # TODO toggleable
        self.menu_loop.setIcon(QIcon(ICON_LOOP))
        self.menu_loop.setShortcut("Ctrl+L")
        self.menu_loop.setCheckable(True)
        # self.menu_loop.setStatusTip("")
        # self.menu_loop.triggered.connect()
        self.menubar_playback.addAction(self.menu_loop)

        self.menu_timer = QAction("&Timer")
        self.menu_timer.setIcon(QIcon(ICON_TIMER)) # TODO toggleable
        self.menu_timer.setShortcut("Ctrl+T")
        self.menu_timer.setCheckable(True)
        # self.menu_timer.setStatusTip("")
        # self.menu_timer.triggered.connect()
        self.menubar_playback.addAction(self.menu_timer)

        self.menu_speed = QMenu("&Speed")
        # self.menu_speed.setStatusTip("")
        # self.menu_speed.triggered.connect()
        self.menubar_playback.addMenu(self.menu_speed)

        self.menu_speed_faster = QAction("&Faster")
        self.menu_speed_faster.setIcon(QIcon(ICON_FASTER))
        # self.menu_speed_faster.setStatusTip("")
        # self.menu_speed_faster.triggered.connect()
        self.menu_speed.addAction(self.menu_speed_faster)

        self.menu_speed_normal = QAction("&Normal Speed")
        # self.menu_speed_normal.setStatusTip("")
        # self.menu_speed_normal.triggered.connect()
        self.menu_speed.addAction(self.menu_speed_normal)

        self.menu_speed_slower = QAction("&Slower")
        self.menu_speed_slower.setIcon(QIcon(ICON_SLOWER))
        # self.menu_speed_slower.setStatusTip("")
        # self.menu_speed_slower.triggered.connect()
        self.menu_speed.addAction(self.menu_speed_slower)


        self.menubar_playback.addSeparator()


        self.menu_track = QMenu("&Tracks")
        # self.menu_track.setStatusTip("")
        # self.menu_track.triggered.connect()
        self.menubar_playback.addMenu(self.menu_track)

        self.menu_track_video = QAction("&Video")
        # self.menu_track_video.setStatusTip("")
        # self.menu_track_video.triggered.connect()
        self.menu_track.addAction(self.menu_track_video)

        self.menu_track_audio = QAction("&Audio")
        # self.menu_track_audio.setStatusTip("")
        # self.menu_track_audio.triggered.connect()
        self.menu_track.addAction(self.menu_track_audio)

        self.menu_track_subtitles = QAction("&Subtitles")
        # self.menu_track_subtitles.setStatusTip("")
        # self.menu_track_subtitles.triggered.connect()
        self.menu_track.addAction(self.menu_track_subtitles)



        # Extra
        self.menubar_extra = self.addMenu("&Extra")

        self.menu_mp3merge = QAction("&Mp3Merge")
        self.menu_mp3merge.setStatusTip("Create condensed anime episodes from 'subs2srs' Anki (.apkg) files.")
        self.menu_mp3merge.triggered.connect(self.showDialogMp3Merge) # TODO
        self.menubar_extra.addAction(self.menu_mp3merge)

        self.menu_debug_player_jumps = QAction("&Debug Player Jumps")
        self.menu_debug_player_jumps.setStatusTip("Experimental setting trying to fix buggy media files.")
        self.menu_debug_player_jumps.triggered.connect(self.showDialogDebugPlayerJumps)
        self.menubar_extra.addAction(self.menu_debug_player_jumps)

        self.menu_analysis = QAction("&Analysis")
        self.menu_analysis.setStatusTip("Show some stats about available shows and some estimated usage values.")
        # self.menu_analysis.triggered.connect(self.showDialogAnalysis) # TODO
        self.menubar_extra.addAction(self.menu_analysis)



        # Help
        self.menubar_help = self.addMenu("&Help")

        self.menu_about = QAction("&About")
        self.menu_about.setStatusTip("About this program")
        # self.menu_about.triggered.connect(self.showDialogAbout) # TODO
        self.menubar_help.addAction(self.menu_about)

        self.menu_tutorial = QAction("&How To...")
        self.menu_tutorial.setStatusTip("How to use this program")
        self.menu_tutorial.triggered.connect(self.showDialogAbout) # TODO
        self.menubar_help.addAction(self.menu_tutorial)

        self.menu_about_qt = QAction("&GUI")
        self.menu_about_qt.setStatusTip("This user interface was programmed using PyQt, Qt for Python.")
        self.menu_about_qt.triggered.connect(lambda: QMessageBox.aboutQt(self))
        self.menubar_help.addAction(self.menu_about_qt)




    @staticmethod # TODO: move somewhere else
    def showDialogAbout(self):
        about = QMessageBox()
        about.setWindowTitle("About")
        about.setText("""Python script with GUI for customizable ordered playback of a randomly picked
series of predefined media files from your local computer.""")
        about.setInformativeText("""TODO finish this, put into own variable.

python-vlc is used to play the media files, see https://www.olivieraubert.net/vlc/python-ctypes/

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.

Source Code: gitlab.com/CptMaister/ranimetv
Contact: alex.mai@posteo.net""")
        about.setIcon(QMessageBox.Information)

        # about.setStandardButtons(QMessageBox.Close|QMessageBox.Ok)
        about.setStandardButtons(QMessageBox.Close)
        about.exec_()


    def showDialogDebugPlayerJumps(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(("Deactivate" if True else "Activate") + " 'Debug Player Jumps'?") # TODO replace True by real setting
        msg.setInformativeText("When creating 'condensed anime immersion' like with the 'Mp3Merge'-feature, the files will sometimes have errors that will jumble the playback up at certain playback positions.\n\nWith this option activated, any anomalies of the playback length will be automatically detected as error and the media first restarted, then the playback position set to 1 second after the error was encountered.")
        msg.setWindowTitle("Debug Player Jumps")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg.accepted.connect(self.activateDebugPlayerJumps)
        msg.rejected.connect(self.deactivateDebugPlayerJumps)
        msg.exec_()


    def activateDebugPlayerJumps(self): # TODO
        print("activate Debug Player Jumps")


    def deactivateDebugPlayerJumps(self): # TODO
        print("deactivate Debug Player Jumps")


    def showDialogMp3Merge(self): # TODO CONTINUE !!!!!
        self.m3m_dialog = QDialog()
        self.m3m_dialog.setWindowTitle("Mp3Merge")
        self.m3m_dialog.setWindowModality(Qt.ApplicationModal)
        self.m3m_dialog.setMinimumWidth(500)

        path_box = UiPathBox(True)

        button_box = QDialogButtonBox(
            QDialogButtonBox.Ok,
            Qt.Horizontal
        )
        button_box.accepted.connect(lambda: print("Start conversion: " + path_box.path_input.text())) # TODO handle save button

        # combine path box and buttons
        layout_all = QVBoxLayout()
        layout_all.addLayout(path_box)
        layout_all.addWidget(button_box)

        self.m3m_dialog.setLayout(layout_all)
        self.m3m_dialog.show()




# TODO: add functionality
class UiMenuStopOptions:

    def __init__(
            self
    ):

        self.menu = QMenu()

        self.menu_stop_queue_time = self.menu.addAction("&Suspend Session")
        self.menu_stop_queue_time.setStatusTip("Stop, continue here next time with current queue.")
        self.menu_stop_queue_time.setCheckable(True)
        self.menu_stop_queue_time.setChecked(True) # TODO: read checked from settings
        # self.menu_stop_queue_time.triggered.connect()

        self.menu_stop_time = self.menu.addAction("&Suspend Show")
        self.menu_stop_time.setStatusTip("Stop, continue here next time, don't save current queue.")
        self.menu_stop_time.setCheckable(True)
        # self.menu_stop_time.triggered.connect()

        self.menu_stop_queue = self.menu.addAction("&Suspend Queue")
        self.menu_stop_queue.setStatusTip("Stop, save current queue, but not current playback position.")
        self.menu_stop_queue.setCheckable(True)
        # self.menu_queue.triggered.connect()

        self.menu_stop_simple = self.menu.addAction("&Stop")
        self.menu_stop_simple.setStatusTip("Stop, discard current position and queue.")
        self.menu_stop_simple.setCheckable(True)
        # self.menu_stop_simple.triggered.connect()


# TODO: add functionality
class UiMenuSkipOptions:

    def __init__(
            self
    ):

        self.menu = QMenu()

        self.menu_skip = self.menu.addAction("&Skip")
        self.menu_skip.setStatusTip("Skip current media.")
        # self.menu_skip.triggered.connect()

        self.menu_skip_show = self.menu.addAction("&Skip Show")
        self.menu_skip_show.setStatusTip("Skip to commercials (if you have any).")
        # self.menu_skip_show.triggered.connect()

        self.menu_skip_all = self.menu.addAction("&Skip All")
        self.menu_skip_all.setStatusTip("Skip to next show's episode.")
        # self.menu_skip_all.triggered.connect()



class UiMenuMediaAreaStates:

    def __init__(
            self,
            toggle_media_area_button,
            set_media_area_state,
            get_media_area_state
    ):
        self.toggle_media_area_button = toggle_media_area_button
        self.set_media_area_state = set_media_area_state
        self.get_media_area_state = get_media_area_state

        self.menu = QMenu()

        self.menu_vid_equ = self.menu.addAction("&Normal")
        self.menu_vid_equ.setStatusTip("Show video, equalizer if audio, management if nothing playing.")
        self.menu_vid_equ.setCheckable(True)
        self.menu_vid_equ.setChecked(True) # TODO: read checked from settings
        self.menu_vid_equ.triggered.connect(lambda: self.selectedOption(MediaAreaState.VIDEO_EQUALIZER))

        self.menu_no_eq = self.menu.addAction("&No Equalizer")
        self.menu_no_eq.setStatusTip("Show Management during audio playback.")
        self.menu_no_eq.setCheckable(True)
        self.menu_no_eq.triggered.connect(lambda: self.selectedOption(MediaAreaState.VIDEO_NOEQUALIZER))

        self.menu_no_vid = self.menu.addAction("&No Video")
        self.menu_no_vid.setStatusTip("Show audio equalizer for video and audio playback.")
        self.menu_no_vid.setCheckable(True)
        self.menu_no_vid.triggered.connect(lambda: self.selectedOption(MediaAreaState.EQUALIZERONLY))

        self.menu_manage_only = self.menu.addAction("&Management only")
        self.menu_manage_only.setStatusTip("Show Management even during media playback.")
        self.menu_manage_only.setCheckable(True)
        self.menu_manage_only.triggered.connect(lambda: self.selectedOption(MediaAreaState.MANAGEMENT_ONLY))

        self.menu_nothing = self.menu.addAction("&Nothing")
        self.menu_nothing.setStatusTip("Show nothing in media area.")
        self.menu_nothing.setCheckable(True)
        self.menu_nothing.triggered.connect(lambda: self.selectedOption(MediaAreaState.NOTHING))


    def selectedOption(self, state: MediaAreaState):
        self.set_media_area_state(state)

        menu_vid_equ_checked = False
        menu_no_eq_checked = False
        menu_no_vid_checked = False
        menu_manage_only_checked = False
        menu_nothing_checked = False

        if state == MediaAreaState.VIDEO_EQUALIZER:
            menu_vid_equ_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Normal. Hold Left Click to choose state.")
        elif state == MediaAreaState.VIDEO_NOEQUALIZER:
            menu_no_eq_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: No Equalizer. Hold Left Click to choose state.")
        elif state == MediaAreaState.EQUALIZERONLY:
            menu_no_vid_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: No Video. Hold Left Click to choose state.")
        elif state == MediaAreaState.MANAGEMENT_ONLY:
            menu_manage_only_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Management only. Hold Left Click to choose state.")
        elif state == MediaAreaState.NOTHING:
            menu_nothing_checked = True
            self.toggle_media_area_button.setStatusTip("Toggle media area. Current: Nothing. Hold Left Click to choose state.")

        self.menu_vid_equ.setChecked(menu_vid_equ_checked)
        self.menu_no_eq.setChecked(menu_no_eq_checked)
        self.menu_no_vid.setChecked(menu_no_vid_checked)
        self.menu_manage_only.setChecked(menu_manage_only_checked)
        self.menu_nothing.setChecked(menu_nothing_checked)



def handlePrintTest():
    print("Hello! :-)")


class VlcPlayerTextPosition(Enum):
    # 0 center, 1 left center, 2 right center, 3 left center, 4 top center, 5 top left, 6 top right, 7 top left, 8 bottom center, 9 bottom left, 10 bottom right, 11 bottom left, 12 top center, 13 top left, ....
    CENTER = 0
    LEFT_CENTER = 1
    RIGHT_CENTER = 2
    TOP_CENTER = 4
    TOP_LEFT = 5
    TOP_RIGHT = 6
    BOTTOM_CENTER = 8
    BOTTOM_LEFT = 9
    BOTTOM_RIGHT = 10


def vlcPlayerTextActivate(
        mediaplayer: vlc.MediaPlayer,
        text: str,
        position: VlcPlayerTextPosition = VlcPlayerTextPosition.TOP_LEFT,
        duration: int = 2000
):
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Enable, 1)
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Size, int(mediaplayer.video_get_height() / 16))  # pixels
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Position, position.value)
    mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Timeout, duration)  # millisec, 0==forever
    # mediaplayer.video_set_marquee_int(vlc.VideoMarqueeOption.Refresh, 1000)  # millisec (or sec?)

    mediaplayer.video_set_marquee_string(vlc.VideoMarqueeOption.Text, vlc.str_to_bytes("^\n.   " + text))


def equalizerAdjustByMedia(
        equalizer: EqualizerBar,
        title: str,
        episodeAmount: int,
        lightBackground: bool = False,
):
    # commercials have white background, shows black TODO: adjust text on equalizer
    equalizer.setBackgroundColor("white" if lightBackground else "black")

    # create hash, will stay the same for a title, but be different for different titles
    sha256_hash_hex = hashlib.sha256(title.encode()).hexdigest()

    # create 3 colors from the hash, first and third full saturation, middle one lighter
    color_1_hsv = (getValueFromHash(sha256_hash_hex, 1), (0.15 if lightBackground else 1.0), 1.0)
    color_2_hsv = (getValueFromHash(sha256_hash_hex, 2), (1.0 if lightBackground else 0.15), 1.0)
    color_3_hsv = (getValueFromHash(sha256_hash_hex, 3), (0.15 if lightBackground else 1.0), 1.0)

    # interpolate colors between
    colors_interpolated = getInterpolatedColors([color_1_hsv, color_2_hsv, color_3_hsv], episodeAmount)

    # print("getValueFromHash")
    # print("hash: " + sha256_hash_hex)
    # print("color value 1: " + str(color_1_hsv[0]))
    # print("color value 2: " + str(color_2_hsv[0]))
    # print("color value 3: " + str(color_3_hsv[0]))

    equalizer.setColors(list(map(hsv2rgb, colors_interpolated)))


def getInterpolatedColors(
        colors_hsv: list,
        amount: int
) -> list:

    if len(colors_hsv) == 1:
        return colors_hsv[0]

    colors_list_out = []

    separators = amount - 1
    len_colors = len(colors_hsv)
    for i in range(separators):
        color_a = colors_hsv[floor(i * ((len_colors - 1) / separators))]
        color_b = colors_hsv[floor(i * ((len_colors - 1) / separators)) + 1]
        interpolation_factor = (i * ((len_colors - 1) / separators)) % 1
        h = color_a[0] * (1 - interpolation_factor) + color_b[0] * interpolation_factor
        s = color_a[1] * (1 - interpolation_factor) + color_b[1] * interpolation_factor
        v = 1
        colors_list_out.append((min(1, h), min(1, s), v))

    colors_list_out.append(colors_hsv[-1])

    return colors_list_out


def getValueFromHash(hash: str, position: int = 0) -> float:
    return int(hash[(5 * position): (5 * position + 2)], 16) / 256


def hsv2rgb(hsv_tuple: tuple) -> tuple:
    return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(hsv_tuple[0], hsv_tuple[1], hsv_tuple[2]))






if __name__ == "__main__":
    # app = QApplication(sys.argv)
    app = QApplication([])
    # app.setStyle(QStyleFactory.create("Fusion"))
    # print(QStyleFactory.keys())
    player = PlayerWindow()
    player.show()
    player.resize(640, 480) # TODO: better sizing
    # if sys.argv[1:]:
    #     player.open_file_dialog(sys.argv[1])
    sys.exit(app.exec_())
