import queue

with open("todo.txt", encoding="utf-8") as f:
    content = f.readlines()

print("Henlo")

test_queue = queue.Queue()

for line in content:
    test_queue.put(line)

# write into new test file
with open("todo-rewrite.txt", 'w', encoding="utf-8") as f:
    while not test_queue.empty():
        line = test_queue.get()
        print(line)
        f.write(line)
    f.close()

