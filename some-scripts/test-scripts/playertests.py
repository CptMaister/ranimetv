
import threading
import time
import os
import re
import vlc
from random import *


script_instance = vlc.Instance()
player = script_instance.media_player_new()

def get_media(path: str) -> vlc.Media:
    return script_instance.media_new_path(path)

base = "/home/bommler/Desktop/Anime Dialoge/mp3merged-files/"
m_chihiro = get_media("/media/bommler/Daten/Benutzer/Admin/Eigene Dateien/Videos/Anime/ghibli_01.mkv")
m_shinchan = get_media("/media/bommler/Daten/Benutzer/Admin/Eigene Dateien/Videos/Anime/Shin Chan/op.avi")
m_shinchan2 = get_media("/media/bommler/Daten/Benutzer/Admin/Eigene Dateien/Videos/Anime/Shin Chan/01/09.avi")

m_jojo32 = get_media(base + "jojo3/JoJo3_02.mp3")
m_samurai3 = get_media(base + "samurai-champloo/Samurai-Champloo_03.mp3")
m_erased1 = get_media(base + "erased/Erased_01.mp3")
m_barakamon2 = get_media(base + "barakamon/Barakamon_02.mp3")
m_deathnote2 = get_media(base + "death-note/Death-Note_02.mp3")
m_steinsgate2 = get_media(base + "steins-gate/Steins-Gate-S1_02.mp3")

def player_pre_play_option_test():
    global player
    player.set_media(m_samurai3)
    player.set_rate(1.2)
    player.audio_set_volume(140)
    player.audio_set_mute(1)
    #player.set_media(m_samurai3)

def player_all_events(player: vlc.MediaPlayer, text):
    # see second answer of https://stackoverflow.com/questions/3595649/vlc-python-eventmanager-callback-type
    events = player.event_manager()
    events.event_attach(vlc.EventType.MediaPlayerAudioDevice, player_event_AudioDevice)
    events.event_attach(vlc.EventType.MediaPlayerAudioVolume, player_event_AudioVolume)
    events.event_attach(vlc.EventType.MediaPlayerBackward, player_event_Backward)
    events.event_attach(vlc.EventType.MediaPlayerBuffering, player_event_Buffering)
    events.event_attach(vlc.EventType.MediaPlayerChapterChanged, player_event_ChapterChanged)
    events.event_attach(vlc.EventType.MediaPlayerCorked, player_event_Corked)
    events.event_attach(vlc.EventType.MediaPlayerESAdded, player_event_ESAdded)
    events.event_attach(vlc.EventType.MediaPlayerESDeleted, player_event_ESDeleted)
    events.event_attach(vlc.EventType.MediaPlayerESSelected, player_event_ESSelected)
    events.event_attach(vlc.EventType.MediaPlayerEncounteredError, player_event_EncounteredError)
    events.event_attach(vlc.EventType.MediaPlayerEndReached, player_event_EndReached)
    events.event_attach(vlc.EventType.MediaPlayerForward, player_event_Forward)
    events.event_attach(vlc.EventType.MediaPlayerLengthChanged, player_event_LengthChanged)
    events.event_attach(vlc.EventType.MediaPlayerMediaChanged, player_event_MediaChanged)
    events.event_attach(vlc.EventType.MediaPlayerMuted, player_event_Muted)
    events.event_attach(vlc.EventType.MediaPlayerNothingSpecial, player_event_NothingSpecial)
    events.event_attach(vlc.EventType.MediaPlayerOpening, player_event_Opening)
    events.event_attach(vlc.EventType.MediaPlayerPausableChanged, player_event_PausableChanged)
    events.event_attach(vlc.EventType.MediaPlayerPaused, player_event_Paused)
    events.event_attach(vlc.EventType.MediaPlayerPlaying, player_event_Playing)
    # position is the position of current playback in percent / as part of the length, is called all the time
    #events.event_attach(vlc.EventType.MediaPlayerPositionChanged, player_event_PositionChanged)
    events.event_attach(vlc.EventType.MediaPlayerScrambledChanged, player_event_ScrambledChanged)
    events.event_attach(vlc.EventType.MediaPlayerSeekableChanged, player_event_SeekableChanged)
    events.event_attach(vlc.EventType.MediaPlayerSnapshotTaken, player_event_SnapshotTaken)
    events.event_attach(vlc.EventType.MediaPlayerStopped, player_event_Stopped, text)
    # called all the time
    #events.event_attach(vlc.EventType.MediaPlayerTimeChanged, player_event_TimeChanged)
    events.event_attach(vlc.EventType.MediaPlayerTitleChanged, player_event_TitleChanged)
    events.event_attach(vlc.EventType.MediaPlayerUncorked, player_event_Uncorked)
    events.event_attach(vlc.EventType.MediaPlayerUnmuted, player_event_Unmuted)
    events.event_attach(vlc.EventType.MediaPlayerVout, player_event_Vout)
    events.event_attach(vlc.EventType.VlmMediaChanged, vlm_media_event_Changed)
    events.event_attach(vlc.EventType.VlmMediaInstanceStarted, vlm_media_event_InstanceStarted)
    events.event_attach(vlc.EventType.VlmMediaInstanceStatusEnd, vlm_media_event_InstanceStatusEnd)
    events.event_attach(vlc.EventType.VlmMediaInstanceStatusError, vlm_media_event_InstanceStatusError)
    events.event_attach(vlc.EventType.VlmMediaInstanceStatusInit, vlm_media_event_InstanceStatusInit)
    events.event_attach(vlc.EventType.VlmMediaInstanceStatusOpening, vlm_media_event_InstanceStatusOpening)
    events.event_attach(vlc.EventType.VlmMediaInstanceStatusPause, vlm_media_event_InstanceStatusPause)
    events.event_attach(vlc.EventType.VlmMediaInstanceStatusPlaying, vlm_media_event_InstanceStatusPlaying)
    events.event_attach(vlc.EventType.VlmMediaInstanceStopped, vlm_media_event_InstanceStopped)
    events.event_attach(vlc.EventType.VlmMediaRemoved, vlm_media_event_Removed)


def player_event_AudioDevice(event):
    print("player_event_AudioDevice")

def player_event_AudioVolume(event):
    print("player_event_AudioVolume")

def player_event_Backward(event):
    print("player_event_Backward")

def player_event_Buffering(event):
    print("player_event_Buffering")

def player_event_ChapterChanged(event):
    print("player_event_ChapterChanged")

def player_event_Corked(event):
    print("player_event_Corked")

def player_event_ESAdded(event):
    print("player_event_ESAdded")

def player_event_ESDeleted(event):
    print("player_event_ESDeleted")

def player_event_ESSelected(event):
    print("player_event_ESSelected")

def player_event_EncounteredError(event):
    print("player_event_EncounteredError")

def player_event_EndReached(event):
    print("player_event_EndReached")

def player_event_Forward(event):
    print("player_event_Forward")

def player_event_LengthChanged(event):
    print("player_event_LengthChanged")

def player_event_MediaChanged(event):
    print("player_event_MediaChanged")

def player_event_Muted(event):
    print("player_event_Muted")

def player_event_NothingSpecial(event):
    print("player_event_NothingSpecial")

def player_event_Opening(event):
    print("player_event_Opening")

def player_event_PausableChanged(event):
    print("player_event_PausableChanged")

def player_event_Paused(event):
    print("player_event_Paused")

def player_event_Playing(event):
    print("player_event_Playing")

def player_event_PositionChanged(event):
    print("player_event_PositionChanged")

def player_event_ScrambledChanged(event):
    print("player_event_ScrambledChanged")

def player_event_SeekableChanged(event):
    print("player_event_SeekableChanged")

def player_event_SnapshotTaken(event):
    print("player_event_SnapshotTaken")

def player_event_Stopped(event, text):
    print("player_event_Stopped")
    print(text)

def player_event_TimeChanged(event):
    print("player_event_TimeChanged")

def player_event_TitleChanged(event):
    print("player_event_TitleChanged")

def player_event_Uncorked(event):
    print("player_event_Uncorked")

def player_event_Unmuted(event):
    print("player_event_Unmuted")

def player_event_Vout(event):
    print("player_event_Vout")


def vlm_media_event_Changed(event):
    print("vlm_media_event_Changed")

def vlm_media_event_InstanceStarted(event):
    print("vlm_media_event_InstanceStarted")

def vlm_media_event_InstanceStatusEnd(event):
    print("vlm_media_event_InstanceStatusEnd")

def vlm_media_event_InstanceStatusError(event):
    print("vlm_media_event_InstanceStatusError")

def vlm_media_event_InstanceStatusInit(event):
    print("vlm_media_event_InstanceStatusInit")

def vlm_media_event_InstanceStatusOpening(event):
    print("vlm_media_event_InstanceStatusOpening")

def vlm_media_event_InstanceStatusPause(event):
    print("vlm_media_event_InstanceStatusPause")

def vlm_media_event_InstanceStatusPlaying(event):
    print("vlm_media_event_InstanceStatusPlaying")

def vlm_media_event_InstanceStopped(event):
    print("vlm_media_event_InstanceStopped")

def vlm_media_event_Removed(event):
    print("vlm_media_event_Removed")


player.set_media(m_chihiro)
player.play()