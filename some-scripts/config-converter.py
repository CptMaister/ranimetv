#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
from enum import Enum


def convert_line(line: str, base_from: str, base_to: str, slash_from: str, slash_to: str) -> str:
    line = line.replace(base_from, base_to, 1)
    line = line.replace(slash_from, slash_to)
    return line


class Convert(Enum):
    win_to_lin = 1
    lin_to_win = 2


##### Customize these strings #####
pathbase_windows = "D:\\Sprachen"
pathbase_linux = "Sprachen"
slash_windows = "\\"
slash_linux = "/"


# stuff
CONFIG_PATH = "ranimetv-config.txt"
ENCODING_CONFIG_FILE = "utf-8"

convert_to = 0


# check if used wrong
if len(sys.argv) > 2:
    print("ERROR: I take one or no arguments, which is the platform to convert to: win or lin")
    exit()


# get all lines
with open(CONFIG_PATH, encoding=ENCODING_CONFIG_FILE) as f:
    content = f.readlines()


# decide which way to convert
if len(sys.argv) == 2:
    arg = sys.argv[1]
    if arg == "win" or arg == "lin":
        if arg == "win":
            convert_to = Convert.lin_to_win
        elif arg == "lin":
            convert_to = Convert.win_to_lin
    else:
        print("ERROR: I take one or no arguments, which is the platform to convert to: win or lin")
        exit()

else:
    print("No arg, will convert depending on first \\ or / found in the first path line (first line that contains an equals sign = ).")
    for line in content:
        if "=" in line:
            if "\\" in line:
                convert_to = Convert.win_to_lin
                print("convert to Linux")
                break
            if "/" in line:
                convert_to = Convert.lin_to_win
                print("convert to Windows")
                break


# set strings for conversion
if convert_to == Convert.win_to_lin:
    pathbase_from = pathbase_windows
    pathbase_to = pathbase_linux
    slash_from = slash_windows
    slash_to = slash_linux
elif convert_to == Convert.lin_to_win:
    pathbase_from = pathbase_linux
    pathbase_to = pathbase_windows
    slash_from = slash_linux
    slash_to = slash_windows


# go through lines, replace stuff if it is a path line (so if it contains "=")
new_content = []
for line in content:
    if "=" in line:
        line = convert_line(line, pathbase_from, pathbase_to, slash_from, slash_to)
    new_content.append(line)

# overwrite file with converted lines
with open(CONFIG_PATH, 'r+', encoding=ENCODING_CONFIG_FILE) as f:
    f.seek(0)
    f.truncate()
    f.write("".join(new_content))
    f.close()

print("DONE :-)")