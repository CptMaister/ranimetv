# ranimetv

Python script for customizable ordered playback of a randomly picked 
series of predefined media files from your local computer.

To run properly it only needs a config file specifying all media files and
preferences. Just start up the script in your terminal and read the
instructions.

python-vlc is used to play the media files, see https://www.olivieraubert.net/vlc/python-ctypes/

This whole script was written with only basic python knowledge and basically no
media handling knowledge. Feel free to improve coding and functionality.